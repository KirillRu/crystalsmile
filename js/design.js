design = function() {
	var previewFrame;

	function _previewFrame() {
		if (!previewFrame) previewFrame = g('js_previewFrame');
		return previewFrame;
	}

	function reloadFrame(src) {
		src = src||_previewFrame().src;
		_previewFrame().src = src;
	}

	function save(module, data) {
		var url = '/management/save/' + module + '.ajax';
		ajax.simpleRequest(url, data);
		return false;
	}

	function active($elem) { $elem.addClass('act').siblings().removeClass('act'); }

    function _slider($logo, $hat, contentId, saveModule) {
        var probegLogoX = Math.floor(($hat.outerWidth() - $logo.outerWidth())/3);
        var probegLogoY = Math.floor(($hat.outerHeight() - $logo.outerHeight())/3);

        return slider().init({
            max: {x: probegLogoX, y: probegLogoY},
            content: contentId,
            onChange: function(options) {
                $logo.css('left', options.step.x*3);
                $logo.css('top', options.step.y*3);
            },
            onDragEnd: function(options){
                save(saveModule, {left: options.step.x*3, top: options.step.y*3});
            }
        }).slideToCoords(Math.floor($logo.position().left/3), Math.floor($logo.position().top/3));
    }

    function changeSizeRunner($logo, $hat, contentId) {
        var $container = $('#' + contentId);
        var cursorX = Math.floor($logo.outerWidth()/($hat.outerWidth()/$container.outerWidth()));
        var cursorY = Math.floor($logo.outerHeight()/($hat.outerHeight()/$container.outerHeight()));
        $container.children('div').css({height:cursorY, width:cursorX});
    }

    function sliderLogo() {
        var $previewFrame = $(_previewFrame()).contents();
        var $logo = $previewFrame.find('#js_hatLogo');
        var $hat = $previewFrame.find('#js_hat');
        changeSizeRunner($logo, $hat, 'js_sliderNameLogo');
        _slider($logo, $hat, 'js_sliderNameLogo', 'logo_position');
   	}

    function sliderHatContacts() {
        var $previewFrame = $(_previewFrame()).contents();
        var $logo = $previewFrame.find('#js_hatContacts');
        var $hat = $previewFrame.find('#js_hat');
        changeSizeRunner($logo, $hat, 'js_sliderNameContacts');
        _slider($logo, $hat, 'js_sliderNameContacts', 'hatContacts_position');
        CKEDITOR.inline('js_contactsText', {
            customConfig : "/js/modules/ckeditor/line_config.js",
            on: {
                blur: function(event) {
                    var content = event.editor.getData();
                    $logo.html(content);
                    console.log({text: content});
                    save('hatContacts_position', {text: content});
                    changeSizeRunner($logo, $hat, 'js_sliderNameContacts');
                    _slider($logo, $hat, 'js_sliderNameContacts', 'hatContacts_position');
                }
            }
        });
   	}

    function sliderHatText() {
        var $previewFrame = $(_previewFrame()).contents();
        var $logo = $previewFrame.find('#js_hatText');
        var $hat = $previewFrame.find('#js_hat');
        changeSizeRunner($logo, $hat, 'js_sliderNameText');
        _slider($logo, $hat, 'js_sliderNameText', 'hatText_position');
        CKEDITOR.inline('js_hatText', {
            customConfig : "/js/modules/ckeditor/line_config.js",
            on: {
                blur: function(event) {
                    var content = event.editor.getData();
                    $logo.html(content);
                    console.log({text: content});
                    save('hatText_position', {text: content});
                    changeSizeRunner($logo, $hat, 'js_sliderNameText');
                    _slider($logo, $hat, 'js_sliderNameText', 'hatText_position');
                }
            }
        });
   	}

	function topMenuClick(t) {
		active($(t).closest('li'));
		ajax.update(t.href + '.ajax', '', 'js_settings');
		return false;
	}

	function leftMenuClick(t) {
		active($(t).closest('li'));
		ajax.replace(t.href + '.ajax', '', 'js_settingsContent');
		return false;
	}

	function change(module, value) {
		var $previewFrame = $(_previewFrame()).contents();
		switch (module) {
			case 'reload': reloadFrame(value); break;
			case 'hat': $previewFrame.find('#js_hat').css('background-image', 'url(' + value + ')'); break;
			case 'color':
				$previewFrame.find('head link[href]').each(function() {
					if (/\/internet_shops\/colors\/color\d+\.css/.test(this.href)) this.href = this.href.replace(/\/internet_shops\/colors\/color\d+\.css/, '/internet_shops/colors/color' + value + '.css');
				});
				break;
			case 'tpl':
				$previewFrame.find('head link[href]').each(function() {
					if (/\/internet_shops\/templates\/overall\d+\.css/.test(this.href)) this.href = this.href.replace(/\/internet_shops\/templates\/overall\d+\.css/, '/internet_shops/templates/overall' + value + '.css');
				});
				break;
			case 'leftMenuFloat':
				switch (value) {
					case 'left':
						$previewFrame.find('#js_leftMenu').removeClass('fright');
						$previewFrame.find('#js_content').removeClass('fleft');
						break;
					case 'right':
						$previewFrame.find('#js_leftMenu').addClass('fright');
						$previewFrame.find('#js_content').addClass('fleft');
						break;
				}
				$previewFrame.find('#js_leftMenu').css('float', value);
				break;
			case 'searchLocation':
				var form = $previewFrame.find('#js_formSearch').get(0);
				switch (value) {
					case 'left':
						var div  = document.createElement('div');
						div.id = 'js_searchLocationLeft';
						element.addClass(div, 'is-sidebar__part');
						element.addClass(div, 'is-sidebar__search');
						div.appendChild(form);
						$previewFrame.find('#js_leftMenu').prepend(div);
						break;
					case 'top':
						element.remove($previewFrame.find('#js_searchLocationLeft').get(0));
						var $locationTop = $previewFrame.find('#js_searchLocationTop');
						$locationTop.html(form);
						break;
				}
				break;
			case 'pathLocation':
				var $path = $previewFrame.find('#js_path');
				switch (value) {
					case 'afterHat': $previewFrame.find('#js_container').prepend($path); break;
					case 'topContent': $previewFrame.find('#js_content').prepend($path); break;
				}
				break;
		}
	}

	function init() {
        scriptLoader('modules/slider.js', 'modules/ckeditor/ckeditor.js').callFunction(function() {
            var div = document.createElement('div');
            element.addClass(div, 'is-show-wrapper');
            $(_previewFrame()).contents().find('body').prepend(div);
            sliderLogo();
            CKEDITOR.disableAutoInline = true;
            sliderHatContacts();
            sliderHatText();
        });
	}

	return {
		save: save,
		change: change,
		active: active,
		topMenuClick: topMenuClick,
		leftMenuClick: leftMenuClick,
		init: init
	};
}();
