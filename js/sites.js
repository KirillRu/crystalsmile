/*functions*/
functions.mergeObjects(functions, function() {

	function slideBlock(block, link, speed) {
		block = g(block);
		if (link) link = g(link);
		if (!element.hasClass(block, 'js_opened')) {
			if (link) element.removeClass(link, 'down').addClass(link, 'up');
			$(block).slideDown(speed || 500, function() {element.removeClass(block, 'js_closed').addClass(block, 'js_opened')});
		} else {
			if (link) element.removeClass(link, 'up').addClass(link, 'down');
			$(block).slideUp(speed || 500, function() {element.removeClass(block, 'js_opened').addClass(block, 'js_closed')});
		}
	}

	//показываем\скрываем все скрытые блоки внутри $('#' + idBlock)
	//меняем текст ссылки link на $link.data('repl')
	var blocksToToggleHidden = {};
	function toggleHiddenChild(link, idBlock){
		if (!blocksToToggleHidden[idBlock]) blocksToToggleHidden[idBlock] = new toggleHidden(link, idBlock);
		blocksToToggleHidden[idBlock].toggle();
	}

	function toggleHidden(link, idBlock){
		this.$toggleItems = $('#' + idBlock).children().filter(function(){return this.style.display === 'none';});
		this.link = link;
		this.toggle = function(){
			this.switchText();
			this.$toggleItems.toggle();
		};
		this.switchText = function(){
			var $link = $(this.link), repl = $link.data('repl');
			$link.data('repl', $link.text());
			$link.text(repl);
		}
	}
	//====

	return {
		slideBlock: slideBlock,
		toggleHiddenChild: toggleHiddenChild
	};
}());

/*popupManager*/
if (typeof popupManager != 'undefined')
functions.mergeObjects(popupManager, function() {

	return {

	};
}());

(function(){
    $(document).ready(function(){
        $('input[type=image]').on('click',function(){
            ajax.requestInPopup('/runModule/forms/add.ajax', 'moduleName=orders');
        });
   	});
}());