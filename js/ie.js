if (!Array.prototype.filter){
	Array.prototype.filter = function(cb) {
		var newArr = [];
		for (var i = 0, len = this.length; i < len; i++) {
			if (typeof(this[i]) != 'undefined' && cb(this[i], i, this)) newArr.push(this[i]);
		}
		return newArr;
	};
}

if (!Array.prototype.forEach){
	Array.prototype.forEach = function(cb){
		for (var i = 0, len = this.length; i < len; i++) cb(this[i], i, this);
	}
}

if (!Array.prototype.map){
	Array.prototype.map = function(cb){
		var newArr = [];
		for (var i = 0, len = this.length; i < len; i++) newArr.push(cb(this[i], i, this));
		return newArr;
	}
}

if (!Function.prototype.bind){
	Function.prototype.bind = function(context){
		return function(func, args){
			return function(){func.apply(context, args)}
		}(this, Array.prototype.slice.call(arguments, 1));
	}
}

if (!Array.prototype.reduce){
	Array.prototype.reduce = function(cb, initialValue){
		if (typeof initialValue == 'undefined') initialValue = this.shift();
		for (var i = 0, len = this.length; i < len; i++) initialValue = cb(initialValue, this[i], i, this);
		return initialValue;
	}
}

if (!Object.keys) {
	Object.keys = function(obj) {
		var keys = [];
		for (var i in obj) {
			if (obj.hasOwnProperty(i)) {
				keys.push(i);
			}
		}
		return keys;
	};
}

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(elt /*, from*/) {
		var len = this.length >>> 0;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
			? Math.ceil(from)
			: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++) {
			if (from in this &&
				this[from] === elt)
				return from;
		}
		return -1;
	};
}