log = function(){
	//распарсивание SQL запроса

	function openSql(sql){
		popupManager.popup.html(prepareSqlString(sql)).show();
	}

	function prepareSqlString(str) {
		var br = ['select', '(?:delete )?from', '(?:on duplicate key )?update(?: ignore)?', 'create view', 'create(?: temporary)? table', 'set',
			'insert(?: low_priority| ignore)? into', 'where', 'order by', 'limit', 'group by', 'optimize table',
			'having', '(?:left |inner |full )?join', 'load data infile',
			'(?:replace )?into table', 'lines terminated by', 'fields terminated by', 'truncate'
		];
		var span = ['like', 'values', 'not exists', 'desc', 'asc'];
		str = str.replace(/(\&gt;|\&lt;)?=|\&gt;|\&lt;/g, '<span class="red">$&</span>');
		str = str.replace(new RegExp("(?:[>'^]|\\b)(" + br.join('|') + ") ", 'ig'), appendBr);
		str = str.replace(new RegExp(" (" + span.join('|') + ") ", 'ig'), inSpan);
		str = str.replace(/,/g, '<span class="red">$&</span> ');
		str = str.replace(/(\;)((\s)*?<br><span class="sqlWord">)/g, '$1<br>$2');
		str = str.replace(new RegExp("\\((<br><span class=\"sqlWord\">select.*?from.*?)\\)", 'ig'), clearSpan);//вложенные селекты
		str = str.replace(new RegExp("<br><span class=\"sqlWord\">(" + br.join('|') + ").*?(' at line| clause)", 'i'), clearSpan);//часть запроса в ошибке
		return str;
	}

	function appendBr(str, q, offset) {
		var result = /\W/.test(str.substr(0, 1)) ? str.substr(0, 1) : '';
		q = '<br>' + inSpan(q.toUpperCase()) + '<br>' + "&nbsp&nbsp";
		return result + q + ' ';
	}

	function inSpan(str) {
		return '<span class="sqlWord">' + str + '</span>';
	}

	function clearSpan(str) {
		return str.replace(/<br>/g, '').replace(/class="sqlWord"/g, 'class="sqlSmallWord"').replace(/&nbsp&nbsp/g, ' ');
	}

	return {
		openSql: openSql
	};
}();