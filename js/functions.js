/**@returns {node}*/
function g(objOrId){
	return (typeof objOrId === 'object') ? objOrId : document.getElementById(objOrId);
}
element = function() {
	/**@returns element*/
	function setInnerHTML(elem, value) {
		elem = g(elem);
		if (elem) elem.innerHTML = value;
		return element;
	}

	/**@returns element*/
	function setValue(elem, value) {
		elem = g(elem);
		if (elem) elem.value = value;
		return element;
	}

	function hasClass(elem, className) {
		return new RegExp('(^|\\s)' + className + '(\\s|$)').test(elem.className || '');
	}

	/**@returns element*/
	function addClass(elem, className) {
		if (!hasClass(elem, className)) {
			if (elem.className && elem.className.substr(-1) != ' ') elem.className += ' ';
			elem.className += className;
		}
		return element;
	}

	/**@returns element*/
	function removeClass(elem, className) {
		if (elem.className) elem.className = elem.className.replace(new RegExp('(^|\\s)' + className + '(\\s|$)'), ' ');
		return element;
	}

	/**
	 * @param elem - что вставляем
	 * @param refElem - после чего вставляем
	 * @returns element
	 */
	function insertAfter(elem, refElem) {
		refElem.parentNode.insertBefore(elem, refElem.nextSibling);
		return element;
	}

	/**
	 * @param elem - что вставляем
	 * @param refElem - перед чем вставляем
	 * @returns element
	 */
	function insertBefore(elem, refElem){
		if (refElem.parentNode) refElem.parentNode.insertBefore(elem, refElem);
		return element;
	}

	/**@returns {node|null}*/
	function firstChild(elem) {
		if (elem.firstChild && elem.firstChild.nodeType === 1) return elem.firstChild;
		return nextElement(elem.firstChild);
	}

	/**@returns {node|null}*/
	function nextElement(elem) {
		if ('nextElementSibling' in elem) return elem.nextElementSibling;
		while (elem = elem.nextSibling) {
			if (elem.nodeType === 1) return elem;
		}
		return null;
	}

	/**@returns {node|null}*/
	function previousElement(elem) {
		if ('previousElementSibling' in elem) return elem.previousElementSibling;
		while (elem = elem.previousSibling) {
			if (elem.nodeType === 1) return elem;
		}
		return null;
	}

	/**@returns {node|null}*/
	function previousElementByClass(elem, className) {
		var prevElement = previousElement(elem);
		if (prevElement) {
			if (!hasClass(prevElement, className)) return previousElementByClass(prevElement, className);
			return prevElement;
		}
		return null;
	}

	/**@returns {node|null}*/
	function nextElementByClass(elem, className) {
		var nextElem = nextElement(elem);
		if (nextElem) {
			if (!hasClass(nextElem, className)) return nextElementByClass(nextElem, className);
			return nextElem;
		}
		return null;
	}

	function getParametersSelect(f){
		if (f.name.substring(f.name.length - 2) != '[]') return '&' + f.name + '=' + encodeURIComponent(f.value);

		/*multi select*/
		var res = '';
		for (var i = 0, optionsLen = f.length; i < optionsLen; i++) {
			if (f.options[i].selected) res += '&' + f.name + '=' + encodeURIComponent(f.options[i].value);
		}
		return res || ('&' + f.name + '=');
	}

	/**@returns {string}*/
	function getParameters(elem) {
		if (!elem) return '';
		var res = '', fields = $(g(elem)).find('select[name], input[name], textarea[name]').get(), multiRadio = {};
		for (var i = 0, len = fields.length; i < len; i++) {
			var f = fields[i];
			switch (f.tagName.toLowerCase()) {
				case 'select':
					res += getParametersSelect(f);
					break;
				case 'input':
					switch (f.type) {
						case 'checkbox':
							res += '&' + f.name + '=' + (f.checked ? encodeURIComponent(f.value) : '');
							break;
						case 'radio':
							if (typeof(multiRadio[f.name]) === 'undefined') multiRadio[f.name] = '';
							if (f.checked) multiRadio[f.name] = encodeURIComponent(f.value);
							break;
						case 'file':
							break;
						default:
							res += '&' + f.name + '=' + encodeURIComponent(f.value);
							break;
					}
					break;
				case 'textarea':
					res += '&' + f.name + '=' + encodeURIComponent((typeof(CKEDITOR) == 'undefined' || !CKEDITOR.instances[f.id]) ? f.value : CKEDITOR.instances[f.id].getData());
					break;
			}
		}
		for (i in multiRadio) {
			res += '&' + i + '=' + multiRadio[i];
		}
		return res.substring(1);
	}

	/**@returns {Number|number} позиция Y верхнего левого угла относительно верхнеей угла страницы*/
	function getOffsetTop(elem) {
		if (elem.getBoundingClientRect) {
			return Math.round(elem.getBoundingClientRect().top + functions.getScrollTop() - (document.documentElement.clientTop || document.body.clientTop || 0));
		} else {
			var top = 0;
			while (elem) {
				top += parseFloat(elem.offsetTop);
				elem = elem.offsetParent;
			}
			return top;
		}
	}

	/**@returns {Number|number} позиция X верхнего левого угла относительно левой стороны страницы*/
	function getOffsetLeft(elem) {
		if (elem.getBoundingClientRect) {
			return Math.round(elem.getBoundingClientRect().left + functions.getScrollLeft() - (document.documentElement.clientLeft || document.body.clientLeft || 0))
		} else {
			var left = 0;
			while (elem) {
				left += parseFloat(elem.offsetLeft);
				elem = elem.offsetParent;
			}
			return left;
		}
	}

	/**
	 * @param elem
	 * @returns element
	 */
	function remove(elem){
		if (elem.parentNode) elem.parentNode.removeChild(elem);
		return element;
	}

	/**
	 * @param newElem что заменяем
	 * @param oldElem на что заменяем
	 * @returns element
	 */
	function replace(newElem, oldElem){
		insertBefore(newElem, oldElem);
		remove(oldElem);
		return element;
	}

	function scrollTo(elem, offset){
		offset = offset || 10;
		if (portal.getRelativeTopHeight) offset += portal.getRelativeTopHeight();

		jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: elem ? (getOffsetTop(elem) - offset) : 0}, 120);
		return element;
	}


	function emptyWithSlideUp(block, cbEnd){
		$(block).slideUp(300, function(){block.innerHTML = ''; if (typeof cbEnd == 'function') cbEnd();});
		return element;
	}

	return {
		emptyWithSlideUp: emptyWithSlideUp,
		setInnerHTML: setInnerHTML,
		setValue: setValue,
		hasClass: hasClass,
		addClass: addClass,
		removeClass: removeClass,
		insertAfter: insertAfter,
		inserBefore: insertBefore,
		nextElement: nextElement,
		firstChild: firstChild,
		previousElement: previousElement,
		previousElementByClass: previousElementByClass,
		nextElementByClass: nextElementByClass,
		getParameters: getParameters,
		getOffsetTop: getOffsetTop,
		getOffsetLeft: getOffsetLeft,
		remove: remove,
		replace: replace,
		scrollTo: scrollTo
	};
}();

cookie = function() {

	function getCookie(key) {
		return $.cookie(key);
	}

	/**@returns cookie*/
	function setCookie(key, value) {
		$.cookie(key, value, { expires: 30, path: '/', domain: functions.getDomainName()});
		return cookie;
	}

	/**@returns cookie*/
	function unsetCookie(key) {
		setCookie(key, null);
		return cookie;
	}

	return {
		'get': getCookie,
		'set': setCookie,
		'unset': unsetCookie
	};
}();

functions = function(w) {

	/**@returns {Number|number} количество Y прокрутки страницы*/
	function getScrollTop() {
		return w.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
	}

	/**@returns {Number|number} количество X прокрутки страницы*/
	function getScrollLeft() {
		return w.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
	}

	/**@param key - по умолчанию main
	 * @returns {*}
	 */
	function cfg(key) {
		key = key || 'main';
		return (typeof(w['_cfg_' + key]) == 'object') ? w['_cfg_' + key] : {};
	}

	function getCharCode(event) {
		event = event || w.event;
		return  (event.which) ? event.which :
				((event.charCode) ? event.charCode :
				((event.keyCode) ? event.keyCode : 0));
	}

	function trim(string){
		return string.replace(/(^\s+)|(\s+$)/g, '');
	}

	function mergeObjects(obj1, obj2){
		for (var key in obj2)
			if (obj2.hasOwnProperty(key)) obj1[key] = obj2[key];
		return obj1;
	}

	function mergeObjectsRecursive(obj1, obj2){
		for (var key in obj2)
			if (obj2.hasOwnProperty(key))
				if (typeof obj1[key] == 'object' && typeof obj2[key] == 'object')
					obj1[key] = mergeObjectsRecursive(obj1[key], obj2[key]);
				else
					obj1[key] = obj2[key];

		return obj1;
	}

	function numberFormat( number, decimals, dec_point, thousands_sep ) {
		var i, j, kw, kd, km;
		if( isNaN(decimals = Math.abs(decimals)) ) decimals = 2;
		if( dec_point == undefined ) dec_point = ',';
		if( thousands_sep == undefined ) thousands_sep = '.';
		i = parseInt(number = (+number || 0).toFixed(decimals)) + '';
		if( (j = i.length) > 3 ){
			j = j % 3;
		} else{
			j = 0;
		}
		km = (j ? i.substr(0, j) + thousands_sep : '');
		kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
		kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : '');
		return km + kw + kd;
	}

	function getDomainName() {
		var domain = w.location.host;
		var d = domain.split('.');
		if (d.length > 2) domain = d[d.length - 2] + '.' + d[d.length - 1];
		return domain;
	}

	function preventDefault(e) {
		e = e || w.event;
		if (e.preventDefault) e.preventDefault();
		e.returnValue = false;
	}

	function disableScroll() {
		if (w.addEventListener) {
		    w.addEventListener('DOMMouseScroll', preventDefault, false);
		}
		w.onmousewheel = document.onmousewheel = preventDefault;
		document.onkeydown = function(e){
			var key = getCharCode(e);
			if (key >= 33 && key <= 40) preventDefault(e);
		};
	}

	function enableScroll() {
	    if (w.removeEventListener) {
	        w.removeEventListener('DOMMouseScroll', preventDefault, false);
	    }
	    w.onmousewheel = document.onmousewheel = document.onkeydown = null;
	}

	function makeDelay(ms) {
	    var timer;
	    return function(callback){
	        if (timer) clearTimeout(timer);
	        if (callback) timer = setTimeout(callback, ms);
	    };
	};

	return {
		getScrollTop: getScrollTop,
		getScrollLeft: getScrollLeft,
		getCharCode: getCharCode,
		mergeObjects: mergeObjects,
		mergeObjectsRecursive: mergeObjectsRecursive,
		numberFormat: numberFormat,
		trim: trim,
		cfg: cfg,
		getDomainName: getDomainName,
		disableScroll: disableScroll,
		enableScroll: enableScroll,
		makeDelay: makeDelay
	};
}(window);