management = function(){
	function getPageData(){
		return functions.cfg('pageData');
	}

	var managementHistory = function(){

		if ('history' in window && 'addEventListener' in window)
			window.addEventListener('popstate', function(e){window.location.reload();}, false);

		function pushState(data){
			data = (typeof data == 'object') ? $.param(data) : data;
			if ('history' in window && 'pushState' in window.history) window.history.pushState([], '', location.origin + location.pathname + '?' + data);
		}

		return {
			pushState: pushState
		};
	}();

	var list = function(){
		function selectByParams(url){
			var pageData = getPageData();
			if ('page' in pageData) delete pageData['page'];
			if ('filter' in pageData) delete pageData['filter'];
			if ('order' in pageData) delete pageData['order'];

			managementAjax.replaceWithHistory(url, $.param({pageData: getPageData()}) + '&' + element.getParameters('js_paramsContent'), 'js_listManagementContent');
		}

		function crearFilter(url, options){
			var pageData = getPageData();
			if ('page' in pageData) delete pageData['page'];
			if ('filter' in pageData) {
				for (var key in pageData['filter']) {
					if (!options[key]) {
						delete pageData['filter'][key];
					}
				}
			}
			managementAjax.replaceWithHistory(url, {pageData: pageData}, 'js_listManagementContent');
		}

		function selectPage(url, page){
			var pageData = getPageData();
			pageData['page'] = page;
			managementAjax.replaceWithHistory(url, {pageData: pageData}, 'js_itemsContent');
		}

		function reload() {
			return ajax.replaceAndFindLazySrc('runModule/List/Items.ajax', {pageData: getPageData()}, 'js_itemsContent');
		}

		return {
			selectByParams: selectByParams,
			crearFilter: crearFilter,
			selectPage: selectPage,
			reload: reload
		};
	}();


	var managementAjax = function(){
		function requestInPopup(url, data){
			return ajax.requestInPopup(url, $.param({pageData: getPageData()}) + '&' + data);
		}

		function simpleRequest(url, data){
			return ajax.simpleRequest(url, $.param({pageData: getPageData()}) + '&' + data);
		}

		function cbOnSuccess(url, data, cbSuccess){
			return ajax.cbOnSuccess(url, $.param({pageData: getPageData()}) + '&' + data, cbSuccess);
		}

		function replaceWithHistory(url, data, block){
			managementHistory.pushState(data);
			ajax.replaceAndFindLazySrc(url, data, block);
		}

		return {
			requestInPopup: requestInPopup,
			simpleRequest: simpleRequest,
			replaceWithHistory: replaceWithHistory,
			cbOnSuccess: cbOnSuccess,
		};
	}();

	var items = function(){
		function Item(id){
			this.id = id;
			this.obj = g('js_itemRow_' + id);
		}

		Item.prototype = {
			setRadio: function(idStatus){
				$(this.obj).find('input:radio[name=\'status[' + this.id + ']\']').prop('checked', false).filter('[value=' + idStatus + ']').prop('checked', true);
			},
			setChecked: function(checked){
				$(this.obj).find('input:checkbox[name=\'group[]\']:first').prop('checked', checked);
			}
		};


		function remove(url, id) {
			portal.confirm('Удалить?').yes(function() {
				var pageData = getPageData();
				ajax.update(url, {pageData: pageData, id: id}, 'js_itemsContent');
			});
		}

		function groupSetRadio(idStatus){
			if (!idStatus) return;

			eachItem(function(Item){Item.setRadio(idStatus);});
		}

		function groupSetChecked(checked){
			eachItem(function(Item){Item.setChecked(checked);});
		}

		function changeCategory(url){
			portal.confirm('Обработать?').yes(function(){
				var pageData = getPageData();
				//pageData['page'] = 1;
				ajax.replaceAndFindLazySrc(url, $.param({pageData: pageData}) + '&' + element.getParameters('js_itemsContent'), 'js_itemsContent');
			});
		}

		function changeCategoryAll(url){
			portal.confirm('Обработать?').yes(function(){
				var pageData = getPageData();
				ajax.replaceAndFindLazySrc(url, $.param({pageData: pageData}) + '&' + element.getParameters('js_itemsContent') + '&categoryName=' + g('js_categoryNameAll').value, 'js_itemsContent');
			});
		}

		function eachItem(cb){
			$('#js_itemsContent').find('.js_itemRow').each(function(id, elem){
				//вырезаем js_itemRow_. Остается id
				cb(new Item(elem.id.substr(11)));
			});
		}

		return {
			remove: remove, //удаляем запись из бд
			groupSetRadio: groupSetRadio,
			groupSetChecked: groupSetChecked,
			changeCategory: changeCategory,
			changeCategoryAll: changeCategoryAll
		};
	}();

	var form = function(){

		function getFormData(){
			return functions.cfg('formData');
		}

		function requestInPopup(url, data){
			return managementAjax.requestInPopup(url, $.param({formData: getFormData()}) + (data ? '&' + data : ''));
		}

		function add(url, data){
			return managementAjax.cbOnSuccess(url, data, function(r){
				//if (~r.indexOf('edit-message')){
				//	popupManager.popup.hide();
				//	list.reload();
				//} else {
				//	popupManager.popup.html(r).show();
				//}
				popupManager.popup.html(r).show();

			});
		}

		return {
			requestInPopup: requestInPopup,
			add: add
		};
	}();

	var groupActions = function(){
		var handlers = {};
		function init(settings){
			handlers[settings.position] = (new GroupAction()).init(settings);
		}

		function getHandler(position){
			return handlers[position];
		}

		function getGroupActionData(position){
			return functions.cfg('groupActionData_' + position);
		}

		//--------
		//функции для работа окна
		function GroupAction(){}
		GroupAction.prototype = {
			/**
			 * @param object settings {position: ...}
			 */
			init: function(settings){
				this.setSettings(settings);
				this.setEvents();
				return this;
			},
			setSettings: function(settings){
				this.select = g('js_groupActions_' + settings.position);
				this.position = settings.position;
			},
			setEvents: function(){
				var self = this;
				if (this.select) {
					this.select.onchange = function(){
						if (this.value) self.openAction(this.value);
					};
					mediator.subscribe('onPopupHide', function(){self.select.value = '';});
				}
			},
			openAction: function(actionName){
				managementAjax.cbOnSuccess('runModule/groupActions/content.ajax', 'actionName=' + actionName + '&position=' + this.position + '&' + element.getParameters('js_itemsContent'), function(r) {
					popupManager.popup.html(r).show();
				});
			}
		};
		//--------
		//функции для работа окна

		//--------
		//функции для ajax запросов
		functions.mergeObjects(GroupAction.prototype, {
			ajax: function(){
				var self = this;

				function cbOnSuccess(url, data, cb){
					var params = {};
					params['position'] = self.position;
					params['groupActionData_' + self.position] = getGroupActionData(self.position);

					return managementAjax.cbOnSuccess(url, $.param(params) + '&' + element.getParameters('js_itemsContent') + '&' + data, cb);
				}

				function updateOnSuccess(url, data, block){
					return cbOnSuccess(url, data, function(r){
						popupManager.popup.html(r);
						if (~r.indexOf('edit-message')){
							setTimeout(function() {
								popupManager.popup.hide();
							}, 600);
							list.reload();
						}
					});
				}

				return {
					updateOnSuccess: updateOnSuccess
				};
			}
		});
		//функции для ajax запросов
		//--------

		return {
			init: init,
			getHandler: getHandler
		};
	}();

	return {
		list: list,
		ajax: managementAjax,
		items: items,
		form: form,
		groupActions: groupActions
	};
}();