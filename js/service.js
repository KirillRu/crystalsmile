mediator = function(){
	var channels = {};

	function subscribe(name, cb, _context, _single){
		if (!channels[name]) channels[name] = [];
		channels[name].push({cb: cb, single: _single || false, context: _context || null});
		return mediator;
	}

	function unsubscribe(name, cb){
		if (channels[name]){
			channels[name] = channels[name].filter(function(obj){
				return !(obj.cb == cb);
			});
			if (!channels[name].length) delete channels[name];
		}
		return mediator;
	}

	function publish(name){
		if (channels[name]){
			var args = Array.prototype.slice.call(arguments, 1);
			channels[name] = channels[name].filter(function(obj){
				obj.cb.apply(obj.context, args);
				return !obj.single;
			});
			if (!channels[name].length) delete channels[name];
		}
		return mediator;
	}

	return {
		subscribe: subscribe,
		unsubscribe: unsubscribe,
		publish: publish
	};
}();

ajax = function(){

	function prepareOptions(options){
		if (!options['type']) options['type'] = 'post';
		if (!options['error']) options['error'] = function(a, b, c){
			if (console) console.log(a, b, c);
		};
		if (!options['beforeSend']) options['beforeSend'] = function(){
			popupManager.ajaxLoad.show();
		};
		if (!options['complete']) options['complete'] = function(){
			popupManager.ajaxLoad.hide();
		};

		return options;
	}

	function requestOptional(options){
		return $.ajax(prepareOptions(options));
	}

	function cbOnSuccess(url, data, cbSuccess){
		return requestOptional({
			url : url,
			data : data,
			success : cbSuccess
		});
	}

	function reloadIfEmpty(url, data, block) {
		return cbOnSuccess(url, data, function(r){
			if (r) {
				$(g(block)).html(r);
			} else {
				document.location.reload();
			}
		});
	}

	function redirectIfEmpty(url, data, block, redirectUrl){
		return cbOnSuccess(url, data, function(r){
			if (r) {
				$(g(block)).html(r);
			} else {
				document.location.href = redirectUrl;
			}
		});
	}

	function redirectIfEmptyWithScroll(url, data, block, scrollBlock, redirectUrl){
		return cbOnSuccess(url, data, function(r){
			if (r) {
				$(g(block)).html(r);
				element.scrollTo(g(scrollBlock));
			} else {
				document.location.href = redirectUrl;
			}
		});
	}

	function update(url, data, block) {
		return cbOnSuccess(url, data, function(r){
			if (block) $(g(block)).html(r);
		});
	}

	function replace(url, data, block) {
		return cbOnSuccess(url, data, function(r){
			$(g(block)).replaceWith(r);
		});
	}

	function replaceAndFindLazySrc(url, data, block){
		return cbOnSuccess(url, data, function(r){
			lazyImageLoader.addToImgList($(g(block)).replaceWith(r).find('img[lazySrc]').get());
		});
	}

	function requestInPopup(url, data) {
		return cbOnSuccess(url, data, function(r){
			popupManager.popup.html(r).show();
		});
	}

	function updateWithSlideDown(url, params, block){
		return cbOnSuccess(url, params, function(r){
			$(g(block)).css('display', 'none').html(r).slideDown(300);
		});
	}

	function updateAndFindLazySrc(url, data, block){
		return cbOnSuccess(url, data, function(r){
			lazyImageLoader.addToImgList($(g(block)).html(r).find('img[lazySrc]').get());
		});
	}

	function pagerUpdate(params){
		cbOnSuccess(
			params['url'],
			params['idForm'] ? element.getParameters(params['idForm']) : '',
			function(r){
				var block = g(params['block']);
				lazyImageLoader.addToImgList($(block).html(r).find('img[lazySrc]').get());
				if (params['scroll']) element.scrollTo(block);
				popupManager.ajaxLoad.hide();
			}
		);
		return false;
	}

	function simpleRequest(url, data){
		return requestOptional({
			url : url,
			data : data,
			beforeSend: function(r){}
		});
	}

	return {
		requestOptional : requestOptional,
		cbOnSuccess: cbOnSuccess,
		update: update,
		reloadIfEmpty : reloadIfEmpty,
		replace: replace,
		replaceAndFindLazySrc: replaceAndFindLazySrc,
		requestInPopup: requestInPopup,
		updateAndFindLazySrc: updateAndFindLazySrc,
		updateWithSlideDown: updateWithSlideDown,
		redirectIfEmpty: redirectIfEmpty,
		redirectIfEmptyWithScroll: redirectIfEmptyWithScroll,
		simpleRequest: simpleRequest,
		pagerUpdate: pagerUpdate
	}
}();

lazyImageLoader = function(){
	var data; 

	function onDomReady(){
		refreshImgList();
		$(window).on('scroll', onScroll);
	}

	function onScroll(){
		var countScroll = functions.getScrollTop() + getWindowHeight();
		data = data.filter(function(elemData){
			if (countScroll > elemData.offsetTop){
				startLoad(elemData.elem);
				return false;
			}
			return true;
		});
	}

	function startLoad(img){
		var newImg = document.createElement('img');
		newImg.alt = img.alt;
		newImg.title = img.title;

		var className = img.className;
		if (img.getAttribute('lazyClass')) className += (className ? ' ' : '') + img.getAttribute('lazyClass');

		newImg.className = className;
		newImg.onload = function() {element.replace(newImg, img);};
		newImg.onerror = function() {
			img.src = '/img/no-foto-large.gif';
			ajax.simpleRequest('/runModule/fotos/noFound.ajax', {lazySrc: img.getAttribute('lazySrc')});
		};
		newImg.src = img.getAttribute('lazySrc');
	}

	function getWindowHeight(){
		if (self.innerHeight) return self.innerHeight;
		if (document.documentElement && document.documentElement.clientHeight) return document.documentElement.clientHeight;
		if (document.body) return document.body.clientHeight;
	}

	function refreshImgList(){
		data = [];
		addToImgList($('img[lazySrc]'));
	}

	function addToImgList(imgs){
		var countScroll = functions.getScrollTop() + getWindowHeight();
		for (var len = imgs.length, offset; len--;) {
			if (imgs[len].getAttribute('lazySrc')){
				offset = element.getOffsetTop(imgs[len]);
				if (countScroll + 100 > offset) startLoad(imgs[len]);
				else data.push({
					offsetTop: offset,
					elem: imgs[len]
				});
			}
		}
	}

	return {
		onDomReady: onDomReady,
		refreshImgList: refreshImgList,
		addToImgList: addToImgList
	};
}();

/**Выравнивание элемента по центру относителдьно окна. Изменения свойства left при изменении размера окна*/
fixedElements = function(w) {
	var elements = {};

	function moveToCenter(elem) {
		elem.style.top = (functions.getScrollTop() + 80) + 'px';
		elem.style.left = (functions.getScrollLeft() + ($(w).width() / 2) - ($(elem).outerWidth() / 2)) + 'px';
		return fixedElements;
	}

	/**
	 * @param elem
	 * @param id для того, что бы не менять свойство left  у одного и того же элемента
	 */
	function addToFixedLeft(elem, id) {
		elements[id || elem.id] = $(elem);
		return fixedElements;
	}

	function removeFromFixedLeft(id) {
		if (id in elements) delete elements[id];
		return fixedElements;
	}

	function onDomReady(){
		$(w).on('resize', function() {
			for (var $elem in elements) {
				elements[$elem].css('left', (functions.getScrollLeft() + ($(w).width() / 2) - (elements[$elem].outerWidth() / 2)));
			}
		});
	}

	return {
		moveToCenter: moveToCenter,
		addToFixedLeft: addToFixedLeft,
		removeFromFixedLeft: removeFromFixedLeft,
		onDomReady: onDomReady
	};
}(window);

portal = function(){

	function alert(text){
		var $focusedElement = $('input:focus');
		var $windowAlert = $('' +
			'<div style="display: none">' +
				'<div id="js_alertBackground" class="pw-wrapper">' +
				'</div>'+
				'<div class="popupWindow">' +
					'<div class="popupWindowBlock">' +
						'<div class="iw-text">' + text + '</div>' +
						'<div class="iw-buttons">' +
							'<button class="button" id="js_alertYes">ОК</button>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>'
		);
		function ok(){
			$windowAlert.remove();
			functions.enableScroll();
			if ($focusedElement.length) $focusedElement.focus();
		}
		var $yesButton = $windowAlert.find('#js_alertYes');
		$windowAlert.appendTo('body').show();
		$windowAlert.find('#js_alertBackground').on('click', ok);
		$yesButton.one('click', ok);
		$windowAlert.find('.popupWindow').on('click', function(){$yesButton.focus()});
		$windowAlert.on('keydown', function(e){if (functions.getCharCode(e) == 27) ok();});
		var $dialogWindow = $windowAlert.find('.popupWindow'), $w = $(window);
		$dialogWindow.css({
			top: functions.getScrollTop() + ($w.height() / 2) - ($dialogWindow.outerHeight() / 2) - 50,
			left: functions.getScrollLeft() + ($w.width() / 2) - ($dialogWindow.outerWidth() / 2)
		});
		fixedElements.addToFixedLeft($dialogWindow.get(0));
		$yesButton.focus();
		functions.disableScroll();
	}

	function confirm(text){
		var $focusedElement = $('input:focus');
		var ret = {
			_yesCb: null,
			_noCb: null,
			yes: function(cb){ret._yesCb = cb; return ret;},
			no: function(cb){ret._noCb = cb; return ret;}
		};

		function accept(){
			$window.remove();
			functions.enableScroll();
			if (ret._yesCb) {
				ret._yesCb();
				delete ret._yesCb;
			}
			if ($focusedElement.length) $focusedElement.focus();
		}

		function deny(){
			$window.remove();
			functions.enableScroll();
			if (ret._noCb) {
				ret._noCb();
				delete ret._noCb;
			}
			if ($focusedElement.length) $focusedElement.focus();
		}

		var $window = $('' +
			'<div style="display:none">' +
				'<div id="js_confirmBackground" class="pw-wrapper">' +
				'</div>'+
				'<div class="popupWindow">' +
					'<div class="popupWindowBlock">' +
						'<div class="iw-text">' + text + '</div>' +
						'<div class="iw-buttons">' +
							'<button class="button" id="js_confirmYes">Да</button><button class="button" id="js_confirmNo">Нет</button>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>'
		);
		var $yesButton = $window.find('#js_confirmYes');
		$window.appendTo('body').show();
		$window.find('#js_confirmBackground').on('click', deny);
		$yesButton.one('click', accept);
		$window.find('#js_confirmNo').one('click', deny);
		$window.find('.popupWindow').on('click', function(){$yesButton.focus()});
		$window.on('keydown', function(e){if (functions.getCharCode(e) == 27) deny();});
		var $dialogWindow = $window.find('.popupWindow'), $w = $(window);
		$dialogWindow.css({
			top: functions.getScrollTop() + ($w.height() / 2) - ($dialogWindow.outerHeight() / 2) - 50,
			left: functions.getScrollLeft() + ($w.width() / 2) - ($dialogWindow.outerWidth() / 2)
		});
		fixedElements.addToFixedLeft($dialogWindow.get(0));
		$yesButton.focus();
		functions.disableScroll();
		return ret;
	}

	return {
		alert: alert,
		confirm: confirm
	};
}();

onMissClick = function(){

	function __hideOnMC(idBlock){
		this.idBlock = idBlock;
		this.ignoredElement = null;
		this.missClick = function(){};

		var self = this;
		$('body').on('click.' + this.idBlock, function(e){
			if (e.target.ignoreHideOnClick !== self.idBlock && !$(e.target).closest('#' + self.idBlock).length && $(e.target).closest('body').length){
				//если функция missClick возвращает false, то не удаляем onclick
				var result = self.missClick(e.target);
				if (result !== false) self.removeEvent();
			}
		});
	}
	__hideOnMC.prototype = {
		setEvent: function(cb){
			this.missClick = cb;
			return this;
		},
		removeEvent: function(){
			$('body').off('click.' + this.idBlock);
			if (this.ignoredElement) delete this.ignoredElement.ignoreHideOnClick;
			return this;
		},
		ignore: function(elem){
			this.ignoredElement = elem;
			elem.ignoreHideOnClick = this.idBlock;
			return this;
		}
	};

	return function(idBlock){
		return new __hideOnMC(idBlock);
	}
}();


$(function(){
	fixedElements.onDomReady();
	lazyImageLoader.onDomReady();
});