relativeTop = function(w) {
	var stepPositions = {};
	var $hhBlock = $('#js_hhBlock');
	var buttonAdd = g('js_hhButtonAdd');
	var hideMenu = parseInt(localStorage.getItem('hideBlocks')||'0', 10);
	var lastStep = '';

	function _setStepPosition() {
		stepPositions['hat'] = element.getOffsetTop(g('js_hat2'));
		stepPositions['button_add'] = element.getOffsetTop(g('js_buttonAdd')) - 10;
	}

	function _getStep() {
		var topScroll = $(w).scrollTop();
		if (topScroll >= stepPositions['button_add']) return 'button_add';
		if (topScroll >= stepPositions['hat']) return 'hat';
		return '';
	}

	function _hide() {
		$hhBlock.slideUp(200, function(){mediator.publish('onRelativeTopHide');});
	}

	function _show() {
		$hhBlock.slideDown(200, function(){mediator.publish('onRelativeTopShow')});
	}

	function _actions() {
		var currentStep = _getStep();
		if (currentStep !== lastStep){
			switch (currentStep) {
				case 'hat':
					if (buttonAdd.style.visibility != 'hidden') buttonAdd.style.visibility = 'hidden';
					_show();
					break;
				case 'button_add':
					if (buttonAdd.style.visibility != 'visible') buttonAdd.style.visibility = 'visible';
					_show();
					break;
				case '':
					_hide();
					break;
			}
			lastStep = currentStep;
		}
	}

	function _onArrowClick() {
		/**Показываем(скрываем) всё, что стоит после стрелки*/
		var arrowDomElem = this;
		if (element.hasClass(this, 'hh-collapse')) {
			arrowDomElem.className = 'hh-arrow hh-expand';
			hideMenu = 1;
			while (arrowDomElem = element.nextElement(arrowDomElem)) $(arrowDomElem).hide();
		} else {
			arrowDomElem.className = 'hh-arrow hh-collapse';
			hideMenu = 0;
			while (arrowDomElem = element.nextElement(arrowDomElem)) $(arrowDomElem).show();
		}
		localStorage.setItem('hideBlocks', hideMenu.toString());
		mediator.publish(hideMenu ? 'onRelativeTopHide' : 'onRelativeTopShow');
	}

	function onDomReady() {
		_setStepPosition(); // устанавливаем позиции, по которым выводим/скрываем шапку
		_actions(); // при открытии страницы показать/скрыть шапку (если страница открылась не на самой верхней точке)
		$(w).on('scroll', _actions); // вешаем событие на скрол, открываем скрываем шапку
		var arrowDomElem = g('js_hhArrow');

		$(arrowDomElem).on('click', _onArrowClick);// событие на стрелку в шапке

		if (hideMenu) {
			arrowDomElem.className = 'hh-arrow hh-expand';
			while (arrowDomElem = element.nextElement(arrowDomElem)) $(arrowDomElem).hide();
		} else {
			arrowDomElem.className = 'hh-arrow hh-collapse';
			while (arrowDomElem = element.nextElement(arrowDomElem)) $(arrowDomElem).show();
			mediator.publish('onRelativeTopHide');
		}
	}

	return {
		onDomReady: onDomReady
	};
}(window);

$(function(){
	relativeTop.onDomReady();
});