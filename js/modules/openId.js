(function() {
	var _socialPrototype = {
		callFunction: function(cbFunction) {
			var self = this;
			if (this.scriptLoaded) cbFunction.call(this);
			else {
				scriptLoader(this.src, true).callFunction(function() {
					self.init();
					cbFunction.call(self);
					self.scriptLoaded = true;
				});
			}
		},

		actionInsert: function(serverResponse) {//Выполняется при успешном checkId
			this.actionInsertSuccess(serverResponse);
			this.saveInput(serverResponse);
			this.getData(this.insertDataInFields);
		},

		actionInsertSuccess: function(responseText) {
			this.clearError();
			var divSuccess = $('#errors .edit-message').get(0), divSocialSuccess;
			if (!(divSocialSuccess = g('socialSuccess_' + this.social))) {
				divSocialSuccess = document.createElement('div');
				divSocialSuccess.id = 'socialSuccess_' + this.social;
			}
			divSocialSuccess.innerHTML = responseText.text;
			if (divSuccess) divSuccess.appendChild(divSocialSuccess);
			else {
				divSuccess = document.createElement('div');
				divSuccess.className = 'edit-message';
				divSuccess.appendChild(divSocialSuccess);
				g('errors').appendChild(divSuccess);
			}
		},

		clearSuccess: function() {
			var item;
			if (item = g('socialSuccess_' + this.social)) {
				item.parentNode.removeChild(item);
				item = $('#errors .edit-message').get(0);
				if (!item.innerHTML) {
					item.parentNode.removeChild(item);
				}
			}
		},

		actionInsertError: function(responseText) {//Выполняется при неуспешном checkId
			this.clearSuccess();
			var divError = $('#errors .error-message').get(0), divSocialError;
			if (!(divSocialError = g('socialError_' + this.social))) {
				divSocialError = document.createElement('div');
				divSocialError.id = 'socialError_' + this.social;
			}
			divSocialError.innerHTML = responseText;
			if (divError) divError.appendChild(divSocialError);
			else {
				divError = document.createElement('div');
				divError.className = 'error-message';
				divError.appendChild(divSocialError);
				g('errors').appendChild(divError);
			}
		},

		clearError: function() {
			var item;
			if (item = g('socialError_' + this.social)) {
				item.parentNode.removeChild(item);
				item = $('#errors .error-message').get(0);
				if (!item.innerHTML) {
					item.parentNode.removeChild(item);
				}
			}
		},

		saveInput: function(serverResponse) {
			var input;
			if (input = g('id_user_' + this.social)) {
				input.value = serverResponse.id;
			} else {
				input = document.createElement('input');
				input.type = 'hidden';
				input.id = 'id_user_' + this.social;
				input.name = 'id_user_' + this.social;
				input.value = serverResponse.id;
				g(this.idForm).appendChild(input);
			}
			if (input = g('id_user_security_' + this.social)) {
				input.value = serverResponse.idSecurity;
			} else {
				input = document.createElement('input');
				input.type = 'hidden';
				input.id = 'id_user_security_' + this.social;
				input.name = 'id_user_security_' + this.social;
				input.value = serverResponse.idSecurity;
				g(this.idForm).appendChild(input);
			}
		},

		checkIdRequest: function(data, cbSuccess, cbError) {
			var self = this;
			ajax.requestOptional({
				url: 'checkOpenId.ajax',
				data: data,
				dataType: 'json',
				beforeSend: function() {},
				onComplete: function(responseText) {
					if (responseText.data)
						if (responseText.status == 'success') {
							if (typeof(cbSuccess) == 'function') cbSuccess.call(self, responseText.data);
						} else {
							if (typeof(cbError) == 'function') cbError.call(self, responseText.data.text);
							self.logout();
						}
				}
			});
		},

		_exit: function() {
			var input;
			this.userData = false;
			this.clearSuccess();
			if (input = g('id_user_' + this.social)) {
				input.value = 0;
			}
			if (input = g('id_user_security_' + this.social)) {
				input.value = 0;
			}
			if (this.activeButtons.authButton) this.callFunction(function() {this._setAuthButton(this.activeButtons.authButton)});
			if (this.activeButtons.insertFieldsButton) this.callFunction(function() {this._setInsertFieldsButton(this.activeButtons.insertFieldsButton)});
		},

		_setAuthButton: function(button) {
			this.checkStatus(
				function() { //login = on
					var self = this;
					button.onclick = function() {window.location.href = self.authorizeHref};
				},
				function() { //login = off
					var self = this;
					button.onclick = function() {
						self.loginUser(
							function() { //after login
								window.location.href = this.authorizeHref;
							}
						);
					}
				}
			);
			this.activeButtons.authButton = button;
		},

		setStandardOnClick: function(button, response) {
			var self = this;
			button.onclick = function() {
				popupManager.ajaxLoad.show();
				self.checkId(// Проверяем id
					response, //передаём id
					function(serverResponse) { //Если такого id нет
						this.actionInsert(serverResponse); //рисуем инпут и заполняем поля
					},
					self.actionInsertError//выполняем, если 2 id соц сети на 1 аккаунт
				)
			};
		},

		setAuthOnClick: function(button) {
			var self = this;
			button.onclick = function() {
				self.loginUser(//Выводим окно авторизации
					function(response) {
						var self = this;
						popupManager.ajaxLoad.show();
						this.checkId( //При успешной регистрации проверяем id
							response, //передаём id
							function(serverResponse) { //выполняем, если такого id у нас нет, всё в норме
								this.setStandardOnClick(button, response);
								this.actionInsert(serverResponse); //рисуем инпут и заполняем поля
							},
							self.actionInsertError//выполняем, если 2 id соц сети на 1 аккаунт
						)
					}
				)
			};
		},

		_setInsertFieldsButton: function(button) {
			this.checkStatus(
				function(response) { //Если авторизован
					this.setStandardOnClick(button, response);
				},
				function() {	//Если не авторизован
					this.setAuthOnClick(button);
				}
			);
			this.activeButtons.insertFieldsButton = button;
		},

		returnData: function() {
			var self = this;
			$('body').on('click', '#cancelSocial_' + this.social, function() {
				self.logout();
			});
			return {
				setInsertFieldsButton: function(idButton) {
					var button;
					if (button = g(idButton)) {
						self.callFunction(function() {this._setInsertFieldsButton(button)});
					}
				},
				setAuthButton: function(idButton) {
					var button;
					if (button = g(idButton)) {
						self.callFunction(function() {this._setAuthButton(button)});
					}
				},
				exit: function() {
					self.logout();
				}
			}
		}
	};

	OpenId_vk = function(params) {
		this.params = params;
		this.social = 'vk';
		var self = this, fields = 'nickname, screen_name, city, contacts', arr = window.location.hostname.split('.').reverse(),
			hrefRedirect = this.params.href || (window.location.protocol + '//' + arr[1] + '.' + arr[0] + '/open_id/' + this.social);
		this.appId = this.params.appId;
		/*id приложения в ВК*/
		this.scriptLoaded = false;
		this.src = 'http://vk.com/js/api/openapi.js';
		this.userData = false;
		/*cache(заполняется после получения данных)*/
		this.authorizeHref = 'http://api.vkontakte.ru/oauth/authorize?client_id=' + this.appId + '&redirect_uri=' + hrefRedirect + '&display=page&response_type=code';
		this.idForm = 'editForm';
		this.activeButtons = {authButton: false, insertFieldsButton: false};

		this.init = function() {
			VK.init({
				apiId: self.appId
			});
		};

		this.checkStatus = function(cbAuth, cbNotAuth) {
			VK.Auth.getLoginStatus(function(response) {
				if (response.session) { //login = on
					if (typeof(cbAuth) == 'function') cbAuth.call(self, response);
				} else { //login = off
					if (typeof(cbNotAuth) == 'function') cbNotAuth.call(self, response);
				}
			});
		};

		this.checkId = function(response, cbSuccess, cbError) {
			response = response.session;
			var param = 'social=' + this.social + '&id=' + response.mid + '&expire=' + response.expire + '&mid=' + response.mid + '&secret=' + response.secret + '&sid=' + response.sid + '&sig=' + response.sig;
			this.checkIdRequest(param, cbSuccess, cbError);
		};

		this.logout = function() {
			VK.Auth.logout(function() {
				self._exit();
			});
		};
		this.loginUser = function(cbSuccess, cbError) {
			VK.Auth.login(function(response) {
				if (response.session) { //ok
					if (typeof(cbSuccess) == 'function') cbSuccess.call(self, response);
				} else { //не ok
					if (typeof(cbError) == 'function') cbError.call(self, response);
					//нажата кнопка отмены
				}
			});
		};

		this.getData = function(callback) {
			if (!this.userData) {
				VK.Api.call('users.get', {'fields': fields}, function(answer) {
					if (self.userData = answer.response[0]) {
						VK.Api.call('getCities', {'cids': self.userData.city, 'format': 'JSON'}, function(data) { //По номеру города получить название
							if (data.response[0]) self.userData.city = data.response[0].name;
							callback.call(self, self.userData);
						});
					}
				});
			}
			else {
				callback(this.userData);
			}
		};

		this.insertDataInFields = function(data) {
			var input;
			if (input = g('registr_login')) {
				input.value = data['nickname'] ? data['nickname'] : '';
				input.onkeyup();
			}
			if (data['city'] && g('registr_id_city')) {
				ajax.requestOptional({
					url: 'select-child/block-block_registr_id_city/name-registr_id_city/table-citys/list.ajax',
					data: 'name=' + data['city'],
					success: function(responseText) {element.setInnerHTML('block_registr_id_city', responseText);},
					beforeSend: function() {}
				});
			}
			if (input = g('registr_face')) input.value = data['first_name'] + ' ' + data['last_name'];
			if (input = g('registr_phone')) input.value = (data['mobile_phone'] ? data['mobile_phone'] : (data['home_phone'] ? data['home_phone'] : ''));
		};

		return this.returnData();
	};
	OpenId_vk.prototype = _socialPrototype;

	OpenId_fb = function(params) {
		this.params = params;
		this.social = 'fb';
		var arr = window.location.hostname.split('.').reverse(),
			self = this, hrefRedirect = this.params.href || (window.location.protocol + '//' + arr[1] + '.' + arr[0] + '/open_id/' + this.social);
		this.appId = this.params.appId;
		/*id приложения в ВК*/
		this.src = '//connect.facebook.net/en_US/all.js';
		this.userData = false;
		/*cache(заполняется после получения данных)*/
		this.scriptLoaded = false;
		this.authorizeHref = 'https://graph.facebook.com/oauth/authorize?client_id=' + this.appId + '&redirect_uri=' + hrefRedirect + '&display=page&response_type=code';
		this.idForm = 'editForm';
		this.activeButtons = {authButton: false, insertFieldsButton: false};

		this.init = function() {
			FB.init({
				appId: self.appId,                  // App ID from the app dashboard
				channelUrl: 'http://promportal.su', 		// Channel file for x-domain comms
				status: true,                        // Check Facebook Login status
				xfbml: true                         // Look for social plugins on the page
			});
		};

		this.checkStatus = function(cbAuth, cbNotAuth) {
			FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					if (typeof(cbAuth) == 'function') cbAuth.call(self, response);
				} else {
					if (typeof(cbNotAuth) == 'function') cbNotAuth.call(self, response);
				}
			});
		};

		this.checkId = function(response, cbSuccess, cbError) {
			response = response.authResponse;
			var param = 'social=' + this.social + '&id=' + response.userID + '&accessToken=' + response.accessToken;
			this.checkIdRequest(param, cbSuccess, cbError);
		};

		this.loginUser = function(cbSuccess, cbError) {
			FB.login(function(response) {
				if (response.authResponse) {
					if (typeof(cbSuccess) == 'function') cbSuccess.call(self, response);
				} else {
					if (typeof(cbError) == 'function') cbError.call(self, response);
				}
			}, {scope: 'email'});
		};

		this.logout = function() {
			this.checkStatus(function() {
				FB.logout(function() {self._exit()});
			});
		};

		this.getData = function(callback) {
			if (!this.userData)
				FB.api('/me', function(answer) {
					if (answer.name) {
						self.userData = answer;
						callback.call(self, self.userData);
					}
				});
			else {
				callback(this.userData);
			}
		};

		this.insertDataInFields = function(data) {
			var input;
			if (data['hometown'] && data['hometown']['name'] && g('registr_id_city')) {
				ajax.requestOptional({
					url: 'select-child/block-block_registr_id_city/name-registr_id_city/table-citys/list.ajax',
					data: 'name=' + data['hometown']['name'].substr(0, data['hometown']['name'].indexOf(',')),
					success: function(responseText) {element.setInnerHTML('block_registr_id_city', responseText);},
					beforeSend: function() {}
				});
			}
			if (input = g('registr_face')) input.value = data['name'];
			if (input = g('registr_email')) input.value = data['email'];
		};

		return this.returnData();
	};
	OpenId_fb.prototype = _socialPrototype;

	OpenId_ma = function(params) {
		this.params = params;
		this.social = 'ma';
		var arr = window.location.hostname.split('.').reverse(),
			self = this, hrefRedirect = this.params.href || (window.location.protocol + '//' + arr[1] + '.' + arr[0] + '/open_id/' + this.social);
		this.appId = this.params.appId;
		this.privateKey = this.params.privateKey;
		this.userData = false;
		this.scriptLoaded = false;
		this.authorizeHref = 'https://connect.mail.ru/oauth/authorize?client_id=' + this.appId + '&redirect_uri=' + hrefRedirect + '&response_type=code';
		this.src = 'http://cdn.connect.mail.ru/js/loader.js';
		this.idForm = 'editForm';
		this.activeButtons = {authButton: false, insertFieldsButton: false};
		this.callFunction = function(cbFunction) {
			if (this.scriptLoaded) cbFunction.call(this);
			else {
				scriptLoader(this.src, true).callFunction(function() {self.init(cbFunction);});
			}
		};
		this.init = function(cbFunction) {
			mailru.loader.require('api', function() {
				mailru.connect.init(self.appId, self.privateKey);
				cbFunction.call(self);
				self.scriptLoaded = true;
			});
		};
		this.checkId = function(response, cbSuccess, cbError) {
			var param = 'social=' + this.social + '&id=' + response.oid + '&session_key=' + response.session_key + '&sig=' + response.sig;
			this.checkIdRequest(param, cbSuccess, cbError);
		};
		this.checkStatus = function(cbAuth, cbNotAuth) {
			mailru.connect.getLoginStatus(function(response) {
				if (response.oid) {
					if (typeof(cbAuth) == 'function') cbAuth.call(self, response);
				} else {
					if (typeof(cbNotAuth) == 'function') cbNotAuth.call(self, response);
				}
			});
		};
		this.loginUser = function(cbSuccess, cbError) {
			mailru.events.listen(mailru.connect.events.login, function(response) {
				if (response.oid) {
					if (typeof(cbSuccess) == 'function') cbSuccess.call(self, response);
				} else {
					if (typeof(cbError) == 'function') cbError.call(self, response);
				}
			});
			mailru.connect.login();
		};
		this.logout = function() {
			this.checkStatus(function() {
				mailru.connect.logout();
				self._exit();
			});
		};
		this.getData = function(callback) {
			if (!this.userData)
				mailru.common.users.getInfo(function(answer) {
					if (answer[0] && answer[0].uid) {
						self.userData = answer[0];
						callback.call(self, self.userData);
					}
				});
			else {
				callback(this.userData);
			}
		};
		this.insertDataInFields = function(data) {
			var input, name = '';
			if (data['location'] && g('registr_id_city')) {
				if (data['location']['city']) name = data['location']['city']['name'];
				else if (data['location']['region']) name = data['location']['region']['name'];
				else if (data['location']['country']) name = data['location']['country']['name'];
				ajax.requestOptional({
					url: 'select-child/block-block_registr_id_city/name-registr_id_city/table-citys/list.ajax',
					data: 'name=' + name,
					success: function(responseText) {element.setInnerHTML('block_registr_id_city', responseText);},
					beforeSend: function() {}
				});
			}
			if (input = g('registr_face')) {
				if (data['last_name']) name = data['last_name'] + ' ';
				if (data['first_name']) name += data['first_name'];
				input.value = name
			}
			if (input = g('registr_login')) {
				input.value = data['nick'] ? data['nick'] : '';
				input.onkeyup();
			}
			if (input = g('registr_email')) input.value = data['email'];
		};
		return this.returnData();
	};
	OpenId_ma.prototype = _socialPrototype;

	var googleLoaded = false;
	initGo = function() {
		googleLoaded = true;
	};
	OpenId_go = function(params) {
		this.params = params;
		this.social = 'go';
		var arr = window.location.hostname.split('.').reverse(),
			self = this, hrefRedirect = this.params.href || (window.location.protocol + '//' + arr[1] + '.' + arr[0] + '/open_id/' + this.social);
		this.appId = this.params.appId;
		this.privateKey = this.params.privateKey;
		this.userData = false;
		this.scriptLoaded = false;
		this.authorizeHref = 'https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/plus.me&redirect_uri=' + hrefRedirect + '&response_type=code&client_id=' + this.appId;
		this.src = 'https://apis.google.com/js/client.js?onload=initGo';
		this.idForm = 'editForm';
		this.activeButtons = {authButton: false, insertFieldsButton: false};
		this.callFunction = function(cbFunction) {
			var self = this;
			if (this.scriptLoaded) cbFunction.call(this);
			else {
				var myFunc = function() {
					if (!googleLoaded) {
						setTimeout(myFunc, 100);
						return;
					}
					self.init();
					cbFunction.call(self);
					self.scriptLoaded = true;
				};
				scriptLoader(this.src, true).callFunction(myFunc);
			}
		};
		this.init = function() {
			gapi.auth.init(function() {});
//			gapi.client.setApiKey(this.privateKey);
		};
		this.checkStatus = function(cbAuth, cbNotAuth) {
			gapi.auth.authorize({client_id: this.appId, scope: 'https://www.googleapis.com/auth/plus.me', immediate: true}, function(response) {
				if (response) {
					if (typeof(cbAuth) == 'function') cbAuth.call(self, response);
				} else {
					if (typeof(cbNotAuth) == 'function') cbNotAuth.call(self, response);
				}
			});
		};
		this.checkId = function(response, cbSuccess, cbError) {
			var param = 'social=' + this.social + '&access_token=' + response.access_token;
			this.checkIdRequest(param, cbSuccess, cbError);
		};
		this.loginUser = function(cbSuccess, cbError) {
			gapi.auth.authorize({client_id: this.appId, scope: 'https://www.googleapis.com/auth/plus.login', immediate: false}, function(response) {
				if (response) {
					if (typeof(cbSuccess) == 'function')  cbSuccess.call(self, response);
				} else {
					if (typeof(cbError) == 'function') cbError.call(self, response);
				}
			});
		};
		this.logout = function() {
			this.checkStatus(function() {
				var img = document.createElement('img');
				img.src = 'https://accounts.google.com/logout';
				img.style.width = '0px';
				img.style.heigth = '0px';
				document.getElementsByTagName('body')[0].appendChild(img);
				img.parentNode.removeChild(img);
				self._exit()
			});
		};
		this.getData = function(callback) {
			if (!self.userData) {
				gapi.client.load('plus', 'v1', function() {
					var request = gapi.client.plus.people.get({
						'userId': 'me',
						'fields': 'displayName,id'
					});
					request.execute(function(resp) {
						if (resp.result) self.userData = resp.result;
						callback.call(self, self.userData);
					});
				});
			} else callback.call(self, self.userData);
		};
		this._exit = function() {
			var input;
			this.userData = false;
			this.clearSuccess();
			if (input = g('id_user_' + this.social)) {
				input.value = 0;
			}
			if (input = g('id_user_security_' + this.social)) {
				input.value = 0;
			}
			if (this.activeButtons.authButton) this.callFunction(function() {this._setAuthButton(this.activeButtons.authButton)});
			if (this.activeButtons.insertFieldsButton) this.callFunction(function() {
				this.setAuthOnClick(this.activeButtons.insertFieldsButton);
			});
		};
		this.insertDataInFields = function(data) {
			var input;
			if ((input = g('registr_face')) && data['displayName']) input.value = data['displayName'];
		};
		return this.returnData();
	};
	OpenId_go.prototype = _socialPrototype;
	function setButton(funcName, params) {
		var i, obj;
		console.log(params);
		for (i in params) if (params.hasOwnProperty(i)) {
			switch (i) {
				case 'vk': obj = new OpenId_vk(params[i]); break;
				case 'fb': obj = new OpenId_fb(params[i]); break;
				case 'ma': obj = new OpenId_ma(params[i]); break;
				case 'go': obj = new OpenId_go(params[i]); break;
				default: obj = false; break;
			}
			if (obj) {
				if (funcName == 'auth') obj.setAuthButton(params[i]['id_button']);
				else obj.setInsertFieldsButton(params[i]['id_button']);
			}
		}
	}

	setInsertFieldsButton = function(params) {
		setButton('insert', params);
	};
	setAuthButton = function(params) {
		setButton('auth', params);
	};
}());
