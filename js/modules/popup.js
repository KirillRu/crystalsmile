if(typeof popupManager === 'undefined')
popupManager = function() {
	var popup = function() {
		var popupHtml = '' +
			'<div id="js_popupContainer">' +
				'<div class="pw-wrapper"></div>' +
				'<div class="popup" id="js_popup">'+
					'<div class="close" id="js_closePopup"><img src="http://static.izhart.ru/img/popup/icon-close.png"></div>'+
					'<div id="js_popupContent"></div>'+
				'</div>' +
			'</div>';
		var popupDom;

		/**@returns popup*/
		function create() {
			if (!popupDom) {
				$('body').append(popupHtml);
				popupDom = g('js_popup');
				g('js_closePopup').onclick = hide;
				popupDom.parentNode.onclick = function(e){
					e = e || window.event;
					if (!$(e.target).closest('#js_popup').length && $(e.target).closest('body').length/*это условие для того, что бы не закрывалось окно при удалении элемента внутри popup*/){
						hide();
					}
				}
			}
			return popup;
		}

		/**@returns popup*/
		function hide() {
			getPopup().parentNode.style.display = 'none';
			mediator.publish('onPopupHide');
			return popup;
		}

		/**@returns popup*/
		function show() {
			create();
			fixedElements.moveToCenter(popupDom).addToFixedLeft(popupDom);
			popupDom.parentNode.style.display = 'block';
			return popup;
		}

		/**@returns {node}*/
		function getPopup(){
			create();
			return popupDom;
		}

		/**@returns popup*/
		function html(data){
			$(getPopup()).find('#js_popupContent').html(data);
			return popup;
		}

		return {
			create: create,
			'get': getPopup,
			hide: hide,
			show: show,
			html: html
		}
	}();

	var ajaxLoad = function() {
		var ajaxLoadHtml = '' +
			'<div id="ajaxLoad">' +
			'<div class="ajaxBlock"></div>' +
			'<div class="ajaxRoller" id="ajaxRoller">' +
			'<img src="http://static.izhart.ru/img/roller.gif"></div>' +
			'<div class="ajaxPercent" id="ajaxPercent"></div>' +
			'<div class="ajaxWait">Подождите...</div>' +
			'</div>';
		var ajaxLoadDom;

		/**@returns ajaxLoad*/
		function create() {
			if (!ajaxLoadDom) {
				$('body').append(ajaxLoadHtml);
				ajaxLoadDom = g('ajaxLoad');
			}
			return ajaxLoad;
		}

		/**@returns ajaxLoad*/
		function hide() {
			getAjaxLoad().style.display = 'none';
			return ajaxLoad;
		}

		/**@returns ajaxLoad*/
		function show() {
			getAjaxLoad().style.display = 'block';
			return ajaxLoad;
		}

		/**@returns {node}*/
		function getAjaxLoad(){
			create();
			return ajaxLoadDom;
		}

		return {
			create: create,
			hide: hide,
			show: show
		}
	}();

//	var popupImage = function() {
//		var popupImageHTML = '' +
//			'<div class="popup" id="popup-image">' +
//			'<div class="close" onclick="popup_close(\'popup-image\');"><img src="http://static.izhart.ru/img/popup/icon-close.png"></div>' +
//			'<div id="popup_image_content"></div>' +
//			'</div>';
//		var popupImageDom;
//
//		function createPopupImage() {
//			if (!popupImageDom) {
//				$('body').append(popupImageHTML);
//				popupImageDom = g('popup-image');
//			}
//		}
//
//		function hidePopupImage() {
//			createPopupImage();
//			popupImageDom.style.display = 'none';
//		}
//
//		function showPopupImage() {
//			createPopupImage();
//			popupImageDom.style.display = 'block';
//		}
//
//		return {
//			create: createPopupImage,
//			hide: hidePopupImage,
//			show: showPopupImage
//		}
//	}();

//	var popupCalendar = function() {
//		var popupCalendarHTML = '' +
//			'<div class="popup" id="popup-calendar">' +
//			'<div class="calendar-close" onclick="popup_close(\'popup-calendar\');"><img src="http://static.izhart.ru/img/calendar-close.png"></div>' +
//			'<div id="popup_calendar_content"></div>' +
//			'</div>';
//		var popupCalendarDom;
//
//		function createPopupCalendar() {
//			if (!popupCalendarDom) {
//				$('body').append(popupCalendarHTML);
//				popupCalendarDom = g('popup-calendar');
//			}
//		}
//
//		function hidePopupCalendar() {
//			createPopupCalendar();
//			popupCalendarDom.style.display = 'none';
//		}
//
//		function showPopupCalendar() {
//			createPopupCalendar();
//			popupCalendarDom.style.display = 'block';
//		}
//
//		return {
//			create: createPopupCalendar,
//			hide: hidePopupCalendar,
//			show: showPopupCalendar
//		}
//	}();

	return {
		popup: popup,
		ajaxLoad: ajaxLoad,
//		popupImage: popupImage,
//		popupCalendar: popupCalendar,
//		windowScrollButton: windowScrollButton
	};
}();