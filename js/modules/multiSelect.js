multiSelect = function() {
	var defaultInputs = [];

	function getDivId(handlerId) {
		return  handlerId.substr(1) + handlerId.substr(0, 1);
	}

	/** Выполнится 1 раз при загрузке страницы
	 * @param handlerId ид ссылки
	 */
	function run(handlerId) {
		hide(handlerId);
		var divObj = $('#' + getDivId(handlerId));
		g(handlerId).onclick = function() {
			change(this);
		};
		divObj.on('change', '.multiSelect_checkAll', function(event) {
			var $inputs = divObj.find('input');
			for (var i = 0; i < $inputs.length; i++) {
				$inputs[i].checked = event.target.checked;
			}
		});
	}

	function change(handlerElem) {
		if ($('#' + getDivId(handlerElem.id) + ' input').length == 1) return;
		var divElem = g(getDivId(handlerElem.id));
		if (divElem.style.display != 'block') display(divElem);
		else hide(handlerElem.id);
	}

	function display(divElem) {
		defaultInputs = [];
		for (var i = 0, $inputs = $(divElem).find('input'); i < $inputs.length; i++) {
			defaultInputs[i] = $inputs.get(i).checked;
		}
		//Закрываем все открытые окна
		var $links = $('.multiselect-link');
		for (i = 0; i < $links.length; i++) {
			if (g(getDivId($links[i].id)).style.display != 'none')
				hide($links[i].id);
		}
		divElem.style.display = 'block';
	}

	function hide(handlerId) {
		var $divElem = $('#' + getDivId(handlerId));
		var handlerElem = g(handlerId);
		var str = '';
		var $inputs = $divElem.find('input:checked:not(.multiSelect_checkAll)');
		for (var i = 0; i < $inputs.length; i++) {
			str += $divElem.find("label[for='" + $inputs[i].id + "']").text() + ', ';
		}
		str = (str != '') ? str.substring(0, str.length - 2) : 'Не выбрано';
		if ($inputs.length <= 1) {
			handlerElem.innerHTML = str;
		} else if ($inputs.length == ($divElem.find(':checkbox:not(.multiSelect_checkAll), :radio:not(.multiSelect_checkAll)').length)) {
			handlerElem.innerHTML = 'Все';
		} else {
			handlerElem.innerHTML = 'Выбрано ' + $inputs.length;
		}
		handlerElem.setAttribute('title', str);
		$divElem.css('display', 'none');
		checkCheckboxAll($divElem);
	}

	function cancel(handlerId) {
		var $divElem = $('#' + getDivId(handlerId)), $inputs = $divElem.find('input');
		for (var i = 0; i < $inputs.length; i++) {
			$inputs.get(i).checked = defaultInputs[i];
		}
		$divElem.css('display', 'none');
	}

	/**Если хотя бы 1 не выделен - то снять выделение с чекбокса "выделить всё", иначе выделить его*/
	function checkCheckboxAll($divElem) {
		var $inputs = $divElem.find('input:checkbox, input:radio');
		for (var len = $inputs.length; --len;) {
			if (!$inputs[len].checked && !element.hasClass($inputs[len], 'multiSelect_checkAll')) {
				$inputs.filter('.multiSelect_checkAll').prop('checked', false);
				return;
			}
		}
		$inputs.filter('.multiSelect_checkAll').prop('checked', true);
	}

	return {
		hide: hide,
		cancel: cancel,
		run: run
	};
}();