payments = function() {

	function getFromForm(result, item) {
		result[item.name] = item.value;
		return result;
	}

	function fromBalance(url){
		ajax.requestInPopup(url, $('#js_typeForm').serialize());
	}

	function saveOrder(url, paymentType, idType, id) {
		var ykForm = g('js_ykForm'), $ykForm = $(ykForm), data = {};

		$ykForm.find('input[name=paymentType]').val(paymentType);

		data['yk'] = $ykForm.serializeArray().reduce(getFromForm, {});
		data['type'] = $('#js_typeForm').serializeArray().reduce(getFromForm, {});
		data['idType'] = idType;
		data['id'] = id;

		ajax.requestOptional({
			url: url,
			data: data,
			dataType: 'json',
			success: function(r) {
				if (r.errors) {
					g('js_ykErrors').innerHTML = r.errors;
				} else {

					if ('url' in r){
						ykForm.setAttribute('action', r[i]);
						delete r['url'];
					}

					for (var i in r) $ykForm.find('input[name=' + i + ']').val(r[i]);

					ykForm.submit();
				}
			}
		});
	}

	var priceParams = {};

	function refreshPriceMessage(){
		var select = g('vipPeriod');
		g('js_ykPriceMessage').innerHTML = priceParams['text'].replace(
			'{{period}}', $(select).children('option[value=' + select.value + ']').text()
		).replace(
			'{{price}}', priceParams['price'] * priceParams['rates'][select.value]
		);
	}

	function setPriceMessageParams(params){priceParams = params;}

	return {
		saveOrder: saveOrder,
		fromBalance: fromBalance,
		refreshPriceMessage: refreshPriceMessage,
		setPriceMessageParams: setPriceMessageParams
	};
}();

