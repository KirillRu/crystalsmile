(function(){

	scrollDiv = function(id, params){
		return new _sd(id, params);
	};

	function _sd(id, params){
		this.id = id;
		if (params) functions.mergeObjects(this.params, params);
	}

	_sd.prototype = {
		params: {
			countScroll: 820
		},
		startMarginLeft: null,
		contentWidth: 0,
		init: function(){
			this.$mainDiv = $('#' + this.id);
			this.$contentDiv = this.$mainDiv.children('.js_scrollDivContent');
			this.paddingRight = parseInt(this.$mainDiv.css('padding-right'), 10);

			this.repair();

			this.setEvents();
		},
		repair: function(){
			var $childrens = this.$contentDiv.children();
			if (this.startMarginLeft === null) this.startMarginLeft = this.startMarginLeft || parseInt($childrens.eq(1).css('margin-left'), 10);

			this.$mainDiv.css('width', '');
			this.shownWidth = this.$mainDiv.parent(':first').width() - this.paddingRight - parseInt(this.$mainDiv.css('padding-left'), 10);
			this.$mainDiv.css('width', this.shownWidth + 'px');

			$childrens.filter(':not(:first)').css('margin-left', this.startMarginLeft);
			var width = this.shownWidth - $childrens.eq(0).outerWidth(true);

			var blockOuterWidth = $childrens.eq(1).outerWidth(true);
			var countBlocks = Math.floor(width / blockOuterWidth);
			var marginLeft = Math.floor((width / countBlocks) - blockOuterWidth + this.startMarginLeft - 3);
			$childrens.filter(':not(:first)').css('margin-left', marginLeft);
			this.params.countScroll = this.shownWidth + marginLeft;

			this.contentWidth = 0;
			$childrens.each(function(i, elem){
				this.contentWidth += $(elem).outerWidth(true);
			}.bind(this));

			if (parseInt(this.$contentDiv.css('margin-left'), 10) < this.shownWidth - this.contentWidth + 3)
				this.$contentDiv.css('margin-left', this.shownWidth - this.contentWidth - this.paddingRight + 3);
		},
		setEvents: function(){
			this.$mainDiv.children('.js_scrollDivLeft').on('click', this.scrollLeft.bind(this));
			this.$mainDiv.children('.js_scrollDivRight').on('click', this.scrollRight.bind(this));
			$(window).on('resize', this.repair.bind(this));
		},
		scrollRight: function(){
			var nextMarginLeft = parseInt(this.$contentDiv.css('margin-left'), 10) - this.params.countScroll - 3;
			if (nextMarginLeft < this.shownWidth - this.contentWidth)
				nextMarginLeft = this.shownWidth - this.contentWidth - this.paddingRight + 3;
			this.$contentDiv.animate({marginLeft: nextMarginLeft}, 500);
		},
		scrollLeft: function(){
			var nextMarginLeft = parseInt(this.$contentDiv.css('margin-left'), 10) + this.params.countScroll + 3;
			if (nextMarginLeft > 0) nextMarginLeft = 0;
			this.$contentDiv.animate({marginLeft: nextMarginLeft}, 500);
		},
	};
}());