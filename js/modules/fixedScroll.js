(function(){
	fixedScroll = function(){
		return new _fs();
	};

	var _fs = function(){
		this.onScroll = function(){
			var scroll = functions.getScrollTop();
			if ((scroll + this.blockHeight + this.offsetTop) >= this.offsetTopStop){
				this.positionBottom();
			} else if (scroll >= (this.offsetTopDisplay - this.offsetTop)){
				this.positionCenter();
			} else {
				this.positionTop();
			}
		}.bind(this);
	};
	_fs.prototype = {
		positionTop: function(){
			var position = this.settings.block.style.position;
			if (position == 'fixed' || position == 'absolute'){
				this.settings.block.style.position = '';
				this.settings.block.style.top = '';
				this.settings.block.style.width = '';
			}
		},
		positionCenter: function(){
			var position = this.settings.block.style.position;
			if (parseInt(this.settings.block.style.top, 10) != this.offsetTop){
				if (position == 'absolute' || position == 'fixed'){
					$(this.settings.block).css({
						position: 'fixed',
						top: this.offsetTop
					});
				} else {
					$(this.settings.block).hide().css({
						position: 'fixed',
						top: this.offsetTop,
						width: 250
					}).fadeIn(350);
				}
			}
		},
		positionBottom: function(){
			if (parseInt(this.settings.block.style.top, 10) != (this.offsetTopStop - this.blockHeight)){
				$(this.settings.block).css({
					position: 'absolute',
					top: this.offsetTopStop - this.blockHeight,
					width: 250
				});
			}
		},
		init: function(settings){
			this.settings = {
				contentBlock: null,
				block: null,
				minOffset: 5,
			};
			functions.mergeObjects(this.settings, settings);
			this.setConsts();
			this.setOffsetTop(0);
			if (this.offsetTopDisplay + this.blockHeight + this.offsetTop < this.offsetTopStop){
				this.setEvents().onScroll();
			}
		},
		setConsts: function(){
			var $contentBlock = $(this.settings.contentBlock);
			this.offsetTopDisplay = Array.prototype.reduce.call($(this.settings.contentBlock).children(), function(previousValue, currentValue){
				return $(currentValue).outerHeight(true) + previousValue;
			}, 0);
			this.offsetTopDisplay += element.getOffsetTop(this.settings.contentBlock);
			this.offsetTopStop = element.getOffsetTop(this.settings.contentBlock) + $contentBlock.height();
			this.offsetTopStop += parseInt($contentBlock.css('border-top') || 0, 10) + parseInt($contentBlock.css('margin-top') || 0, 10) + parseInt($contentBlock.css('padding-top') || 0, 10);
			this.blockHeight = $(this.settings.block).outerHeight(true);
			return this;
		},
		setOffsetTop: function(height){
			this.offsetTop = height + this.settings.minOffset;
			return this;
		},
		onRelativeResize: function(){
			this.setOffsetTop(portal.getRelativeTopHeight());
			this.onScroll();
		},
		setEvents: function(){
			$(window).on('scroll', this.onScroll);
			mediator.subscribe('onRelativeTopHide', this.onRelativeResize, this)
					.subscribe('onRelativeTopShow', this.onRelativeResize, this);
			return this;
		},
		remove: function(){
			$(window).off('scroll', this.onScroll);
			mediator.unsubscribe('onRelativeTopHide', this.onRelativeResize)
					.unsubscribe('onRelativeTopShow', this.onRelativeResize);
		}
	};
}());