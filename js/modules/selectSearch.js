selectSearch = function() {
	var consts = {}, $body = $('body');

	function run(id) {
		if (consts.id && (consts.id != id)) closeWindow();
		setConst(id);
		setEvents();
		if (windowIsOpened()) closeWindow();
		else openWindow();
	}

	function openWindow() {
		consts.mainWindow.style.display = 'block';
		consts.textInput.focus();
		slide(consts.$currentItem.length && consts.$currentItem.position().top);
		$body.on('click', ':not(#' + consts.mainWindow.id + ')', function(event) {
			if (event.target.id != consts.textElement.id && !$(event.target).closest('#' + consts.mainWindow.id).length) closeWindow();
		});
	}

	function clear(id) {
		g('js_SSText_' + id).innerHTML = 'Выбрать...';
		g(id).value = 0;
		g('js_SSButtonClear_' + id).style.display = 'none';
		if (id == consts.id) closeWindow();
	}

	function closeWindow() {
		consts.mainWindow.style.display = 'none';
		$body.off('click', ':not(#' + consts.mainWindow.id + ')');
	}

	function windowIsOpened() {return (consts.mainWindow.style.display == 'block');}

	function setConst(id) {
		consts = {};
		consts.id = id;
		consts.mainWindow = g('js_SSWindow_' + consts.id);
		consts.formInput = g(consts.id);
		consts.textInput = g('js_SSTextInput_' + consts.id);
		consts.textNoResults = g('js_SSNoResults_' + consts.id);
		consts.textElement = g('js_SSText_' + consts.id);
		consts.buttonClear = g('js_SSButtonClear_' + consts.id);
		consts.slideWindow = g('js_SSSlideWindow_' + consts.id);
		consts.slideWindowHeight = $(consts.slideWindow).innerHeight();
		consts.$list = $('#js_SSList_' + consts.id).find('.js_SSValue');
		consts.$currentItem = consts.$list.filter('.SS-selected').eq(0);
	}

	function setEvents() {
		var lastLength = consts.textInput.value.length;
		consts.textInput.onkeyup = function(event) {
			consts.$list.on('mouseenter', function() {//мышка мешает перемотке при зажатой кливише
				selectItem(getActiveItems().index(this));
			});
			switch (functions.getCharCode(event)) {
				case 13:
					saveValue();
					return false;
					break;
				case 38:
				case 40:
					break;
				default:
					if (lastLength != this.value.length) {
						refreshList(this.value);
						lastLength = this.value.length;
					}
					break;
			}
			return true;
		};
		consts.textInput.onkeydown = function(event) {
			consts.$list.off('mouseenter');//мышка мешает перемотке при зажатой кливише
			switch (functions.getCharCode(event)) {
				case 13:
					return false;
					break;
				case 38:
					moveItem('up');
					break;
				case 40:
					moveItem('down');
					break;
			}
			return true;
		};

		consts.$list.off('click').on('click', function() {
			saveValue(this.getAttribute('value'), this.innerHTML);
		});
		consts.$list.off('mouseenter').on('mouseenter', function() {
			selectItem(getActiveItems().index(this));
		});
	}

	function moveItem(direction) {
		var items = getActiveItems(), count = items.length, currIndex = items.index(consts.$currentItem), offset;
		if (!count) return;
		switch (direction) {
			case 'up':
				selectItem((currIndex == 0) ? (count - 1) : (currIndex - 1));
				offset = consts.$currentItem.position().top;
				if (consts.slideWindow.scrollTop > offset || consts.slideWindow.scrollTop + consts.slideWindowHeight - consts.$currentItem.outerHeight() < offset) slide(offset);
				break;
			case 'down':
				selectItem((currIndex == (count - 1)) ? 0 : (currIndex + 1));
				offset = consts.$currentItem.position().top + consts.$currentItem.outerHeight() - consts.slideWindowHeight;
				if (consts.slideWindow.scrollTop < offset || offset - consts.$currentItem.outerHeight() + consts.slideWindowHeight < consts.slideWindow.scrollTop) slide(offset);
				break;
		}
	}

	function slide(offset) {
		consts.slideWindow.scrollTop = offset;
	}

	function refreshList(namePart) {
		var item;
		namePart = namePart.toLowerCase();
		for (var i = 0, len = consts.$list.length; i < len; i++) {
			item = consts.$list.get(i);
			if (item.innerHTML.toLowerCase().indexOf(namePart) == -1) item.style.display = 'none';
			else item.style.display = '';
		}
		if (getActiveItems().length == 0) element.removeClass(consts.textNoResults, 'none');
		else {
			$(consts.textNoResults).addClass('none');
		}
		selectItem(0);
		slide(0);
	}

	function selectItem(index) {
		consts.$currentItem.removeClass('SS-selected');
		consts.$currentItem = getActiveItems().eq(index).addClass('SS-selected');
	}

	function saveValue(value, name) {
		if (typeof(value) == 'undefined') {
			var first = consts.$currentItem.get(0);
			value = first && first.getAttribute('value') || 0;
			name = first && first.innerHTML || '';
		}
		if (!name) {
			name = 'Выбрать...';
			consts.buttonClear.style.display = 'none';
		} else consts.buttonClear.style.display = 'block';
		consts.textElement.innerHTML = name;
		consts.formInput.value = value;
		closeWindow();
	}

	function getActiveItems() {return consts.$list.filter(function() {return (this.style.display != 'none');})}

	return {
		run: run,
		clear: clear
	};
}();