(function(){
	relativeLeft = function(){
		return new _rl();
	};

	var _rl = function(){};
	_rl.prototype = {
		onScroll: function(){
			if (functions.getScrollTop() <= this.getOffsetTopHide()){
				this.positionTop();
			} else {
				this.positionBottom();
			}
		},
		positionTop: function(){
			if (this.settings.blockHide.style.display === 'none'){
				$(this.settings.blockHide).show();
			}
		},
		positionBottom: function(){
			if (this.settings.blockHide.style.display !== 'none'){
				this.settings.blockHide.style.display = 'none';
			}
		},
		init: function(settings){
			this.settings = {
				blockHide: null,
				minOffset: 20,
			};
			functions.mergeObjects(this.settings, settings);
			this.setConsts();
			if (this.check()) this.setEvents().onScroll();
		},
		setConsts: function(){
			this.blockHideHeight = Array.prototype.reduce.call($(this.settings.blockHide).children(), function(previousValue, currentValue){
				return $(currentValue).outerHeight(true) + previousValue;
			}, 0);
			this.offsetTopHide = this.blockHideHeight + element.getOffsetTop(this.settings.blockHide);

			this.setOffsetTop(0);
			return this;
		},
		check: function(){
			var parentHeight = $(this.settings.blockHide).parent().height();
			return parentHeight > this.blockHideHeight + $(window).height();
		},
		getOffsetTopHide: function(){
			return this.offsetTopHide + this.offsetTop;
		},
		setOffsetTop: function(height){
			this.offsetTop = height + this.settings.minOffset;
			return this;
		},
		onRelativeResize: function(){
			this.setOffsetTop(portal.getRelativeTopHeight());
			this.onScroll();
		},
		setEvents: function(){
			$(window).on('scroll.relativeLeft', this.onScroll.bind(this));
			mediator.subscribe('onRelativeTopHide', this.onRelativeResize, this)
					.subscribe('onRelativeTopShow', this.onRelativeResize, this);
			return this;
		},
		remove: function(){
			$(window).off('scroll.relativeLeft');
			mediator.unsubscribe('onRelativeTopHide', this.onRelativeResize)
					.unsubscribe('onRelativeTopShow', this.onRelativeResize);
		}
	};
}());