(function() {
	multiString = function(options) {
		return new _MultiString(options);
	};

	function _MultiString() {}
	_MultiString.prototype = {
		init: function(options){
			this.setConst(options);
			this.setEvents();
		},
		appendRow: function() {
			if (this.currentCount < this.maxCount) {
                for (var key in this.wordpanel) CKEDITOR.instances[key].destroy(true); //убираем html-редактор

				var $row = this.$container.children('.js_msRow:first').clone().css('display', 'none');
				var globalCount = ++this.globalCount;
				var postfixId = this.postfixId;
				var row = $row.get(0).outerHTML;
				$row.find('[id]').each(function(id, elem) {
					row = row.replace(new RegExp("'" + elem.id + "'",'g'), "'" + elem.id + postfixId + globalCount + "'");
					row = row.replace(new RegExp('"' + elem.id + '"','g'), '"' + elem.id + postfixId + globalCount + '"');
				});
				$row = $(row);

				$row.children('.js_msButtonsContainer').empty().append(this.getButtonDelete());
				$row.find('input:text').val('');
				$row.find('input:checkbox').prop('checked', false);
				$row.find('select option:selected').removeAttr('selected');
				$row.insertAfter(this.$container.children('.js_msRow:last')).slideDown(160);

                for (var key in this.wordpanel) {
                    CKEDITOR.replace(key, this.wordpanel[key]);
                    if (CKEDITOR.instances[key + postfixId + globalCount]) CKEDITOR.instances[key + postfixId + globalCount].destroy(true);
                    CKEDITOR.replace(key + postfixId + globalCount, this.wordpanel[key]);
                }//html-редактор для нового поля
                for (var key in this.colorpicker) $('#' + key + postfixId + globalCount).ColorPicker({
                            onSubmit: function(hsb, hex, rgb) {
                                $('#' + key + postfixId + globalCount).val(hex);
                            }
                        });
				if ((++this.currentCount) == this.maxCount) this.buttonAdd.style.display = 'none';
			}
		},
		deleteRow: function(buttonDelete) {
			$(buttonDelete).closest('.js_msRow').slideUp(160, function(){element.remove(this)});
			this.currentCount--;
			if ((this.currentCount + 1) == this.maxCount) this.buttonAdd.style.display = '';
		},
		goUp: function(buttonPosition) {
			var $row = $(buttonPosition).closest('.js_msRow');
			var upRow = element.previousElement($row.get(0));
			if (upRow) $(upRow).before($row);
		},
		goDown: function(buttonPosition) {
			var $row = $(buttonPosition).closest('.js_msRow');
			var downRow = element.nextElement($row.get(0));
			if (downRow) $(downRow).after($row);
		},
		goFirst: function(buttonPosition) {
			var $row = $(buttonPosition).closest('.js_msRow');
			$row.prependTo(this.$container);
		},
		goLast: function(buttonPosition) {
			var $row = $(buttonPosition).closest('.js_msRow');
			$row.appendTo(this.$container);
		},
		getButtonDelete: function() {
			var button = document.createElement('img');
			button.className = 'js_msButtonDelete ci-delete';
			button.src = '/img/multistring-delete.png';
			return button;
		},
		setConst: function(options) {
			this.$container = $('#' + options.id);
            this.wordpanel = options.wordpanel || [];
            this.colorpicker = options.colorpicker || [];
            this.postfixId = options.postfixId || '';
			this.maxCount = options.maxCount || 5;
			this.currentCount = this.$container.children('.js_msRow').length;
			this.globalCount = this.currentCount;
			this.buttonAdd = this.$container.find('.js_msButtonAdd:first').get(0);
			if (this.currentCount == this.maxCount) this.buttonAdd.style.display = 'none';
		},
		setEvents: function() {
			var self = this;
			this.buttonAdd.onclick = function() { self.appendRow(); };
			this.$container.on('click', '.js_msButtonDelete', function(){ self.deleteRow(this); });
			this.$container.on('click', '.js_msButtonUp', function(){ self.goUp(this); });
			this.$container.on('click', '.js_msButtonDown', function(){ self.goDown(this); });
			this.$container.on('click', '.js_msButtonFirst', function(){ self.goFirst(this); });
			this.$container.on('click', '.js_msButtonLast', function(){ self.goLast(this); });
		}
	}
}());