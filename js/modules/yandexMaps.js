(function(ymaps) {

	yandexMaps_selectAddress = function(id, params) {
		return new ym_sa(id, params);
	};

	var maps = {};
	yandexMaps_viewAddress = function(id, params) {
		if (!maps[id]) maps[id] = new ym_va(id, params);

		return maps[id];
	};

	function ym_va(id, params) {
		this.id = id;
		if (!params.firmInfo) params.firmInfo = {};
		if (!params.smallMapSize) params.smallMapSize = {};
		this.params = params;
		this.ym = {
			coordinates: null,
			map: null,
			placemark: null
		};
		this.ym.balloonContent = '<div>';
		if (this.params.firmInfo.img) this.ym.balloonContent += '<img src="' + this.params.firmInfo.img + '" />';
		if (this.params.firmInfo.name) this.ym.balloonContent += '<div><b>' + this.params.firmInfo.name + '</b></div>';

		var address = typeof params.firmInfo.addressView !== 'undefined' ? params.firmInfo.addressView : params.firmInfo.address;
		if (address) address = functions.trim(address);
		if (address) this.ym.balloonContent += '<div>' + address + '</div>';

		this.ym.balloonContent += '</div>';
	}

	ym_va.prototype = {
		init: function() {
			ymaps.ready()
				.then(this.prepareYmParams, null, null, this)
				.then(this.startMap, null, null, this);
		},
		startMap: function() {
			if (this.ym.coordinates){
				var map = g(this.id);
				var ymParams = [
					'll=' + this.ym.coordinates[1] + ',' + this.ym.coordinates[0],
					'z=15',
					'size=' + (this.params.smallMapSize.x || '200') + ',' + (this.params.smallMapSize.y ||'200'),
					'l=map',
					'pt=' + this.ym.coordinates[1] + ',' + this.ym.coordinates[0] + ',pm2blm'
				];
				map.innerHTML = '<img src="http://static-maps.yandex.ru/1.x/?' + ymParams.join('&') + '"/>';
				map.onclick = this.openMap.bind(this);
			} else {
				g(this.id).parentNode.style.display = 'none';
			}
		},
		openMap: function(){
			popupManager.popup.html('<div id="js_popupYM" style="height: 450px; width: 800px;"></div>').show();
			this.ym.map = new ymaps.Map('js_popupYM', {center: this.ym.coordinates, zoom: 16});
			this.ym.map.controls.add(new ymaps.control.ZoomControl());
			this.movePlacemark(this.ym.coordinates);
		},
		/**Получаем координаты из параметров. Если есть адрес, то ищем по этому адресу координаты*/
		prepareYmParams: function() {
			if (this.params.firmInfo.coordinates) {
				this.ym.coordinates = this.params.firmInfo.coordinates;
			} else if (this.params.firmInfo.address) {
				return ymaps.geocode(this.params.firmInfo.address).then(
					function(res) {
						var obj = res.geoObjects.get(0);
						if (obj) this.ym.coordinates = obj.geometry.getCoordinates();
					},null, null, this
				);
			}
		},
		movePlacemark: function(coords) {
			if (this.ym.placemark) this.ym.map.geoObjects.remove(this.ym.placemark);
			this.ym.placemark = new ymaps.Placemark(coords, {balloonContent: this.ym.balloonContent});
			this.ym.map.geoObjects.add(this.ym.placemark);
			this.ym.placemark.balloon.open();
		},
	};

	function ym_sa(id, params) {
		this.id = id;
		if (!params.firmInfo) params.firmInfo = {};
		this.params = params;
		this.ym = {
			coordinates: null,
			map: null,
			placemark: null
		};
		this.ym.balloonContent = '<div>';
		if (this.params.firmInfo.img) this.ym.balloonContent += '<img src="' + this.params.firmInfo.img + '" />';
		if (this.params.firmInfo.name) this.ym.balloonContent += '<div><b>' + this.params.firmInfo.name + '</b></div>';
		this.ym.balloonContent += '</div>';
	}
	ym_sa.prototype = {
		run: function() {
			ymaps.ready()
				.then(this.prepareYmParams, null, null, this)
				.then(this.startMap, null, null, this);
		},
		startMap: function() {
			this.ym.map = new ymaps.Map('map_content_' + this.id, {center: this.ym.coordinates || [55.76, 37.64], zoom: 10});
			this.ym.map.options.set('dragCursor', 'pointer');
			this.ym.map.controls.add(new ymaps.control.ZoomControl());
			var sc = new ymaps.control.SearchControl({options: {noPlacemark: true}});
			this.ym.map.controls.add(sc);
			if (this.params.dragPoint){
				sc.events.add('resultshow', function(e) {
					var self = this;
					/**Тайм аут нужен, потому что он не сразу перемещает карту после поиска*/
					setTimeout(function() {self.onAfterSearch(e);}, 100);
				}, this);
				this.ym.map.events.add('click', this.onMapClick, this);
			}
			if (this.ym.coordinates) this.movePlacemark(this.ym.coordinates);
		},
		/**Получаем координаты из параметров. Если есть адрес, то ищем по этому адресу координаты*/
		prepareYmParams: function() {
			if (this.params.firmInfo.coordinates) {
				this.ym.coordinates = this.params.firmInfo.coordinates;
			} else if (this.params.firmInfo.address) {
				return ymaps.geocode(this.params.firmInfo.address).then(
					function(res) {
						var obj = res.geoObjects.get(0);
						if (obj) this.ym.coordinates = obj.geometry.getCoordinates();
					},null, null, this
				);
			}
		},
		onAfterSearch: function(e) {
			this.movePlacemark(e.get('target').getResultsArray()[e.get('index')].geometry.getCoordinates());
			element.setValue(this.id, this.getPlacemarkCoordinates().join(','));
		},
		onMapClick: function(e) {
			this.movePlacemark(e.get('coords'));
			element.setValue(this.id, e.get('coords').join(','));
			ymaps.geocode(e.get('coords')).then(function(res){
				element.setValue(this.params.idTextAddress, res.geoObjects.get(0).properties.get('text'));
			}, null, null, this);
		},
		movePlacemark: function(coords) {
			if (this.ym.placemark) this.ym.map.geoObjects.remove(this.ym.placemark);
			this.ym.placemark = new ymaps.Placemark(coords, {balloonContent: this.ym.balloonContent});
			this.ym.map.geoObjects.add(this.ym.placemark);
			this.ym.placemark.balloon.open();
		},
		getPlacemarkCoordinates: function() {return this.ym.placemark.geometry.getCoordinates();}
	};


}(ymaps));