relativeTop = function() {
	var hideMenu, staticElements = [], offsetStaticElements = [], dynamicElements = [], paramsDynamicElements = {},
		displayDynamicElementsKeys, hideDynamicElementsKeys, displayedDynamicElements = {};

	function run() {
		hideMenu = cookie.get('hideMenu') || 0;
		staticElements = [
			g('js_topRegion'),
			g('js_topSearch'),
			g('js_leftAddFirm')
		];
		offsetStaticElements = [
			element.getOffsetTop(staticElements[0]),
			element.getOffsetTop(staticElements[1]) - 25,//25 - ����� � �������
			element.getOffsetTop(staticElements[2]) - 20
		];
		dynamicElements = [
			g('js_ttBlockTop'),
			g('js_ttArrow'),
			g('js_ttAuth') || g('js_ttIcons'),
			g('js_ttSearch'),
			g('js_ttButtonAdd'),
			g('js_ttBlockBottom')
		];

		dynamicElements.forEach(function(elem) {
			var $elem = $(elem);
			paramsDynamicElements[elem.id] = {
				'paddingTop': $elem.css('padding-top'),
				'paddingBottom': $elem.css('padding-bottom'),
				'marginTop': $elem.css('margin-top'),
				'height': $elem.height()
			};
			displayedDynamicElements[elem.id] = true;
		});

		function prepareToScroll() {
			if (hideMenu) {
				g('js_ttArrow').className = 'tt-arrow tt-expand';
				hideArray(dynamicElements.slice(2, 6));
				displayDynamicElementsKeys = [
					[],
					[0],
					[0, 1],
					[0, 1]
				]; //��� ���� 2 �������� �������� � ���������  0,1,5
				hideDynamicElementsKeys = [
					[0, 1],
					[1],
					[],
					[]
				];
			} else {
				g('js_ttArrow').className = 'tt-arrow tt-collapse';
				displayDynamicElementsKeys = [
					[],
					[0],
					[0, 1, 2, 3, 5],
					[0, 1, 2, 3, 4, 5]
				];
				hideDynamicElementsKeys = [
					[0, 1, 2, 3, 4, 5],
					[1, 2, 3, 4, 5],
					[4],
					[]
				]; //��� ���� 2 ������ �������� � ���������  4
			}
		}

		g('js_ttArrow').onclick = function(event) {
			hideMenu = !hideMenu;
			prepareToScroll();
			if (hideMenu) {
				cookie.set('hideMenu', 1);
			} else {
				cookie.unset('hideMenu');
				onScroll();
			}
		};
		$(window).on('scroll', onScroll);
		prepareToScroll();
		onScroll();
		g('js_ttBlock').style.display = 'block';
	}

	function onScroll() {
		hideArray();
		displayArray();
	}

	function getHideArray() {
		return hideDynamicElementsKeys[getStep()].map(function(elem) {
			return dynamicElements[elem];
		});
	}

	function getDisplayArray() {
		return displayDynamicElementsKeys[getStep()].map(function(elem) {
			return dynamicElements[elem];
		});
	}

	function hideArray(hideArr) {
		hideArr = hideArr || getHideArray();
		hideArr.forEach(function(elem) {
			if (displayedDynamicElements[elem.id]) {
				$(elem).stop(true).animate({marginTop: 0, paddingTop: 0, paddingBottom: 0, height: 0}, 100);
				displayedDynamicElements[elem.id] = false;
			}
		});
		afterHide();
	}

	function afterHide() {

	}

	function displayArray() {
		getDisplayArray().forEach(function(elem) {
			var id = elem.id;
			if (!displayedDynamicElements[id]) {
				$(elem).stop(true).animate({marginTop: paramsDynamicElements[id].marginTop, paddingTop: paramsDynamicElements[id].paddingTop, paddingBottom: paramsDynamicElements[id].paddingBottom, height: paramsDynamicElements[id].height}, 100);
				displayedDynamicElements[id] = true;
			}
		});
		afterDisplay();
	};

	function afterDisplay() {

	}

	function getStep() {
		//offsetStaticElements: 0 - ������� ����  2 - �������� �������� 1 - �����
		var topScroll = functions.getScrollTop();
		if (topScroll <= offsetStaticElements[0]) return 0;
		if (topScroll > offsetStaticElements[0] && topScroll <= offsetStaticElements[1]) return 1;
		if (topScroll > offsetStaticElements[1] && topScroll <= offsetStaticElements[2]) return 2;
		return 3;
	}

	return {
		run: run
	};
}();

(function() {
	function searchRelativeTop() {};
	searchRelativeTop.prototype = search.parent = search;
	search = new searchRelativeTop();
}());
functions.mergeObjects(search, {
	inputAutocompleteMirror: null,
	inputMirror: null,
	fillInput: function(text) {
		this.parent.fillInput.call(this, text);
		this.inputMirror.val(text);
	},
	fillInputMirror: function(text) {
		this.inputMirror.val(text).focus();
		this.inputAutocompleteMirror.onValueChange();
		this.input.val(text);
	},
	onDomReady: function() {
		var self = this;
		this.parent.onDomReady.call(this);
		this.inputMirror = $('#js_searchInputMirror');
		this.inputAutocompleteMirror = this.inputMirror.autocomplete({
			serviceUrl: '/runModule/LiveSearch.ajax',
			width: 549.89,
			onSelect: function(val, data, el) {
				el.val(val);
			}
		});
		$('#js_searchVariantMirror').on('click', function() {self.fillInputMirror(this.getAttribute('title'));});
		this.inputMirror.on('keyup', function() {
			self.input.val(this.value);
		});
		this.input.on('keyup', function() {
			self.inputMirror.val(this.value);
		});
		$(window).on('scroll', this.onScroll.bind(this));
	},
	onScroll: function(){
		this.inputAutocompleteMirror.container.css('display','none');
		this.inputAutocompleteMirror.fixPosition();
		this.inputAutocomplete.container.css('display','none');
	}
});

$(relativeTop.run);