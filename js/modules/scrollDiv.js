(function(){

	scrollDiv = function(id, params){
		return new _sd(id, params);
	};

	function _sd(id, params){
		this.id = id;
		this.params = {
			minMarginLeft: 10
		};
		if (params) for(var i in this.params) if (params.hasOwnProperty(i)) this.params[i] = params[i];
	}

	var scrollDivs = {};
	function addNewSd(id, obj){
		scrollDivs[id] = obj;
	}

	$(window).on('resize', function(){
		for (var i in scrollDivs) {
			scrollDivs[i].cleanWidth();
		}
		for (i in scrollDivs) {
			scrollDivs[i].onResize();
		}
	});

	_sd.prototype = {
		countVisibleBlocks: 0,
		currentBlockNo: 0,
		currentMarginLeft: 0,
		$blocks: null,
		blockSize: 0,
		firstBlockSize: 0,
		init: function(){
			this.$mainDiv = $('#' + this.id);
			this.$contentDiv = this.$mainDiv.children('.js_scrollDivContent');
			this.$blocks = this.$contentDiv.children();
			this.firstBlockSize = this.$blocks.eq(0).outerWidth(true);


			this.setShownWidth();
			this.setBlockSize();
			this.setButtonsStyles();
			this.setEvents();
			addNewSd(this.id, this);
		},
		setBlockSize: function(){
			var blockSize = this.$blocks.eq(1).outerWidth();

			var countShownBlocks = Math.floor(this.shownWidth / (blockSize + this.params.minMarginLeft));
			if (countShownBlocks > 1){
				this.currentMarginLeft = Math.floor(((this.shownWidth - this.firstBlockSize) / (countShownBlocks - 1)) - blockSize - 3);
				this.$blocks.filter(':first').css('margin-left', 0);
			} else {
				this.currentMarginLeft = this.shownWidth - this.firstBlockSize;
				this.$blocks.filter(':first').css('margin-left', Math.floor(this.currentMarginLeft / 2));
			}
			this.$blocks.filter(':not(:first)').css('margin-left', this.currentMarginLeft);

			this.blockSize = this.$blocks.eq(1).outerWidth(true);
			this.countVisibleBlocks = countShownBlocks;
		},
		cleanWidth: function(){
			this.$mainDiv.css('width', '');
		},
		setShownWidth: function(){
			this.shownWidth = this.$mainDiv.parent(':first').width() - parseInt(this.$mainDiv.css('padding-right'), 10) - parseInt(this.$mainDiv.css('padding-left'), 10);
			this.$mainDiv.css('width', this.shownWidth);
		},
		onResize: function(){
			this.setShownWidth();
			this.setBlockSize();
			if (this.currentBlockNo) {
				this.$contentDiv.css('margin-left', this.getNextMarginLeft(this.currentBlockNo));
				this.setButtonsStyles();
			}
		},
		setEvents: function(){
			var self = this;
			this.$mainDiv.children('.js_scrollDivLeft').on('click', function(){
				if (!$(this).hasClass('das-prior-noact')) self.scrollLeft();
			});
			this.$mainDiv.children('.js_scrollDivRight').on('click', function(){
				if (!$(this).hasClass('das-next-noact')) self.scrollRight();
			});
		},
		scrollRight: function(){
			this.scrollTo(this.getNextMarginLeft(this.currentBlockNo + this.getCountScroll()));
		},
		scrollLeft: function(){
			this.scrollTo(this.getNextMarginLeft(this.currentBlockNo - this.getCountScroll()));
		},
		getCountScroll: function(){
			return this.countVisibleBlocks;
		},
		getNextMarginLeft: function(blockNo){
			var marginLeft = 0;
			if (blockNo > 0){
				blockNo = Math.min(blockNo, this.$blocks.length - this.countVisibleBlocks);
				marginLeft = this.firstBlockSize + (this.blockSize + 3) * (blockNo - 1) + this.currentMarginLeft + 3;
				this.currentBlockNo = blockNo;
			} else {
				marginLeft = this.currentBlockNo = 0;
			}
			return -marginLeft;
		},
		setButtonsStyles: function(){
			if (this.$blocks.length <= this.countVisibleBlocks){
				this.$mainDiv.children('.js_scrollDivLeft').removeClass('das-prior').addClass('das-prior-noact');
				this.$mainDiv.children('.js_scrollDivRight').removeClass('das-next').addClass('das-next-noact');
			} else if (this.currentBlockNo == 0){
				this.$mainDiv.children('.js_scrollDivRight').removeClass('das-next-noact').addClass('das-next');
				this.$mainDiv.children('.js_scrollDivLeft').removeClass('das-prior').addClass('das-prior-noact');
			} else if (this.currentBlockNo == this.$blocks.length - this.countVisibleBlocks){
				this.$mainDiv.children('.js_scrollDivLeft').removeClass('das-prior-noact').addClass('das-prior');
				this.$mainDiv.children('.js_scrollDivRight').removeClass('das-next').addClass('das-next-noact');
			} else {
				this.$mainDiv.children('.js_scrollDivLeft').removeClass('das-prior-noact').addClass('das-prior');
				this.$mainDiv.children('.js_scrollDivRight').removeClass('das-next-noact').addClass('das-next');
			}
		},
		scrollTo: function(marginLeft){
			var scrollSpeed = Math.abs(parseInt(this.$contentDiv.css('margin-left'), 10) - marginLeft);
			scrollSpeed = Math.floor(scrollSpeed * 0.8);
			this.$contentDiv.animate({marginLeft: marginLeft}, scrollSpeed);
			this.setButtonsStyles();
		}
	};
}());