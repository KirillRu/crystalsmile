mainCalendar = {

	monthObj: new Array(),
	inputDateStart: null,
	inputDateEnd: null,
	sliderObj: null,
	buttonSave: null,
	buttonClose: null,
	settings: {
		id_start: '', //inputы который находится в форме
		id_end: '',
		startYear: 2008, //setSettings()
		countMonth: 4,
		minMonth: 0,
		maxMonth: 68,
		className: '',
		target: '' //DOM element ссылки(<a>)
	},

	display: function(options, target) {
		var before = this.clone(this.settings);
		this.setSettings(options, target);
		if (this.checkNew(before)) {
			$('#mainCalendar_mainWindow').remove(); //2 календаря на 1 странице
		}
		if (!document.getElementById('mainCalendar_mainWindow')) {
			this.createMainWindow();
			this.setEvents();
		}
		this.setCSSCalendar();
		this.startSlider();
		this.onCreate();
	},

	setCSSCalendar: function() {
		var calendar = $('#mainCalendar_mainWindow'),
			left = $(this.settings.target).offset().left,
			calendarWidth = calendar.outerWidth(),
			windowWidth = $(window).outerWidth();
		calendar.css('top', $(this.settings.target).offset().top + 15);
		if (windowWidth < calendarWidth + left) left = windowWidth - calendarWidth;
		calendar.css('left', left);
		calendar.css('display', 'block');
	},

	checkNew: function(before) {
		return (
			before.startYear != this.settings.startYear ||
				before.maxMonth != this.settings.maxMonth ||
				before.countMonth != this.settings.countMonth ||
				before.className != this.settings.className);
	},

	clone: function(o) {
		if (!o || typeof(o) !== "object") return o;
		var c = (typeof(o.pop) === "function" ? [] : {});
		var p, v;
		for (p in o) {
			if (o.hasOwnProperty(p)) {
				c[p] = o[p];
			}
		}
		return c;
	},

	setSettings: function(options, target) {
		this.settings = {
			id_start: '', //inputы который находится в форме
			id_end: '',
			startYear: function() {return (new Date()).getFullYear() - 5;}(), //setSettings()
			countMonth: 4,
			minMonth: 0,
			maxMonth: 68,
			className: '',
			target: target //DOM element ссылки(<a>)
		};
		for (var key in options) if (options.hasOwnProperty(key)) this.settings[key] = options[key];
	},

	setEvents: function() {
		this.inputDateStart.add(mainCalendar.inputDateEnd).keyup(function(event) {
			mainCalendar.onInputChange.call(mainCalendar, event);
		});
		var body = $('body');
		body.on('click', ':not(#mainCalendar_mainWindow)', function(event) {
			if (!$(event.target).closest('#mainCalendar_mainWindow').length && (event.target != mainCalendar.settings.target)) mainCalendar.closeCalendar();
		});
		var monthTr = $('#mainCalendar_month_tr');
		monthTr.on('click', '.mainCalendar_weekday, .mainCalendar_holiday', function(event) {
			mainCalendar.onDayClick(event);
			event.stopPropagation();
		});
		monthTr.on('click', '.mainCalendar_week', function(event) {
			mainCalendar.onWeekClick(event);
			event.stopPropagation();
		});
		monthTr.on('click', '.mainCalendar_month', function(event) {
			mainCalendar.onMonthClick(event);
			event.stopPropagation();
		});
		this.inputDateStart.on('click', function(event) {
			mainCalendar.inputDateStartClick();
			event.stopPropagation();
		});
		this.inputDateEnd.on('click', function(event) {
			mainCalendar.inputDateEndClick();
			event.stopPropagation();
		});
		this.buttonSave.on('click', function(event) {
			mainCalendar.save();
			event.stopPropagation();
		});
		this.buttonClose.on('click', function(event) {
			mainCalendar.closeCalendar();
			event.stopPropagation();
		});
	},

	inputDateStartClick: function() {
		this.inputDateEnd.attr('class', 'mainCalendar_input');
		this.inputDateStart.attr('class', 'mainCalendar_input mainCalendar_input_date_selected');
	},

	inputDateEndClick: function() {
		this.inputDateStart.attr('class', 'mainCalendar_input');
		this.inputDateEnd.attr('class', 'mainCalendar_input mainCalendar_input_date_selected');
	},

	createMainWindow: function() {
		var mainCalendar_mainWindow =
			$('<div/>', {id: 'mainCalendar_mainWindow', 'class': 'mainCalendar ' + this.settings.className, style: ''}
			).append(
					$('<div/>', {'class': 'mainCalendar_years', 'id': 'mainCalendar_years'})
				).append(
					$('<div/>', {'class': 'mainCalendar_slider'}).append(
						$('<div/>', {id: 'js_sliderContent', 'class': 'mainCalendar_sliderContent'}).append(
							$('<div/>', {id: 'js_dragObject', 'class': 'mainCalendar_dragObject'})
						)
					)
				).append(
					$('<div/>', {'class': 'mainCalendar_content'}).append(
						$('<table/>', {'cellPadding': '0', 'cellSpacing': '0'}).append(
								$('<tr/>', {'class': 'mainCalendar_firstLetters_tr'})).append(
								$('<tr/>', {id: 'mainCalendar_month_tr', 'class': 'mainCalendar_month_tr'})
							)
					)
				).append(
					$('<div/>', {'class': 'mainCalendar_inputs'}).append(
							$('<input/>', {id: 'mainCalendar_input_date_start', 'class': 'mainCalendar_input mainCalendar_input_date_selected', 'type': 'text'})
						).append('&nbsp; — &nbsp;').append(
							$('<input/>', {id: 'mainCalendar_input_date_end', 'class': 'mainCalendar_input', 'type': 'text'})
						).append(
							$('<input/>', {id: 'mainCalendar_button_save', 'class': 'mainCalendar_button button', value: 'Выбрать', 'type': 'button'})
						).append(
							$('<input/>', {id: 'mainCalendar_button_close', 'class': 'mainCalendar_button button', value: 'Отмена', 'type': 'button'})
						)
				);
		$('body').append(mainCalendar_mainWindow);
		$('#mainCalendar_years').html(this.getYearDiv());
		for (var i = 0; i < this.settings.countMonth; i++) {
			$('#mainCalendar_mainWindow .mainCalendar_content .mainCalendar_firstLetters_tr').append('<td/>');
			$('#mainCalendar_mainWindow .mainCalendar_content #mainCalendar_month_tr').append($('<td/>', {id: 'mainCalendar_month_td_' + i, 'class': 'mainCalendar_month_td'}));
		}
		this.displayMonth(this.getMonthTime(this.settings.maxMonth), this.settings.maxMonth);
		this.inputDateStart = $('#mainCalendar_input_date_start');
		this.inputDateEnd = $('#mainCalendar_input_date_end');
		this.buttonSave = $('#mainCalendar_button_save');
		this.buttonClose = $('#mainCalendar_button_close');
	},

	getYearDiv: function() {
		var countYears = (this.settings.maxMonth + this.settings.countMonth) / 12, div = $('<div/>', {'class': 'mainCalendar_years_value'}),
			yearDiv, sliderContentWidth = $('#js_sliderContent').width(), oneStep = sliderContentWidth / countYears;
		$('#js_dragObject').width((sliderContentWidth / (this.settings.maxMonth + this.settings.countMonth)) * this.settings.countMonth + 1);
		for (var i = 0; i <= countYears; i++) {
			yearDiv = $('<div/>', {'text': (this.settings.startYear + i)});
			yearDiv.css('left', i * oneStep + 'px');
			yearDiv.on('click', '', {'index': 12 * i}, function(event) {
				mainCalendar.slide(event.data.index);
			});
			div.append(yearDiv);
		}
		return div;
	},

	startSlider: function() {
		this.slider = slider();
		this.slider.init({
			max: {x: mainCalendar.settings.maxMonth - mainCalendar.settings.minMonth},
			content: 'js_sliderContent',
			onChange: function(options) { mainCalendar.onChange(options) }
		});
	},

	removeClassInterval: function() {
		for (var i in this.monthObj) {
			this.getDaysByIndex(i).removeClass('mainCalendar_inInterval');
		}
	},

	save: function() {
		if (this.datesIsCorrect()) {
			this.saveDates();
			this.saveText();
		}
		this.closeCalendar();
	},

	saveDates: function() {
		var start = this.inputDateStart.val(),
			end = this.inputDateEnd.val();
		$('#' + this.getIdStart()).val(this.prepareDateToSave(start));
		$('#' + this.getIdEnd()).val(this.prepareDateToSave(end));
	},

	prepareDateToSave: function(date) {//Дата уже проверена
		var arr = date.split('.');
		if (arr[0].length == 1) arr[0] = '0' + arr[0];
		if (arr[1].length == 1) arr[1] = '0' + arr[1];
		return arr.join('.');
	},

	closeCalendar: function() {
		$('#mainCalendar_mainWindow').css('display', 'none');
	},

	saveText: function(id_target) {
		var start = this.inputDateStart.val(),
			end = this.inputDateEnd.val(),
			target = document.getElementById(id_target) || this.settings.target;
		var startDate = this.getDateByStr(start);
		var endDate = this.getDateByStr(end);
		var text = this.getDateString(startDate, endDate);
		$(target).text(text);
	},

	getIdStart: function() {
		if ((this.settings.id_start.indexOf('[') + 1) || (this.settings.id_start.indexOf(']') + 1)) return this.settings.id_start.replace(new RegExp('\\[', 'g'), '\\[').replace(new RegExp('\\]', 'g'), '\\]');
		return this.settings.id_start;
	},

	getIdEnd: function() {
		if ((this.settings.id_end.indexOf('[') + 1) || (this.settings.id_end.indexOf(']') + 1)) return this.settings.id_end.replace(new RegExp('\\[', 'g'), '\\[').replace(new RegExp('\\]', 'g'), '\\]');
		return this.settings.id_end;
	},

	getDateString: function(startDate, endDate) {
		if (
			(startDate.getFullYear() == endDate.getFullYear()) &&
				(startDate.getMonth() == endDate.getMonth()) &&
				(startDate.getDate() == endDate.getDate())
			) return (startDate.getDate() + ' ' + this.getMonthName(startDate.getMonth(), 'r') + ' ' + startDate.getFullYear() + ' г.');
		else if (
			(startDate.getFullYear() == endDate.getFullYear()) &&
				(startDate.getMonth() == endDate.getMonth()) &&
				(startDate.getDate() != endDate.getDate())
			) return ('C ' + startDate.getDate() + ' по ' + endDate.getDate() + ' ' + this.getMonthName(startDate.getMonth(), 'r') + ' ' + startDate.getFullYear() + ' г.');
		else if (
			(startDate.getFullYear() == endDate.getFullYear())
			) return ('С ' + startDate.getDate() + ' ' + this.getMonthName(startDate.getMonth(), 'r') + ' по ' + endDate.getDate() + ' ' + this.getMonthName(endDate.getMonth(), 'r') + ' ' + startDate.getFullYear() + ' г.');
		return ('С ' + startDate.getDate() + ' ' + this.getMonthName(startDate.getMonth(), 'r') + ' ' + startDate.getFullYear() + ' г. по ' + endDate.getDate() + ' ' + this.getMonthName(endDate.getMonth(), 'r') + ' ' + endDate.getFullYear() + ' г.');
	},

	getDateByStr: function(str) {
		var arr = str.split('.');
		return new Date(arr[2], arr[1] - 1, arr[0]);
	},

	slide: function(index) {
		this.slider.slideTo(index);
	},

	displayMonth: function(date, curVal) {
		for (var i = curVal, j = 0; i < curVal + this.settings.countMonth; i++, j++) {
			if (typeof(this.monthObj[i]) == 'undefined') {
				this.monthObj[i] = this.constructMonth(new Date(date.getFullYear(), date.getMonth() + j, 1));
			}
			$('#mainCalendar_month_td_' + (i - curVal)).html(this.monthObj[i]);
		}
	},

	constructMonth: function(date) {
		var table = $('<table/>', {'class': 'mainCalendar_month_table', 'cellPadding': '0', 'cellSpacing': '0'});
		table.append(this.getTopPart(date)); //название месяца+год
		table.append(this.getBodyLines(date)); //числа месяца
		return table;
	},

	onInputChange: function(event) {
		this.removeClassInterval(); //очистить выделение
		if (!this.datesIsCorrect()) { //проверка обеих дат
			return false;
		}
		this.setEndDay();
		var arr = this.inputDateStart.val().split('.');
		this.slide(this.getIndexInMonthObj(arr[1], arr[2]));
		return true;
	},

	onMonthClick: function(event) {
		var dateText = this.getPartDateByTarget(event.target); //получить год и месяц
		var month = dateText.substr(0, dateText.indexOf('.'));
		var year = dateText.substr(dateText.indexOf('.') + 1, dateText.length);
		var maxDay = this.getMaxDate(year, month - 1); //максимальный день в месяце
		this.inputDateEnd.val(maxDay + '.' + dateText);
		this.inputDateStart.val('01.' + dateText);
		this.removeClassInterval();
		//getDaysByIndex - получить объект дней выбранного месяца
		this.getDaysByIndex(this.getIndexInMonthObj(month, year)).addClass('mainCalendar_inInterval');
		this.inputDateStartClick();
	},

	onWeekClick: function(event) {
		var week = $(event.target).parents('tr').eq(0).find('.mainCalendar_weekday, .mainCalendar_holiday');//получить год и месяц
		var firstDay = week.get(0).innerHTML;
		var lastDay = week.get(week.length - 1).innerHTML;
		var date = this.getPartDateByTarget(event.target);
		this.inputDateEnd.val(this.prepareNumber(lastDay) + '.' + date);
		this.inputDateStart.val(this.prepareNumber(firstDay) + '.' + date);
		this.removeClassInterval();
		week.addClass('mainCalendar_inInterval');
		this.inputDateStartClick();
	},

	onDayClick: function(event) {
		var day = this.prepareNumber(event.target.innerHTML);
		var date = this.getPartDateByTarget(event.target);
		var id = $('.mainCalendar_input_date_selected').attr('id');
		if (id == this.inputDateStart.attr('id') || !this.datesIsCorrect()) {
			this.onFirstDayClick(day + '.' + date);
			this.setStartDay(event.target);
			this.inputDateEndClick();
		}
		else {
			this.onSecondDayClick(day + '.' + date);
			this.setEndDay();
			this.inputDateStartClick();
		}
	},

	onFirstDayClick: function(start) {
		this.inputDateEnd.val(start);
		this.inputDateStart.val(start);
	},

	onSecondDayClick: function(val) {
		var start = this.inputDateStart.val();
		if (this.startBeforeEnd(start, val)) this.inputDateEnd.val(val);
		else this.inputDateStart.val(val);
	},

	onCreate: function() {
		this.inputDateStart.val($('#' + this.getIdStart()).val());
		this.inputDateEnd.val($('#' + this.getIdEnd()).val());
		var date, index;
		if (!this.datesIsCorrect()) {
			date = new Date();
			index = this.getIndexInMonthObj((date.getMonth() + 1), date.getFullYear());
			this.slide(index);
			this.getDaysByIndex(index).eq(date.getDate() - 1).click();
		} else {
			this.onInputChange();
		}
		this.inputDateStartClick();
	},

	onSlide: function(event, ui) {
//		this.onChange(event, ui);
	},

	onChange: function(options) {
		var curVal = options.step.x + mainCalendar.settings.minMonth;
		var date = this.getMonthTime(curVal);
		this.displayMonth(date, curVal);
	},

	setStartDay: function(target) {
		this.removeClassInterval();
		$(target).addClass('mainCalendar_inInterval');
	},

	setEndDay: function() {
		var help, days, len;
		var Arr = this.inputDateStart.val().split(".");
		var startIndex = this.getIndexInMonthObj(Arr[1], Arr[2]);
		var startIndexInObj = Arr[0] - 1;
		Arr = this.inputDateEnd.val().split(".");
		var endIndex = this.getIndexInMonthObj(Arr[1], Arr[2]);
		var endIndexInObj = Arr[0] - 1;

		if (startIndex > endIndex) {
			help = startIndex;
			startIndex = endIndex;
			endIndex = help;
		}
		else if (startIndexInObj == endIndexInObj) {
			help = startIndexInObj;
			startIndexInObj = endIndexInObj;
			endIndexInObj = help;
		}
		if (startIndex != endIndex) {
			days = this.getDaysByIndex(startIndex);
			len = this.getDaysByIndex(startIndex).length;
			for (var i = startIndexInObj; i < len; i++) {
				days.eq(i).addClass('mainCalendar_inInterval');
			}
			for (i = startIndex + 1; i < endIndex; i++) this.getDaysByIndex(i).addClass('mainCalendar_inInterval');
			days = this.getDaysByIndex(endIndex);
			for (i = 0; i <= endIndexInObj; i++) {
				days.eq(i).addClass('mainCalendar_inInterval');
			}
		} else {
			days = this.getDaysByIndex(startIndex);
			for (i = startIndexInObj; i <= endIndexInObj; i++) {
				days.eq(i).addClass('mainCalendar_inInterval');
			}
		}
	},

	startBeforeEnd: function(start, end) {
		var startArr = start.split(".");
		var endArr = end.split(".");
		if (startArr[2] - endArr[2] < 0) return true;
		else if (startArr[2] - endArr[2] > 0) return false;
		if (startArr[1] - endArr[1] < 0) return true;
		else if (startArr[1] - endArr[1] > 0) return false;
		if (startArr[0] - endArr[0] <= 0) return true;
		return false;
	},

	datesIsCorrect: function() {
		var start = this.inputDateStart.val();
		var end = this.inputDateEnd.val();
		var myRe = /^(([0-2]?[0-9]{1})|([3][01]{1}))\.([0]?[1-9]{1}|([1]{1}[012]{1}))\.(\d{4})$/;
		if ((start.search(myRe) == -1) || (end.search(myRe) == -1) || (!this.startBeforeEnd(start, end))) {
			return false;
		}
		var arr = end.split('.');
		var date = new Date(arr[2], (arr[1] - 1), arr[0]);
		if (date.getMonth() - arr[1] == 0) return false;
		arr = start.split('.');
		date = new Date(arr[2], (arr[1] - 1), arr[0]);
		if (date.getMonth() - arr[1] == 0) return false;
		return true;
	},

	getDaysByIndex: function(index) {
		if (typeof(this.monthObj[index]) == 'undefined') this.monthObj[index] = this.constructMonth(this.getMonthTime(index));
		return this.monthObj[index].find('.mainCalendar_weekday, .mainCalendar_holiday');
	},

	getPartDateByTarget: function(target) {
		return $(target).parents('table').eq(0).find('.mainCalendar_dateString').text();
	},

	getIndexInMonthObj: function(month, year) {
		var index = year - this.settings.startYear;
		if (index < 0) return -1;
		return index * 12 + (month - 1);
	},

	getBodyLines: function(date) {
		var day = date.getDay();
		if (day == 0) day = 7;
		var maxDay = this.getMaxDate(date.getFullYear(), date.getMonth()),
			lines = this.getBodyLines_1and2(date, day),
			cur_week = 1, start = 0, end = 0;
		while (maxDay > start + 7 - 1) {
			start = 2 - day + cur_week * 7;
			end = 2 - day + cur_week * 7 + 7;
			if (end > maxDay + 1) end = maxDay + 1;
			lines = lines.add(this.getBodyLines_other(start, end, date));
			cur_week += 1;
		}
		return lines;
	},

	getBodyLines_1and2: function(date, day) {
		var first = this.getBodyLines_1(),
			second = $('<tr/>'),
			td_class = 'mainCalendar_weekday', td_obj;
		second.append($('<td/>', {'class': 'mainCalendar_week'}).append('<div/>'));
		if (day > 1) {
			for (var i = 1; i < day; i++) {
				second.append($('<td/>', {'class': 'mainCalendar_day_empty', text: ''}));
			}
		}
		for (i = 1; i <= 7 - day + 1; i++) {
			if (i + day - 1 > 5) td_class = 'mainCalendar_holiday';
			td_obj = $('<td/>', {
				'class': td_class,
				text: i
			});
			second.append(td_obj);
		}
		return first.add(second);
	},

	getBodyLines_1: function() {
		var mainCalendar_bodyLines_1 = $('<td/>', {text: ' '}).add('<td/>', {text: 'Пн', 'class': 'mainCalendar_weekday_header'}).add('<td/>', {text: 'Вт', 'class': 'mainCalendar_weekday_header'}).add('<td/>', {text: 'Ср', 'class': 'mainCalendar_weekday_header'}).add('<td/>', {text: 'Чт', 'class': 'mainCalendar_weekday_header'}).add('<td/>', {text: 'Пт', 'class': 'mainCalendar_weekday_header'}).add('<td/>', {text: 'Сб', 'class': 'mainCalendar_weekday_header'}).add('<td/>', {text: 'Вс', 'class': 'mainCalendar_weekday_header'});
		return $('<tr/>').append(mainCalendar_bodyLines_1);
	},

	getBodyLines_other: function(start, end, date) {
		var line = $('<tr/>'),
			td_class = 'mainCalendar_weekday',
			td_obj;
		line.append($('<td/>', {'class': 'mainCalendar_week'}).append('<div/>'));
		for (var i = start; i < end; i++) {
			if (i - start + 1 > 5) td_class = 'mainCalendar_holiday';
			td_obj = $('<td/>', {
				'class': td_class,
				text: i
			});
			line.append(td_obj);
		}
		return line;
	},

	getTopPart: function(date) {
		var text = this.getMonthName(date.getMonth());
		var b = $('<b/>', {'class': 'mainCalendar_dateString none', text: (this.prepareNumber(date.getMonth() + 1)) + '.' + date.getFullYear()});
		var th = $('<th/>', {
			'class': 'mainCalendar_month',
			colspan: 8,
			title: text,
			text: text + ' ' + date.getFullYear()
		}).append(b);
		return $('<tr/>').prepend(th);
	},

	monthArray: {
		'i': {0: 'Январь', 1: 'Февраль', 2: 'Март', 3: 'Апрель', 4: 'Май', 5: 'Июнь', 6: 'Июль', 7: 'Август', 8: 'Сентябрь', 9: 'Октябрь', 10: 'Ноябрь', 11: 'Декабрь'},
		'r': {0: 'января', 1: 'февраля', 2: 'марта', 3: 'апреля', 4: 'мая', 5: 'июня', 6: 'июля', 7: 'августа', 8: 'сентября', 9: 'октября', 10: 'ноября', 11: 'декабря'}
	},

	prepareNumber: function(number) {
		return (number < 10 && number > 0) ? ('0' + number) : number;
	},

	getMonthName: function(id_month, padezh) {
		if (typeof(padezh) == 'undefined') padezh = 'i';
		var month = this.monthArray[padezh][id_month];
		if (typeof(month) == 'undefined') month = '';
		return month;
	},

	getMonthTime: function(val) {
		var year = this.settings.startYear + (val / 12 | 0);
		var month = val % 12;
		return new Date(year, month, 1);
	},

	getMaxDate: function(y, m) {
		if (m == 1) return y % 4 || (!(y % 100) && y % 400 ) ? 28 : 29;
		return m == 3 || m == 5 || m == 8 || m == 10 ? 30 : 31;
	}
};