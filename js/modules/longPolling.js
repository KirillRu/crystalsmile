(function() {

	var cachedObjects = {};
	longPolling = function(uniqueId){
		//нельзя открыть несколько соединений с uniqueId
		uniqueId = uniqueId || 'longPolling';

		function listen(settings){
			if (!(uniqueId in cachedObjects)) cachedObjects[uniqueId] = new longPollingObj(settings);

			cachedObjects[uniqueId].listen();
			return cachedObjects[uniqueId];
		}

		function closeConnection(){
			if (uniqueId in cachedObjects){
				cachedObjects[uniqueId].closeConnection();
				delete cachedObjects[uniqueId];
			}
		}

		function connectionOpened(){
			if (uniqueId in cachedObjects) return cachedObjects[uniqueId].connectionOpened;
			return false;
		}

		return {
			listen: listen,
			closeConnection: closeConnection,
			connectionOpened: connectionOpened
		}
	};

	function longPollingObj(settings){
		this.settings = {
			url: '',
			data: '',
			timeout: 2000,//значение, через которое отправлять повторный запрос на сервер после успешного получения данных
			onMessage: function(){}
		};
		this.connectionOpened = false;

		for(var i in this.settings)
			if (settings.hasOwnProperty(i)) this.settings[i] = settings[i];
	}
	longPollingObj.prototype = {
		listen: function(){
			if (!this.connectionOpened) this.openConnection();
		},
		openConnection: function(){
			this.connectionOpened = true;
			$.ajax(this.settings.url, {
				type: 'POST',
				data: this.settings.data,
                dataType: "jsonp",
				cache: false,
				success: this.success.bind(this),
				error: this.error.bind(this)
			});
		},
		error: function(){
			if (!this.connectionOpened) return;

			var self = this;
			setTimeout(function() {
				self.openConnection();
			}, this.settings.timeout);
		},
		success: function(events){
			if (!this.connectionOpened) return;

			for(var i = 0, len = events.length; i < len; i++){
				switch(events[i].state){
					case 'timeout':break;
					case 'message':
						this.settings.onMessage(events[i].data);
						break;
				}
			}

			var self = this;
			setTimeout(function() {
				self.openConnection();
			}, this.settings.timeout);
		},
		closeConnection: function(){
			this.connectionOpened = false;
		}
	};
}());