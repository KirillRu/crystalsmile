gallery = function() {
	var container, $popup, content, smallImgs, currentPos, maxPos, sizeNames, maxHeight;

	$(function() {
		//set Events
		$('body').on('click', '.js_galleryShow', function() {
			var img = (this.tagName.toLocaleLowerCase() == 'img' ? this : $(this).find('img:first').get(0));
			var $galleryContainer = $(img).closest('.js_galleryContainer'), $imgs;
			if ($galleryContainer.length) {
				$imgs = $galleryContainer.find('.js_galleryFoto');
				if ($imgs.length <= 1) openFoto(img, $galleryContainer.data('maxSizeName'));
				else openGallery(img, $imgs);
			}
			return false;
		});

		$('.js_galleryMiddle').on('click', function(){
			detail.openGallery(this);
			return false;
		});
	});

	function createPopup() {
		if (!container) {
			$('body').append(''+
				'<div id="js_popupContainer">' +
					'<div class="pw-wrapper"></div>' +
					'<div class="popup gallery" id="js_popupImage">' +
						'<div class="close" id="js_galleryClose"><img src="/img/popup/icon-close.png"></div>' +
						'<div id="js_galleryPrev" class="gallery-prev" title="Предыдущая"></div>' +
						'<div id="js_galleryNext" class="gallery-next" title="Следующая"></div>' +
						'<div id="js_galleryContent" class="gallery-content"><img /></div>' +
						'<div class="gallery-smalls" id="js_gallerySmalls"></div>' +
					'</div>' +
				'</div>'

			);
			container = g('js_popupContainer');

			container.onclick = function(e){
				e = e || window.event;
				if (!$(e.target).closest('#js_popupImage').length && $(e.target).closest('body').length/*это условие для того, что бы не закрывалось окно при удалении элемента внутри popup*/){
					containerClose();
				}
			};

			$popup = $(container).children('#js_popupImage');

			$popup.children('#js_galleryClose').on('click', containerClose);

			$popup.children('#js_galleryPrev').on('click', prev);
			$popup.children('#js_galleryNext').on('click', next);

			content = $popup.children('#js_galleryContent');
			content.on('click', next);

			content = content.get(0);
			clearData();
		}
	}

	function containerShow(){
		container.style.display = 'block';
	}

	function containerClose(){
		container.style.display = 'none';
		clearData();
	}

	function popupToCenter(maxWidth){
		$popup.css({
			top: functions.getScrollTop() + 70,
			left: functions.getScrollLeft() + ($(document).width() / 2) - (maxWidth / 2)
		});
	}

	function beforeOpenGallery(img, $imgs) {
		var $galleryContainer = $(img).closest('.js_galleryContainer');
		if (!$imgs) $imgs = $galleryContainer.find('.js_galleryFoto');
		createPopup();

		//---set consts---
		maxPos = $imgs.length - 1;
		currentPos = $imgs.index(img);
		sizeNames = {
			min: $galleryContainer.data('minSizeName'),
			max: $galleryContainer.data('maxSizeName')
		};
		for (var len = $imgs.length; len--;) smallImgs.unshift($imgs[len].cloneNode());
		//---set consts---

		//---set styles---
		popupToCenter($galleryContainer.data('maxWidth'));
		$popup.children('#js_galleryPrev, #js_galleryNext, #js_galleryContent')
			  .css('height', $galleryContainer.data('maxHeight'))
			  .filter('#js_galleryContent')
			  .css('width', $galleryContainer.data('maxWidth'))
			  .addClass('gallery-content');
		//---set styles---

		//---set events---
		$('body').on('keydown.galleryKeys', function(e){
			var charCode = functions.getCharCode(e);
			if (charCode == 39) next();
			else if(charCode == 37) prev();
		});
	}

	function prev() {showByIndex((--currentPos < 0) ? (maxPos) : currentPos);}
	function next() {showByIndex((++currentPos > maxPos) ? 0 : currentPos);}
	function showByIndex(i) {showCurrentFoto(currentPos = i);}

	function showCurrentFoto() {
		if (smallImgs[currentPos]){
			var img = smallImgs[currentPos].cloneNode();
			element.replace(img, content.firstChild);
			prepareImg(img, 'max');
			$popup.children('#js_gallerySmalls').children('div').removeClass('act').eq(currentPos).addClass('act');
		}
	}

	function prepareImg(img, size) {
		img.src = getSrc(img.src, sizeNames[size]);
		img.className = 'img-' + sizeNames[size].substr(5);
		img.onclick = null;
		return img;
	}

	function clearData() {
		currentPos = maxPos = 0;
		smallImgs = [];
		sizeNames = {};
		$popup.children('#js_galleryContent').html('<img />');
		$popup.children('#js_gallerySmalls').empty();
		$('body').off('keydown.galleryKeys');
	}

	function onStartLoad(img, cb, counter){
		/*Как только img начал загружаться - изменится width*/
		counter = counter || 0;
		if (counter < 50){
			if ($(img).width()) cb();
			else setTimeout(function(){onStartLoad(img, cb, ++counter)}, 80);
		} else {
			cb();
		}
	}

	function openGallery(img, $imgs) {
		beforeOpenGallery(img, $imgs);
		var smalls = $popup.children('#js_gallerySmalls').get(0);
		smallImgs = smallImgs.map(function(el, i) {
			var a = document.createElement('a');
			a.className = 'img-smallest';
			a.appendChild(el);
			
			var div = document.createElement('div');
			div.appendChild(a);
			div.onclick = (function(pos) {
				return function() {showByIndex(pos)};
			}(i));
			smalls.appendChild(div);
			return el = prepareImg(el, 'min');
		});
		showCurrentFoto();
		containerShow();
	}

	function openFoto(img, sizeName){
		createPopup();
		$popup.children('#js_galleryPrev, #js_galleryNext').css('height', 0);
		$popup.children('#js_galleryContent').removeClass('gallery-content');

		var newImg = document.createElement('img');
		newImg.src = getSrc(img.src, sizeName);
		newImg.className = 'img-' + sizeName.substr(5);
		newImg.style.display = 'inline';
		element.replace(newImg, content.firstChild);
		showPopupAfterStartLoad(newImg)
	}

	function showPopupAfterStartLoad(img){
		/*определяем ширину и показываем popup(еще до конца загрузки img)*/
		container.style.visibility = 'hidden';
		containerShow();
		popupToCenter($popup.width());

		fixedElements.addToFixedLeft($popup);

		onStartLoad(img, function(){
			img.style.cursor = 'default';
			container.style.visibility = '';
		});
	}

	function getSrc(src, sizeName){
		if (/(foto_smallest|foto_small|foto_middle|foto_large|foto_largest)(\.[a-z]{3})$/.test(src)){
			return src.replace(/(foto_[a-z]+)\.[a-z]{3}$/, function(str, p) {
				return sizeName + str.substr(p.length);
			});
		}
		return src;
	}

	var detail = function(){
		var activeFoto = 0;

		function showMiddleFoto(src, id, pos) {
			g('js_galleryMiddle_' + id).src = src;
			activeFoto = pos;
			return false;
		}

		function openGallery(img){
			gallery.openGallery($(img).closest('.js_galleryContainer').find('.js_galleryFoto').get(activeFoto));
		}

		return {
			showMiddleFoto: showMiddleFoto,
			openGallery: openGallery
		};
	}();

	return {
		openFoto: openFoto,
		openGallery: openGallery,
		detail: detail
	};
}();