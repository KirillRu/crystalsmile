(function() {
	sort = function(){
		return new _sort();
	};

	function isIe(){
		var isIe = navigator.appVersion.match(/MSIE\u0020([.\d]+);/), version = false;
		if (isIe) version = parseInt(isIe[1], 10);
		return version && version < 10;
	}

	var _sort = function(){
		this.settings = {};
		this.$container = this.ui = null;
	};
	_sort.prototype = {
		uiSettings: {
			opacity: 0.55,
			scroll: false
		},
		init: function($container, settings){
			if (!isIe()){
				this.setConsts($container, settings);
				this.initSortable();
				this.setEvents();
			}
		},
		initSortable: function() {
			if (this.$container.sortable('instance')) this.$container.sortable('destroy');
			this.ui = this.$container.sortable(this.uiSettings);
		},
		setEvents: function() {
			var self = this;
			this.ui.on('sortupdate', function(e, ui) {
				ajax.requestOptional({
					url: self.settings.url,
					beforeSend: function(){},
					data: ui.item.data('data') + '&pos=' + (self.$container.children().index(ui.item) + 1)
				});
			});
		},
		setConsts: function($container, settings){
			if ('uiSettings' in settings) {functions.mergeObjects(this.uiSettings, settings.uiSettings); delete settings.uiSettings};
			this.$container = $container;
			this.settings = settings;
		}
	};
}());