(function() {
	autocomplete = function() {
		return new _Autocomplete();
	};

	autocompleteForm = function() {
		return new _AutocompleteForm();
	};

	var mainProto = {
		init: function(settings) {
			this.setSettings(settings);
			this.startAutocomplete();
			return this;
		},
		setSettings: function(settings){
			this.settings = {
				url: 'runModule/liveSearch.ajax',
				idText: 'js_searchInput',
				idInput: null,
				data: {},
				autocompleteParams: {
					minLength: 2,
					position: { my: 'left top', at: 'left bottom+2', collision: 'none' },
					source: this.source.bind(this),
					focus: this.onFocus.bind(this),
					select: this.onSelect.bind(this),
					open: this.onOpen.bind(this),
					change: this.onChange.bind(this)
				}
			};
			functions.mergeObjectsRecursive(this.settings, settings);
			this.cache = {};
			this.$input = $('#' + this.settings.idText);
		},
		startAutocomplete: function(){
			this.$input.autocomplete(this.settings.autocompleteParams);
		},
		source: function(request, response){
			if (this.cache[request.term]) return response(this.cache[request.term]);

			var self = this;
			ajax.requestOptional({
				url: this.settings.url,
				dataType: 'json',
				data: functions.mergeObjects(request, this.settings.data),
				success: function(data, status, xhr) {
					self.cache[request.term] = data;
					response(data);
				},
				beforeSend: function(){}
			});
		},
		onOpen: function(event, ui) {
			this.$input.autocomplete('widget').outerWidth(this.$input.outerWidth());
		},
		onFocus: function(event, ui){},
		onSelect: function(event, ui){},
		onChange: function(event, ui){},
		hideWindow: function(){
			this.$input.autocomplete('widget').hide();
		}
	};

	function _AutocompleteForm(){

		this.onSelect = function(event, ui){
			g(this.settings.idInput).value = ui.item.id;
		};

		this.onFocus = function(event, ui){
			if (typeof event.keyCode !== 'undefined' && this.settings.idInput) g(this.settings.idInput).value = ui.item.id;
		};

		this.onChange = function(event, ui){
			var text = this.$input.val();
			//текст после изменения пустой, или в нем мало букв - очищаем инпут
			if (!text || text.length < this.settings.autocompleteParams.minLength){
				g(this.settings.idInput).value = null;
				return;
			}

			var self = this, itemFromCache = this.searchInCache(text);
			if (itemFromCache){//имеется полное совпадение в кэше по имени
				g(self.settings.idInput).value = itemFromCache['id'];
				self.$input.val(itemFromCache['value']);
			} else {
				//ищем по частичному совпадению в кэше(или на серве)
				this.source({term: text}, function(items) {
					if (items && items[0]) {//берем первый попавшийся вариант
						g(self.settings.idInput).value = items[0]['id'];
						self.$input.val(items[0]['value']);
					} else {
						g(self.settings.idInput).value = null;
					}
				});
			}
		};

		this.searchInCache = function(text){
			for(var i in this.cache){
				for(var k in this.cache[i]) {
					var item = this.cache[i][k];
					if (item.value === text) return item;
				}
			}
			return false;
		}
	}
	_AutocompleteForm.prototype = mainProto;

	function _Autocomplete(){};
	_Autocomplete.prototype = mainProto;
}());

(function( $ ) {

var proto = $.ui.autocomplete.prototype,
	initSource = proto._initSource;

function filter( array, term ) {
	var matcher = new RegExp( $.ui.autocomplete.escapeRegex(term), "i" );
	return $.grep( array, function(value) {
		return matcher.test( $( "<div>" ).html( value.label || value.value || value ).text() );
	});
}

$.extend( proto, {
	_initSource: function() {
		if ( $.isArray(this.options.source) ) {
			this.source = function( request, response ) {
				response( filter( this.options.source, request.term ) );
			};
		} else {
			initSource.call( this );
		}
	},

	_renderItem: function( ul, item) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append(item.label)
			.appendTo( ul );
	}
});

})( jQuery );