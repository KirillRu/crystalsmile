windowSearch = function() {
	var consts = {};

	function init(id, options) {
		if (consts.id  && (consts.id != id)) closeWindow();
		setConst(id, options);
		setEvents();
		if (windowIsOpened()) closeWindow();
		else openWindow();
	}

	function setConst(id, options) {
		consts = {};
		consts.id = id;
		consts.table = options.selectChild.table;
		consts.toogle = g('ws_' + id + '_toggle');
		consts.mainWindow = g('ws_' + id + '_mainWindow');
		consts.form = $(consts.toogle).closest('form').get(0);
		consts.data = options.data;
		consts.container = g('ws_' + id + '_container');
		consts.inputField = g('ws_' + id);
		consts.url = options.url || 'form/windowSearch.ajax';
		consts.selectChildOptions = options.selectChild;
	}

	function setEvents() {
		var previousCharCode = 0, lastValidCharCode = 32, timeout = false, timeoutMS = 300;
		consts.inputField.onkeydown = function(event) {
			var charCode = functions.getCharCode(event);
			if (charCode == 13 || charCode == 8 || (charCode == 86 && previousCharCode == 17)) { //enter backspace  ctrl+V
				if (timeout) clearTimeout(timeout);
				timeout = setTimeout(function() {startSearch()}, timeoutMS);
			}
			previousCharCode = functions.getCharCode(event);
		};
		consts.inputField.onkeypress = function(event) {
			if (checkChar(functions.getCharCode(event))) {
				if (timeout) clearTimeout(timeout);
				timeout = setTimeout(function() {startSearch()}, timeoutMS);
			} else return false;
		};
		$(consts.container).on('click.wsItems', '.ws_items', function() {
			onSelect($(this).data('leaf'));
		});
		function checkChar(charCode) {
			if (!/[a-zа-я0-9;:'",<\.>\[\{\]\}\- ]/i.test(String.fromCharCode(charCode))) return false;
			if (lastValidCharCode == 32 && charCode == 32) return false; //2 пробела подряд
			lastValidCharCode = charCode;
			return true;
		}
		$(document).on('click.wsCloseWindow', function(e){
			if (!element.hasClass(e.target, 'js_wsToggle') && !$(e.target).closest('#ws_' + consts.id + '_mainWindow').length){
				closeWindow();
			}
			return false;
		});
	}

	function onSelect(id) {
		scriptLoader('form.js').callFunction(function() {
			closeWindow();
			if (consts.selectChildOptions.reloadForm) formFunctions.selectCategoryUpdate(id, consts.selectChildOptions);
			else formFunctions.selectChildUpdate(id, consts.selectChildOptions);
		});
	}

	function startSearch() {
		var currentValue = functions.trim(consts.inputField.value);
		if (currentValue.length < 3) {
			displayError('Для поиска необходимо ввести больше двух символов');
			return;
		}
		ajax.cbOnSuccess(consts.url, 'ws[table]=' + consts.table + '&' + 'ws[searchPhrase]=' + currentValue + (consts.data ? '&' + consts.data : '') + (this.form ? '&' + element.getParameters(this.form) : ''), function(r){
			afterSearch(r);
		});
	}

	function afterSearch(responseText) {
		if (responseText) consts.container.innerHTML = responseText;
		else displayError('Ничего не найдено');
	}

	function displayError(text) {
		var error = g('ws_' + consts.id + '_error');
		if (error) error.innerHTML = text;
		else consts.container.innerHTML = '<div id="ws_' + consts.id + '_error" class="ws_error">' + text + '</div>';
	}

	function openWindow() {
		consts.mainWindow.style.display = 'block';
		consts.inputField.focus();
	}

	function closeWindow() {
		consts.mainWindow.style.display = 'none';
		var errorContent = g('ws_' + consts.name + 'error');
		if (errorContent) element.remove(errorContent);
		$(consts.container).off('click.wsItems');
		$(document).off('click.wsCloseWindow');
	}

	function windowIsOpened() {
		return (consts.mainWindow.style.display == 'block');
	}

	return {
		init: init,
		close: closeWindow
	};
}();