files = function(){

	function moveDown(url, data, target){
		ajax.requestOptional({url: url, data: data, beforeSend: function(){}});
		var currentBlock = $(target).closest('.js_fileBlock');
		var nextBlock = currentBlock.next('.js_fileBlock');
		if (nextBlock.length) currentBlock.insertAfter(nextBlock);
		else currentBlock.insertBefore(currentBlock.siblings('.js_fileBlock:first'));
	}

	function moveUp(url, data, target){
		ajax.requestOptional({url: url, data: data, beforeSend: function(){}});
		var currentBlock = $(target).closest('.js_fileBlock');
		var nextBlock = currentBlock.prev('.js_fileBlock');
		if (nextBlock.length) currentBlock.insertBefore(nextBlock);
		else currentBlock.insertAfter(currentBlock.siblings('.js_fileBlock:last'));
	}

	function remove(url, data, target){
		ajax.requestOptional({url: url, data: data, beforeSend: function(){}});
		var fotoBlock = $(target).closest('.js_fileBlock'), siblings = fotoBlock.siblings('.js_fileBlock');
		fotoBlock.remove();
		if (siblings.length == 1) siblings.find('.js_moveDivs').remove();
	}

	function initUploader(settings){
		if (isIe()) {
			(new _ByFrameUploader()).init(settings);
		} else {
			scriptLoader('modules/fileUploadLib.js').callFunction(function(){
				(new _Uploader()).init(settings);
			});
		}
	}

	function isIe(){
		var isIe = navigator.appVersion.match(/MSIE\u0020([.\d]+);/), version = false;
		if (isIe) version = parseInt(isIe[1], 10);
		return version && (version < 10);
	}

	//===Загрузка файлов===//
		function _Uploader(){}
		_Uploader.prototype = {
			init: function(settings){
				this.setSettings(settings);
				this.initFileUpload();
				this.initDrag();
				return this;
			},
			setSettings: function(settings){
				this.settings = {
					containerId: 'js_fileUploadContainer_X',
					dropZoneId: 'js_dropZone_X',
					singleFileUploads: false,
					url: '',
					fileLoadingHtml: '',
					maxSize: 16777216
					//idForm: false,
				};
				functions.mergeObjects(this.settings, settings);
				this.$container = $('#' + this.settings.containerId);
				this.$input = this.$container.find('input:file');
			},
			initFileUpload: function(){
				return this.$input.fileupload({
				    url: this.settings.url,
			        dataType: 'html',
				    sequentialUploads: true,
					singleFileUploads: this.settings.singleFileUploads,
			        done: this.settings.done ? this.settings.done.bind(this) : this.done.bind(this) ,
				    add: this.add.bind(this),
					progress: this.progress.bind(this),
					dropZone: $('#' + this.settings.dropZoneId),
			    });
			},
			done: function (e, data) {
				var $loadingBlocks = this.$container.find('.js_fileLoadingBlock').hide();
				this.$container.find('.js_content').html(data.result);
				this.$container.find('.js_listFiles').append($loadingBlocks);
				data.$loadingBlocks.remove();
				$loadingBlocks.show();
	        },
			progress: function(e, data){
				if (data.$progressBar){
					var progress = parseInt(data.loaded / data.total * 100, 10);
					data.$progressBar.css('width', progress + '%');
				}
			},
			add: function(e, data){
				if (this.settings.idForm) data.url = this.settings.url + '?' + element.getParameters(this.settings.idForm);
				data['files'] = this.splitFiles(data['files']);
				if (data['files'].length){
					data.$loadingBlocks = this.showLoadingBlocks(data);
					data.$progressBar = data.$loadingBlocks.find('.js_progress');
					data.submit();
				}
			},
			showLoadingBlocks: function(data){
				var $loadingBlocks = $(this.repeatString(this.settings.fileLoadingHtml, data['files'].length)),
					$listFiles = this.$container.find('.js_listFiles');

				if (this.settings.singleFileUploads && !$listFiles.find('.js_fileLoadingBlock').length) $listFiles.empty();

				$listFiles.append($loadingBlocks);

				$loadingBlocks.fadeIn(500);
				return $loadingBlocks;
			},
			repeatString: function(str, count){
				var newStr = '';
				while(count--) newStr += str;
				return newStr;
			},
			splitFiles: function(files) {
				var splitedFiles = [[]], currentIndex = 0, maxSize = this.settings.maxSize, queueSize = 0;
				for(var i = 0; i <= files.length - 1; i++) {
					if (files[i]['size'] < maxSize) {
						queueSize += files[i]['size'];
						if (queueSize > maxSize){
							splitedFiles[++currentIndex] = [];
							queueSize = 0;
						}
						splitedFiles[currentIndex].push(files[i]);
					}
				}
				for(i = 0; i < splitedFiles.length - 1; i++) {
					$('<input type="file">').fileupload({
					    url: this.settings.url,
				        dataType: 'html',
					    sequentialUploads: true,
						singleFileUploads: this.settings.singleFileUploads,
				        done: this.done.bind(this),
					    add: this.add.bind(this),
						progress: this.progress.bind(this)
				    }).fileupload('add', {files: splitedFiles[i]}).fileupload('destroy');
				}
				return splitedFiles.pop();
			},
			initDrag: function(){
				var $doc = $(document);
				/**css стили при наведение файла в зону*/
				$doc.bind('dragover', function(e) {
					var dropZones = $('.js_dropzone'), currDropZone = e.target;

					if (window.dropZoneTimeout) clearTimeout(window.dropZoneTimeout);

					do {
						if (element.hasClass(currDropZone, 'js_dropzone')) break;
						currDropZone = currDropZone.parentNode;
					} while (currDropZone != null);

					dropZones.removeClass('hover');
					if (currDropZone) element.addClass(currDropZone, 'hover');

					window.dropZoneTimeout = setTimeout(function() {
						window.dropZoneTimeout = null;
						dropZones.removeClass('hover');
					}, 100);
				});

				$doc.bind('drop dragover', function (e) {
				    e.preventDefault();
				});
			}
		};
	//===Загрузка файлов===//

	//===Загрузка файлов IE===//
		function _ByFrameUploader(){}
		_ByFrameUploader.prototype = {
			init: function(settings) {
				this.settings = settings;
				this.$input = $('#' + settings.containerId).find('input:file');
				this.prepareInput();
			},
			prepareInput: function() {
				var $frame = $('<div style="display: none"><iframe name="fileUploadFrame_' + this.settings.containerId + '" src=""></iframe></div>').appendTo('body').children();
				this.$input.wrap('<form enctype="multipart/form-data" method="post" action="' + this.settings.url + '" target="fileUploadFrame_' + this.settings.containerId + '"></form>');

				var self = this;
				$frame.on('load', function(){
					var $body = $(this).contents().find('body');
					if ($body.find('.js_listFiles:first').length) $('#' + self.settings.containerId).find('.js_listFiles').html($body.html());
					popupManager.ajaxLoad.hide();
					$body.empty();
				});
				this.$input.on('change', function(){
					popupManager.ajaxLoad.show();
					$(this).closest('form').submit();
				});
			}
		};
	//===Загрузка файлов IE===//

	return {
		moveDown: moveDown,
		moveUp: moveUp,
		remove: remove,
		initUploader: initUploader
	};
}();