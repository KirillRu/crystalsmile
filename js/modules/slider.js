(function(){
	slider = function(){
		return new __slider();
	};

	function __slider(settings){}
	__slider.prototype = {
		init: function(settings){
			this.setSettings(settings);
			this.setConsts();
			this.refreshData();
			this.setEvents();
			return this;
		},
		setConsts: function(){
			this.content =  {
				obj: null,//объект контента
				width: null,
				height: null,
				offset: {
					left: null,//offset от края экрана
					top: null
				}
			};
			this.element = {
				obj: null,//объект ползунка
				width: null,
				height: null
			};

			this.content.obj = g(this.settings.content);
			this.element.obj = element.firstChild(this.content.obj);

			this.documentObj = this.content.obj.ownerDocument;
		},
		setSettings: function(settings){
			this.settings = {
				max: {x: 0, y: 10},
				content: null, //id или объект контента
				onChange: function(){},
				onDragEnd: function(){}
			};
			functions.mergeObjects(this.settings, settings);

			if (!this.settings.max.x) this.settings.max.x = 0;
			if (!this.settings.max.y) this.settings.max.y = 0;
		},
		refreshData: function(){
			var $elementObj = $(this.element.obj);
			this.element.width = $elementObj.outerWidth(true);
			this.element.height = $elementObj.outerHeight(true);

			var $contentObj = $(this.content.obj);
			this.content.width = $contentObj.width() - this.element.width;
			this.content.height = $contentObj.height() - this.element.height;

			this.content.offset = $(this.content.obj).offset();

			this.oneStepPxX = this.settings.max.x ? (this.content.width / this.settings.max.x) : 0;
			this.oneStepPxY = this.settings.max.y ? (this.content.height / this.settings.max.y) : 0;

			this.mouseOffset = [0, 0];//смещение мышки(что бы тянуть за то, за что схатили(или за центр)), а не за верхний левый угол
			this.hasNewStep = false;//не вызывать функцию end, если элемент не двигался
		},
		setEvents: function(){
			var self = this;
			this.content.obj.onmousedown = function(e){self.mouseDownContent(self.fixEvent(e))};
			this.element.obj.onmousedown = function(e){self.mouseDownElement(self.fixEvent(e))};
		},
		mouseDownContent: function(event){
			if (event.which != 1) return;

			this.setMouseOffset(event);
			this.slideToCoords.apply(this, this.getMousePosition(event));
			this.startSlide();
			event.stopPropagation();
		},
		mouseDownElement: function(event){
			if (event.which != 1) return;

			this.setMouseOffset(event);
			this.startSlide();
			event.stopPropagation();
		},
		mouseMove: function(event){
			if (event.which != 1) return;

			this.slideToCoords.apply(this, this.getMousePosition(event));
			event.stopPropagation();
		},
		mouseUp: function(event){
			this.stopSlide();
			if (this.hasNewStep){
				this.callSettingsEvent(this.getMousePosition(event), 'onDragEnd');
				this.hasNewStep = false;
			}
		},
		setMouseOffset: function(event){
			var mousePosition = [event.pageX - this.content.offset.left, event.pageY - this.content.offset.top];
			var elementPosition = $(this.element.obj).position();

			var mouseUnderWidth = mousePosition[0] >= elementPosition.left && mousePosition[0] <= (elementPosition.left + this.element.width);
			var mouseUnderHeight = mousePosition[1] >= elementPosition.top && mousePosition[1] <= (elementPosition.top + this.element.height);

			if (mouseUnderWidth && mouseUnderHeight){
				this.mouseOffset[0] = mousePosition[0] - elementPosition.left;
				this.mouseOffset[1] = mousePosition[1] - elementPosition.top;
			} else {
				this.mouseOffset[0] = this.element.width / 2;
				this.mouseOffset[1] = this.element.height / 2;
			}
		},
		getMousePosition: function(event){
			return [
				event.pageX - this.content.offset.left - this.mouseOffset[0],
				event.pageY - this.content.offset.top - this.mouseOffset[1]
			];
		},
		//запускается, когда мы кликаем на контент(или на ползунок)
		startSlide: function(){
			var self = this;
			// эти обработчики отслеживают процесс и окончание переноса
			this.documentObj.onmousemove = function(e){self.mouseMove(self.fixEvent(e))};
			this.documentObj.onmouseup = function(e){self.mouseUp(self.fixEvent(e))};
			// отменить перенос и выделение текста при клике на тексте
			this.documentObj.ondragstart = function() { return false };
			this.documentObj.body.onselectstart = function() { return false };
		},
		stopSlide: function(){
			// очистить обработчики, т.к перенос закончен
			this.documentObj.onmousemove = null;
			this.documentObj.onmouseup = null;
			this.documentObj.ondragstart = null;
			this.documentObj.body.onselectstart = null;
		},
		slideTo: function(stepX, stepY){
			if (this.isNewStep(stepX, stepY)){
				var coords = this.getCoords(stepX, stepY);
				this.moveElement.apply(this, coords);

				this.hasNewStep = true;
				this.callSettingsEvent(coords, 'onChange');
			}
		},
		callSettingsEvent: function(mousePosition, name){
			var coords = this.getFixCoords.apply(this, mousePosition);
			var step = this.getSteps.apply(this, coords);
			this.settings[name]({step: {x: step[0], y: step[1]}, coords: {x: coords[0], y: coords[1]}});
		},
		slideToCoords: function(pxX, pxY){
			this.slideTo.apply(this, this.getSteps(pxX, pxY));
		},
		moveElement: function(pxX, pxY){
			if (this.settings.max.x)
				this.element.obj.style.left = pxX + 'px';

			if (this.settings.max.y)
				this.element.obj.style.top = pxY + 'px';
		},
		isNewStep: function(stepX, stepY){
			var position = $(this.element.obj).position();
			var lastSteps = this.getSteps(position.left, position.top);
			return lastSteps[0] !== stepX || lastSteps[1] !== stepY;
		},
		getCoords: function(stepX, stepY){
			var coords = this.getFixSteps(stepX, stepY);
			return this.getFixCoords(Math.round(coords[0] * this.oneStepPxX), Math.round(coords[1] * this.oneStepPxY));
		},
		getSteps: function(pxX, pxY){
			var coords = this.getFixCoords(pxX, pxY);
			coords[0] = this.oneStepPxX ? Math.round(coords[0] / this.oneStepPxX) : 0;
			coords[1] = this.oneStepPxY ? Math.round(coords[1] / this.oneStepPxY) : 0;
			return this.getFixSteps.apply(this, coords);
		},
		getFixSteps: function(stepX, stepY){
			return [
				Math.max(0, Math.min(stepX, this.settings.max.x)),
				Math.max(0, Math.min(stepY, this.settings.max.y))
			];
		},
		getFixCoords: function(pxX, pxY){
			return [
				Math.max(0, Math.min(pxX, this.content.width)),
				Math.max(0, Math.min(pxY, this.content.height))
			];
		},
		fixEvent: function(event) {
			// получить объект событие для IE
			event = event || window.event;
			// добавить pageX/pageY для IE
			var html = this.documentObj.documentElement, body = this.documentObj.body;
			if (event.pageX == null && event.clientX != null) {
				event.pageX = event.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
			}
			if (event.pageY == null && event.clientY != null) {
				event.pageY = event.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
			}
			// добавить which для IE
			if (!event.which && event.button) event.which = event.button == 1 ? 1 : ( event.button == 2 ? 3 : ( event.button == 4 ? 2 : 0 ));
			if (!event.stopPropagation) event.stopPropagation = function(){this.cancelBubble = true};
			return event
		}
	};
}());