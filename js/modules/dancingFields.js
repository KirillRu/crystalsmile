(function() {
	dancingFields = function(options) {
		return new _DancingFields(options);
	};

	function _DancingFields() {}
	_DancingFields.prototype = {
		init: function(options){
			this.setConst(options);
			this.setEvents();
		},
		getValue: function() {
			var counts = this.buttons.length;
			for(var i=0; i < counts; i++) {
				if (this.buttons[i].checked) return this.buttons[i].value;
			}
		},
		showFields: function() {
			var counts = this.siblingRows.length;
			for(var i=0; i < counts; i++) {
				var $elem = $(this.siblingRows[i]);
				if ($elem.find('[name ^= "' + this.alias + '[' + this.value + ']"]').length) $elem.show();
				else $elem.hide();
			}

		},
		setConst: function(options) {
			this.name = options.name;
			this.alias = options.alias;
			this.buttons = document.getElementsByName(this.name);
			var $thisRow = $(this.buttons).closest('.form-edit__row');//текущая строка с кнопками для выбора
			this.row = $thisRow.get(0);
			this.value = this.getValue();
			this.siblingRows = [];
			var self = this;
			if (options.alias) {
				var countRows = 0;
				$thisRow.siblings().each(function(id, elem) {//пробегаемся по всем соседним элементам
					if ($(elem).find('[name ^= "' + self.alias + '"]').length)
						self.siblingRows[countRows++] = elem;
				});
			}
			this.showFields();
		},
		setEvents: function() {
			var self = this;
			this.row.onclick = function() {
				var value = self.getValue();
				if (value != self.value) {
					self.value = value;
					self.showFields();
				}
			};
		}
	}
}());