/*
 ���� ����� ��������� ������� ����� �������� 2� ��������:
 var script1  = scriptLoader('script1.js').load()
 scriptLoader('script2.js').callFunction(function(){script1.callFunction(function(){alert('2x script loaded')})});
 */

(function() {
	var loadedScripts = {}, scripts = false;

	/**@returns {callFunction : callFunction, load : load}*/
	scriptLoader = function(name, notJsPath) {
		if (!loadedScripts[name]) {
			var src;
			if (notJsPath) src = name; else src = '/js/' + name;
			var loaded = scriptLoaded(src);
			loadedScripts[name] = new ScriptLoader({'src': src, '_loadStarted': loaded, '_loaded': loaded});
		}
		return loadedScripts[name];
	};

	function scriptLoaded(src) {
		var len;
		if (!scripts) {
			var headNodes = document.getElementsByTagName('head')[0].childNodes, loaded = false;
			scripts = [];
			for (len = headNodes.length; len--;) { //���� ��� �������
				if ((headNodes[len].nodeType == 1) && (headNodes[len].tagName == 'SCRIPT')) {
					if ((headNodes[len].src == src) && (!loaded)) loaded = true; //����������� ���� src
					scripts.push(headNodes[len]);
				}
			}
			return loaded;
		}
		for (len = scripts.length; len--;)
			if (scripts[len].src == src) return true;
		return false;
	}

	function ScriptLoader(params) {
		this._loaded = false;
		this._loadStarted = false;
		this.src = '';
		this._callbackStack = [];
		var i;
		for (i in params)
			if (params[i]) this[i] = params[i];
		return this;
	}

	ScriptLoader.prototype = {
		callFunction: function(cbFunction) {
			if (this._loaded) $(document).ready.call(this, cbFunction);//���� ������� ������� �� �������� �������
			else {
				if (this._loadStarted) this._pushStack(cbFunction); //� ����� �������, ������� ������ ���� ������� ����� ��������
				else {
					this._pushStack(cbFunction);
					this.load();
				}
			}
			return this;
		},

		_callStack: function() {
			var item;
			while (item = this._callbackStack.shift()) item.call(this);
		},

		_pushStack: function(cbFunction) {
			this._callbackStack.push(cbFunction);
		},

		load: function() {
			if (!this._loadStarted) {
				var head = document.getElementsByTagName('head'), script = document.createElement('SCRIPT'), self = this;
				script.type = 'text/javascript';
				script.async = true;
				script.src = this.src;
				head[0].appendChild(script);
				this._loadStarted = true;
				script.onreadystatechange = function() {  //ie
					if ((this.readyState == 'complete') || (this.readyState == 'loaded')) {
						self._loaded = true;
						self._callStack();
						return true;
					}
				};
				script.onload = function() {
					self._loaded = true;
					self._callStack();
					return true;
				};
			}
			return this;
		}
	};
}());