(function() {
	selectToDiv = function(options) {
		return new _SelectToDiv(options);
	};

	function _SelectToDiv() {}
    _SelectToDiv.prototype = {
		init: function(options){
			this.setConst(options);
			this.setEvents();
            this.drawing();
		},
		setConst: function(options) {
            var select = g(options.id);
            var nexElem = element.nextElement(select);
            if (element.hasClass(nexElem, 'js_tzSelect')) element.remove(nexElem);

            this.$select = $(select);
            this.$selectBoxContainer = $('<div>', {
                id          : options.id + '_drawDiv',
           		width		: this.$select.closest('div').outerWidth(),
           		class	: 'js_tzSelect',
           		html		: '<div class="selectBox"></div>'
           	});

            this.$dropDown = $('<ul>',{class:'dropDown'});
            this.$selectBox = this.$selectBoxContainer.find('.selectBox');
		},
        drawing: function() {
            var self = this;
            this.$select.find('option').each(function(i){
          		var $option = $(this);
                var selected = this.getAttribute('selected') || 0;
          		if(selected) {
                    if ($option.data('photopath')) self.$selectBox.html('<img width="40" class="mr10" src="' + $option.data('photopath') + '" />' + $option.text());
                    else self.$selectBox.html($option.text());
                }

          		if($option.data('skip')) return true;
          		var $li = $('<li>',{
          			html: '<img src="' + $option.data('photopath') + '" />' +
                        '<span>' + $option.text() + ' - ' + $option.data('price') + ' руб/' + $option.data('ed_izm') + '.</span>'
          		});
          		$li.click(function() {
                    self.$selectBox.html($option.text());
                    self.$dropDown.trigger('hide');
                    self.$select.val($option.val());
          			return false;
          		});
                self.$dropDown.append($li);
          	});

          	this.$selectBoxContainer.append(this.$dropDown.hide());
            this.$select.hide().after(this.$selectBoxContainer);
        },
		setEvents: function() {
			var self = this;
            this.$dropDown.bind('show',function(){
           		if(self.$dropDown.is(':animated')) return false;
                self.$selectBox.addClass('expanded');
                self.$dropDown.slideDown();

           	}).bind('hide',function(){

           		if(self.$dropDown.is(':animated')) return false;
                self.$selectBox.removeClass('expanded');
                self.$dropDown.slideUp();

           	}).bind('toggle',function(){
           		if(self.$selectBox.hasClass('expanded')) self.$dropDown.trigger('hide');
           		else self.$dropDown.trigger('show');
           	});

            this.$selectBox.click(function(){
                self.$dropDown.trigger('toggle');
           		return false;
           	});

            this.$dropDown.on('blur',function(){
                self.$dropDown.trigger('hide');
            });

            $(document).click(function(){
                self.$dropDown.trigger('hide');
            });
        }
	}
}());
