(function() {
	sliderImgMainPage = function(settings){
		var slider = new __sliderImgMainPage();
		slider.setSettings(settings);
		return {
			run: slider.run.bind(slider)
		};
	};


	var __sliderImg = function(){};
	__sliderImg.prototype = {
		settings: {
			idContent: 'js_sliderImgContent',
			idSubCatsContent: 'js_subCatsContent'
		},
		items: {},
		subCats: {},
		count: 0,
		buttons: {},
		imgs: {},
		lastId: null,
		setSettings: function(settings){
			for(var i in settings)
				if (settings.hasOwnProperty(i)) this.settings[i] = settings[i];
		},
		setItems: function(items){
			this.items = items;
			this.count = Object.keys(items).count;
		},
		setSubCats: function(subCats) {
			this.subCats = subCats;
		},
		run: function(){
			this.setConsts();
			this.setEvents();
			this.createImgs();
			this.createButtons();
			this.firstSlide();
		},
		setConsts: function(){
			this.content = g(this.settings.idContent);
			this.subCatsContent = g(this.settings.idSubCatsContent);
			this.imgContent = $(this.content).children('.js_imgsContent').get(0);
		},
		setEvents: function(){
			$(this.content).
				on('click', '.js_slideLeft', this.slideLeft.bind(this)).
				on('click', '.js_slideRight', this.slideRight.bind(this))
		},
		slideLeft: function(){
			var keys = Object.keys(this.items);
			var index = keys.indexOf(this.lastId);
			if (index === 0) index = keys.length;

			this.slideTo(keys[index - 1]);
		},
		slideRight: function(){
			var keys = Object.keys(this.items);
			var index = keys.indexOf(this.lastId);
			if (index === keys.length - 1) index = -1;

			this.slideTo(keys[index + 1]);
		},
		slideTo: function(id){
			if (this.lastId !== id){
				this.showImg(id);
				this.setActiveButton(id);
				this.showSubCats(id);
				this.lastId = id;
			}
		},
		firstSlide: function(){
			var key = Object.keys(this.items);
			key = key[Math.floor(Math.random() * key.length)];

			this.imgContent.appendChild(this.imgs[key]);
			this.showSubCats(key);
			this.setActiveButton(key);
			this.lastId = key;
		},
		setActiveButton: function(id){
			if (this.lastId) $(this.buttons[this.lastId]).removeClass('act');
			$(this.buttons[id]).addClass('act');
		},
		showSubCats: function(id){
			this.subCats[id].style.display = 'none';
			this.subCatsContent.appendChild(this.subCats[id]);
			$(this.subCats[id]).fadeIn(1200);
		},
		showImg: function(id){
			this.imgs[id].style.display = 'none';
			this.imgContent.appendChild(this.imgs[id]);
			$(this.imgs[id]).fadeIn(1200);
		},
		createImgs: function(){
			this.everyItem(function(id){
				var a = document.createElement('a');
				a.href = this.items[id]['href'];
				a.title = this.items[id]['name'];
				var img = document.createElement('img');
				img.src = this.items[id]['src'];
				img.alt = this.items[id]['name'];
				a.appendChild(img);

				this.imgs[id] = a;
			});
		},
		createButtons: function(){
			var buttonsContainer = $(this.content).children('.js_buttonsContent').get(0);
			this.everyItem(function(id){
				this.buttons[id] = this.createButton(id);
				buttonsContainer.appendChild(this.buttons[id]);
			});
		},
		createButton: function(id){
			var self = this;
			var button = document.createElement('a');
			button.appendChild(document.createElement('span'));
			button.onclick = function(){
				self.slideTo(id);
			};
			return button;
		},
		everyItem: function(cb){
			for(var i in this.items)
				if (this.items.hasOwnProperty(i)) {
					cb.call(this, i, this.items[i]);
				}
		}
	};

	var __sliderImgMainPage = function(){
		//устанавливаем items
		var items = {}, subCats = {};

		$('#js_subCatsContent').find('ul[data-id-category]').each(function(i, el){
			var $el = $(el), idCategory = $el.data('idCategory');
			items['_' + idCategory] = {
				href: $el.data('href'),
				name: $el.data('name'),
				src: '/img/mp_pictures/' + idCategory + '.jpg'
			};
			subCats['_' + idCategory] = el;
		});
		this.setItems(items);
		this.setSubCats(subCats);
	};
	__sliderImgMainPage.prototype = new __sliderImg();
}());