print_r = function(){

	var $document = $(document);

	$(document).on('mousedown', '.js_portalPrint_rContentItem', function(e){
		if (e.which == 2){
			log.openSql($(this).find('.js_print_rText').html());
			return false;
		}
	});

	function toggle(id){
		$('#' + id).slideToggle('fast');
	}

	function toggleContent(target){
		var $content = $(target).closest('.js_portalPrint_rContentItem');
		if ($content.height() >= 26){
			if ($content.hasClass('js_closed')) {
				$content.css('height', '');
				$content.removeClass('js_closed');
			}
			else $content.animate({height: 26}, function(){$content.addClass('js_closed')});
		}
		return false;
	}

	return {
		toggle: toggle,
		toggleContent: toggleContent
	}
}();