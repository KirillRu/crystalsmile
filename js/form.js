formFunctions = function(){

	function selectCategoryUpdate(value, options){
		var input = g(options['idName']), $container = $(input).closest('.js_formBlocks');

		input.name = ''; //чтобы функция getParameters не вносила в строку ненужный input

		options['data'] = options['data'] ? (options['data'] + '&') : '';
		options['data'] += element.getParameters($container.get(0)) + '&' + options['name'] + '=' + value;

		return ajax.replace(options['url'], options['data'], $container);
	}

	function selectChildUpdate(value, options){
		options['data'] = options['data'] ? (options['data'] + '&') : '';
		options['data'] += 'selectChild[value]=' + value + '&' + 'selectChild[name]=' + options['name'] + '&' + 'selectChild[table]=' + options['table'];

		var $container = $(g(options['idName'])).closest('.select-child'), $toggle = $container.children('.js_wsToggle');

		ajax.cbOnSuccess(options['url'] || 'form/selectChildAjax.ajax', options['data'], function(r){
			if ($toggle.length){//window_search_select_child
				$container.replaceWith($(r).filter('.select-child').append($toggle));
			} else {
				$container.replaceWith(r);
			}
		});
	}

	function selectClick(elem, id, val){
		var $elem = $(elem);
		$elem.addClass('act').siblings().removeClass('act');
		g(id).value = val;
	}

	//====
	//Добавляем класс act к выбранной вкладке. Показываем/скрываем блок
	function toggleVideoBlock(link, switchForm){
		var $block = $(link).closest('.js_videoBlock');
		$(link).addClass('act').siblings().removeClass('act');
		$block.find('.js_toggleBlock').hide().filter('.js_toggleBlock_' + switchForm).show();
		$block.find('.js_switchForm:first').val(switchForm);
	}
	//====

	return {
		selectChildUpdate: selectChildUpdate,
		selectCategoryUpdate: selectCategoryUpdate,
		selectClick: selectClick,
		toggleSelectBlock: toggleSelectBlock,
		toggleVideoBlock: toggleVideoBlock
	};
}();