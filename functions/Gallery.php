<?php
/*Created by Edik (30.10.14 14:35)*/
function Gallery__getFotoParams($tableFotos){
	$settings = Fotos__getFotoSettings($tableFotos);
	$sizeNames = array_keys($settings['labels']);
	$galleryParams = array(
		'minSizeName' => current($sizeNames),
		'maxSizeName' => array_pop($sizeNames),
	);
	$galleryParams['minWidth'] = $settings['labels'][$galleryParams['minSizeName']]['width'];
	$galleryParams['maxWidth'] = $settings['labels'][$galleryParams['maxSizeName']]['width'];
	$galleryParams['maxHeight'] = $settings['labels'][$galleryParams['maxSizeName']]['height'];
	return $galleryParams;
}