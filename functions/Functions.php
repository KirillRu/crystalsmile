<?php
function Functions__getQueryStr($data, $ignore = array()) {
	if (!$data) return '';
	unset($data['q']);
	$data = array_diff_key($data, array_flip((array)$ignore));
	return $data ? urldecode(http_build_query($data)) : '';
}

function Functions__insertInFile($pathFile, $data) {
	return $data ? file_put_contents($pathFile, implode("\t~!~\t", $data) . "~?~\n", FILE_APPEND) : false;
}

function Functions__log($fileName, $data) {
	if (!file_exists(LOG_PATH . PROJECT_NAME)) Functions__mkdirRecursive(LOG_PATH . PROJECT_NAME);
	return Functions__insertInFile(LOG_PATH . PROJECT_NAME . '/' . $fileName, $data);
}

/**Рекурсивно объединяет массивы. Значения массивов, переданных в конце заменят значения массивов, переданных в начале*/
function Functions__mergeArrays(...$arrays) {
	if (count($arrays) > 2){
		$firstArray = array_shift($arrays);
		$secondArray = call_user_func_array('Functions__mergeArrays', $arrays);
	}
	else list($firstArray, $secondArray) = $arrays;
	foreach ($secondArray as $k2 => $v2) {
		if (!empty($firstArray[$k2]) && is_array($firstArray[$k2]) && is_array($v2)) $firstArray[$k2] = Functions__mergeArrays($firstArray[$k2], $v2);
		elseif ($v2 === null) unset($firstArray[$k2]);
		else $firstArray[$k2] = $v2;
	}
	return $firstArray;
}

function Functions__arrayMes($array) {
	foreach ($array as &$val) $val = (int)$val;
	return $array;
}

function Functions__arrayMest($array) {
	foreach ($array as &$val) $val = Text::get()->mest($val);
	return $array;
}

function Functions__getFromStructure($fieldName, $table){
	static $data = array();
	$table = Text::get()->strToLower($table); //иногда передаём сюда moduleName, который может начинаться с заглавной буквы
	if (!isset($data[$table])){
		require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
		$structure = new ModelsStructureFrom_files($table);
		$data[$table] = array();
		if ($val = $structure->getTable()) $data[$table]['table'] = $val;
		if ($val = $structure->getId()) $data[$table]['id'] = $val;
		if ($val = $structure->getTableFotos()) $data[$table]['tableFotos'] = $val;
		if ($val = $structure->getCategoryName()) $data[$table]['categoryName'] = $val;
	}
	return (isset($data[$table][$fieldName]) ? $data[$table][$fieldName] : null);
}

function Functions__getCacheFilename($folder, $key){
	$intkey = (int)$key;
	$dir = Cfg__get('path', 'DOCUMENT_ROOT') . 'cache' . $folder . ($intkey > 0 ? floor($intkey/10000).'/' : '');
	if (!$key) $key = '0/0';//если ключ числовой, то может произойти ситуация когда в папке $folder скрипт одновременно может создавать папку "0" и файл "0"
	$result = $dir . $key;
	if (_isShops()) $result .= '_' . str_replace('http://', '', ShopsUser::get()->shop_url);
	return $result;
}

/** передаем массив, в котором у значений есть id_parent. Ключи массива - это id записи(id_parent указывает на этот id)
 *  если дерево начинается не с id_parent = 0, то передаем id_parent.
 * @param $items
 * @param int $idParent
 * @return array Массив, в котором все дочерние элементы перемещены в children
 */
function Functions__buildTree($items, $idParent = 0){
	$hash = [];
	foreach ($items as $i=>$item) {
		if (!isset($items[$i]['children'])) $items[$i]['children'] = [];
		$hash[$i] =& $items[$i];
	}

	$root = [];
	foreach ($items as $i=>$item) {
		if (($item['id_parent'] != $idParent) && isset($hash[$item['id_parent']])) {
			$items[$i]['parent'] =& $hash[$items[$i]['id_parent']];
			$hash[$items[$i]['id_parent']]['children'][$i] =& $items[$i];
		}
		if ($items[$i]['id_parent'] == $idParent) $root[$i] =& $items[$i];
	}
	return $root;
}

/*Создает директорию, если она не создана, с правами 775*/
function Functions__mkdirRecursive($dirName){
    $dir = '';
    foreach (explode('/', $dirName) as $part) {
        $dir .= $part . '/';
        if (!is_dir($dir) && $part !== '') {
	        if (@mkdir($dir, 0775)){
		        chmod($dir, 0775);
	        } else {
		        $error = error_get_last();
		        trigger_error('Ошибка mkdir. ' . $dir . '. msg: ' . $error['message']);
		        break;
	        }
        }
    }
}