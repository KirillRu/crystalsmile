<?php


function Date__getDateInterval($dateInterval) {
	$interval = "";
	eval("\$interval = ".preg_replace("/(\d+) - (\d+)/", "date('d.m.Y', \$1).' - '.date('d.m.Y', \$2);", $dateInterval));
	return $interval;
}

function Date__getDateTimeInterval($dateInterval) {
	$interval = "";
	eval("\$interval = ".preg_replace("/(\d+) - (\d+)/", "date('d.m.Y H:i', \$1).' - '.date('d.m.Y H:i', \$2);", $dateInterval));
	return $interval;
}

function Date__getTimeInterval($dateInterval) {
	$interval = "";
	eval("\$interval = ".preg_replace("/(\d+) - (\d+)/", "date('H:i', \$1).' - '.date('H:i', \$2);", $dateInterval));
	return $interval;
}

function Date___explodeDate($date) {
	return array("day" => date("j", $date), "month" => date("n", $date), "year" => date("Y", $date));

}

function Date___getMontNames($p) {
	$montNames = array();
	$montNames['i'] = array();				$montNames['r'] = array();
	$montNames['i'][1] =  "январь";			$montNames['r'][1] =  "января";
	$montNames['i'][2] =  "февраль";		$montNames['r'][2] =  "февраля";
	$montNames['i'][3] =  "март";			$montNames['r'][3] =  "марта";
	$montNames['i'][4] =  "апрель";			$montNames['r'][4] =  "апреля";
	$montNames['i'][5] =  "май";			$montNames['r'][5] =  "мая";
	$montNames['i'][6] =  "июнь";			$montNames['r'][6] =  "июня";
	$montNames['i'][7] =  "июль";			$montNames['r'][7] =  "июля";
	$montNames['i'][8] =  "август";			$montNames['r'][8] =  "августа";
	$montNames['i'][9] =  "сентябрь";		$montNames['r'][9] =  "сентября";
	$montNames['i'][10] = "октябрь";		$montNames['r'][10] = "октября";
	$montNames['i'][11] = "ноябрь";			$montNames['r'][11] = "ноября";
	$montNames['i'][12] = "декабрь";		$montNames['r'][12] = "декабря";
	if (!array_key_exists($p, $montNames)) $p = 'i';
	return $montNames[$p];
}

function Date___getDayName($day,$p = 'i') {
	$montNames = array();
	$number = substr($day,-1);
	$montNames['i'] = array();			$montNames['r'] = array();
	$montNames['i'][1] = "день";		$montNames['r'][1] = "дня";
	$montNames['i'][2] = "дня";			$montNames['r'][2] = "дней";
	$montNames['i'][3] = "дня";			$montNames['r'][3] = "дней";
	$montNames['i'][4] = "дня";			$montNames['r'][4] = "дней";
	$montNames['i'][5] = "дней";		$montNames['r'][5] = "дней";
	$montNames['i'][6] = "дней";		$montNames['r'][6] = "дней";
	$montNames['i'][7] = "дней";		$montNames['r'][7] = "дней";
	$montNames['i'][8] = "дней";		$montNames['r'][8] = "дней";
	$montNames['i'][9] = "дней";		$montNames['r'][9] = "дней";
	$montNames['i'][0] = "дней";		$montNames['r'][0] = "дней";
	if (!array_key_exists($p, $montNames)) $p = 'i';
	return $montNames[$p][$number];
}

function Date__getStringDateInterval($start, $end) {
	$start = Date___explodeDate($start);
	$end = Date___explodeDate($end);
	$months = Date___getMontNames('r');

	$result = array();
	foreach($start as $key=>$value)
		$result[$key] = $start[$key]==$end[$key];

	if ($result["year"]) {
		if ($result["month"]) $date = ($result["day"]) ? $end["day"] : "С " . $start["day"] . " по " . $end["day"];
		else $date = "С " . $start["day"] . " " . $months[$start["month"]] . " по " . $end["day"];
	}
	else $date = "С " . $start["day"] . " " . $months[$start["month"]] . " " . $start["year"] . " г. по " . $end["day"];

	return $date . " " . $months[$end["month"]] . " " . $end["year"] . " г.";

}

//этой функции не будет
//function Date__getSmartDateInterval($dateInterval) {
//	if (empty($dateInterval)) return null;
//	$dates = explode("-", str_replace(" ", "", trim($dateInterval)));
//	return Date__getStringDateInterval($dates[0], $dates[1]);
//}

//function Date__getDateIntervalRealTime($start, $end, $checkStartDate = true) {
//	$time = time();
//	if ((($checkStartDate)&&($start < $time)&&($end > $time))||((!$checkStartDate)&&($end > $time)))
//		return 'до '.date("d.m.Y", $end);
//
//	return 'c '.date("d.m.Y", $start).' по '.date("d.m.Y", $end);
//}

function Date__getMontNames($p = 'i') {
	$mount = array();
	foreach(Date___getMontNames($p) as $value)
		$mount[] = $value;
	return $mount;
}

function Date__getMonthName($numberMonth,$p = 'i') {
	$month = Date__getMontNames($p);
	return $month[intval($numberMonth) - 1];
}

function Date__timeDifferent($beginTime, $endTime){
    if ($endTime < $beginTime){
        $tmp = $endTime;
        $endTime = $beginTime;
        $beginTime = $tmp;
    }
    $diff = array();
    $d1 = array(date('Y', $beginTime), date('m', $beginTime), date('d', $beginTime), date('H', $beginTime), date('i', $beginTime), date('s', $beginTime));
    $d2 = array(date('Y', $endTime), date('m', $endTime), date('d', $endTime), date('H', $endTime), date('i', $endTime), date('s', $endTime));

    $md1 = array(31, $d1[0]%4||(!($d1[0]%100)&&$d1[0]%400)?28:29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    $md2 = array(31, $d2[0]%4||(!($d2[0]%100)&&$d2[0]%400)?28:29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    $min_v = array(NULL, 1, 1, 0, 0, 0);
    $max_v = array(NULL, 12, $d2[1]==1?$md2[11]:$md2[$d2[1]-2], 23, 59, 59);
    for($i=5; $i>=0; $i--) {
        if($d2[$i]<$min_v[$i]) {
            $d2[$i-1]--;
            $d2[$i]=$max_v[$i];
        }
        $diff[$i] = $d2[$i]-$d1[$i];
        if($diff[$i]<0) {
            $d2[$i-1]--;
            $i==2 ? $diff[$i] += $md1[$d1[1]-1] : $diff[$i] += $max_v[$i]-$min_v[$i]+1;
        }
    }
    return array_reverse($diff);
}

function Date__getPeriodName($beginTime, $endTime, $y=true, $m=true){
	$s = array();
	$diffTime = Date__timeDifferent($beginTime, $endTime);
	if (!$y){
		$diffTime[1] += $diffTime[0]*12;
		$diffTime[0] = 0;
	}
	//_print_r($diffTime);
	if ($diffTime[0] > 0) $s[] = $diffTime[0].' '.Text::get()->endOfWord($diffTime[0], 'год', 'года', 'лет');
	if ($diffTime[1] > 0) $s[] = $diffTime[1].' '.Text::get()->endOfWord($diffTime[1], 'месяц', 'месяца', 'месяцев');
	if ($diffTime[2] > 0) $s[] = $diffTime[2].' '.Text::get()->endOfWord($diffTime[2], 'день', 'дня', 'дней');
	return implode(' ', $s);
}

function Date__getHours($sec) {
	$h = floor($sec/3600);
	if ($h < 100)
		return sprintf("%02d", $h);

	return $h;
}



?>