<?php
/*Created by Edik (12.08.2015 15:55)*/
/**
 * @param string $pathFile строка вида 'Statistic/detail_page.log'.
 * @return bool|int
 */
function Temporary__preparePath($pathFile){
	$pathFile = TEMPORARY_PATH . $pathFile;

	//создаем папку(если не существует)
	$folder = substr($pathFile, 0, strrpos($pathFile, '/'));
	if (!file_exists($folder)) Functions__mkdirRecursive($folder);

	return $pathFile;
}