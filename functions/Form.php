<?php
/*Created by Edik (18.07.14 12:08)*/
function Form__getName($nameEn, $alias = null) {
	return !empty($alias) ? ($alias . '[' . $nameEn . ']') : $nameEn;
}

function Form__getId($name, $postfix = '') {
	return str_replace(array('[', ']'), array('_', ''), $name) . $postfix;
}

function Form__getAttributesString(array $attributes){
	$result = '';
	foreach($attributes as $k => $attribute) $result .= $k.'="'.htmlentities($attribute).'" ';
	return $result;
}