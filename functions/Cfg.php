<?php
function Cfg__get($arrName, $key = null, $default = null, $set = false) {
	static $_cfg = array();
	if (!isset($_cfg[$arrName])){
		$_cfg[$arrName] = array();
		if (file_exists(CFG_PATH . 'config_' . $arrName . '.php'))
			$_cfg[$arrName] = include(CFG_PATH . 'config_' . $arrName . '.php');
		if (defined('PROJECT_NAME') && file_exists(CFG_PATH . PROJECT_NAME . '/config_' . $arrName . '.php'))
			$_cfg[$arrName] = Functions__mergeArrays($_cfg[$arrName], include(CFG_PATH . PROJECT_NAME . '/config_' . $arrName . '.php'));
	}
	if ($key === null) return isset($_cfg[$arrName]) ? $_cfg[$arrName] : $default;
	if ($set) $_cfg[$arrName][$key] = $default;
	return isset($_cfg[$arrName][$key]) ? $_cfg[$arrName][$key] : $default;
}
