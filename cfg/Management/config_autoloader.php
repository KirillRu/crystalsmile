<?php
/**Для autoloader в security. Что подключать когда не находит класс*/
require_once(ROOT_DIR . 'js/modules/ckeditor/kcfinder/security.php');
$autoload = [ 'ManagementListManager' => CLASSES_PATH . 'Management/Modules/List/Manager.php',];

if (defined('is_kcfinder')&&is_kcfinder) {
	$autoload['Path'] = null;
	$autoload['Text'] = null;
	$autoload['uploader_extended'] = KCFINDER_ROOT_DIR . "core/uploader_extended.php";
	$autoload['browser_extended'] = KCFINDER_ROOT_DIR . "core/browser_extended.php";
	$autoload['type_img']=KCFINDER_ROOT_DIR . "core/types/type_img.php";
	$autoload['type_mime']=KCFINDER_ROOT_DIR . "core/types/type_mime.php";
	$autoload['gd']=KCFINDER_ROOT_DIR . "lib/class_gd.php";
	$autoload['input']=KCFINDER_ROOT_DIR . "lib/class_input.php";
	$autoload['zipFolder']=KCFINDER_ROOT_DIR . "lib/class_zipFolder.php";
	$autoload['dir']=KCFINDER_ROOT_DIR . "lib/helper_dir.php";
	$autoload['file']=KCFINDER_ROOT_DIR . "lib/helper_file.php";
	$autoload['httpCache']=KCFINDER_ROOT_DIR . "lib/helper_httpCache.php";
	$autoload['path']=KCFINDER_ROOT_DIR . "lib/helper_path.php";
	$autoload['text']=KCFINDER_ROOT_DIR . "lib/helper_text.php";
}
return $autoload;