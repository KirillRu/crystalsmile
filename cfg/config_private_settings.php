<?php
/*Created by Edik (14.01.15 10:55)*/
return array(
	'money_transfers' => array(
		/*Идентификаторы пользователя jCat на всех порталах*/
		'jCat' => array(
			'PromportalSu' => 269461,
			'UniboRu' => 152853,
			'DomovedSu' => 17251,
			'ElectrogidRu' => 26315,
			'SpeceriaRu' => 5790,
			'StroyboardSu' => 41033
		),
		'kirill' => array(
			'PromportalSu' => 178608,
			'UniboRu' => 1,
			'DomovedSu' => 5847,
			'ElectrogidRu' => 21278,
			'SpeceriaRu' => 1,
			'StroyboardSu' => 20127
		)
	)
);