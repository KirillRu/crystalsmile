<?php
/*
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com (09.06.14 15:22)
*/

return array(
	'enable' 		=> 1,
	'host' 			=> 'localhost',
	'port' 			=> 11211,
	'time_regular' 	=> 43200,
	'time_temporary' => 3600,
	'timeout'       => 1
);