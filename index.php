<?php
define('isMyIp', true);
$path = urldecode($_SERVER['REQUEST_URI']);
if (strpos($path, '/') === 0) $path = substr($path, 1);
if (substr($path, 0, 10) == 'management') {
	$path = substr($path, 11);
	if (substr($path, 0, 8) == 'kcfinder') define('is_kcfinder', 1);
	else define('is_kcfinder', 0);
    define('PROJECT_NAME', 'Management');
}
else define('PROJECT_NAME', 'Zubi');
require_once(dirname(__FILE__) . '/system/define.php');
require_once(dirname(__FILE__) . '/system/require.php');
require_once(dirname(__FILE__) . '/system/system.php');

$ind = strpos($path, "?");
if ($ind !== false) $path = substr($path, 0, $ind);

try {
	require_once(PROJECT_PATH . 'Sitemap/Sitemap.php');
	$actionName = PROJECT_NAME . 'Sitemap';
	$action = new $actionName(['path' => $path]);
	$view = $action->getAction()->run();
	$view->display();
} catch (Exception $e){
	_runException($e);
}