<?php
/*Created by Кирилл (05.02.2016 22:26)*/
ini_set('display_errors' , 1);//не показываем стандартные фатальные php ошибки(не фатальные все-равно не покажутся из-за обработчика)
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

define('ROOT_DIR',          dirname(dirname(__FILE__)) . '/');
define('CLASSES_PATH', 	    ROOT_DIR . 'classes/');
define('FUNCTIONS_PATH',    ROOT_DIR . 'functions/');
define('PROJECT_PATH',      CLASSES_PATH . PROJECT_NAME . '/');
define('LOG_PATH',          dirname(ROOT_DIR) . '/_log/');
define('TEMPORARY_PATH',    ROOT_DIR . 'temporary/');
define('CFG_PATH',          ROOT_DIR . 'cfg/');
define('STRUCTURES_PATH',   ROOT_DIR . 'structures/');
if (!defined('DOMAIN')) define('DOMAIN',            'crystalsmile.ru');
define('SITE_ROOT',         'http://' . DOMAIN . '/');
define('SITE_ENCODING',     'utf-8');