<?php
/*Created by Кирилл (05.02.2016 22:26)*/
mb_regex_encoding(SITE_ENCODING);
mb_internal_encoding(SITE_ENCODING);
session_start();

/**Функция подключает классы, если при их вызове они не были найдены*/
spl_autoload_register(function($className){
	$file = Cfg__get('autoloader', $className);
	if ($file) require_once($file);
});

function _getIp() {
	if (!empty($_SERVER['HTTP_X_REAL_IP'])) $ips = explode(',', $_SERVER['HTTP_X_REAL_IP']);
	elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) $ips = explode(',', $_SERVER['HTTP_CLIENT_IP']);
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
	else $ips = [getenv('REMOTE_ADDR')];
	return trim(array_shift($ips));
}

function _isMyIp() {
	if (isset($_GET['un'])) return true;
	if (isset($_GET['re'])) return false;
	if (defined('isMyIp')) return isMyIp;//имеется define на тестовом сервере

	$ourIps = [
	];
	if (in_array(getenv('REMOTE_ADDR'), $ourIps)) return true;

	return false;
}

function _isDebug() {
	if (isset($_GET['debug'])) return true;
	//на тестовом показываем все принты
	if (defined('isDebug')) return isDebug;
	if (defined('isMyIp')) return isMyIp;
	return false;
}

function _getIpSum($ip = null){
	$ipsum = (float)0;
	$ipa = explode('.', $ip?:_getIp());
	if (sizeof($ipa) == 4) $ipsum = ($ipa[0]<<24) + ($ipa[1]<<16) + ($ipa[2]<<8) + $ipa[3];
    return $ipsum;
}

function _getIpFormSum($ipSum){
	$ipSum = (float)$ipSum;
	return ($ipSum >> 24) .'.'. (($ipSum >> 16) & 0xFF) .'.'. (($ipSum >> 8) & 0xFF) .'.'. ($ipSum & 0xFF);
}

function _logError($message, $filename, $lineno, $errTypeName){
	$data = [
		'isMyIp' => _getIp(),
		'date' => date('d.m.Y H:i', time()),
		'type' => $errTypeName,
		'prev_page' => (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '',
		'page' => (!empty($_SERVER['HTTP_HOST']) && !empty($_SERVER['REQUEST_URI'])) ? $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] : '',
		'message' => $message,
	    'file' => $filename . ':' . $lineno
	];
	require_once(FUNCTIONS_PATH . '/Functions.php');
	Functions__insertInFile(LOG_PATH . PROJECT_NAME . '/phpErrors.log', $data);
}

function _runException($exception) {
	try {
		if (method_exists($exception, 'run')) $exception->run()->display();
		else {
			require_once(CLASSES_PATH . 'Exception/Portal.php');
			(new ExceptionPortal($exception->getMessage(), 0, $exception))->run()->display();
		}
	} catch (ExceptionSql $e) {
		if (!$exception instanceof ExceptionSql) _runException($e); //Ошибки sql запросов
	} catch (ExceptionPortal $e) {
		if (!$exception instanceof ExceptionPortal) _runException($e); //Ошибки портала
	} catch (ExceptionAction $e) {
		if (!$exception instanceof ExceptionAction) _runException($e); //Рисуем Exception
	} catch (Exception $e){
		SitesHeader::get()->unavailable();
		echo '<pre><h1>Страница временно недоступна</h1></pre>';
	}
}

set_error_handler(function($severity, $message, $filename, $lineno) {
	//в эту функцию попадают только не фатальные ошибки
	if (error_reporting() === 0) return true;//если перед функцией стоит @, то не пишем логи и не показываем ошибку.
	require_once(CLASSES_PATH . 'Exception/PhpWarning.php');
	$handler = new ExceptionPhpWarning($message, $severity);
	$handler->setErrorLine($filename . ':' . $lineno);
	if (!_isMyIp()) _logError($message, $filename, $lineno, $handler->getErrorType($severity));

	//не показываем пользователям обычные ошибки(варнинги, стрикты и т.п.)
	//показываем для нас все ошибки, для пользователя только фатальные
	if (_isMyIp()) $handler->run()->display();
	return true;//не показываем стандартную ошибку php
});

register_shutdown_function(function() {
	if (error_reporting() === 0) return;//если перед функцией стоит @, то не пишем логи

	if ($error = error_get_last()){
		require_once(CLASSES_PATH . 'Exception/PhpError.php');
		$handler = new ExceptionPhpError($error['message'], $error['type']);

		//с не фатальными ошибками разбирается set_error_handler
		if (!$handler->isFatalError($error['type'])) return;

		$handler->setErrorLine($error['file'] . ':' . $error['line']);

		if (!_isMyIp()) {
			_logError($error['message'], $error['file'], $error['line'], $handler->getErrorType($error['type']));
		}

		//показываем для нас все ошибки, для пользователя только фатальные
		$handler->run()->display();
    }
});

function _checkAccess($login, $passw){
    $AUTH_USER = trim(preg_replace("/[^a-z0-9]/i", "", $login));
    $AUTH_PW = trim(preg_replace("/[^a-z0-9]/i", "", $passw));

	$fileContent = CLASSES_PATH . 'Zubi/FileData/manager_users.db';
	if (file_exists($fileContent)) {
		require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Manager_users/SerializeText.php');
		foreach ((new ManagementModulesListIteratorsManager_usersSerializeText()) as $item) $users[$item['login']] = $item['passw_cache'];
		if (isset($users[$AUTH_USER])) {
			if ($users[$AUTH_USER] == '') {
				@unlink(CLASSES_PATH . 'Zubi/FileData/manager_users.db');
				$users[$AUTH_USER] = md5($AUTH_USER . $AUTH_PW);
				foreach ($users as $login=>$pw) {
					file_put_contents(CLASSES_PATH . 'Zubi/FileData/manager_users.db', $login . ':' . $pw . "\n<!>\n", FILE_APPEND);
				}
				$_SESSION["login"] = $AUTH_USER;
				setcookie('login', $AUTH_USER, time()+60*30, '/', DOMAIN);
				return true;
			}
			elseif ($users[$AUTH_USER] == md5($AUTH_USER . $AUTH_PW)) {
				$_SESSION["login"] = $AUTH_USER;
				setcookie('login', $AUTH_USER, time()+60*30, '/', DOMAIN);
				return true;
			}
		}
	}
	return false;
//	header("WWW-Authenticate: Basic realm=\"My Realm\"");
//	header("Status: 401 Unauthorized");
//	header("HTTP-Status: 401 Unauthorized");
//	echo "Ты кто такой, давай до свидания!";
//	exit();
}
