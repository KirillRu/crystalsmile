<?php
/*Created by Edik (11.07.14 9:40)*/
/*Порядок объединения настроек:
	generalFrame + настройки страницы портала + настройки модуля страницы портала
	пример: PromportalSu/settings_generalFrame + PromportalSu/settings_defaultPage + PromportalSu/DefaultPage/settings_catalog_goods
	Приоритет по возрастанию. PromportalSu/settings_generalFrame - самый низний. Все настройки правее заменяют то, что стоит левее при совпадении
*/
return array(
	/*firmBanner, path, ... - это то, что будет передаваться в generalFrame. */
	'firmBanner' => null, //если не массив, то эта часть не передаётся в generalFrame => не показывается
	'path'=>null,
	'meta'=>array(
		'classFile'=>CLASSES_PATH . 'Sites/Panels/Meta/Action/MainPage.php', //Путь до класса, который вернёт View
		'className'=>'SitesPanelsMetaActionMainPage' //Имя этого класса
	),
	'left'=>array(
		'data'=>array(//Этот массив передасться в класс(который указан в className). Здесь className,classFile нет => они есть в других настройках(при объединении они появятся)
			'partners'=>array(
				'classFile'=>CLASSES_PATH . 'Sites/Panels/Partners/Action/Banners.php',
				'className'=>'SitesPanelsPartnersActionBanners',
				'fileCache'=>array( //Если указан этот параметр, то результат положится в файловый кэш
					'folder'=>'/overall/', //Папка кэша
					'key'=>'partners', //Ключ кэша
					'replace'=>array(), //Может быть массив(ключи - что заменять, значения - на что заменять)
					/*А может быть:
					'replace'=>array(
						'classFile' => '...',
						'className' => '...'
					)
					В этот класс в функцию getAction передасться путь до файла(в котором лежит закэшированный результат). Класс должен вернуть объект View
					*/
					'lifetime'=>300 //Количество секунд, через которые обновить кэш
				),
			),
		),
	),
	'content'=>array(
		'data'=>array(//Эта дата передасться в className (которого тут нет)
			'also_regions'=>array(
				'classFile'=>CLASSES_PATH . '/Sites/Modules/Also/Action/Region.php',
				'className'=>'SitesModulesAlsoActionRegion',
				'tpl'=>CLASSES_PATH . '/Sites/Modules/Also/View/regions_mainPage.tpl',
				'countCitys'=>30,
				'countRegions'=>15,
				'data'=>array( //Эта дата передасться в SitesModulesAlsoActionRegion
					'citys'=>array(
						'classFile'=>CLASSES_PATH . '/Sites/Panels/Also/Action/Citys_mainPage.php',
						'className'=>'SitesPanelsAlsoActionCitys_mainPage',
						'data'=>array('tpl'=>CLASSES_PATH . '/Sites/Panels/Also/View/regions_mainPage.tpl'),
					),
					'regions'=>array(
						'classFile'=>CLASSES_PATH . '/Sites/Panels/Also/Action/Regions_mainPage.php',
						'className'=>'SitesPanelsAlsoActionRegions_mainPage',
						'data'=>array('tpl'=>CLASSES_PATH . '/Sites/Panels/Also/View/regions_mainPage.tpl'),
					),
				),
				'fileCache'=>array(
					'folder'=>'/alsoRegions/mainPage/',
					'key'=>'city_' . SitesRegion::get()->id_city,
					'replace'=>array(),
				),

			),
		)
	),
);