<?php
/*Created by Кирилл (08.02.2016 14:25)*/
return [
	'id' => '',
	'tableName' => 'Контент сайта',
	'table' => '',
	'fields' => [
        'project_table_fields_0' => [
            'name' => 'Текст',
            'nameEn' => 'site_text',
            'dataType' => 'serialize',
            'type' => 'multi_lines',
            'showForm' => ['requireField'=>0],
            'showManagement' => ['location'=>1],
            'idSpr' => [
                'sourceData' => 'structure',
                'table' => 'wordpanel_field',
            ],

            'idAllField' => 'project_table_fields_0',
        ],
    ]
];