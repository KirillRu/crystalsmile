<?php $structure = Array
(
    'id' => 'id_send_letter',
    'tableName' => 'Отправить сообщение',
    'categoryName' => 'Cообщения',
    'table' => 'send_letters',
    'fields' => Array
        (
            'send_letters_0' => Array
                (
                    'name' => 'Дата',
                    'nameEn' => 'date_add',
                    'dataType' => 'integer',
                    'type' => 'none',
                    'idAllField' => 'send_letters_0',
                ),

            'send_letters_1' => Array
                (
                    'name' => 'Контактное лицо',
                    'nameEn' => 'name',
                    'dataType' => 'string',
					'type' => 'text',
                    'showForm' => 'a:1:{s:12:"requireField";s:1:"1";}',
                    'idAllField' => 'send_letters_1',
                ),

            'send_letters_2' => Array
                (
                    'name' => 'E-mail',
                    'nameEn' => 'email',
                    'dataType' => 'email',
                    'type' => 'text',
                    'showForm' => 'a:1:{s:12:"requireField";s:1:"1";}',
                    'idAllField' => 'send_letters_2',
                ),

            'send_letters_3' => Array
                (
                    'name' => 'Телефон',
                    'nameEn' => 'phone',
                    'dataType' => 'string',
                    'type' => 'text',
                    'showForm' => 'a:0:{}',
                    'idAllField' => 'send_letters_3',
                ),

            'send_letters_4' => Array
                (
                    'name' => 'Текст сообщения',
                    'nameEn' => 'text',
                    'dataType' => 'string',
                    'type' => 'textarea_simple',
                    'showForm' => 'a:1:{s:12:"requireField";s:1:"1";}',
                    'idAllField' => 'send_letters_4',
                ),

            'send_letters_6' => Array
                (
                    'name' => 'Новый',
                    'nameEn' => 'new_send_letter',
                    'dataType' => 'bool',
                    'type' => 'none',
                    'idAllField' => 'send_letters_6',
                ),

        ),

);
return $structure;