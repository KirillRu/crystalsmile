<?php
/*Created by Кирилл (13.02.2016 23:37)*/
return [
	'id' => 'id_order',
	'tableName' => 'Заказы',
	'table' => 'orders',
	'fields' => [
		'goods_0' => [
			'name' => 'Товар',
			'nameEn' => 'goods',
			'dataType' => 'serialize',
			'type' => 'multi_lines',
			'showForm' => [
				'requireField'=>1,
			],
			'showManagement' => ['location'=>1,],
			'idSpr' => [
				'sourceData' => 'structure',
				'table' => 'order_goods',
			],

			'idAllField' => 'goods_0',
			'isRootField'=>1,
		],
		'orders_0' => [
			'name' => 'Дата',
			'nameEn' => 'date_add',
			'dataType' => 'integer',
			'showManagement' => ['location'=>1,],
			'type' => 'none',
			'idAllField' => 'orders_0',
			'isRootField'=>1,
		],

		'orders_1' => [
			'name' => 'Имя',
			'nameEn' => 'name',
			'dataType' => 'string',
			'type' => 'text',
			'showForm' => 'a:1:{s:12:"requireField";s:1:"1";}',
			'showManagement' => ['location'=>1,],
			'idAllField' => 'orders_1',
			'isRootField'=>1,
		],

		'orders_2' => [
			'name' => 'E-mail',
			'nameEn' => 'email',
			'dataType' => 'email',
			'type' => 'text',
			'showForm' => 'a:1:{s:12:"requireField";s:1:"1";}',
			'showManagement' => ['location'=>1,],
			'idAllField' => 'orders_2',
			'isRootField'=>1,
		],

		'orders_3' => [
			'name' => 'Телефон',
			'nameEn' => 'phone',
			'dataType' => 'string',
			'type' => 'text',
			'showForm' => 'a:1:{s:12:"requireField";s:1:"1";}',
			'showManagement' => ['location'=>1,],
			'idAllField' => 'orders_3',
			'isRootField'=>1,
		],

		'orders_6' => [
			'name' => 'Новый',
			'nameEn' => 'new_order',
			'dataType' => 'bool',
			'type' => 'none',
			'idAllField' => 'orders_6',
			'isRootField'=>1,
		],
    ]
];