<?php
/*Created by Кирилл (13.02.2016 17:23)*/
return [
	'id' => 'id_good',
	'tableName' => 'Профессиональные отбеливающие полоски',
	'table' => 'goods',
	'fields' => [
        'goods_0' => [
            'name' => 'Товар',
            'nameEn' => 'goods',
            'dataType' => 'serialize',
            'type' => 'multi_lines',
            'showForm' => ['requireField'=>0],
            'showManagement' => ['location'=>1],
            'idSpr' => [
                'sourceData' => 'structure',
                'table' => 'good',
            ],

            'idAllField' => 'goods_0',
        ],
    ]
];