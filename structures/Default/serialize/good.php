<?php
/*Created by Кирилл (13.02.2016 17:28)*/
return [
    'id' => '',
    'tableName' => 'Настройка справочника-массива',
    'table' => '',
    'fields' => [
        'good_1' => [
            'name' => 'Позиция',
            'nameEn' => 'position',
            'dataType' => 'integer',
            'type' => 'text',
            'idAllField' => 'good_1',
            'showForm' => ['requireField'=>0],
        ],
        'good_2' => [
            'name' => 'Сокр. название',
            'nameEn' => 'id_good',
            'dataType' => 'string',
            'type' => 'text',
            'idAllField' => 'good_2',
            'showForm' => ['requireField'=>0],
        ],
        'good_3' => [
            'name' => 'Наименование',
            'nameEn' => 'name',
            'dataType' => 'string',
            'type' => 'text',
            'idAllField' => 'good_3',
            'showForm' => [
	            'requireField'=>0,
			],
        ],
		'good_4' => [
			'name' => 'Цена',
			'nameEn' => 'price',
			'dataType' => 'integer',
			'type' => 'text',
			'idAllField' => 'good_4',
			'showForm' => [
				'requireField'=>0,
			],
		],
			'good_5' => [
			'name' => 'Ед. изм.',
			'nameEn' => 'ed_izm',
			'dataType' => 'string',
			'type' => 'text',
			'idAllField' => 'good_5',
			'showForm' => [
				'requireField'=>0,
			],
		],
    ],

];