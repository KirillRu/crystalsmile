<?php
/*Created by Кирилл (14.02.2016 0:42)*/
return [
    'id' => '',
    'tableName' => 'Заказные позиции',
    'table' => 'order_goods',
    'fields' => [
        'order_goods_0' => [
            'name' => 'Название',
            'nameEn' => 'id_good',
            'dataType' => 'string',
            'type' => 'selectByFile',
	        'idSpr' => [
                'sourceData' => 'table',
                'table' => 'goods',
            ],
            'idAllField' => 'order_goods_0',
            'showForm' => [
	            'requireField'=>0,
	            'attributes'=>['class'=>'js_selectToDiv'],
            ],
        ],
        'order_goods_1' => [
            'name' => 'Количество',
            'nameEn' => 'counts',
            'dataType' => 'string',
            'type' => 'text',
            'idAllField' => 'order_goods_1',
            'showForm' => [
	            'requireField'=>0,
			],
        ],
    ],

];