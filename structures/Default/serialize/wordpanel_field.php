<?php
/*Created by Кирилл (22.01.2016 17:27)*/
return [
    'id' => '',
    'tableName' => 'Настройка справочника-массива',
    'table' => '',
    'fields' => [
        'wordpanel_field_1' => [
            'name' => 'Позиция',
            'nameEn' => 'position',
            'dataType' => 'integer',
            'type' => 'text',
            'idAllField' => 'wordpanel_field_1',
            'showForm' => ['requireField'=>0],
        ],
        'wordpanel_field_2' => [
            'name' => 'h2',
            'nameEn' => 'h2',
            'dataType' => 'string',
            'type' => 'text',
            'idAllField' => 'wordpanel_field_2',
            'showForm' => ['requireField'=>0],
        ],
        'wordpanel_field_3' => [
            'name' => 'Фоновый цвет',
            'nameEn' => 'color',
            'dataType' => 'string',
            'type' => 'colorpicker',
//            'type' => 'text',
            'idAllField' => 'wordpanel_field_3',
            'showForm' => [
	            'requireField'=>0,
			],
        ],
        'wordpanel_field_0' => [
            'name' => 'текст',
            'nameEn' => 'text',
            'dataType' => 'string',
            'type' => 'wordpanel',
            'showForm' => [
	            'requireField' => 0,
	            'jsParams'=>['customConfig' => '/js/modules/ckeditor/full_config.js'],
            ],
            'idAllField' => 'wordpanel_field_0',
        ],
    ],

];