<?php
/*Created by Edik (21.05.2015 10:10)*/
class Text{

	function __call($name, $arguments){
//		trigger_error('Подключается класс TextExtended! - ' . $name);
		require_once(CLASSES_PATH . 'TextExtended.php');
		return call_user_func_array([TextExtended::get(), $name], $arguments);
	}

	function removeDoubleSpaces($s) {
		return preg_replace('/\s+/u', ' ', $s);
	}

	function strToLower($text) {
		return strtolower($text);
	}

	function strToUpper($text) {
		return strtoupper($text);
	}

	function strToUpperFirst($text) {
		if ($text) return $this->strToUpper(substr($text, 0, 1)) . substr($text, 1);
		return $text;
	}

	function translit($text, $sep = '_') {
		$text = $this->strToLower($text);

		$text = preg_replace("/[\\\,\.\/&\-_]/u", " ", $text);
		$text = preg_replace("/[^ \w\dабвгдеёжзийклмнопрстуфхцчшщъыьэюя]/u", "", $text);
		$text = trim($text);

		$text = Text::get()->removeDoubleSpaces($text);
		$text= strtr($text, array(
			"а"=>"a",
			"б"=>"b",
			"в"=>"v",
			"г"=>"g",
			"д"=>"d",
			"е"=>"e",
			"ё"=>"e",
			"з"=>"z",
			"и"=>"i",
			"й"=>"y",
			"к"=>"k",
			"л"=>"l",
			"м"=>"m",
			"н"=>"n",
			"о"=>"o",
			"п"=>"p",
			"р"=>"r",
			"с"=>"s",
			"т"=>"t",
			"у"=>"u",
			"ф"=>"f",
			"х"=>"h",
			"ъ"=>"b",
			"ы"=>"i",
			"ь"=>"j",
			"э"=>"e",
			"ж"=>"zh",
			"ц"=>"c",
			"ч"=>"ch",
			"ш"=>"sh",
			"щ"=>"sch",
			"э"=>"e",
			"ю"=>"yu",
			"я"=>"ya",
			" "=>$sep
		));
		return $text;
	}

	function multylineHtmlEditor($message) {
		$message = Text::get()->multylineMessage($message);
		$message = preg_replace("/<\/p>([[:space:]]*(<br( \/)?>)+[[:space:]]*)+/iu", "</p>\n\n", $message);
		$message = preg_replace("/<p([^>]*)>([[:space:]]*(<br( \/)?>)+[[:space:]]*)+/iu", "<p\\1>", $message);
		$message = preg_replace("/<p([^>]*)>([[:space:]]*(&nbsp;)+[[:space:]]*)+/iu", "", $message);
		return $message;
	}

	function wordwrap($string, $width = 75, $break = "\n", $cut = false) {
		if ($cut){
			$pattern = '/(.{1,' . $width . '})(?:\s|$)|(.{' . $width . '})/uS';
			$replace = '$1$2' . $break;
		} else {
			$pattern = '/(?=\s)(.{1,' . $width . '})(?:\s|$)/uS';
			$replace = '$1' . $break;
		}
		return preg_replace($pattern, $replace, $string);
	}

	function multylineMessage($message) {
		if (preg_match("/<p([^>]*)>/i", $message)) return $message;
		elseif ((strpos($message, '<br>') === false) && (strpos($message, '<br />') === false) && (strpos($message, '<p>') === false)) return nl2br($message);
		else return $message;
	}

	function prepareName($s, $countSymbols = 250, $countSymbol = 30) {
		$s = strip_tags($s);
		$s = str_replace(array(',', '.'), array(', ', '. '), $s);
		$s = trim(Text::get()->removeDoubleSpaces($s));
		$s = $this->wordwrap($s, $countSymbol, " ", 1);
		if ($countSymbols > 0) {
			if (strlen($s) > $countSymbols) {
				$p = strpos($s, ' ', $countSymbols);
				if ($p !== false) {
					$s = trim(substr($s, 0, $p));
					if (substr($s, -1) == ',') $s = trim(substr($s, 0, -1));
					$s = $s.' ...';
				}
			}
		}
		return Text::get()->strToUpperFirst($s);
	}

	function prepareSmallText($s, $countSymbols = 200, $countSymbol = 30) {
		$s = $this->multylineMessage($s);
		$s = str_replace(array('<br>', '<br />', '<br/>', '<p>', '</p>', '&nbsp;'), array(' ', ' ', ' ', ' ', ' ', ' '), $s);
		$s = strip_tags($s);
		$s = trim($this->removeDoubleSpaces($s));
		$s = $this->wordwrap($s, $countSymbol, " ", 1);
		if ($countSymbols > 0) {
			if (strlen($s) > $countSymbols) {
				$p = strpos($s, ' ', $countSymbols);
				if ($p !== false) {
					$s = trim(substr($s, 0, $p));
					if (substr($s, -1) == ',') $s = trim(substr($s, 0, -1));
					$s = $s.' ...';
				}
			}
		}
		return $this->strToUpperFirst($s);
	}

	function mest($s) {
		return "'" . ModelsDriverBDSimpleDriver::getDriver()->real_escape_string($s) . "'";
	}

	/**@return Text|TextExtended*/
	static function get() {
		static $self = null;
		if ($self === null) $self = new Text();
		return $self;
	}
}