<?php
/*Created by Edik (03.10.14 11:14)*/
require_once(CLASSES_PATH . 'Exception/Portal.php');
class ExceptionSql extends ExceptionPortal{
	private $_sql = '';

	function run(){
		if (_isMyIp()) return parent::run();

		if (substr(Data::get()->g('path'), -5) === '.ajax'){
			//при ajax запросах не показываем header. Так лучше
			return new View();
		}
		$view = new ViewObjects();
		$view->setTpl(dirname(__FILE__) . '/View/user.tpl');
		return $view;
	}

	protected function _getView(){
		$view = parent::_getView();
		$view->setTpl(dirname(__FILE__) . '/View/sql.tpl');
		$view->assign('sql', $this->_sql);
		return $view;
	}

	function setSql($sql){
		$this->_sql = $sql;
	}

	function getSql(){
		return $this->_sql;
	}
}