<?php
/*Created by Edik (27.02.2015 15:31)*/
class ExceptionAction extends Exception{
	private $_action;

	function run(){
		require_once(CLASSES_PATH . str_replace('.', '/', $this->_action) . '.php');
		$className = str_replace('.', '', $this->_action);
		return (new $className(['code' => $this->getCode(), 'message' => $this->getMessage()]))->getAction()->run();
	}

	function setAction($action){
		$this->_action = $action;
	}
}