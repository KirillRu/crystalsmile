<?php
/*Created by Edik (25.05.2015 13:42)*/
require_once(CLASSES_PATH . 'Exception/PhpError.php');
class ExceptionPhpWarning extends ExceptionPhpError{

	function run(){
		//сюда попадает только isMyIp(в функции обработки ошибок)
		$view = parent::_getView();
		$view->setTpl(dirname(__FILE__) . '/View/php_warning.tpl');
		$view->assign('errorCodeName', $this->getErrorType($this->getCode()));
		$view->assign('errorLine', $this->_errorLine);
		return $view;
	}
}