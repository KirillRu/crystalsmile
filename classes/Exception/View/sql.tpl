<?php /**@var $this ViewObjects*/ $printStrings = $this->get('printStrings'); $randNom = rand(1, 10000);?>
<h1>SQL ERROR</h1><br>
<div onclick="log.openSql('<?= addslashes(str_replace('"', '\'', $this->get('message') . '<br>' . $this->get('sql')))?>')" class="error-div">
	<div class="ed-message"><?= $this->get('message') ?></div>
	<div class="ed-sql"><?= $this->get('sql') ?></div>
</div>
<div class="error-table-container">
	<table class="error-table" cellspacing="0" cellpadding="0">
		<tr>
			<th>Файл</th>
			<th>Функция</th>
			<th>Аргументы</th>
			<th>Класс</th>
		</tr>
		<?php foreach ($this->get('trace') as $i=>$trace): ?>
			<?php if ($trace['args']): ?>
				<tr class="et-row">
					<td>
						<?php if (!empty($trace['file'])): ?>
							<?= str_replace('/usr/home/vlad01/www','',$trace['file']); ?>:<?= $trace['line'] ?>
						<?php endif; ?>
					</td>
					<td><?= $trace['function']; ?></td>
					<td>
						<?php foreach ($trace['args'] as $arg): ?>
							<?php if (empty($arg)): ?>
								<div class="mb5"><?php var_dump($arg); ?></div>
							<?php else: ?>
								<div class="mb5"><?= substr(print_r($arg, true),0,50); ?></div>
							<?php endif; ?>
						<?php endforeach; ?>
						<a onclick="$('#js_printString_<?=$randNom . '_' . $i?>').toggle();" style="cursor: pointer">Просмотр</a>
						<div class="pr-popup" id="js_printString_<?=$randNom . '_' . $i?>"><?=$printStrings[$i];?></div>
					</td>
					<td><?= (!empty($trace['class']) ? $trace['class'] : ''); ?></td>
				</tr>
			<?php else: ?>
				<tr class="et-row">
					<td><?= str_replace('/usr/home/vlad01/www','',$trace['file']); ?>:<?= $trace['line'] ?></td>
					<td><?= $trace['function']; ?></td>
					<td>-</td>
					<td><?= (!empty($trace['class']) ? $trace['class'] : ''); ?></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		<tr>
			<td colspan="4" class="error-form-data">
				<div class="form-data-get">
					<b>$_GET:</b> <br/>
					<pre><?php print_r($_GET) ?></pre>
				</div>
				<?php if ($_POST):?>
					<div class="form-data-post">
						<b>$_POST:</b><br/><br/>
						<pre><?php print_r($_POST) ?></pre>
					</div>
				<?php endif;?>
			</td>
		</tr>
	</table>
</div>