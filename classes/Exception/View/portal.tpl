<?php /**@var $this ViewObjects*/ $printStrings = $this->get('printStrings'); $traceLines = $this->get('trace'); $randNom = rand(1, 1000);?>
<?php $errorLine = (!empty($traceLines[0]['file']) ? $traceLines[0] : $traceLines[1]); ?>
<h1>Portal exception</h1>
<table class="php-error-table">
	<tr class="error-table-tr-message">
		<td colspan="2"><?= $this->get('message'); ?></td>
	</tr>
	<tr>
		<td>FILE</td>
		<td><?= str_replace('/usr/home/vlad01/www','',$errorLine['file']); ?>:<?= $errorLine['line'] ?></td>
	</tr>
	<tr>
		<td>TRACE</td>
		<td><span class="button" onclick="$('#js_traceTable_<?=$randNom?>').toggle()" style="cursor: pointer">Show Trace</span></td>
	</tr>
</table>

<div class="error-table-container" id="js_traceTable_<?=$randNom?>" style="display: none">
	<table class="error-table" cellspacing="0" cellpadding="0">
		<tr>
			<th>Файл</th>
			<th>Функция</th>
			<th>Аргументы</th>
			<th>Класс</th>
		</tr>
		<?php foreach ($traceLines as $i=>$trace): ?>
			<?php if (!empty($trace['args'])): ?>
				<tr class="et-row">
					<td>
						<?php if (isset($trace['line'])): ?>
							<?= str_replace('/usr/home/vlad01/www','',$trace['file']); ?>:<?= $trace['line'] ?>
						<?php endif; ?>
					</td>
					<td><?= $trace['function']; ?></td>
					<td>
						<?php foreach ($trace['args'] as $arg): ?>
							<?php if (empty($arg)): ?>
								<div><?php var_dump($arg); ?></div>
							<?php else: ?>
								<div><?= substr(print_r($arg, true),0,50); ?></div>
							<?php endif; ?>
						<?php endforeach; ?>
						<a onclick="$('#js_printString_<?=$randNom . '_' . $i?>').toggle();" style="cursor: pointer">Просмотр</a>
						<div class="pr-popup" id="js_printString_<?=$randNom . '_' . $i?>"><?=$printStrings[$i];?></div>
					</td>
					<td><?= (!empty($trace['class']) ? $trace['class'] : ''); ?></td>
				</tr>
			<?php else: ?>
				<tr class="et-row">
					<td><?= str_replace('/usr/home/vlad01/www','',$trace['file']); ?>:<?= $trace['line'] ?></td>
					<td><?= $trace['function']; ?></td>
					<td>-</td>
					<td><?= (!empty($trace['class']) ? $trace['class'] : ''); ?></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		<tr>
			<td colspan="4" class="error-form-data">
				<div class="form-data-get">
					<b>$_GET:</b> <br/>
					<pre><?php print_r($_GET) ?></pre>
				</div>
				<?php if ($_POST):?>
					<div class="form-data-post">
						<b>$_POST:</b><br/><br/>
						<pre><?php print_r($_POST) ?></pre>
					</div>
				<?php endif;?>
			</td>
		</tr>
	</table>
</div>
