<?php /**@var $this ViewObjects*/?>
<meta content="text/html; charset=utf-8" http-equiv=content-type>
<link rel="stylesheet" href="<?= SITE_ROOT ?>css/index_0.css">
<link rel="stylesheet" href="<?= SITE_ROOT ?>css/tool.css">
<link rel="stylesheet" href="<?= SITE_ROOT ?>css/service.css">
<link rel="stylesheet" href="<?= SITE_ROOT ?>css/management.css">
<link rel="stylesheet" href="<?= SITE_ROOT ?>css/error.css">

<script type="text/javascript" src="//yandex.st/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="<?= SITE_ROOT ?>js/service.js"></script>
<script type="text/javascript" src="<?= SITE_ROOT ?>js/functions.js"></script>
<script type="text/javascript" src="<?= SITE_ROOT ?>js/log.js"></script>
<script type="text/javascript" src="<?= SITE_ROOT ?>js/modules/popup.js"></script>


<?php $this->content->display(); ?>
