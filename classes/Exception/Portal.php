<?php
/*Created by Edik (03.10.14 11:12)*/
require_once(CLASSES_PATH . 'View/Objects.php');
class ExceptionPortal extends Exception{

    function run(){
	    if (!_isMyIp()) return new View();
	    return $this->_getGeneralFrame($this->_getView());
    }

	protected function _getView(){
		$view = new ViewObjects();
		$view->setTpl(dirname(__FILE__) . '/View/portal.tpl');
		$view->assign('message', $this->getMessage());
		$view->assign('trace', $this->_getTrace());
		$view->assign('printStrings', $this->_getPrintStrings());
		return $view;
	}

	protected function _getTrace(){
		return parent::getTrace();
	}

	protected function _getPrintStrings() {
		$result = array();
		foreach($this->_getTrace() as $i => $trace){
			if (!empty($trace['args'])){
				$result[$i] = "<div class='_print_r'>";
				foreach ($trace['args'] as $num=>$arg){
					$result[$i] .= '<pre class="pr'.($num%5).'">';
					ob_start();
					if (empty($arg)) var_dump($arg);
					else print_r($arg);
					$arg_content = ob_get_contents();
					ob_end_clean();
					$result[$i] .= htmlspecialchars($arg_content);
					$result[$i] .= '</pre>';
				}
				$result[$i] .= "</div>\n";
			}
		}
		return $result;
	}

	protected function _getGeneralFrame($content){
		$view = new ViewObjects();
		$view->setTpl(dirname(__FILE__) . '/View/general_frame.tpl');
		$view->content = $content;
		return $view;
	}


}