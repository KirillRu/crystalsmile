<?php
/*Created by Edik (19.03.2015 12:44)*/
require_once(CLASSES_PATH . 'Exception/Portal.php');
class ExceptionPhpError extends ExceptionPortal{
	protected $_errorTypes = [
		E_ERROR => 'E_ERROR',
		E_WARNING => 'E_WARNING',
		E_PARSE => 'E_PARSE',
		E_NOTICE => 'E_NOTICE',
		E_CORE_ERROR => 'E_CORE_ERROR',
		E_CORE_WARNING => 'E_CORE_WARNING',
		E_COMPILE_ERROR => 'E_COMPILE_ERROR',
		E_COMPILE_WARNING => 'E_COMPILE_WARNING',
		E_USER_ERROR => 'E_USER_ERROR',
		E_USER_WARNING => 'E_USER_WARNING',
		E_USER_NOTICE => 'E_USER_NOTICE',
		E_STRICT => 'E_STRICT',
		E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
		E_DEPRECATED => 'E_DEPRECATED',
		E_USER_DEPRECATED => 'E_USER_DEPRECATED',
		E_ALL => 'E_ALL'
	], $_errorLine;

	function run(){
		if (_isMyIp()) return parent::run();
		if ($this->isFatalError($this->getCode())){
			$view = new ViewObjects();
			$view->setTpl(dirname(__FILE__) . '/View/user.tpl');
			return $view;
		}
		return new View();
	}

	protected function _getView(){
		$view = parent::_getView();
		$view->setTpl(dirname(__FILE__) . '/View/php_error.tpl');
		$view->assign('errorCodeName', $this->getErrorType($this->getCode()));
		$view->assign('errorLine', $this->_errorLine);
		return $view;
	}

	function getErrorType($code){
		return isset($this->_errorTypes[$code]) ? $this->_errorTypes[$code] : '';
	}

	function isFatalError($code){
		return in_array($code, [E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR]);
	}

	function setErrorLine($line){
		$this->_errorLine = $line;
	}
}