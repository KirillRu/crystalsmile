<?php
/*Created by Edik (06.08.2015 14:26)*/
class Exceptions{

	/**
	 * @param string $handler строка вида 'Sites.Action.NotFoundAction'
	 * @param int $code
	 * @param string $message
	 *
	 * @return ExceptionAction
	 */
	function getExcceptionAction($handler, $code = 0, $message = ''){
		require_once(CLASSES_PATH . 'Exception/Action.php');
		$exception = new ExceptionAction($message, $code);
		$exception->setAction($handler);
		return $exception;
	}

	static function get(){
		return new self;
	}
}