<?php
/*Created by Edik (26.06.14 16:06)*/

class Data {
	private $_data = array();

	function g($key, $default = null) {
		return (isset($this->_data[$key]) ? $this->_data[$key] : $default);
	}

	function s(array $data) {
		$this->_data = array_merge($this->_data, $data);
	}

	function gav($arrName, $key, $default = null) {
		return isset($this->_data[$arrName][$key]) ? $this->_data[$arrName][$key] : $default;
	}

	function getData() { return $this->_data; }

	static function get() {
		static $self = null;
		if ($self === null) {
			$self = new self;
		}
		return $self;
	}
}