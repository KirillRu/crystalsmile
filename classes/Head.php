<?php

class Head {
	protected static $_self = null;
	protected $_scripts = array(), $_options = array();

	function includeScript(...$scripts) {
		foreach ($scripts as $name) {
			if (!in_array($name, $this->_scripts)) $this->_scripts[] = $name;
		}
	}

	function echoScripts() {
		if (!empty($this->_scripts)) foreach ($this->_scripts as $script) {
			echo '<script type="text/javascript" src="' . SITE_ROOT . 'js/' . $script . '"></script>';
		}
	}

	function appendOptions($array, $key = 'main') {
		if (empty($this->_options[$key])) $this->_options[$key] = array();
		$this->_options[$key] = array_merge($this->_options[$key], $array);
	}

	function echoScriptOptions($key = 'main') {
		if (!empty($this->_options[$key])){
			if (_isDebug() && $key == 'main') $this->_options['main']['_isDebug'] = true;
			echo '<script type="text/javascript">window[\'_cfg_' . $key . '\'] = ' . json_encode($this->_options[$key]) . '</script>';
			unset($this->_options[$key]);
		}
	}

	static function get() {
		if (self::$_self === null) self::$_self = new self();
		return self::$_self;
	}
}