<?php
require_once(CLASSES_PATH . 'Sitemap/Sitemap.php');
class ManagementSitemap extends Sitemap {

	function getAction() {
		if (empty($_SESSION['login'])) {
			if (isset($_POST['login'])&&isset($_POST['passw'])&&_checkAccess($_POST['login'], $_POST['passw'])) {}
			else {
				require_once(CLASSES_PATH . 'Management/Action/Auth.php');
				$handler = new ManagementActionAuth();
				return $handler;
			}
		}
		else {
			require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Manager_users/SerializeText.php');
			$login = null;
			foreach ((new ManagementModulesListIteratorsManager_usersSerializeText()) as $item) {
				if ($_SESSION['login'] == $item['login']) $login = $_SESSION['login'];
			}
			if (empty($login)) {
				unset($_SESSION['login']);
				require_once(CLASSES_PATH . 'Management/Action/Auth.php');
				$handler = new ManagementActionAuth();
				return $handler;
			}
		}
		$path = $this->g('path');
        if (substr($path, -5) == '.ajax') $handler = $this->_ajaxAction($path);
        else $handler = $this->_getHandler($path);
        if ($handler) return $handler->getAction($path);
		return $this;
	}

	protected function _getHandler($path) {
		$data = array_merge($_POST, $_GET);
		switch(true) {
			case ($path == 'kcfinder/browse.php'):
				$classFile = CLASSES_PATH . 'Management/Action/KCFinder.php';
				$className = 'ManagementActionKCFinder';
				$data['action'] = 'browse';
				break;
			case ($path == 'kcfinder/upload.php'):
				$classFile = CLASSES_PATH . 'Management/Action/KCFinder.php';
				$className = 'ManagementActionKCFinder';
				$data['action'] = 'upload';
				break;
			case preg_match(Path::get()->preparePattern('@s/$'), $path, $m):
				$data['moduleName'] = $m[1];
				$classFile = PROJECT_PATH . 'Action/DefaultPage.php';
				$className = PROJECT_NAME . 'ActionDefaultPage';
				break;
			default:
				$classFile = PROJECT_PATH . 'Action/GeneralFrame.php';
				$className = PROJECT_NAME . 'ActionGeneralFrame';
				break;
		}

		Data::get()->s($data);
		if (!empty($classFile) && !empty($className) && file_exists($classFile)){
			require_once($classFile);
			return new $className;
		} else {
			return parent::_getHandler($path);
		}
	}

    protected function _ajaxAction($path) {
   		require_once(PROJECT_PATH . '/Sitemap/Ajax.php');
   		$className = PROJECT_NAME . 'SitemapAjax';
   		return (new $className(array('path' => $path)))->getAction();
   	}

}