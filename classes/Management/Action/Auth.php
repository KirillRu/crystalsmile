<?php
/*Created by ������ (11.02.2016 22:26)*/

class ManagementActionAuth extends AbstractAction {

	protected $_alias;

	function run() {
		$view = new ViewObjects();
		$view->login = $this->_viewLogin();
		$view->passw = $this->_viewPassw();
		$view->setTpl(PROJECT_PATH . 'View/auth.tpl');
		return $view;
	}

	protected function _viewLogin() {
		return FormRow::get()->viewValue(
			array('nameEn'=>'login', 'type'=>'text'),
			Data::get()->gav($this->g('alias', $this->_alias), 'login'),
			$this->g('alias', $this->_alias)
		);
	}

	protected function _viewPassw() {
		return FormRow::get()->viewValue(
			array('nameEn'=>'passw', 'type'=>'password'),
			null,
			$this->g('alias', $this->_alias)
		);
	}

}