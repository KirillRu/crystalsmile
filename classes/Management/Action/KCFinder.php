<?php
/*Created by ������ (10.02.2016 23:35)*/
class ManagementActionKCFinder extends AbstractAction {
	function run() {
		$_SESSION['KCFINDER'] = array();
		$_SESSION['KCFINDER']['disabled'] = false;
		$_SESSION['KCFINDER']['uploadURL'] = SITE_ROOT . 'img';
		$_SESSION['KCFINDER']['uploadDir'] = ROOT_DIR . 'img';

		$kcfile = ROOT_DIR . 'js/modules/ckeditor/kcfinder/' . Data::get()->g('action') . '.php';
		if (file_exists($kcfile)) {
			include $kcfile;
			exit;
		}
		return new View();
	}
}