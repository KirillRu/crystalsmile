<?php
class ManagementActionDefaultPage extends AbstractAction {

    function run() {
	    require_once(CLASSES_PATH . 'Management/Action/GeneralFrame.php');
 		$view = (new ManagementActionGeneralFrame())->getAction()->run();
	    $view->content = $this->_viewContent();
	    return $view;
    }

	protected function _viewContent() {
		$moduleName = Text::get()->strToUpperFirst(Data::get()->g('moduleName'));
		$actionFile = CLASSES_PATH . 'Management/Modules/Action/' . $moduleName . '.php';
		if (file_exists($actionFile)) {
			require_once($actionFile);
			$action = 'ManagementModulesAction' . $moduleName;
			return (new $action)->getAction()->run();
		}
		require_once(CLASSES_PATH . 'View/Text.php');
		return new ViewText('Страница не найдена');
	}

}