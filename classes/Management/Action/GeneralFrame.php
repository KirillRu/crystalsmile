<?php
class ManagementActionGeneralFrame extends AbstractAction {

	function run() {
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Management/View/general_frame.tpl');

		$view->top = $this->_viewTop();
 		return $view;
	}

	protected function _viewTop() {
		$top = new ViewObjects();
		$top->setTpl(CLASSES_PATH . 'Management/View/top.tpl');
		return $top;
	}
}
 