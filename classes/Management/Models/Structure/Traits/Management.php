<?php
/*Created by ������ (29.10.2015 14:24)*/

/**
 * @property string $_table
 */
trait ManagementModelsStructureTraitsManagement{

	protected function _getPath() {
		$path = STRUCTURES_PATH . 'Management/' . PROJECT_NAME . '/' . $this->_table . '.php';
		if (file_exists($path)) return $path;

		$path = STRUCTURES_PATH . 'Management/' . $this->_table . '.php';
		if (file_exists($path)) return $path;

		return parent::_getPath();
	}
}