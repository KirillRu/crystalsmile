<?php
/*Created by Viktor (12.01.2016 10:02)*/
require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
require_once(CLASSES_PATH . 'Management/Models/Structure/Traits/Management.php');
class ManagementModelsStructureSearch extends ModelsStructureFrom_files {
	use ManagementModelsStructureTraitsManagement;
	protected $_table = null;

	function __construct($table) {
		$this->_table = $table;
		$path = $this->_getPath();
		if ($path) {
			$structure = include($path);
			$structure['fields'] = $this->_prepareStructureFields($structure['fields']);
			foreach ($structure as $k=>$v) $this->_parseStructureInFileArray($k, $v);
		}
	}

	/*protected function _prepareField($field, $showsField = []) {
		return parent::_prepareField($field, ['idSpr', 'showForm', 'showList']);
	}*/

	protected function _prepareField($field, $showsField = []) {
		return parent::_prepareField($field, ['showForm', 'showList']);
	}

	// Подключает структуру в зависимости от раздела (Videos и т.д.)
/*	protected function _getPath() {
		//проверяем наличия файла в структурах, созданных руками.
		$path = STRUCTURES_PATH . 'Adminboard/' . PROJECT_NAME . '/' . $this->_table . '.php';
		if (file_exists($path)) return $path;
		return parent::_getPath();
	}*/

	protected function _prepareStructureFields($fields) {
		$result = [];
		$existFields = [];
		foreach ($fields as $field) $existFields[$field['nameEn']] = true;
		foreach ($this->_getFilterFields() as $field) {
			if (isset($existFields[$field['nameEn']])) {
				$result[$field['nameEn']] = $field;
			}
		}
		return $result;
	}

	protected function _getFilterFields() {
		$moduleName = Text::get()->strToUpperFirst(Data::get()->g('moduleName', Data::get()->gav('pageData', 'moduleName')));
		if (file_exists(CLASSES_PATH . 'Management/Projects/'.PROJECT_NAME.'/Modules/List/'.$moduleName.'/FilterFields.php')) {
			$fields = include(CLASSES_PATH . 'Management/Projects/'.PROJECT_NAME.'/Modules/List/'.$moduleName.'/FilterFields.php');
		} elseif (file_exists(CLASSES_PATH . 'Management/Projects/'.PROJECT_NAME.'/Modules/List/FilterFields.php')) {
			$fields = include(CLASSES_PATH . 'Management/Projects/'.PROJECT_NAME.'/Modules/List/FilterFields.php');
		} elseif (file_exists(CLASSES_PATH . 'Management/Modules/List/'.$moduleName.'/FilterFields.php')) {
			$fields = include(CLASSES_PATH . 'Management/Modules/List/'.$moduleName.'/FilterFields.php');
		} else {
			$fields = include(CLASSES_PATH . 'Management/Modules/List/FilterFields.php');
		}
		return $fields;
	}
}