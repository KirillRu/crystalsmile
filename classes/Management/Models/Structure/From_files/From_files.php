<?php
/*Created by ������ (29.10.2015 14:26)*/
require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
require_once(CLASSES_PATH . 'Management/Models/Structure/Traits/Management.php');
class ManagementModelsStructureFrom_files extends ModelsStructureFrom_files{
	use ManagementModelsStructureTraitsManagement;

	protected function _getFields($fields) {
		$managementFields = array();
		$position = 0;
		foreach($fields as $iAF=>$field) {
			$showManagement = $this->_serializeField($field, 'showManagement');
			if ($showManagement) {
				$managementFields[$iAF] = $this->_prepareField($field, array('showManagement', 'showForm', 'idSpr'));
				$managementFields[$iAF]['showManagement'] = $showManagement;
				$managementFields[$iAF]['position'] = ++$position;
			}
		}
		return $managementFields;
	}
}