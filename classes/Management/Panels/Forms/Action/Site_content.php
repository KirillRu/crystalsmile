<?php
/*Created by ������ (07.02.2016 20:59)*/

require_once(PROJECT_PATH . 'Panels/Forms/Action/Forms.php');
require_once(CLASSES_PATH . 'Management/Modules/Forms/Site_content/Traits/Site_content.php');
class ManagementPanelsFormsActionSite_content extends ManagementPanelsFormsAction {
    use ManagementModulesFormsSite_contentTraits;

	protected function _getDataFromForm() {
        $data = parent::_getDataFromForm();

        $data['defaultBlockName'] = '';
        return $data;
    }

}