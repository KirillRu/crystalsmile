<?php
/*Created by Кирилл (13.02.2016 17:20)*/
require_once(PROJECT_PATH . 'Panels/Forms/Action/Forms.php');
require_once(CLASSES_PATH . 'Management/Modules/Forms/Goods/Traits/Goods.php');
class ManagementPanelsFormsActionGoods extends ManagementPanelsFormsAction {
    use ManagementModulesFormsGoodsTraits;

	protected function _getDataFromForm() {
        $data = parent::_getDataFromForm();

        $data['defaultBlockName'] = '';
        return $data;
    }

}