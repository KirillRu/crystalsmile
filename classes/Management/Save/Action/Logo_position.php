<?php
/*Created by Кирилл (07.02.2016 14:45)*/
class ManagementSaveActionLogo_position extends AbstractAction {

	function run() {
        $this->_save();
        return new View();
	}

    protected function _save() {
        $makeup = ModelsCacheFilePhp::get()->read(CLASSES_PATH . 'Zubi/Settings/hat_preview.php');
        if (empty($makeup)) $makeup = [];
        if (empty($makeup['js_hatLogo'])) $makeup['js_hatLogo'] = [];
        foreach (Data::get()->getData() as $k=>$v) {
            $makeup['js_hatLogo'][$k] = $v;
        }
        ModelsCacheFilePhp::get()->write(CLASSES_PATH . 'Zubi/Settings/hat_preview.php', $makeup);
   	}

}