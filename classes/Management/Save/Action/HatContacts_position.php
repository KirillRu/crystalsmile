<?php
/*Created by Кирилл (07.02.2016 14:32)*/

class ManagementSaveActionHatContacts_position extends AbstractAction {

	function run() {
        $this->_save();
        return new View();
	}

    protected function _save() {
        $makeup = ModelsCacheFilePhp::get()->read(CLASSES_PATH . 'Zubi/Settings/hat_preview.php');
        if (empty($makeup)) $makeup = [];
        if (empty($makeup['js_hatContacts'])) $makeup['js_hatContacts'] = [];
        foreach (Data::get()->getData() as $k=>$v) {
            $makeup['js_hatContacts'][$k] = $v;
        }
        ModelsCacheFilePhp::get()->write(CLASSES_PATH . 'Zubi/Settings/hat_preview.php', $makeup);
   	}

}