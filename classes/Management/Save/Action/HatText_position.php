<?php
/*Created by Кирилл (07.02.2016 14:47)*/
class ManagementSaveActionHatText_position extends AbstractAction {

	function run() {
        $this->_save();
        return new View();
	}

    protected function _save() {
        $makeup = ModelsCacheFilePhp::get()->read(CLASSES_PATH . 'Zubi/Settings/hat_preview.php');
        if (empty($makeup)) $makeup = [];
        if (empty($makeup['js_hatText'])) $makeup['js_hatText'] = [];
        foreach (Data::get()->getData() as $k=>$v) {
            $makeup['js_hatText'][$k] = $v;
        }
        ModelsCacheFilePhp::get()->write(CLASSES_PATH . 'Zubi/Settings/hat_preview.php', $makeup);
   	}

}