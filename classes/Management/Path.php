<?php
/*Created by Кирилл (04.02.2016 11:10)*/
require_once(CLASSES_PATH . 'Path.php');
class ManagementPath extends Path {

    function getManagementPath($settingsRazd = '') {
        if (!empty($settingsRazd)) $settingsRazd .= '/';
        return parent::getUrl() . 'management/' . $settingsRazd;
    }

}
