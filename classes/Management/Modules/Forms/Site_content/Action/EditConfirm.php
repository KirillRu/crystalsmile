<?php
/*Created by ������ (08.02.2016 22:13)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Site_content/Traits/Site_content.php');

class ManagementModulesFormsSite_contentActionEditConfirm extends ManagementModulesFormsActionEditConfirm {
    use ManagementModulesFormsSite_contentTraits;

    protected function _edit() {
	    require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Site_content/SerializeText.php');
		foreach ((new ManagementModulesListIteratorsSite_contentSerializeText()) as $item) {
			if (file_exists($item['text'])) unlink(CLASSES_PATH . $item['text']);
		}
	    if (file_exists(CLASSES_PATH . 'Zubi/FileData/site_content.db')) unlink(CLASSES_PATH . 'Zubi/FileData/site_content.db');

        $values = $this->_getStructure()->getValues($this->_getFormValues());
	    $fieldContent = $this->_getStructure()->getField('site_text');
	    if (!empty($values[$fieldContent['idAllField']])) {
		    foreach ($values[$fieldContent['idAllField']] as $position=>$value) {
			    $item = [];
			    foreach ($fieldContent['idSpr']->getStructure()->getFields() as $iAF=>$field) {
				    switch($field['nameEn']) {
					    case 'position': $item[$field['nameEn']] = $position+1; break;
					    case 'text':
							$fileName = 'Zubi/FileData/text_' . ($position+1) . '.tpl';
						    file_put_contents(CLASSES_PATH . $fileName, $value[$iAF]);
						    $item[$field['nameEn']] = $fileName;
						    break;
					    default: $item[$field['nameEn']] = $value[$iAF]; break;
				    }
			    }
			    if (!empty($item)) file_put_contents(CLASSES_PATH . 'Zubi/FileData/site_content.db', serialize($item) . "\n<!>\n", FILE_APPEND);
		    }
	    }
//        $fileContent = CLASSES_PATH . 'Zubi/FileData/textContent.db';
//        file_put_contents($fileContent, Data::get()->gav('site_content', 'site_text', ''));
        return false;
   	}

}