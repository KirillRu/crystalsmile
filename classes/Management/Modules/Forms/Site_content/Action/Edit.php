<?php
/*Created by ������ (08.02.2016 22:05)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Site_content/Traits/Site_content.php');

class ManagementModulesFormsSite_contentActionEdit extends ManagementModulesFormsActionEdit {
    use ManagementModulesFormsSite_contentTraits;

    function run() {
        $view = parent::run();
        $view->setTpl(PROJECT_PATH . 'Modules/Forms/Site_content/View/edit.tpl');
        return $view;
    }

}