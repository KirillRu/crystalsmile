<?php
/*Created by Кирилл (05.02.2016 11:41)*/
/**
 * @property ModelsStructureFrom_files _structure
 */
//добавляя этот трейт к классам мы изменяем структуру. Добавлять одновременно в класс отрисовки формы и в класс добавления\редактирования!
trait ManagementModulesFormsSite_contentTraits {

    protected function _getValuesFromBase() {
           $fileContent = CLASSES_PATH . 'Zubi/FileData/site_content.db';
           if (file_exists($fileContent)) {
	           require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Site_content/SerializeText.php');
	           $result = [];
	           foreach ((new ManagementModulesListIteratorsSite_contentSerializeText()) as $item) {
		           if (file_exists(CLASSES_PATH . $item['text'])) $item['text'] = file_get_contents(CLASSES_PATH . $item['text']);
		           $result[] = $item;
	           }
	           return ['site_text'=>$result];
           }
           return [];
   	}

}