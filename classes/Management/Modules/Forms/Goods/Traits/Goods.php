<?php
/*Created by Кирилл (13.02.2016 17:08)*/
/**
 * @property ModelsStructureFrom_files _structure
 */
//добавляя этот трейт к классам мы изменяем структуру. Добавлять одновременно в класс отрисовки формы и в класс добавления\редактирования!
trait ManagementModulesFormsGoodsTraits {

    protected function _getValuesFromBase() {
           $fileContent = CLASSES_PATH . 'Zubi/FileData/goods.db';
           if (file_exists($fileContent)) {
	           require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Goods/SerializeText.php');
	           $result = [];
	           foreach ((new ManagementModulesListIteratorsGoodsSerializeText()) as $item) $result[] = $item;
	           return ['goods'=>$result];
           }
           return [];
   	}

}