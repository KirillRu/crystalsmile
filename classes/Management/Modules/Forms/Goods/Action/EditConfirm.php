<?php
/*Created by Кирилл (13.02.2016 17:10)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Goods/Traits/Goods.php');

class ManagementModulesFormsGoodsActionEditConfirm extends ManagementModulesFormsActionEditConfirm {
    use ManagementModulesFormsGoodsTraits;

    protected function _edit() {
	    require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Goods/SerializeText.php');
	    if (file_exists(CLASSES_PATH . 'Zubi/FileData/goods.db')) unlink(CLASSES_PATH . 'Zubi/FileData/goods.db');

        $values = $this->_getStructure()->getValues($this->_getFormValues());
	    $fieldContent = $this->_getStructure()->getField('goods');
	    if (!empty($values[$fieldContent['idAllField']])) {
		    foreach ($values[$fieldContent['idAllField']] as $position=>$value) {
			    $item = [];
			    foreach ($fieldContent['idSpr']->getStructure()->getFields() as $iAF=>$field) {
				    switch($field['nameEn']) {
					    case 'position': $item[$field['nameEn']] = $position+1; break;
					    default: $item[$field['nameEn']] = $value[$iAF]; break;
				    }
			    }
			    if (!empty($item)) file_put_contents(CLASSES_PATH . 'Zubi/FileData/goods.db', serialize($item) . "\n<!>\n", FILE_APPEND);
		    }
	    }
        return false;
   	}

}