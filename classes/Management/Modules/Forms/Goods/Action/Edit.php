<?php
/*Created by Кирилл (13.02.2016 17:09)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Goods/Traits/Goods.php');

class ManagementModulesFormsGoodsActionEdit extends ManagementModulesFormsActionEdit {
    use ManagementModulesFormsGoodsTraits;

	function run() {
		$view = parent::run();
		$view->help = $this->_viewHelp();
		return $view;
	}

	protected function _viewHelp() {
		$view = new ViewObjects();
		$view->setTpl(PROJECT_PATH . 'Modules/Forms/Goods/View/help.tpl');
		return $view;
	}

}