<?php
/*Created by ������ (21.01.2016 10:04)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Traits/Edit.php');
class ManagementModulesFormsActionEdit extends AbstractAction {
	use ManagementModulesFormsTraitsEdit;

	function getAction(){
		if (static::class === self::class){
			$table = Text::get()->strToUpperFirst($this->_getModuleName());
			$file = CLASSES_PATH . 'Management/Modules/Forms/' . $table . '/Action/Edit.php';
			if (file_exists($file)){
				require_once($file);
				$className = 'ManagementModulesForms' . $table . 'ActionEdit';
				return (new $className)->getAction();
			}
		}
		return parent::getAction();
	}

	function run(){
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Management/Modules/Forms/View/edit.tpl');
		$view->form = $this->_getForm();

		Head::get()->appendOptions($this->_getFormData(), 'formData');
		return $view;
	}

	protected function _getForm(){
        $moduleName = Text::get()->strToUpperFirst($this->_getModuleName());
        $file = CLASSES_PATH . 'Management/Panels/Forms/Action/' . $moduleName . '.php';
		$data = $this->_getPanelData();

		$moduleName = Text::get()->strToUpperFirst($this->_getModuleName());
		if (file_exists($file)){
			require_once($file);
			$className = 'ManagementPanelsFormsAction' . $moduleName;
			return (new $className($data))->getAction()->run();
		}
		require_once(CLASSES_PATH . 'Management/Panels/Forms/Action/Forms.php');
		return (new ManagementPanelsFormsAction($data))->getAction()->run();
	}

	protected function _getPanelData(){
		$moduleName = $this->_getModuleName();
		$data = ['moduleName' => $moduleName, 'alias' => $moduleName, $moduleName => $this->_getValues()];
		return $data;
	}

	protected function _setFormData(){
		$this->_traitSetFormData();
		$this->_formData['id'] = Data::get()->g('id', $this->_getFormData('id'));
	}
}