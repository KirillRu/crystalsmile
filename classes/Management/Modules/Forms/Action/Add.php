<?php
/*Created by ������ (21.01.2016 17:12)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Traits/Form.php');
class ManagementModulesFormsActionAdd extends AbstractAction {
	use ManagementModulesFormsTraitsForm;

	function getAction(){
		if (static::class === self::class){
			$table = Text::get()->strToUpperFirst($this->_getModuleName());
			$file = CLASSES_PATH . 'Management/Modules/Forms/' . $table . '/Action/Add.php';
			if (file_exists($file)){
				require_once($file);
				$className = 'ManagementModulesForms' . $table . 'ActionAdd';
				return (new $className)->getAction();
			}
		}
		return parent::getAction();
	}

	function run(){
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Management/Modules/Forms/View/add.tpl');
		$view->form = $this->_getForm();

		Head::get()->appendOptions($this->_getFormData(), 'formData');
		return $view;
	}

	protected function _getForm(){
		$data = $this->_getPanelData();

		$moduleName = Text::get()->strToUpperFirst($this->_getModuleName());
		$file = CLASSES_PATH . 'Management/Panels/Forms/Action/' . $moduleName . '.php';
		if (file_exists($file)){
			require_once($file);
			$className = 'ManagementPanelsFormsAction' . $moduleName;
			return (new $className($data))->getAction()->run();
		}
		require_once(CLASSES_PATH . 'Management/Panels/Forms/Action/Forms.php');
		return (new ManagementPanelsFormsAction($data))->getAction()->run();
	}

	protected function _getPanelData(){
		$moduleName = $this->_getModuleName();
		$data = ['moduleName' => $moduleName, 'alias' => $moduleName, $moduleName => $this->_getFormValues(), 'defaultBlockName'=>$this->g('defaultBlockName')];
		return $data;
	}
}