<?php
/*Created by ������ (11.02.2016 15:14)*/
trait ManagementModulesFormsManager_usersTraits {

    protected function _getValuesFromBase() {
           $fileContent = CLASSES_PATH . 'Zubi/FileData/manager_users.db';
           if (file_exists($fileContent)) {
	           require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Manager_users/SerializeText.php');
	           $result = [];
	           foreach ((new ManagementModulesListIteratorsManager_usersSerializeText()) as $item) {
		           $result[] = $item;
	           }
	           return ['users'=>$result];
           }
           return [];
   	}

}