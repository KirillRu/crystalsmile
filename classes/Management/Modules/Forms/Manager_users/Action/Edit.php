<?php
/*Created by ������ (11.02.2016 15:10)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Manager_users/Traits/Manager_users.php');

class ManagementModulesFormsManager_usersActionEdit extends ManagementModulesFormsActionEdit {
    use ManagementModulesFormsManager_usersTraits;

    function run() {
        $view = parent::run();
        $view->setTpl(PROJECT_PATH . 'Modules/Forms/Manager_users/View/edit.tpl');
        return $view;
    }

}