<?php
/*Created by ������ (11.02.2016 15:11)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Manager_users/Traits/Manager_users.php');

class ManagementModulesFormsManager_usersActionEditConfirm extends ManagementModulesFormsActionEditConfirm {
    use ManagementModulesFormsManager_usersTraits;

    protected function _edit() {
	    $users = $this->_getValuesFromBase();
	    @unlink(CLASSES_PATH . 'Zubi/FileData/manager_users.db');

        $values = $this->_getStructure()->getValues($this->_getFormValues());
	    $fieldContent = $this->_getStructure()->getField('site_text');
	    if (!empty($values[$fieldContent['idAllField']])) {
		    foreach ($values[$fieldContent['idAllField']] as $position=>$value) {
			    $item = [];
			    foreach ($fieldContent['idSpr']->getStructure()->getFields() as $iAF=>$field) {
				    switch($field['nameEn']) {
					    case 'password':
							if ($value[$iAF] == '') {
								foreach ($users as $user) {
									if ($item['login'] == $user['login']) {
										$item[$field['nameEn']] = $user[$field['nameEn']];
										break;
									}
								}
							}
						    else $item[$field['nameEn']] = md5($item['login'] . $value[$iAF]);
						    break;
					    default: $item[$field['nameEn']] = $value[$iAF]; break;
				    }
			    }
			    if (!empty($item)) file_put_contents(CLASSES_PATH . 'Zubi/FileData/manager_users.db', serialize($item) . "\n<!>\n", FILE_APPEND);
		    }
	    }
        return false;
   	}

}