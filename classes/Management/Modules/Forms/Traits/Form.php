<?php
/*Created by ������ (21.01.2016 10:36)*/
//��������� �������� �������, ����������� �� ���� ������� ���������\��������� ����
trait ManagementModulesFormsTraitsForm{
	protected $_structure = null, $_formData = null;

	protected function _getStructure(){
		if ($this->_structure === null){
			require_once(CLASSES_PATH . 'Models/Structure/From_files/Form.php');
			$this->_structure = new ModelsStructureFrom_filesForm($this->_getModuleName());
		}
		return $this->_structure;
	}

	/** ��� ����, ������� ���������� � ��������� ����� � �������� �� ������.
	 * ��� ���������\����������\��������� sql �������(��� ��������������) ���� ������� ����� �� ��������, � �� �� ���� �������! (����� �������������� _getStructure)
	 * @return array
	 */
	protected function _getFields(){
		return $this->_getStructure()->getFields();
	}

	//�� �� ����� ��� � pageData, ������ ������ �����
	protected function _getFormData($key = null, $default = null){
		if ($this->_formData === null) $this->_setFormData();

		if ($key === null) return $this->_formData;
		return isset($this->_formData[$key]) ? $this->_formData[$key] : $default;
	}

	protected function _setFormData(){
		//������� ��� ����, ��� �� ����� ���� �������� ��� ������� �� ������ ��� ��������������� � ��
		$this->_traitSetFormData();
	}

	protected function _traitSetFormData(){
		$this->_formData = Data::get()->g('formData', []);
		if (empty($this->_formData['moduleName'])) $this->_formData['moduleName'] = Data::get()->g('moduleName', $this->_getPageData('moduleName'));
	}

	protected function _getPageData($key = null, $default = null){
		if ($key === null) return Data::get()->g('pageData', $default);
		return Data::get()->gav('pageData', $key, $default);
	}

	protected function _getFormValues(){
		return Data::get()->g($this->_getModuleName());
	}

	protected function _getModuleName(){
		return Data::get()->g('moduleName') ?: $this->_getFormData('moduleName') ?: $this->_getPageData('moduleName') ?: $this->_getPageData('table');
	}
}