<?php /**@var $this ViewObjects*/ ?>
<div id="js_textContent">
<?php Head::get()->echoScriptOptions('formData')?>
<?php $this->errors->display(); ?>
<form onsubmit="ajax.replace('<?= Path::get()->getManagementPath() ?>runModule/Forms/editConfirm.ajax', element.getParameters(this) + '&moduleName=<?= Data::get()->g('moduleName') ?>', 'js_textContent'); return false;">

	<?php foreach ($this->_widgets as $name => $widget): ?>
	    <?php if (!in_array($name, ['form', 'errors'])) $widget->display(); ?>
	<?php endforeach; ?>

	<?php $this->form->display(); ?>

	<button type="submit" class="button">Изменить</button>
</form>
</div>