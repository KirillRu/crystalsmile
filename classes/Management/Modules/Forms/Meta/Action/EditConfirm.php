<?php
/*Created by ������ (08.02.2016 22:13)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Meta/Traits/Meta.php');

class ManagementModulesFormsMetaActionEditConfirm extends ManagementModulesFormsActionEditConfirm {
    use ManagementModulesFormsMetaTraits;

    protected function _edit() {
        $fileContent = CLASSES_PATH . 'Zubi/FileData/meta.db';
        file_put_contents($fileContent, serialize(Data::get()->g('meta', [])));
        return false;
   	}

}