<?php
/*Created by ������ (08.02.2016 22:05)*/
require_once(CLASSES_PATH . 'Management/Modules/Forms/Meta/Traits/Meta.php');

class ManagementModulesFormsMetaActionEdit extends ManagementModulesFormsActionEdit {
    use ManagementModulesFormsMetaTraits;

    function run() {
        $view = parent::run();
        $view->setTpl(PROJECT_PATH . 'Modules/Forms/Meta/View/edit.tpl');
        return $view;
    }

}