<?php
/*Created by Кирилл (08.02.2016 14:16)*/
class ManagementModulesActionContent extends AbstractAction {

    function run() {
        Data::get()->s(['moduleName' => 'site_content']);
        require_once(PROJECT_PATH . 'Modules/Forms/Action/Edit.php');
        $handler = new ManagementModulesFormsActionEdit();
        $view = $handler->getAction()->run();
        return $view;
    }

}
