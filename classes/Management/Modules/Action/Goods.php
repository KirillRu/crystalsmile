<?php
/*Created by Кирилл (13.02.2016 17:06)*/
class ManagementModulesActionGoods extends AbstractAction {

    function run() {
        Data::get()->s(['moduleName' => 'goods']);
        require_once(PROJECT_PATH . 'Modules/Forms/Action/Edit.php');
        $handler = new ManagementModulesFormsActionEdit();
        $view = $handler->getAction()->run();
        return $view;
    }

}
