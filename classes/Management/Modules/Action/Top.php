<?php
/*Created by Кирилл (06.02.2016 23:01)*/

class ManagementModulesActionTop extends AbstractAction {

    function run() {
        $view = new ViewObjects();
        $view->setTpl(PROJECT_PATH . 'Modules/View/top.tpl');

	    $defaultSettings = [
			'js_hatLogo'=>['left'=>0,'top'=>0],
			'js_hatContacts'=>['left'=>0,'top'=>0,'text'=>''],
			'js_hatText'=>['left'=>0,'top'=>0,'text'=>''],
		];
	    $makeup = Functions__mergeArrays($defaultSettings, include(CLASSES_PATH . 'Zubi/Settings/hat_preview.php'));
	    $view->assign('makeup', $makeup);
        return $view;
    }

}