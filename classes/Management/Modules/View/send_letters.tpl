<?php /**@var $this ViewObjects*/ ?>
<div id="js_itemsContent">
	<?php Head::get()->echoScriptOptions('pageData'); ?>
	<?php $this->count->display()?>
	<?php $this->pager->display()?>
	<?php $this->itemsList->display()?>
	<?php $this->pager->display()?>
</div>


<table>
    <tr><th>Дата</th><th>Контактное лицо</th><th>Email</th><th>Телефон</th></tr>
<?php foreach ($this->get('items') as $item):?>
    <tr>
        <td><?= empty($item['date_add'])?'':date('d.m.Y H:i', $item['date_add']) ?></td>
        <td><?= htmlspecialchars($item['name']) ?></td>
        <td><?= htmlspecialchars($item['email']) ?></td>
        <td><?= htmlspecialchars($item['phone']) ?></td>
    </tr>
    <tr><td colspan="4"><?= str_replace("\n", '<br>', htmlspecialchars($item['text'])) ?></td></tr>
<?php endforeach; ?>
</table>
