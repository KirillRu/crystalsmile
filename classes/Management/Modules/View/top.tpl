<?php /**@var $this ViewObjects*/
$makeup = $this->get('makeup');
?>
<div class="sd-block" id="js_settingFrame"><div class="sd-content">
    <div id="js_settings" class="sd-body fleft">
        <div class="mb10">Расположение логотипа</div>
        <div class="sd-hat-position" id="js_sliderNameLogo">
        	<div id="js_sliderRollerLogo"></div>
        </div>
    </div>
    <div id="js_settings" class="sd-body fleft ml10">
        <div class="mb10">Расположение контактов в шапке</div>
        <div class="sd-hat-position" id="js_sliderNameContacts">
            <div id="js_sliderRollerContacts"></div>
        </div>
        <div class="mt10"><div id="js_contactsText" contenteditable="true"><?= $makeup['js_hatContacts']['text'] ?></div></div>
    </div>
    <div id="js_settings" class="sd-body fleft ml10">
        <div class="mb10">Расположение текста в шапке</div>
        <div class="sd-hat-position" id="js_sliderNameText">
            <div id="js_sliderRollerText"></div>
        </div>
        <div class="mt10"><div id="js_hatText" contenteditable="true"><?= $makeup['js_hatText']['text'] ?></div></div>
    </div>
</div>
    <div class="clr"></div>
    <button onclick="ajax.simpleRequest('/management/save/makeup.ajax', '');">Сохранить</button>
</div>
<iframe src="<?= Path::get()->getUrl() ?>?preview=1" style="width:100%;height:100%;" id="js_previewFrame" onload="design.init();"></iframe>
