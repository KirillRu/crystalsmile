<?php
/*Created by ������ (11.02.2016 15:06)*/
require_once(CLASSES_PATH . 'Models/Iterators/SerializeText.php');
class ManagementModulesListIteratorsManager_usersSerializeText extends ModelsIteratorsSerializeText {

    protected function _setSerialiseText() {
   		$file = CLASSES_PATH . 'Zubi/FileData/manager_users.db';
   		if (file_exists($file)) $this->_serializeText = file_get_contents($file);
        else $this->_serializeText = '';
   	}

	function current(){
		list($login, $passw) = explode(':', $this->_nextSerializeString);
		return ['login'=>trim($login), 'passw_cache'=>trim($passw)];
	}

	protected function _nextItem(){
		$this->_nextSerializeString = false;

		$pos = strpos($this->_serializeText, self::delimiter);
		if ($pos){
			$this->_nextSerializeString = substr($this->_serializeText, 0, $pos + 1);
			$this->_serializeText = substr($this->_serializeText, $pos + strlen(self::delimiter));
		}
		elseif (($pos === false)&&strlen($this->_serializeText)) {
			$this->_nextSerializeString = $this->_serializeText;
			$this->_serializeText = '';
		}
		return $this->_nextSerializeString;
	}

}