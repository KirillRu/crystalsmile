<?php
/*Created by Кирилл (13.02.2016 17:19)*/
require_once(CLASSES_PATH . 'Models/Iterators/SerializeText.php');
class ManagementModulesListIteratorsGoodsSerializeText extends ModelsIteratorsSerializeText {

    protected function _setSerialiseText() {
   		$file = CLASSES_PATH . 'Zubi/FileData/goods.db';
   		if (file_exists($file)) $this->_serializeText = file_get_contents($file);
        else $this->_serializeText = '';
   	}

	function current(){
		$current = parent::current();
		$current['photoPath'] = '';
		$photoName = Text::get()->strToLower($current['id_good']);
		if (file_exists(ROOT_DIR . 'img/goods/' . $photoName . '.jpg')) $current['photoPath'] = SITE_ROOT . 'img/goods/' . $photoName . '.jpg';
		elseif (file_exists(ROOT_DIR . 'img/goods/' . $photoName . '.png')) $current['photoPath'] = SITE_ROOT . 'img/goods/' . $photoName . '.png';
		elseif (file_exists(ROOT_DIR . 'img/goods/' . $photoName . '.gif')) $current['photoPath'] = SITE_ROOT . 'img/goods/' . $photoName . '.gif';
		return $current;
	}



}