<?php
/*Created by ������ (08.02.2016 8:41)*/
require_once(CLASSES_PATH . 'Models/Iterators/SerializeText.php');
class ManagementModulesListIteratorsSend_lettersSerializeText extends ModelsIteratorsSerializeText {

    protected function _setSerialiseText() {
   		$file = CLASSES_PATH . 'Zubi/FileData/send_letters.db';
   		if (file_exists($file)) $this->_serializeText = file_get_contents($file);
        else $this->_serializeText = '';
   	}

    protected function _nextItem(){
   		$this->_nextSerializeString = false;

   		$pos = strrpos($this->_serializeText, self::delimiter);
   		if ($pos !== false){
   			$this->_nextSerializeString = substr($this->_serializeText, $pos + strlen(self::delimiter));
   			$this->_serializeText = substr($this->_serializeText, 0, (-1)*(strlen($this->_nextSerializeString) + strlen(self::delimiter)));
   		}
   		return $this->_nextSerializeString;
   	}


}