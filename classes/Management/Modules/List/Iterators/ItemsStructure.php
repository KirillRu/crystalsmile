<?php
/*Created by ������ (29.10.2015 15:18)*/
class ManagementModulesListIteratorsItemsStructure extends ArrayIterator {
	static protected $_tplFields = [];//������ �������� ��� �����, ���� �� ������ ������ �� ������ ������� ��� ������� ����

	function current(){
		$view = new ViewObjects();
		//������� ��� ������ ����� � �����
		$view->setTpl($this->_getTpl());// �� ��������� ��� ����
		$view->assign('value', parent::current());
		$view->assign('field', $this->_getField());
		return $view;
	}

	function valid(){
		if (!parent::valid()) return false;
		if (!$this->_displayField()) {
			$this->next();
			return $this->valid();
		}
		return true;
	}

	function key(){
		$field = ManagementListManager::get()->getStructure()->getField(parent::key());
		return $field['name'];
	}

	private function _displayField() {
		$field = $this->_getField();
		return $field && !empty($field['showManagement']) && $field['showManagement']['location'] == 1;
	}

	private function _getField() {
		$iAF = parent::key();
		$fields = ManagementListManager::get()->getStructure()->getFields();
		if (empty($fields[$iAF])) $this->_field = false;
		return $fields[$iAF];
	}

	private function _getTpl() {
		$field = $this->_getField();
		if (!isset(self::$_tplFields[$field['nameEn']])) {
			//������� ��� ������ ����� � �����
			$tpl = CLASSES_PATH . 'Management/Modules/List/' . Text::get()->strToUpperFirst(ManagementListManager::get()->getTable()) . '/View/Fields/NameEn/' . $field['nameEn'] . '.tpl';
			if (file_exists($tpl)) self::$_tplFields[$field['nameEn']] = $tpl;//� ���������� �������, �� ����� ����
			else {
				$tpl = CLASSES_PATH . 'Management/Modules/List/' . Text::get()->strToUpperFirst(ManagementListManager::get()->getTable()) . '/View/Fields/Types/' . $field['type'] . '.tpl';
				if (file_exists($tpl)) self::$_tplFields[$field['nameEn']] = $tpl;//� ���������� �������, �� ���� ����
				else {
					$tpl = CLASSES_PATH . 'Management/Modules/List/View/Fields/NameEn/' . $field['nameEn'] . '.tpl';
					if (file_exists($tpl)) self::$_tplFields[$field['nameEn']] = $tpl;//�� ����� ����
					else {
						$tpl = CLASSES_PATH . 'Management/Modules/List/View/Fields/Types/' . $field['type'] . '.tpl';
						if (file_exists($tpl)) self::$_tplFields[$field['nameEn']] = $tpl;//�� ���� ����
						else self::$_tplFields[$field['nameEn']] = CLASSES_PATH . 'Management/Modules/List/View/Fields/default.tpl';//��� ����
					}
				}
			}
		}
		return self::$_tplFields[$field['nameEn']];
	}

}