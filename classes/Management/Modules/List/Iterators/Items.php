<?php
/*Created by ������ (29.10.2015 15:17)*/
require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/ItemsStructure.php');
class ManagementModulesListIteratorsItems extends IteratorIterator{

	function current() {
		$current = parent::current();
		$structure = ManagementListManager::get()->getStructure();
		$current['itemsStructure'] = new ManagementModulesListIteratorsItemsStructure((new ModelsStructureValues($structure, $current, 'type'))->get());
		if (!empty($current[$structure->getId()])) $current['id'] = $current[$structure->getId()];
		return $current;
	}

	function key(){
		return parent::key() + (ManagementListManager::get()->getPageData('page') - 1) * ManagementListManager::get()->getPageData('limit');
	}
}