<?php
/*Created by ������ (29.10.2015 14:17)*/
class ManagementListManager {
	protected static $_instance = null;

	protected $_pageData = [], $_count = null, $_items = null, $_conditionJoin = null, $_structureList = null, $_structureSearch = null, $_userCategorys = null;
	protected $_limit = 100; // ����������� ��� ���-�� ������� �� ��������

	function __construct(){
		$this->_pageData = Data::get()->g('pageData', []);

//		//��������� ����� ���� filter, page, vm, vy,... ���� ����� �� �� pageData, � �� data
//		foreach ($this->_getOutPageDataKeys() as $key) {
//			if (Data::get()->g($key) && !array_key_exists($key, $this->_pageData)) $this->_pageData[$key] = Data::get()->g($key);
//		}

		if (!array_key_exists('moduleName', $this->_pageData)) $this->_pageData['moduleName'] = Data::get()->g('moduleName');
		$this->_pageData['filter'] = $this->_getPageDataFilter();
		$this->_pageData['order'] = $this->_getPageDataOrder();

		//�������� page � ����������� ����
		$this->_pageData['page'] = !empty($this->_pageData['page']) ? (int)$this->_pageData['page'] : 1;

		if (empty($this->_pageData['filter']) || Data::get()->g('filter') !== null)
			$this->_pageData['filter'] = Data::get()->g('filter')?:[];

		//�������� limit � ����������� ����
		if (empty($this->_pageData['limit']) || Data::get()->g('limit'))
			$this->_pageData['limit'] = Data::get()->g('limit', 30);

		$this->_pageData['limit'] = min($this->_limit, max(1, (int)$this->_pageData['limit']));
		$this->_pageData['table'] = $this->getTable();
		$this->_pageData = Functions__mergeArrays($this->_pageData, $this->_getRequiredPageData());
	}

	/*��������� ��������� ����� ����� �� �� pageData.��� �� ��� �������� �� ��������� ����� �� �������� ��
	pageData, ��� �� �� ����� �������� ������ ������(����. ������ ������ ���� filer � Data.*/
//	protected function _getOutPageDataKeys(){
//		return ['page', 'filter', 'moduleName'];
//	}
//
//	function getQueryStr() {
//		$data = $this->getPageData();
//		//����� ������ page. ��� �� ����� �� � ������������ ������, �� � ��������� ��� ������������ ��������
//		unset($data['page']);
//
//		$result = ['pageData' => $data];
//
//		//��������� filter, ...  �� pageData. ��� ���� ��� �� filter ���������� �� ������ ������(���� ���� ������� ����. ��������)
//		foreach ($this->_getOutPageDataKeys() as $moveFromPD) {
//			if (isset($data[$moveFromPD])) $result[$moveFromPD] = $data[$moveFromPD];
//		}
//
//		return $data ? urldecode(http_build_query($result)) : '';
//	}

	function getTable() {
		return $this->getPageData('moduleName');
	}

	//���������� ���� ��������������� �������
	function getCount(){
		if ($this->_count === null) $this->_setItems();
		return $this->_count;
	}

	function getStructureSearch(){
		if ($this->_structureSearch === null){
			require_once(CLASSES_PATH . 'Management/Models/Structure/Search.php');
			$this->_structureSearch = new ManagementModelsStructureSearch($this->getTable());
		}
		return $this->_structureSearch;
	}

	function getStructure(){
		if ($this->_structureList === null){
			require_once(CLASSES_PATH . 'Management/Models/Structure/From_files/From_files.php');
			$this->_structureList = new ManagementModelsStructureFrom_files($this->getTable(), $this->getPageData('id_category'));
		}
		return $this->_structureList;
	}

	/**� pageData �������� ��� ���� �� ��������(�������, ����������, ����� ��������, moduleName)...*/
	function getPageData($key = null, $default = null){
		if ($key === null) return $this->_pageData;
		return isset($this->_pageData[$key]) ? $this->_pageData[$key] : $default;
	}

	function getPageDataValue($arrName, $key, $default = null) {
		$array = $this->getPageData($arrName);
		if (!is_array($array) || !isset($array[$key])) return $default;
		return $array[$key];
	}

	function getAllOrders(){
		return [];
	}

	/*��� �������� ����� ������������ ��� �� ���������� ������� ������ � �����. ��� �� �� ��� �������� ������� � ������� _getConditionJoinUnique*/
	function getFiltersUnique(){
		return [];
	}

	/**
	 * @return ModelsIteratorsMySqli
	 */
	function getItems(){
		if ($this->_items === null) $this->_setItems();
		return $this->_items;
	}

	function getFilterStrings(){
		require_once(CLASSES_PATH . 'Models/Structure/FilterListBuilder.php');
		$filterBuilder = new ModelsStructureFilterListBuilder($this->getStructureSearch());
		$filterStrings = $filterBuilder->getFilterStrings($this->getStructureSearch(), $this->getPageData('filter', []));

		return array_merge($filterStrings, $this->_getFilterStringsUnique());
	}

	//��� pageData ����� � ����� ������ � �� ����������� �� ���� ��� ������ �� �����
	//���� ��� ������� ������ "����� ������" ������ �� ���� ������� �� ��������
	protected function _getRequiredPageData() {
		$requiredPageData = [];
		return $requiredPageData;
	}

	protected function _getPageDataFilter(){
		$result = Data::get()->g('filter', Data::get()->gav('pageData', 'filter'));
		if ($result !== null) return $result;

		return $this->_getPageDataFilterDefault();
	}

	protected function _getPageDataFilterDefault(){
		return [];
	}

	protected function _getPageDataOrder(){
		$order = Data::get()->g('order', Data::get()->gav('pageData', 'order'));
		if ($order !== null) return $order;

		return $this->_getPageDataOrderDefault();
	}

	protected function _getPageDataOrderDefault(){
		$allOrders = $this->getAllOrders();
		$order = [];
		return $order;
	}

	protected function _setItems(){
		list($condition, $join) = $this->_getConditionJoin();
		$join = $join ? ' ' . implode(' ', $join) : '';
		$condition = $condition ? ' where ' . implode(' and ', $condition) : '';

		$limit = $this->getPageData('limit');
		$limit = ' limit ' . (($this->getPageData('page') - 1) * $limit) . ', ' . $limit;

		$order = $this->_getOrder();
		$order = $order ? ' order by ' . implode(', ', $order) : '';

		$table = $this->getStructure()->getTable();
		$idName = $this->getStructure()->getId();

		$sql = 'select sql_calc_found_rows t.' . $idName . ' as id from ' . $table . ' t' . $join . $condition . $order . $limit . ';';
		$sql .= 'select found_rows();';

		list($itemsIds, $this->_count) = (new ModelsDriverBDIteratorDriver())->multiSelectCount($sql);

		if ($itemsIds){
			$ids = '';
			foreach ($itemsIds as $itemId) $ids .=  ', ' . $itemId['id'];
			$ids = substr($ids, 2); unset($itemsIds);

			list($select, $join) = $this->_getSelectJoin();
			$sql = 'select ' . implode(', ', $select) . ' from ' . $table . ' t ' .
				($join ? implode(' ', $join) . ' ' : '') .
				'where t.' . $idName . ' in (' . $ids . ') '.
				'order by FIELD(t.' . $idName . ', ' . $ids . ')' .
				'';
			$this->_items = (new ModelsDriverBDIteratorDriver())->multiSelect($sql);
		}
		else {
			$this->_items = [];
		}
	}

	/*���������� ������ - [������ select, ������ join(����������� ��� select)]*/
	protected function _getSelectJoin(){
		require_once(CLASSES_PATH . 'Models/Structure/QueryBuilder.php');
		list($select, $join) = (new ModelsStructureQueryBuilder($this->getStructure()))->getSelectJoin('showManagement');
		return [$select, $join];
	}

	//���������� ������� � join, �� ������� ����� ����������� items(���� ids) �� ��.
	protected function _getConditionJoin(){
		if ($this->_conditionJoin === null){
			$condition = $join = [];
			$filters = ManagementListManager::get()->getPageData('filter', []);
			if ($filters){
				$fields = $this->getStructureSearch()->getFields();
				foreach ($filters as $nameEn => $value) {
				    if (isset($fields[$nameEn])){
					    list($fieldCondition, $fieldJoin) = $this->_getConditionJoinField($fields[$nameEn], $value);
					    if ($fieldCondition) $condition = array_merge($condition, $fieldCondition);
					    if ($fieldJoin) $join = array_merge($join, $fieldJoin);
				    }
				}

				list($fieldCondition, $fieldJoin) = $this->_getConditionJoinUnique();
			    if ($fieldCondition) $condition = array_merge($condition, $fieldCondition);
			    if ($fieldJoin) $join = array_merge($join, $fieldJoin);
			}
			$this->_conditionJoin = [$condition, $join];
		}
		return $this->_conditionJoin;
	}

	/*��� ������� �� �����, ������� ��� � �����(�������� �������� ����)*/
	protected function _getConditionJoinUnique(){
		$filters = ManagementListManager::get()->getPageData('filter', []);
		$condition = $join = [];
		foreach ($this->getFiltersUnique() as $nameEn) {
			if (empty($filters[$nameEn])) continue;

			switch(true){
				default:
					$condition[$nameEn] = 't.' . $nameEn . ' = ' . $filters[$nameEn];
					break;
			}
		}
		return [$condition, $join];
	}

	protected function _getFilterStringsUnique(){
		$filters = ManagementListManager::get()->getPageData('filter', []);
		$filterStrings = [];
		if (!empty($filters['id_user'])){
			$view = new ViewObjects();
			$view->setTpl(CLASSES_PATH . 'Management/Modules/List/View/filter_strings_id_user.tpl');
			$view->assign('idUser', $filters['id_user']);
			$filterStrings['id_user'] = $view->fetch();
		}
		if (!empty($filters['reiterative'])){
			$filterStrings['reiterative'] = '�������������';
		}
		return $filterStrings;
	}

	//����, �� ������� ����� ����� ���� ���������
	protected function _getSearchFields(){
		return ['t.name'];
	}

	/*������� �� �����, ������� ���� � ����� ������*/
	protected function _getConditionJoinField($field, $value){
		$condition = $join = [];
		switch ($field['nameEn']) {
		    case 'name':
				if (!$value) break;

			    $searchFields = $this->_getSearchFields();
				if ($searchFields){
					$keywords = [];
					foreach ($searchFields as $fieldName) {
						$keywords[] = $fieldName . ' like ' . Text::get()->mest('%' . $value . '%');
					}
					$condition['name'] = '(' . implode(' or ', $keywords) . ')';
					unset($searchFields, $keywords);
				}

			    if (is_numeric($value)) {
				    if (!empty($condition['name'])) $condition['name'] .= ' or ';
				    $condition['name'] .= '(t.' . $this->getFilterStrings()->getId() . ' = ' . (int) $value . ')';
				}
			    break;
		    case 'id_category':
//			    if (!$value) break;

//			    require_once(CLASSES_PATH . 'Models/Structure/FilterListBuilder.php');
//				$filterBuilder = new ModelsStructureFilterListBuilder($this->getStructureSearch());
//			    $condition['id_category'] = $filterBuilder->conditionUnique($field, (int)$value, 't', 'sprNested');
			    $condition['id_category'] = 't.id_category = ' . (int) $value;
			    break;
	    }
		return [$condition, $join];
	}

	/*������� ����� ��� ��������� sql �������*/
	protected function _getOrder(){
		$result = [];
		$order = $this->getPageData('order');
		if ($order){
			$allOrders = $this->getAllOrders();
			foreach ($order as $orderName => $value) {
				if (isset($allOrders[$orderName])){
					$nameEn = $value;
					$order = 'desc';
					if (strpos($value, '-') !== false)
						list($nameEn, $order) = explode('-', $value);//� $value ������ ���� "$nameEn-asc"


					if (!empty($allOrders[$orderName][$nameEn]['order'])) $result[$nameEn] = $allOrders[$orderName][$nameEn]['order'];
					elseif($nameEn) $result[$nameEn] = 't.' . $nameEn . ' ' . $order;
				}
			}
		}
		return $result;
	}

	/**@return ManagementListManager*/
	static function get(){
		if (self::$_instance === null) {
			$moduleName = Text::get()->strToUpperFirst(Data::get()->gav('pageData', 'moduleName', Data::get()->g('moduleName')));
			$file = CLASSES_PATH . 'Management/Modules/List/' . $moduleName . '/Manager.php';
			if (file_exists($file)){
                require_once($file);
				$className = 'ManagementModulesList' . $moduleName . 'Manager';
				self::$_instance = new $className;
				return self::$_instance;
			}
			self::$_instance = new self;
		}
		return self::$_instance;
	}
}