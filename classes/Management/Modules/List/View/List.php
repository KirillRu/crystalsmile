<?php
/*Created by ������ (29.10.2015 15:15)*/
class ManagementModulesListView extends ViewObjects{

	function __construct($fileName) {
		$listPath = CLASSES_PATH . 'Management/Modules/List/';
		$moduleName = Text::get()->strToUpperFirst(ManagementListManager::get()->getPageData('moduleName'));
		if (file_exists($listPath . $moduleName . '/View/' . $fileName)) {
			$this->_tpl = $listPath . $moduleName . '/View/' . $fileName;
		} else {
			$this->_tpl = $listPath . 'View/' . $fileName;
		}
	}
}