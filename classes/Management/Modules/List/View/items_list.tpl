<?php /**@var $this ViewObjects */
/**@var $structure ModelsStructure */
foreach ($this->get('items') as $k => $item): ?>
<div id="js_itemRow_<?=$item['id']?>" class="js_itemRow items-list no_active_border">
	<table class="items-list">
			<tr>
				<td class="num" rowspan="2"><?=$k?>.</td>
				<td class="service" nowrap="nowrap" rowspan="2">
                    <?php foreach ($item['itemsStructure'] as $filed):
                        ?><?php $field->display();
                    endforeach; ?>
                </td>
			</tr>
	</table>
</div>
<?php endforeach; ?>