<?php /**@var $this ViewObjects*/
$value = $this->get('value');
if ($value === null) $value = '';
else {
	$field = $this->get('field');
	$items = $field['idSpr']->getItems();
	if (isset($items[$value])): ?>
		<?= (string) $items[$value]; ?>
	<?php endif;
}