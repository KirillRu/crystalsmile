<?php /** @var ViewObjects */?>
<div id="js_listManagementContent" class="items-content">
	<h2><?=$this->get('header')?></h2>
	<?php $this->actions->assign('position', 'top')->display();?>
	<?php $this->items->display();?>
	<?php $this->actions->assign('position', 'bottom')->display();?>
</div>