<?php /**@var $this ViewObjects*/?>
<div class="group-action">
	<span class="pr10 pointer"><a data-ignore-miss-click="true" onclick="management.items.groupSetChecked(true)">Выделить все</a></span>
	<span class="pr10 pointer"><a data-ignore-miss-click="true" onclick="management.items.groupSetChecked(false)">Убрать выделение</a></span>

	<?php if ($this->get('isGroupAct')) :?>
		<select data-ignore-miss-click="true" id="js_groupActions_<?=$this->get('position')?>">
			<option value="">Выбрать групповую операцию...</option>
			<?php foreach ($this->get('items') as $actionName => $name): ?>
				<option value="<?=$actionName?>"><?=$name?></option>
			<?php endforeach; ?>
		</select>
	<?php endif ?>
	<script type="text/javascript">
		management.groupActions.init({position: '<?=$this->get('position')?>'});
	</script>
</div>