<?php
/*Created by Кирилл (08.02.2016 8:34)*/
class ManagementModulesListSend_lettersManager extends ManagementListManager {

    protected function _setItems(){
        require_once(PROJECT_PATH . 'Modules/List/Iterators/Send_letters/SerializeText.php');
        $this->_items = new ManagementModulesListIteratorsSend_lettersSerializeText();
        $this->_count = $this->_items->count();
   	}

    /**в pageData хранится вся инфа по странице(фильтры, сортировки, номер страницы, moduleName)...*/
   	function getPageData($key = null, $default = null){
   		if ($key === null) return $this->_pageData;
        if ($key == 'limit') return $this->getCount();
   		return isset($this->_pageData[$key]) ? $this->_pageData[$key] : $default;
   	}


}

