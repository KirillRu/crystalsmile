<?php /**@var $this ViewObjects */
/**@var $structure ModelsStructure */
foreach ($this->get('items') as $k => $item): ?>
<div id="js_itemRow_<?=$item['id']?>" class="js_itemRow items-list no_active_border">
	<table class="items-list">
			<tr>
				<td class="num" rowspan="2"><?=($k+1)?>.</td>
				<td class="service" nowrap="nowrap" rowspan="2">
                    <div><?= date('d.m.Y H:i', $item['date_add']) ?></div>
                    <div><?= htmlspecialchars($item['name']) ?></div>
                    <div><?= htmlspecialchars($item['email']) ?></div>
                    <div><?= htmlspecialchars($item['phone']) ?></div>
                </td>
				<td class="info"><?= str_replace("\n", '<br>', htmlspecialchars($item['text'])) ?></td>
			</tr>
	</table>
</div>
<?php endforeach; ?>