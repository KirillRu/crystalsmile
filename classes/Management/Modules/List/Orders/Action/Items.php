<?php
/*Created by Кирилл (14.02.2016 20:11)*/

require_once(CLASSES_PATH . 'Management/Modules/List/Action/Items.php');
class ManagementModulesListOrdersActionItems extends ManagementModulesListActionItems {

	function _getViewItemsList() {
		$view = parent::_getViewItemsList();

		$goods = [];
		$fileContent = CLASSES_PATH . 'Zubi/FileData/goods.db';
		if (file_exists($fileContent)) {
			require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Goods/SerializeText.php');
			foreach ((new ManagementModulesListIteratorsGoodsSerializeText()) as $item) $goods[$item['id_good']] = $item;
		}

		$view->assign('goods', $goods);
		return $view;
	}

	protected function _getIteratorItems($items){
		require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Orders/Items.php');
		return new ManagementModulesListIteratorsOrdersItems($items);
	}


}