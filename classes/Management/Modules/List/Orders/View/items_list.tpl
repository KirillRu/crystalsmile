<?php /**@var $this ViewObjects */
/**@var $structure ModelsStructure */
$goods = $this->get('goods', []);
foreach ($this->get('items') as $k => $item): ?>
<div id="js_itemRow_<?=$item['id']?>" class="js_itemRow items-list no_active_border">
	<table class="items-list">
		<tr>
			<td class="num" rowspan="2"><?=($k+1)?>.</td>
			<td class="service" nowrap="nowrap" rowspan="2">
                <div><?= date('d.m.Y H:i', $item['date_add']) ?></div>
                <div><?= htmlspecialchars($item['name']) ?></div>
                <div><?= htmlspecialchars($item['email']) ?></div>
                <div><?= htmlspecialchars($item['phone']) ?></div>
            </td>
			<td class="info">
				<table>
					<tr><th class="">№</th><th>Наименование</th><th>Кол-во</th><th>Ед. изм.</th></tr>
					<?php foreach ($item['goods'] as $i=>$good): ?><tr>
						<td><?= $i+1 ?></td>
						<td><?php
						if (isset($goods[$good['id_good']])): ?><?= $goods[$good['id_good']]['name'] ?><?php
						else: ?><?= $good['id_good'] ?><?php
						endif; ?></td>
						<td><?= (int) $good['counts'] ?></td>
						<td><?php if (isset($goods[$good['id_good']])): ?><?= $goods[$good['id_good']]['ed_izm'] ?><?php endif; ?></td>
					</tr><?php
					endforeach; ?>
				</table>
		</td>
			</tr>
	</table>
</div>
<?php endforeach; ?>