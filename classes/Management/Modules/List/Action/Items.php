<?php
/*Created by Кирилл (29.10.2015 15:11)*/
require_once(CLASSES_PATH . 'Management/Modules/List/View/List.php');
class ManagementModulesListActionItems extends AbstractAction{

	function getAction(){
		if (static::class === self::class){
			$moduleName = ManagementListManager::get()->getPageData('moduleName');
			$file = CLASSES_PATH . 'Management/Modules/List/'.Text::get()->strToUpperFirst($moduleName).'/Action/Items.php';
			if (file_exists($file)){
				require_once($file);
				$className = 'ManagementModulesList'.Text::get()->strToUpperFirst($moduleName).'ActionItems';
				return (new $className)->getAction();
			}
		}
		return parent::getAction();
	}

	function run(){
		$view = new ManagementModulesListView('items.tpl');
        $view->count = $this->_getViewCount();
		$view->pager = $this->_getViewPager();
		$view->itemsList = $this->_getViewItemsList();
		Head::get()->appendOptions(ManagementListManager::get()->getPageData(), 'pageData');
		return $view;
	}

	protected function _getViewCount(){
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Management/Modules/List/View/count.tpl');
		$view->assign('count', ManagementListManager::get()->getCount());
		return $view;
	}

	protected function _getViewPager(){
		$view = new ViewPager();
		$view->assign('page', ManagementListManager::get()->getPageData('page'));
		$view->assign('countToPage', ManagementListManager::get()->getPageData('limit'));
		$view->assign('amount', ManagementListManager::get()->getCount());
		$view->assign('onclick', 'management.list.selectPage(\'runModule/List/Items.ajax\', {page});');
		return $view;
	}

	//----------------
	//ViewItemsList
	protected function _getViewItemsList(){
		if (!ManagementListManager::get()->getCount()) return new View();
		require_once(CLASSES_PATH . 'Models/Structure/Values.php');
		$view = new ManagementModulesListView('items_list.tpl');
		$view->assign('items', $this->_getIteratorItems(ManagementListManager::get()->getItems()));
		$view->assign('isPosition', ManagementListManager::get()->getStructure()->isPosition());
		$view->buttons = $this->_getViewItemsListButtons();
		return $view;
	}

	protected function _getIteratorItems($items){
		require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Items.php');
		return new ManagementModulesListIteratorsItems($items);
	}

	protected function _getViewItemsListButtons(){
		$view = new ManagementModulesListView('items_list_buttons.tpl');
		$view->assign('moduleName', ManagementListManager::get()->getPageData('moduleName'));
		return $view;
	}
	//ViewItemsList
	//----------------
}