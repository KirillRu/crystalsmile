<?php
/*Created by Кирилл (20.01.2016 16:36)*/
require_once(CLASSES_PATH . 'Sites/Admin/Modules/List/Manager.php');
class ManagementModulesListActionDelete extends AbstractAction {

	function getAction() {
		if (static::class === self::class){
			$path = CLASSES_PATH . 'Management/Modules/List/' . ManagementListManager::get()->getTable() . '/Action/Delete.php';
			if (file_exists($path)){
				require_once($path);
				$className = 'ManagementModulesList' . ManagementListManager::get()->getTable() . 'ActionDelete';
				return (new $className)->getAction();
			}
		}
		return parent::getAction();
	}

    function run() {
		$this->_delete();
	    require_once(CLASSES_PATH . 'Management/Modules/List/Action/Items.php');
        return (new ManagementModulesListActionItems)->getAction()->run();
    }

	protected function _delete() {
		$table = ManagementListManager::get()->getStructure()->getTable();
		if (!empty($table)) {
			$sql = ''.
				'delete from ' . $table . ' ' .
				'where (' . ManagementListManager::get()->getStructure()->getId() . ' = ' . (int)Data::get()->g('id') . ') '.
			'';
			ModelsDriverBDSimpleDriver::getDriver()->simpleQuery($sql);
			ModelsCacheMemcache::get()->formatting(array_unique([$table, ManagementListManager::get()->getTable()]));
		}
	}
}