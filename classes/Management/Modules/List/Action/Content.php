<?php
/*Created by ������ (29.10.2015 14:10)*/
class ManagementModulesListActionContent extends AbstractAction{

	function getAction() {
		if (static::class === self::class) {
			$moduleName = ManagementListManager::get()->getTable();
			if (!empty($moduleName)) {
				$file = CLASSES_PATH . 'Management/Modules/List/'.Text::get()->strToUpperFirst($moduleName).'/Action/Content.php';
				if (file_exists($file)){
					require_once($file);
					$className = 'ManagementModulesList'.Text::get()->strToUpperFirst($moduleName).'ActionContent';
					return (new $className)->getAction();
				}
			}
		}
		return parent::getAction();
	}

	function run() {
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Management/Modules/List/View/content.tpl');
		$view->assign('header', ManagementListManager::get()->getStructure()->getTableName());
		$view->actions = $this->_viewActions();
		$view->items = $this->_viewItems();
		return $view;
	}

	protected function _viewActions() {
		require_once(CLASSES_PATH . 'Management/Modules/List/View/List.php');
		return new ManagementModulesListView('actions.tpl');
	}

	protected function _viewItems(){
		require_once(dirname(__FILE__) . '/Items.php');
		return (new ManagementModulesListActionItems())->getAction()->run();
	}
}