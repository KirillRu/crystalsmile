<?php /**@var $this ViewObjects*/
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>Редактор оформления сайта</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="ru" />
    <link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/index_0.css">
   	<link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/tool.css">
   	<link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/service.css">
    <link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/design.css">
	<link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/colorPicker/colorpicker.css" />
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/modules/scriptLoader.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/modules/popup.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/modules/colorPicker/colorpicker.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/functions.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/service.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/design.js"></script>
</head>
<body><?php
    $this->top->display();
    $this->content->display();
    $this->bottom->display();
?></body>
</html>


