<?php
/*
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com (08.07.14 17:20)
*/

/**
 * @method string name($id_money)
 * @method float rate($id_money)
 * @method string char_code($id_money)
 */
class Price {

	/** отформатировать число $price.
	 * @param $price - цена(число)
	 * @param int $idMoney_or_charCode - поле idModey(ид из таблицы), или char_code(RUR, USD,..), или name (руб., $, грн.)
	 * @param string $unit - если передано, то после цены поставится $separator . $unit
	 * @param bool $useSpan - поставится '<span>' . $separator . $unit . '</span>'
	 * @param string $separator
	 * @param bool $show_0 показывать 0, если цена 0?
	 *
	 * @return mixed|string
	 */
	function textPrice($price, $idMoney_or_charCode = 1, $unit = '', $useSpan = true, $separator = ' / ', $show_0 = false) {
		if (empty($price) || !is_numeric($price)) $price = 0;
		if (empty($price) && !$show_0) return '';

		$textPrice = str_replace(' ', '&thinsp;', number_format($price, ($price - floor($price) > 0 ? 2 : 0), ',', ' '));
		if (!empty($idMoney_or_charCode)) $textPrice .= ' ' . $this->name($idMoney_or_charCode);
		if (!empty($unit)) $textPrice .= $useSpan ? '<span>' . $separator . $unit . '</span>' : $separator . $unit;
		return $textPrice;
	}

	function __call($name, $arguments) {
		$money = $this->getMoney(array_shift($arguments));
		if (isset($money[$name])) return $money[$name];
		return null;
	}

	/**
	 * @param $value
	 * @param int $idMoneyFrom - текущая валюта
	 * @param int $idMoneyTo - валюта, в которую переводим
	 * @return float
	 */
	function convert($value, $idMoneyFrom = 1, $idMoneyTo = 1) {
		return ($idMoneyFrom != $idMoneyTo) ? round($value * $this->rate($idMoneyFrom) / $this->rate($idMoneyTo), 2) : $value;
	}

	function getMoney($idMoney_or_charCode) {
		if (!$idMoney_or_charCode) return array();

		if (is_numeric($idMoney_or_charCode))
			$sql = 'select * from _sprs_utf8.moneys where (id_money = ' . (int)$idMoney_or_charCode . ') limit 0, 1';
		else {
			$idMoney_or_charCode = Text::get()->strToLower(trim($idMoney_or_charCode));
			$sql = 'select * from _sprs_utf8.moneys where (lower(char_code) = ' . Text::get()->mest($idMoney_or_charCode) . ') or (lower(name) = ' . Text::get()->mest($idMoney_or_charCode) . ') limit 0, 1';
		}

		return ModelsDriverBDCache::get()->rowSelect($sql, ['moneys'], 86400);
	}

	/**
	 * @return Price
	 */
	static function get() {
		static $self = null;
		if ($self === null) $self = new Price();
		return $self;
	}

}
 