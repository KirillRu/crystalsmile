<?php
/*Created by Edik (24.10.14 10:51)*/
class __view{
	private $_vars = array();
	function assign($key, $value){$this->_vars[$key] = $value;}
	function get($key, $default = null){return isset($this->_vars[$key]) ? $this->_vars[$key] : $default;}
	function display(){include(dirname(__FILE__) . '/View/P/print_r.tpl');}
}

class P{
	private static $_timeUsage = 0, $_memoryUsage = 0;

	static function _print_r(...$args){
		if (!_isDebug()) return;
		$view = new __view();
		$view->assign('trace', debug_backtrace());
		$view->assign('args', $args);
		$view->display();
		return;
	}

	static function _print_r_in_file($_ = null){
		$args = func_get_args();
	    ob_start();
			$view = new __view();
			$view->assign('trace', debug_backtrace());
			$view->assign('args', $args);
			$view->display();
	        $content = ob_get_contents();
	    ob_end_clean();
	    $file = '/usr/home/vlad01/www/_service/_nps/classes/Management/Modules/Web_sock_print_r/_print_r.txt';
	    if (!file_exists($file)) {
	        $f = fopen($file, 'w');
	        fwrite($f, $content.'<br>');
	    } else {
	        $f = fopen($file, 'a+');
	        fwrite($f, $content.'<br>');
	    }
	    fclose($f);
	}

	static function memoryUsageStart(){
		if (!_isDebug()) return;
		self::_print_r('Установлена точка отсчета.');
		self::$_memoryUsage = memory_get_usage();
	}

	static function memoryUsageEnd(){
		if (!_isDebug()) return;
		$mu = memory_get_usage();
		if (self::$_memoryUsage)
			self::_print_r('Память, выделенная от начала точки отсчёта: ' . number_format($mu - self::$_memoryUsage, 0, '', '\'') . " bytes\n\n" .
				'Память, выделенная c начала работы скрипта: ' . number_format($mu, 0, '', ' ') . " bytes\n\n".
				'Пиковая память: ' . number_format(memory_get_peak_usage(), 0, '', ' ') . ' bytes'
			);
		else {
			self::_print_r('Память, выделенная c начала работы скрипта: ' . number_format($mu, 0, '', ' ') . " bytes\n\n".
				'Пиковая память: ' . number_format(memory_get_peak_usage(), 0, '', ' ') . ' bytes'
			);
		}
	}

	static function timeUsageStart(){
		if (!_isDebug()) return;
		self::$_timeUsage = microtime(true);
	}

	static function timeUsageEnd(){
		if (!_isDebug()) return;
		$str = '';
		if (isset($_SERVER['REQUEST_TIME_FLOAT'])){
			$str .= 'С начала выполнения скрипта прошло: ' . number_format(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 5) . ' sec';
		}
		if (self::$_timeUsage) {
			$str .= "\n" . 'С точки отчёта прошло: ' . number_format(microtime(true) - self::$_timeUsage, 5) . ' sec';
		}
		self::_print_r($str);
	}

	static function exception($_args = null){
		if (!_isDebug()) return;
		require_once(CLASSES_PATH . 'Exception/Portal.php');
		$arg_content = '';
		if (func_num_args()){
			ob_start();
				self::_print_r(func_get_args());
				$arg_content = ob_get_contents();
			ob_end_clean();
		}
		throw new ExceptionPortal($arg_content);
	}
}

//почему-то вызывая эти функции в первый раз результат не верный. Поэтому вызываем их тут что бы это поправить
ob_start();
P::memoryUsageStart();
P::memoryUsageEnd();
ob_end_clean();
