<?php
class Path {
	/**
	 * @var Path
	 */
	static protected $_self = null;

	protected function __construct() {
		self::$_self = $this;
	}

	/**
	 * @param $url
	 * @return string $url в котором все двойные слэши(//) заменены на один(/) кроме http://
	 */
	function clearUrl($url){
		return preg_replace('/(?<!^http:)\/{2,}/i', '/', $url);
	}

	/**Убираем слэши в начале и в конце
	 * @param $string
	 * @return string
	 */
	function clearSlashes($string){
		while (substr($string, 0, 1) == '/') $string = substr($string, 1);
		while (substr($string, -1) == '/') $string = substr($string, 0, -1);
		return $string;
	}

	/**
	 * @param string $cityName строка, которая вставится после http://
	 * @return string полный url адрес с учётом текушего региона.
	 */
	function getUrl() {
		return 'http://' . Cfg__get('path', 'site') . '/';
	}

	/** из http://www.izh.equiptorg.ru получим equiptorg.ru
	 * @param $url
	 * @return string доменное имя
	 */
	function getDomainName($url){
		if (empty($url)) return null;
		preg_match("/^(http:\/\/)?([^\/]+)/i", $url, $matches);
		$host = $matches[2];
		preg_match("/[^\.\/]+\.[^\.\/]+$/", $host, $matches);
		if (isset($matches[0])) return $matches[0];
		return $host;
	}

	/** из http://www.izh.equiptorg.ru получим www.izh
	 * @param $url
	 * @return string|null субдомен
	 */
	function getSubDomainName($url){
		if (preg_match("/^(http:\/\/)?([^\/]+)/i", $url, $matches)){
			return preg_replace("/\.?[^\.\/]+\.[^\.\/]+$/", '', $matches[2]);
		}
		return null;
	}

	function preparePattern($pattern) {
		return '/^' . str_replace(
			array('/', '@d', "\\$", '@s'),
			array("\/", "(\d+)", '$', "([\w\d-абвгдеёжзийклмнопрстуфхцчшщьыэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЭЮЯ]+)"),
			$pattern
		) . '/u';
	}

	/**@return ZubiPath*/
	static function get() {
		if (self::$_self !== null) return self::$_self;
		require_once(PROJECT_PATH . 'Path.php');
		$class = PROJECT_NAME . 'Path';
		self::$_self = new $class();
		return self::$_self;
	}

	function prepareName($name) {
		$name = trim($name);
		if (!empty($name)){
			//если после 75 символа есть пробел, то обрезаем строку до этого пробела. Убираем ",", если она была перед пробелом
			$countSymbols = 75;
			if (strlen($name) > $countSymbols){
				$pos = strpos($name, ' ', $countSymbols);
				if ($pos !== false) $name = substr($name, 0, ($name[$pos - 1] == ',') ? ($pos - 1) : $pos);
			}
		}
		return $name;
	}

}
 