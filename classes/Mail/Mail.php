<?php

class Mail {
	private $_replace = array();
	const replaceXMLsymbols = array('"'=>'&quot;', '&'=>'&amp;', '>'=>'&gt;', '<'=>'&lt;', "'"=>'&apos;');

	function __construct() {}

	function __destruct() {
		unset($this);
	}

	function send($mailXml) {
		if (empty($mailXml)||!file_exists($mailXml)) return false;
		$mail = $this->_prepare($mailXml);
		return $this->_save($mail);
	}

	function fillReplace($k, $v) { $this->_replace[$k] = $v; }

	protected function _getReplace() {
		$companyCfg = Cfg__get('company');
		foreach ($companyCfg as $k=>$v) $this->_replace['company.' . $k] = $v;

		$pathCfg = Cfg__get('path');
		foreach ($pathCfg as $k=>$v) $this->_replace['path.' . $k] = $v;

		$this->_replace['current.datetime'] = date('d.m.Y H:i');

		if(!empty($this->_replace['item.toEmail']) || !empty($this->_replace['item.toemail'])){

			$email = (empty($this->_replace['item.toEmail'])) ? $this->_replace['item.toemail'] : $this->_replace['item.toEmail'];
			$this->_replace['confirm.email'] = $email;
			$login = (!empty($this->_replace['item.login']))? $this->_replace['item.login'] : '';
			$password = (!empty($this->_replace['item.password']))? $this->_replace['item.password'] : '';

			$this->_replace['confirm.authCode'] =  md5(serialize(array(
				'email' => $email,
				'login' => $login,
				'password' => $password,
			)));
		}

		return $this->_replace;
	}

	protected function _prepare($mailXml) {
		$xml = simplexml_load_file($mailXml);
		$mail = array();
		$mail['fromEmail'] = $this->_getText($xml->fromEmail[0]);
		$mail['fromName'] = $this->_getText($xml->fromName[0]);
		$mail['to'] = $this->_getText($xml->to[0]);
		$mail['subject'] = $this->_getText($xml->subject[0]);
		$mail['msg'] = $this->_getText($xml->msg[0]);
		return $mail;
	}

	private function _save(array $mail) {
		if (empty($mail['to']) || empty($mail['msg'])) return false;
		require_once(FUNCTIONS_PATH . 'Temporary.php');
		$fileMailsXML = Temporary__preparePath('Mail/xml_mails.xml');
		$changeAccess = false;
		$xml = false;
		if (file_exists($fileMailsXML)) $xml = simplexml_load_file($fileMailsXML);

		if ($xml === false){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><mails></mails>');
			$changeAccess = true;
		}

		$xmlMail = $xml->addChild('mail');
		$xmlMail->addChild('fromEmail', $this->_codeText($mail['fromEmail']));
		$xmlMail->addChild('fromName', $this->_codeText($mail['fromName']));
		$xmlMail->addChild('to', $this->_codeText($mail['to']));
		$xmlMail->addChild('subject', $this->_codeText($mail['subject']));
		$xmlMail->addChild('msg', $this->_codeText($mail['msg']));

		$return = $xml->saveXML($fileMailsXML);
		if ($return && $changeAccess) chmod($fileMailsXML, 0777);
		return $return;
	}

	private function _getText($sXml) {
		$s = (string) $sXml;
		$s = Text::get()->removeDoubleSpaces($s);

		$replace = $this->_getReplace();
		$keys = array();
		foreach ($replace as $k=>&$v) {
			$keys[] = '{' . $k . '}';
			$v = nl2br($v);
		}
		$s = str_replace($keys, $replace, $s);
		$s = preg_replace('/{[^\}]+}/u', '', $s);
		return trim($s);
	}

	private function _codeText($s) {
		$s = Text::get()->multylineMessage($s);
		$s = str_replace(array_keys(self::replaceXMLsymbols), self::replaceXMLsymbols, $s);
		return trim($s);
	}

}
