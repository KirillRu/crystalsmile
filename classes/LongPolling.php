<?php
/*Created by Edik (07.04.2015 13:09)*/
class LongPolling{
	private $_delay = 1, $_maxListenTime = 10;
	const MESSAGE = 'message', TIMEOUT = 'timeout';

	/*произошло событие. Добавляем его в кэш(потом клиент все события получит)*/
	function message($key, $data){
		$this->_appendByKey($key, self::MESSAGE, $data);
	}

	/*
	 * При возникновении события возвращается массив событий. События делегируются через функцию message.
	 */
	function listen($key){
		$currentTime = 0;
		while($this->_maxListenTime > $currentTime){
			$events = $this->_getByKey($key);
			if ($events) {
				$this->_deleteByKey($key);
				return $events;
			}
			sleep($this->_delay);
			$currentTime += $this->_delay;
		}
		return [['state' => self::TIMEOUT, 'data' => null]];
	}

	private function _setByKey($key, $data){
		ModelsCacheMemcache::get()->write('longPolling-' . $key, $data, 'longPolling', $this->_maxListenTime);
	}

	private function _getByKey($key){
		return ModelsCacheMemcache::get()->read('longPolling-' . $key);
	}

	private function _appendByKey($key, $state, $data = null){
		$allEvents = $this->_getByKey($key)?:[];
		$allEvents[] = ['state' => $state, 'data' => $data];
		$this->_setByKey($key, $allEvents);
	}

	private function _deleteByKey($key){
		ModelsCacheMemcache::get()->delete('longPolling-' . $key);
	}
}