<?php
/*Created by Edik (06.02.2015 14:59)*/
require_once(dirname(__FILE__) . '/Mysqli.php');
require_once(CLASSES_PATH . 'Models/Iterators/MySqli.php');
class ModelsDriverBDIteratorDriver extends ModelsDriverBDMysqli{

	/**
	 * @param $sql
	 * @return ModelsIteratorsMySqli|array
	 * @throws ExceptionSql
	 */
	function multiSelect($sql){
		$result = $this->query($sql);
		if ($result) return $result->num_rows ? new ModelsIteratorsMySqli($result) : [];
		$this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return array();
	}

	/**
	 * @param string $sql 2 запроса. Прим.: 'select SQL_CALC_FOUND_ROWS .... from ... where... ; select FOUND_ROWS();"'
	 * @return ModelsIteratorsMySqli|bool
	 */
	function multiSelectCount($sql){
		$array = array(); $count = 0;
		if ($this->_multi_query($sql)){
			if ($res = self::$_mysqli->store_result()) $array = $res->num_rows ? new ModelsIteratorsMySqli($res) : [];
			if (self::$_mysqli->more_results() && self::$_mysqli->next_result() && ($res = self::$_mysqli->store_result())){
				$count = $res->fetch_row()[0];
				$res->free();
			}
		}
		if (self::$_mysqli->error) $this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return array($array, $count);
	}
}