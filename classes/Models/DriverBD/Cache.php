<?php
/** выполняет функции из ModelsDriverBDSimpleDriver результат записывает в кэш
 * @method array rowSelect($sql, $partition, $countSeconds = null)
 * @method array multiSelect($sql, $partition, $countSeconds = null)
 * @method array multiSelectKey($sql, $partition, $countSeconds = null, $key)
 * @method int|string fieldSelect($sql, $partition, $countSeconds = null)
 * @method array queryCol($sql, $partition, $countSeconds = null)
 * @method array multiQuery($sql, $partition, $countSeconds = null)
 * Class ModelsDriverBDCache
 */
/*(ВАЖНО!) Правила заполнения массива(строки) $partition:
	1. $partition должен быть заполнен
	2. В массиве $partition должны быть указаны ВСЕ таблицы, учавствующие в запросе
	3. Если в запросе имеется условие на пользователя (select .... from $table where $table.id_user = 232123...), то в $partition
		попадает строка $table . '-232123'.
*/
class ModelsDriverBDCache {

	function __call($name, array $args) {
		if (empty($args[1])) throw new ExceptionPortal('Не указан $partition');
		if (empty($args[2])) $args[2] = Cfg__get('memcache', 'time_regular');

		list($sql, $partition, $cacheTime) = array_splice($args, 0, 3);
		if (Cfg__get('memcache', 'enable')) {
			$key = $sql;
			$result = ModelsCacheMemcache::get()->read($key);
			if ($result === false) {
				$result = $this->_noCache($name, $sql, $args);
				if ($result === false) $result = null;

				ModelsCacheMemcache::get()->write($key, $result, $partition, $cacheTime);
			}
			return $result;
		}
		return $this->_noCache($name, $sql, $args);
	}

	protected function _noCache($funcName, $sql, $data) {
		$driver = ModelsDriverBDSimpleDriver::getDriver();
		array_unshift($data, $sql);
		if (method_exists($driver, $funcName)) return call_user_func_array(array($driver, $funcName), $data);
		return null;
	}

	/**
	 * @return ModelsDriverBDCache
	 */
	static function get() {
		static $self = null;
		if ($self === null) $self = new ModelsDriverBDCache();
		return $self;
	}

}
 