<?php
/**
 * Class ModelsDriverBDDriver_iu
 * @method static ModelsDriverBDDriver_iu getDriver()
 */
class ModelsDriverBDDriver_iu extends ModelsDriverBDMysqli {

	function insert($sql, $returnId = true) {
		if ($this->query($sql)) return $returnId ? self::$_mysqli->insert_id : true;
		$this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return false;
	}

	function update($sql) {
		return $this->query($sql);
	}
}