<?php
/*Created by Edik (03.12.14 10:58)*/
function ModelsDriverBDManagersPrepareValues__serialize_user_contacts($field, $values){
	$result = array();
	$fields = $field['idSpr']->getStructure()->getFields();
	foreach($values as $i => $row){
		foreach($row as $idAF => $value){
			$result[$i][$fields[$idAF]['nameEn']] = Text::get()->mest($value);
		}
	}
	return $result;
}

