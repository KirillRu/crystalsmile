<?php
/*Created by Edik (03.12.14 10:46)*/
function ModelsDriverBDManagersPrepareValues__default($field, $value){
	require_once(CLASSES_PATH . 'Models/Structure/DataType/DataType.php');
	$dataTypes = new ModelsStructureDataType();

	$result = array();
	switch (true){
		case ($dataTypes->checkStringType($field['dataType'])):
			$value = trim($value);
			if (is_string($value) && !in_array($field['nameEn'], array('js', 'url_banner', 'text_full', 'example', 'text_task', 'comment', 'text_link')))
				$value = strip_tags($value, '<br>');
			$result[$field['nameEn']] = Text::get()->mest($value);
			break;
		case ($dataTypes->checkFloatType($field['dataType'])): $result[$field['nameEn']] = (float)$value; break;
		default: $result[$field['nameEn']] = (int)$value; break;
	}
	return $result;
}