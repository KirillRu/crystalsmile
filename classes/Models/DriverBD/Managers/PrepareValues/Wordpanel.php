<?php
/*Created by Edik (03.12.14 10:41)*/
function ModelsDriverBDManagersPrepareValues__wordpanel($field, $value){
	if ($value) {
		$exceptions = '<p><div><strong><em><u><strike><sub><sup><ol><ul><li><a><table><tr><td><hr><img><object><param><embed><br><h2><h3><h4><br><span>';
		if (_isMyIp()) $exceptions .= '<input><b>';
		$value = strip_tags($value, $exceptions);
	}

	return array($field['nameEn'] => Text::get()->mest($value));
}