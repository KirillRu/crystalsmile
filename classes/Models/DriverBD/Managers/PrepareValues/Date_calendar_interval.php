<?php
/*Created by Viktor (28.07.2015 16:27)*/
function ModelsDriverBDManagersPrepareValues__date_calendar_interval($field, $value){
	return [
		$field['nameEn'] . '_start' => $value['date_start'],
		$field['nameEn'] . '_end' => $value['date_end']
	];
}