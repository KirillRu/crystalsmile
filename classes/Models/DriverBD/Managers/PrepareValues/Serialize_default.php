<?php
/*Created by Edik (03.12.14 10:58)*/
function ModelsDriverBDManagersPrepareValues__serialize_default($field, $values){
	$result = array();
	$fields = $field['idSpr']->getStructure()->getFields();
	foreach($values as $idAF => $value){
//		P::_print_r($fields[$idAF], $values);
		if (in_array($fields[$idAF]['dataType'], ['serialize', 'multiFields'])) {
			$result[$fields[$idAF]['nameEn']] = ModelsDriverBDManagersPrepareValues__serialize_default_switch($fields[$idAF], $value);
		}
		else $result[$fields[$idAF]['nameEn']] = $value;
	}
	return $result;
}

function ModelsDriverBDManagersPrepareValues__serialize_default_switch($field, $value) {
	$type = 'default';
	if (file_exists(CLASSES_PATH . 'Models/DriverBD/Managers/PrepareValues/Serialize_' . $field['type'] . '.php')){
		$type = $field['type'];
		require_once(CLASSES_PATH . 'Models/DriverBD/Managers/PrepareValues/Serialize_' . $type . '.php');
	}
	return call_user_func('ModelsDriverBDManagersPrepareValues__serialize_' . $type, $field, $value);
}

