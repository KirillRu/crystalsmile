<?php
/*Created by Edik (03.12.14 10:30)*/
function ModelsDriverBDManagersPrepareValues__select_other($field, $value){
	if ($value[$field['nameEn']] == -1)
		return array($field['nameEn'] . '_other' => Text::get()->mest($value[$field['nameEn'] . '_other']));
	return array($field['nameEn'] => (int)$value[$field['nameEn']]);
}
