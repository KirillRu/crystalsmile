<?php
/*Created by ������ (26.01.2016 17:37)*/
function ModelsDriverBDManagersPrepareValues__serialize_multi_lines($field, $values){
	$result = array();
	$fields = $field['idSpr']->getStructure()->getFields();
	foreach($values as $i => $row){
		foreach($row as $idAF => $value){
			if (in_array($fields[$idAF]['dataType'], ['serialize', 'multiFields'])) {
				$result[$i][$fields[$idAF]['nameEn']] = ModelsDriverBDManagersPrepareValues__serialize_default_switch($fields[$idAF], $value);
			}
			else $result[$i][$fields[$idAF]['nameEn']] = $value;
//			$result[$i][$fields[$idAF]['nameEn']] = $value;
		}
	}
	return $result;
}
