<?php
/*Created by Edik (09.09.14 16:13)*/
require_once(CLASSES_PATH . 'Models/DriverBD/Managers/SimpleStructure.php');
/*
Если поле с dataType = multiFields имеет аттрибут isRootField, то к запросу добавятся поля, как будто это поле состоит из несколько других
Если поле с dataType =  multiFields  не имеет аттрибут isRootField, то все поля из структуры поля будут добавлены в таблицу, указанную в структуре поля.
	Новых строк в той таблице может быть добавлено несколько.
	Значения в переменной $this->_values[$iAF] должен быть массивом из массивов значений
Если поле с dataType = serialize, то в таблицу будет добавлена serialize строка.
	Значения в переменной $this->_values[$iAF] должен быть массивом любой глубины(все это превратиться в serialize строку)
	Вне зависимости от аттрибута isRootField информация добавляется таблицу, указанную в основной структуре(не в структуре поля) (по логике нет смысла добавлять в другую таблицу только 1 поле с serialize строкой)
Если поле имеет аттрибут isRootField, то оно добавляется в основную таблицу
Если в структуре имеются поля без аттрибута isRootField(за исключением multiFields и serialize), то:
	В этой структуре обязан быть массив ChildTables.
	Все эти поля добавятся в таблицу $this->_structure->getTable() . '_' . reset($this->_structure->getChildTables())
*/
class ModelsDriverBDManagersStructure extends ModelsDriverBDManagersSimpleStructure {

	protected $_structure, $_structureItems, $_values;

	/**
	 * @param ModelsStructure $structure
	 * @param array $values массив значений из $structure->getValues()
	 */
	function __construct(ModelsStructure $structure, array $values) {
		$this->_values = $values;
		$this->_structure = $structure;
		$this->_structureItems = array(
			'rootFields' => array(),
			'childFields' => array(),
			'otherTableFields' => array(),
		);

		foreach ($structure->getFields() as $iAF => $field) {
			if (!empty($field['bdParams']['notInsert'])) continue;

			if (($field['dataType'] == 'multiFields') && $field['idSpr']->getStructure() && empty($field['isRootField'])){
				$this->_structureItems['otherTableFields'][$iAF] = $field;
			} elseif (empty($field['isRootField']) && ($field['dataType'] != 'serialize')) {
				$this->_structureItems['childFields'][$iAF] = $field;
			} else {
				$this->_structureItems['rootFields'][$iAF] = $field;
			}
		}
	}

	/** Добавляет запись в таблицы root, child, other(таблицы из serialize)
	 * @return bool|int id записи из root table или false в случае неудачи
	 */
	function insertValuesByStructure() {
		if ($this->gSI('rootFields')){
			$id = parent::insertValues($this->gSI('rootFields'), $this->_values, $this->_structure->getTable());
			if ($this->gSI('otherTableFields')) $this->_insertOtherTableFields($id);

			$this->_valuesToSql = [];//они мешают при вставке childFields
			if ($this->gSI('childFields')) $this->_insertChildFields($id);
			if ($id && $this->_structure->getTableFotos()){
				$this->_moveFotos($id);
			}
			return $id;
		}
		return false;
	}

	function updateValuesByStructure($id){
		if ($this->gSI('rootFields')){
			parent::updateValues($this->gSI('rootFields'), $this->_values, $this->_structure->getTable(), $this->_structure->getId(), $id);
			if ($this->gSI('otherTableFields')) $this->_insertOtherTableFields($id);

			$this->_valuesToSql = [];//они мешают при вставке childFields
			if ($this->gSI('childFields')) $this->_insertChildFields($id);
			return true;
		}
		return false;
	}

	protected function _moveFotos($id){
		require_once(CLASSES_PATH . 'HandlerFotos/Add.php');
		$handler = new HandlerFotosAdd();
		$handler->moveSessionFotos('session_' . $this->_structure->getTableFotos(), $this->_structure->getTableFotos(), $this->_structure->getId(), $id);
	}

	/*Сюда попадают поля только с dataType = multiFields*/
	protected function _insertOtherTableFields($id) {
		foreach ($this->gSI('otherTableFields') as $iAF => $otherTableField) {
			$sprStructure = $otherTableField['idSpr']->getStructure();
			ModelsDriverBDSimpleDriver::getDriver()->simpleQuery('delete from ' . $sprStructure->getTable() . ' where (' . $this->_structure->getId() . ' = ' . $id . ');');

			//иногда нужно добавить несколько строк(контакты пользователя), а иногда одну(регистрация - email).
			//этот код нужен для того, что бы добавить несколько строк, если $structure->getValues() вернула массив массивов
			$allValues = $this->_values[$iAF];
			if (!is_array(current($allValues))) $allValues = [$allValues];

			//добавляем значения которые заполняются не через форму
			$valuesToSql = [$this->_structure->getId() => $id];
			if (!empty($this->_valuesToSql[$iAF]) && is_array($this->_valuesToSql[$iAF]))
				$valuesToSql += $this->_valuesToSql[$iAF];

			foreach($allValues as $values){
				$handler = new $this($sprStructure, $values);
				$handler->addValuesToSql($valuesToSql);
				$handler->insertValuesByStructure();
			}
		}
	}

	protected function _insertChildFields($id) {
		$insertValues = $this->prepareValues($this->gSI('childFields'), $this->_values);
		if ($insertValues) {
			$sql = 'delete from ' . $this->_structure->getTable() . '_' . $this->_structure->getChildTables()[0] . ' where (' . $this->_structure->getId() . ' = ' . $id . ');';

			$insertValues[$this->_structure->getId()] = (int)$id;
			$sql .= 'insert into ' . $this->_structure->getTable() . '_' . $this->_structure->getChildTables()[0] . ' (`' . implode('`, `', array_keys($insertValues)) . '`) ' .
				'values (' . implode(', ', array_values($insertValues)) . ') ' .
			'';
			ModelsDriverBDSimpleDriver::getDriver()->multiQuery($sql);
		}
	}

	/**getFromStructureItems*/
	function gSI($key) {
		return (array_key_exists($key, $this->_structureItems) ? $this->_structureItems[$key] : null);
	}
}