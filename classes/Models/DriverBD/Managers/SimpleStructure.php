<?php

/*Created by Edik (09.09.14 16:08)*/
/*Менеджер для добавления\редактирования записей. В отличие от sructure менеджера не разбивает поля на root/child/otherTable*/
class ModelsDriverBDManagersSimpleStructure {
	protected $_valuesToSql = [], $_defaultValues = [];

	/** Вставляет запись в БД по полям.
	 * @param array $fields
	 * @param array $values массив вида array($idAllField1=>$value1, ...)
	 * @param string $table
	 * @return int id вставленной записи
	 */
	function insertValues(array $fields, array $values, $table) {
		return ModelsDriverBDDriver_iu::getDriver()->insert($this->_getInsertQuery($fields, $values, $table), true);
	}

	/** Update записи по ключу.
	 * @param array $fields
	 * @param array $values массив вида array($idAllField1=>$value1, ...)
	 * @param string $table
	 * @param string $idName имя ключа
	 * @param int $id значение ключа
	 * @return bool
	 */
	function updateValues(array $fields, array $values, $table, $idName, $id) {
		return ModelsDriverBDDriver_iu::getDriver()->update($this->_getUpdateQuery($fields, $values, $table, $idName, $id));
	}

	/**Строит insert запрос по полям.*/
	protected function _getInsertQuery(array $fields, array $values, $table) {
		$values = $this->prepareValues($fields, $values);
		return ''.
			'insert into `' . str_replace('.', '`.`', $table) . '` (`' . implode('`, `', array_keys($values)) . '`) '.
				'values (' . implode(', ', array_values($values)) . ') '.
		'';
	}

	/**Строит update запрос по полям.*/
	protected function _getUpdateQuery(array $fields, array $values, $table, $idName, $id) {
		$updateValues = array();
		foreach ($this->prepareValues($fields, $values) as $k => $v) $updateValues[] = '`' . $k . '` = ' . $v;
		return 'update `' . str_replace('.', '`.`', $table) . '` set ' . implode(', ', $updateValues) . ' where (' . $idName . ' = ' . (int) $id . ')';
	}

	/** Подгатавливает значения к добавлению в БД.
	 * @param array $fields
	 * @param array $values массив вида array($idAllField1=>$value1, ...)
	 * @return array подготовленный массив вида array($nameEn1 => $value1, ...) с уже экранированными значениями
	 */
	function prepareValues(array $fields, array $values) {
		$result = array();
		foreach ($fields as $idAllField => $field) {
			if (array_key_exists($idAllField, $values) && empty($field['bdParams']['notInsert'])){
				$value = $this->_getValue($values, $idAllField);
				if ($field['dataType'] == 'serialize'){
					$result[$field['nameEn']] = Text::get()->mest(serialize($this->_getValuesSerialize($field, $value)));
				} else if ($field['dataType'] == 'multiFields'){
					$result = array_merge($result, $this->_getValuesSerialize($field, $value));
				} else {
					$result = array_merge($result, $this->_getValuesSimple($field, $value));
				}
			}
		}
		foreach ($this->_valuesToSql as $nameEn => $defaultValue) {
			if (!is_array($defaultValue)) $result[$nameEn] = Text::get()->mest($defaultValue);
		}
		return $result;
	}

	private function _getValue($values, $idAllField){
		return $values[$idAllField];
	}

	/*Возвращает массив: ключи- это имена полей в БД, значения - это то, что добавится(значения уже с ковычками должны быть)*/
	private function _getValuesSerialize($field, $value){
		require_once(CLASSES_PATH . 'Models/DriverBD/Managers/PrepareValues/Serialize_default.php');
		return ModelsDriverBDManagersPrepareValues__serialize_default_switch($field, $value);
	}

	/*Возвращает массив: ключи- это имена полей в БД, значения - это то, что добавится(значения уже с ковычками должны быть)*/
	private function _getValuesSimple($field, $value){
		$type = 'default';
		if (file_exists(CLASSES_PATH . 'Models/DriverBD/Managers/PrepareValues/' . Text::get()->strToUpperFirst($field['type']) . '.php')){
			$type = $field['type'];
		}

		if ($value === '' && isset($this->_defaultValues[$field['nameEn']])) $value = $this->_defaultValues[$field['nameEn']];
		require_once(CLASSES_PATH . 'Models/DriverBD/Managers/PrepareValues/' . Text::get()->strToUpperFirst($type) . '.php');
		return call_user_func('ModelsDriverBDManagersPrepareValues__' . $type, $field, $value);
	}

	/*Ключ массива - nameEn. Значение будет добавлено к sql запросу(в любом случае).*/
	function addValuesToSql(array $values){$this->_valuesToSql = array_merge($this->_valuesToSql, $values);}

	/**Удаляет дефолтные значения.
	 * @param array $keys прим. array('date_user', 'id_reg')
	 */
	function removeValuesToSql(array $keys){$this->_valuesToSql = array_diff_key($this->_valuesToSql, array_flip($keys));}

	/*Ключ массива - nameEn. Значение будет добавлено к sql запросу, если оно не было заполнено в форме(поле должно быть showForm!).*/
	function addDefaultValues(array $values){$this->_defaultValues = array_merge($this->_defaultValues, $values);}
}