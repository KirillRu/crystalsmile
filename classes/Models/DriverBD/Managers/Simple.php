<?php
/*Created by Edik (30.09.14 11:38)*/
class ModelsDriverBDManagersSimple{

	function insert($values, $table){
		return ModelsDriverBDDriver_iu::getDriver()->insert($this->_getInsertQuery($this->escapeArray($values), $table), true);
	}

	function update($values, $table, $condition){
		return ModelsDriverBDDriver_iu::getDriver()->update($this->_getUpdateQuery($values, $table, $condition));
	}

	/**Строит insert запрос по полям.*/
	protected function _getInsertQuery(array $values, $table) {
		return ''.
			'insert into ' . $table . ' (`' . implode('`, `', array_keys($values)) . '`) '.
				'values (' . implode(', ', array_values($values)) . ') '.
		'';
	}

	/**Строит update запрос по полям.*/
	protected function _getUpdateQuery(array $values, $table, $condition) {
		$driver = ModelsDriverBDSimpleDriver::getDriver();
		$updateValues = [];
		foreach ($values as $k => $v) $updateValues[] = '`' . $k . '` = \'' . $driver->real_escape_string($v) . '\'';
		return 'update ' . $table . ' set ' . implode(', ', $updateValues) . ' where ' . implode(' and ', $condition);
	}

	function escapeArray(array $array){
		$driver = ModelsDriverBDSimpleDriver::getDriver();
		foreach ($array as &$v) $v = "'" . $driver->real_escape_string($v) . "'";
		return $array;
	}
}