<?php
abstract class ModelsDriverBDMysqli {
	/**@var mysqli*/
	static protected $_mysqli = null;

	/**
	 * @return ModelsDriverBDSimpleDriver
	 */
	final static function getDriver() {
		static $drivers = array();
		$className = static::class;
		if (!isset($drivers[$className])) $drivers[$className] = new static();
		return $drivers[$className];
	}

	final function getMysqli(){
		if (!self::$_mysqli) {
			self::$_mysqli = new mysqli(Cfg__get('db', 'host'), Cfg__get('db', 'username'), Cfg__get('db', 'password'), Cfg__get('db', 'database'));
			if (self::$_mysqli->connect_errno) {
				require_once(CLASSES_PATH . 'Exception/Portal.php');
				throw new ExceptionPortal('Could not select database');
			}
			self::$_mysqli->set_charset('utf8');
		}
		return self::$_mysqli;
	}

	/**
	 * @param $sql
	 * @return bool|mysqli_result
	 * @throws ExceptionPortal
	 */
	final function query($sql) {
		$start = microtime(true);
		$result = $this->getMysqli()->query($sql);
		$resultTime = microtime(true) - $start;
		$queryResult = ($result !== false);

		if (!$queryResult) {
			switch ((int) $this->getMysqli()->errno) {
				case 1213: case 1205://deadlock
					$countQuery = 0;
					while (($countQuery++ < 10)&&!$queryResult) {
						sleep(1);
						$result = $this->getMysqli()->query($sql);
						$resultTime = microtime(true) - $start;
						$queryResult = ($result !== false);
					}
					break;
				case 2006: case 2013://соединение разорвано
					$start = microtime(true);
					$this->getMysqli()->connect(Cfg__get('db', 'host'), Cfg__get('db', 'username'), Cfg__get('db', 'password'), Cfg__get('db', 'database'));
					$result = $this->getMysqli()->query($sql);
					$resultTime = microtime(true) - $start;
					$queryResult = ($result !== false);
					break;
/*				case 1146: //если нет таблицы
					if (strpos($sql, '_cache_categorys.')) {
						sleep(1);
						$result = $this->_driver->query($sql);
						$resultTime = microtime(true) - $start;
						$queryResult = ($result !== false);
					}
					elseif (preg_match("/table '([\._a-z]+)' doesn't exist/i", $this->_driver->error, $m)) {
						$tableName = $m[1];
						$baseName = '';
						$pos = strpos($tableName, '.');
						if ($pos !== false) {
							$baseName = substr($tableName, 0, $pos++);
							$tableName = substr($tableName, $pos);
						}
						if ($baseName != '_service') {
							$sqlCreateView = 'create view ' . $tableName . ' as select * from _service.' . $tableName;
							if ($this->_driver->query($sqlCreateView)) {
								$result = $this->_driver->query($sql);
								$resultTime = microtime(true) - $start;
								$queryResult = ($result !== false);
								$this->_log($sqlCreateView, true, 0);
							}
							else $this->_log($sqlCreateView, false, 0);
						}
					}
					break;*/
			}
		}
		$this->_log($sql, ($queryResult !== false), $resultTime);
		return $result;
	}

	/**
	 * @param $sql
	 * @return bool|mysqli_result
	 */
	final protected function _multi_query($sql) {
		$start = microtime(true);
		$result = $this->getMysqli()->multi_query($sql);

		$this->_log($sql, ($result !== false), microtime(true) - $start);
		return $result;
	}

	final private function _log($sql, $success, $resulTime) {
		if (!defined('cronAction')) {
			//только НЕ для крона
			if ($success) {
				//запрос успешен
				if ($resulTime > 2) {
					//пишем долгие запросы
					$data = [date('d.m.Y H:i:s')];
					$data[] = number_format($resulTime, 4) . ' сек';
					$data[] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
					$data[] = $sql;
					Functions__log('sql.log', $data);
				}
			} else {
				//ошибка при запросе
				$data = [date('d.m.Y H:i:s')];
				$data[] = _getIp();
				$data[] = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
				$data[] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
				$data[] = 'Request error ' . self::$_mysqli->errno . ': ' . self::$_mysqli->error . '<br>' . $sql;
				$trace = debug_backtrace();
				$debug = '';
				if ($trace) for($i = 1; $i < 5; $i++)
					if (!empty($trace[$i]['file']) && !empty($trace[$i]['line'])) $debug .= $trace[$i]['file'] . ':' . $trace[$i]['line'] . ' ';
				$data[] = $debug;

				Functions__log('error.sql.log', $data);
			}
		}
		elseif ($success&&($resulTime > 2)) {
			//запрос успешен, пишем долгие запросы
			$data = [date('d.m.Y H:i:s')];
			$data[] = number_format($resulTime, 4) . ' сек';
			$data[] = $sql;
			$file = dirname(ROOT_DIR) . '/_log/Cron/sql.log';
			Functions__insertInFile($file, $data);
		}

		if (_isMyIp()) {
			$data = [date('d.m.Y H:i:s')];
			$data[] = number_format($resulTime, 4) . ' сек';
			$data[] = $success ? 'SUCCESS' : 'ERROR';
			$data[] = defined('cronAction') ? 'cronAction' : $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$data[] = (!$success ? 'Request error ' . self::$_mysqli->errno . ': ' . self::$_mysqli->error . '<br>' : '') . $sql;
			Functions__log('our.sql.log', $data);
		}
	}

	protected function _throwException($message, $sql, $code = 0) {
		require_once(CLASSES_PATH . 'Exception/Sql.php');
		$handler = new ExceptionSql($message, $code);
		$handler->setSql($sql);
		throw $handler;
	}

	function real_escape_string($s) {
		return $this->getMysqli()->real_escape_string($s);
	}

	function selectDB($dbName) {
		$this->getMysqli()->select_db($dbName);
	}
}

 