<?php
class ModelsDriverBDSimpleDriver extends ModelsDriverBDMysqli {

	function multiSelect($sql) {
		$result = $this->query($sql);
		if ($result) {
			$return = $result->fetch_all(MYSQLI_ASSOC);
			$result->free();
			return $return;
		}
		$this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return array();
	}

	function multiSelectKey($sql, $key) {
		$result = $this->query($sql);
		if ($result) {
			$return = array();
			while ($row = $result->fetch_assoc()) $return[$row[$key]] = $row;
			$result->free();
			return $return;
		}
		$this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return array();
	}

	function simpleQuery($sql) {
		$result = $this->query($sql);
		if ($result) return self::$_mysqli->affected_rows;
		$this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return false;
	}

	function rowSelect($sql) {
		$result = $this->query($sql);
		if ($result) {
			$row = $result->fetch_assoc();
			$result->free();
			return $row;
		}
		$this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return array();
	}

	function queryCol($sql) {
		$result = $this->query($sql);
		if ($result) {
			$return = array();
			while ($row = $result->fetch_row()) $return[] = array_shift($row);
			$result->free();
			return $return;
		}
		$this->_throwException('Request error ' . self::$_mysqli->error, $sql, self::$_mysqli->errno);
		return array();
	}

	function fieldSelect($sql) {
		$result = $this->rowSelect($sql);
		return $result ? array_shift($result) : false;
	}

	function multiQuery($sqls){
		/*возвращает многомерный массив. Каждый элемент массива - результат по каждому запросу(одна строка)*/
		$return = array();
		if ($this->_multi_query($sqls)){
			$i = 0;
			do {
				$return[$i] = array();
				if ($res = self::$_mysqli->store_result()) {
					$return[$i] = $res->fetch_all(MYSQLI_ASSOC);
					$res->free();
				}
				$i++;
			} while ($this->_nextResult($sqls));
			return $return;
		}
		$this->_throwException('Request error ' . self::$_mysqli->error, $sqls, self::$_mysqli->errno);
		return array();
	}

	private function _nextResult($sqls){
		if (!self::$_mysqli->more_results()) return false;
		if (self::$_mysqli->next_result()) return true;
		$this->_throwException('Request error '. self::$_mysqli->error, $sqls, self::$_mysqli->errno);
		return false;
	}


}