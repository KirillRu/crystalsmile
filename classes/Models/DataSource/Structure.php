<?php
require_once(CLASSES_PATH . 'Models/DataSource/DataSource.php');
class ModelsDataSourceStructure extends ModelsDataSourceClass {
	private $_runFileName;

	function getStructure() {
		$table = $this->getSprTable();
		$path = $this->getSprData();
		$path = empty($path['path']) ? false : $path['path'];
		if (!$table && !$path) return null;

		if ($this->_runFileName){
			$pathClass = CLASSES_PATH . 'Models/Structure/From_serialize/' . $this->_runFileName . '.php';
			if (file_exists($pathClass)){
				require_once($pathClass);
				$nameClass = 'ModelsStructureFrom_serialize' . $this->_runFileName;
				return new $nameClass($table, $path);
			}
		}
		require_once(CLASSES_PATH . 'Models/Structure/From_serialize/From_serialize.php');
		return new ModelsStructureFrom_serialize($table, $path);
	}

	function getItems(array $queryParams = array()) {
		if (!$queryParams) return array();
		return parent::getItems($queryParams);
	}

	function setRunFilename($fileName) {
		$this->_runFileName = $fileName;
	}
}