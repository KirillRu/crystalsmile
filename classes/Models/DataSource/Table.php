<?php
require_once(CLASSES_PATH . 'Models/DataSource/DataSource.php');
class ModelsDataSourceTable extends ModelsDataSourceClass {


	function __construct($spr) {
		if (!empty($spr['data']['table'])) $spr['table'] = $spr['data']['table'];
		parent::__construct($spr);
	}

	///-----
	function getQueryParams(){
		$data = $this->getSprData();
		return (!empty($data['query']) ? $data['query'] : array());
	}

	///-----

	/**
	 * @param array $queryParams array('select'=>array(...), 'condition'=>array(...))
	 * @return array
	 */
	function getItems(array $queryParams = array()) {
		$query = $this->buildQuery(Functions__mergeArrays($this->getQueryParams(), $queryParams));
		if (!$query) return array();
		$items = ModelsDriverBDCache::get()->multiSelect($query, $this->getStructure()->getTable());
		return $this->_prepareItems($items);
	}

	/**Если в многомерном массиве по 2 значения в каждом из вложенных массивов, то делаем из него одномерный массив*/
	protected function _prepareItems($items){
		$firstItem = current($items);
		if ($firstItem && count($firstItem) == 2){
			unset($firstItem);
			$result = array();
			foreach($items as $item){
				$result[array_shift($item)] = array_shift($item);
			}
			return $result;
		}
		return $items;
	}

	function buildQuery(array $queryParams) {
		if (empty($queryParams['select'])) $queryParams['select'] = ['t.*'];
		if (empty($queryParams['from'])) $queryParams['from'] = [$this->getSprTable() . ' as t'];

		$structure = $this->getStructure();
		if ($structure->isActive() && empty($queryParams['where']['active'])) $queryParams['where']['active'] = '(t.active = 1)';
		if (empty($queryParams['order'])){
			if ($structure->isNested()) $queryParams['order']['leftKey'] = 't.leftKey';
			elseif ($structure->isPosition()) $queryParams['order']['position'] = 't.position';
		}

		if (!empty($queryParams['where'])) {
			foreach ($queryParams['where'] as $k=>$v) {
				if (preg_match_all("/{(\w+)}/i", $v, $m)) {
					foreach ($m[1] as $i=>$qName) $queryParams['where'][$k] = str_replace($m[0][$i], (int) Data::get()->g($qName), $queryParams['where'][$k]);
				}
			}
		}

		$sql = ''.
			'select ' . implode(', ', $queryParams['select']) . ' '.
			'from ' . implode(', ', $queryParams['from']) . ' '.
			(empty($queryParams['join'])?'':implode(' ', $queryParams['join']) . ' ').
			(empty($queryParams['where'])?'':'where ' . implode(' and ', $queryParams['where']) . ' ').
			(empty($queryParams['group'])?'':'group by ' . implode(' and ', $queryParams['group']) . ' ').
			(empty($queryParams['having'])?'':'having ' . implode(' and ', $queryParams['having']) . ' ').
			(empty($queryParams['order'])?'':'order by ' . implode(' and ', $queryParams['order']) . ' ').
		'';
		return $sql;
	}

	function getItem($value, $typeInt = true) {
		$data = $this->getSprData();
		if ($typeInt) $value = (int) $value;
		else $value = Text::get()->mest($value);
		$queryParams = $this->getQueryParams();
		$queryParams['where'] = ['(' . $data['id'] . ' = ' . $value . ')'];
		unset($queryParams['having'], $queryParams['order']);
		$query = $this->buildQuery($queryParams);
		if (!$query) return false;
		$query .= 'limit 1 ';
		$item = ModelsDriverBDCache::get()->rowSelect($query, $this->getStructure()->getTable());
		if (count($item) == 2) return array_pop($item);
		return $item;
	}

}