<?php
require_once(CLASSES_PATH . 'Models/DataSource/DataSource.php');
class ModelsDataSourceQuery extends ModelsDataSourceClass {

	function getItems(array $queryParams = array()) {
		$query = $this->getSprData();
		if (!$query) return array();
		$items =  ModelsDriverBDCache::get()->multiSelect($query, 'sprs');
		return $this->_prepareItems($items);
	}

	/**Если в многомерном массиве по 2 значения в каждом из вложенных массивов, то делаем из него одномерный массив*/
	protected function _prepareItems($items){
		$firstItem = current($items);
		if (!empty($firstItem) && count($firstItem) == 2){
			unset($firstItem);
			$result = array();
			foreach($items as $item){
				$result[array_shift($item)] = array_shift($item);
			}
			return $result;
		}
		return $items;
	}
}

