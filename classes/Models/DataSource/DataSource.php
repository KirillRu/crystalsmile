<?php
interface ModelsDataSourceInterface {
	function getStructure();
}

abstract class ModelsDataSourceClass implements ModelsDataSourceInterface {
	protected $_spr;

	function __construct($spr) {
		$this->_spr = $spr;
	}

	function getStructure(){
		$table = $this->getSprTable();
		if (empty($table)) return null;
		require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
		$structure = new ModelsStructureFrom_files($table);
		return $structure;
	}

	function getItems(array $queryParams = array()){
		return array();
	}

	function getSprTable(){
		return $this->_spr['table'];
	}

	function getSprData() {
		return isset($this->_spr['data'])?$this->_spr['data']:array();
	}
}

/**
 * @param $spr
 * @return ModelsDataSourceClass|null
 */
function ModelsDataSource__getObject($spr) {
	if (empty($spr['sourceData'])) return null;
	$classFile = CLASSES_PATH . 'Models/DataSource/'.Text::get()->strToUpperFirst($spr['sourceData']).'.php';
	if (file_exists($classFile)){
		require_once($classFile);
		$className = 'ModelsDataSource'.Text::get()->strToUpperFirst($spr['sourceData']);
		return new $className($spr);
	}
	$classFile = CLASSES_PATH . str_replace('.', '/', $spr['sourceData']) . '.php';
	if (file_exists($classFile)){
		require_once($classFile);
		$className = str_replace('.', '', $spr['sourceData']);
		return new $className($spr);
	}
	return null;
}
