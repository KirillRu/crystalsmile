<?php
/**
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com
 * Date: 27.02.14
 * Time: 9:37
 * To change this template use File | Settings | File Templates.
 */

require_once(CLASSES_PATH . 'Models/Structure/From_serialize/From_serialize.php');
class ModelsStructureFrom_serializeForm extends ModelsStructureFrom_serialize {

	protected function _getFields($structureFields) {
		$fields = array();
		$position = 0;
		foreach($structureFields as $iAF=>$field) if (!empty($field['showForm'])) {
			$fields[$iAF] = $this->_prepareField($field, array('showForm', 'idSpr', 'bdParams'));
			if (!isset($fields[$iAF]['showForm']['requireField'])) $fields[$iAF]['showForm']['requireField'] = 0;
			$fields[$iAF]['position'] = ++$position;
			$fields[$iAF]['isRootField'] = 1;
		}
		return $fields;
	}


}