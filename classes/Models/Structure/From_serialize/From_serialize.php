<?php
require_once(CLASSES_PATH . 'Models/Structure/Structure.php');
class ModelsStructureFrom_serialize extends ModelsStructure {
	protected $_table, $_path;

	function __construct($table, $path = '') {
		$this->_table = $table;
		$this->_path = $this->_getPath($path);
		if ($this->_path) {
			$structure = include($this->_path);
			foreach ($structure as $k=>$v) $this->_parseStructureInFileArray($k, $v);
		}
	}

	protected function _parseStructureInFileArray($k, $v) {
		switch ($k) {
			case 'fields': $this->_structure[$k] = $this->_getFields($v); break;
			default: parent::_parseStructureInFileArray($k, $v);
		}
	}

	protected function _getFields($fields) {
		$fileFields = array();
		$position = 0;
		foreach($fields as $iAF=>$field) {
			$fileFields[$iAF] = $this->_prepareField($field, array('showForm', 'showDetail', 'idSpr'));
			$fileFields[$iAF]['position'] = ++$position;
		}
		return $fileFields;
	}

	protected function _getPath($path = false) {
		if ($path && file_exists($path)) return $path;

		$path = STRUCTURES_PATH . 'Default/' . PROJECT_NAME . '/serialize/' . $this->_table . '.php';
		if (file_exists($path)) return $path;

		$path = STRUCTURES_PATH . 'Default/serialize/' . $this->_table . '.php';
		if (file_exists($path)) return $path;
		return null;
	}

}