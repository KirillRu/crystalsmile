<?php
require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
class ModelsStructureFrom_filesDetail extends ModelsStructureFrom_files {

	protected function _getFields($structureFields) {
		$fields = array();
		$position = 0;
		foreach($structureFields as $iAF=>$field)
			if (!empty($field['showDetail'])) {
				$fields[$iAF] = $this->_prepareField($field, array('showDetail', 'idSpr', 'relation'));
				$fields[$iAF]['position'] = ++$position;
				$fields[$iAF]['isRootField'] = 1;
			}
		return $fields;
	}

}