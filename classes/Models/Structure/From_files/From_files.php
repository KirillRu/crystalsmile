<?php
/**
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com
 * Date: 11.02.14
 * Time: 10:32
 * To change this template use File | Settings | File Templates.
 */

require_once(CLASSES_PATH . 'Models/Structure/Structure.php');
class ModelsStructureFrom_files extends ModelsStructure {
	protected $_table = null;

	function __construct($table) {
		$this->_table = $table;
		$path = $this->_getPath();
		if ($path) {
			$structure = include($path);
			foreach ($structure as $k=>$v) $this->_parseStructureInFileArray($k, $v);
		}
	}

	protected function _parseStructureInFileArray($k, $v) {
		switch ($k) {
			case 'fields': $this->_structure[$k] = $this->_getFields($v); break;
			default: parent::_parseStructureInFileArray($k, $v);
		}
	}

	protected function _getFields($fields) {
		$fileFields = array();
		$position = 0;
		foreach($fields as $iAF=>$field) {
			$fileFields[$iAF] = $this->_prepareField($field);
			$fileFields[$iAF]['position'] = ++$position;
			if (!isset($fields[$iAF]['isRootField'])) $fields[$iAF]['isRootField'] = 1;
		}
		return $fileFields;
	}

	protected function _getPath() {
		//проверяем наличия файла в структурах, созданных пересчетом
		$path = STRUCTURES_PATH . PROJECT_NAME . '/' . $this->_table . '.php';
		if (file_exists($path)) return $path;

		//проверяем наличия файла в структурах, созданных нами для определенного портала
		$path = STRUCTURES_PATH . 'Default/' . PROJECT_NAME . '/' . $this->_table . '.php';
		if (file_exists($path)) return $path;

		//проверяем наличия файла в структурах, созданных нами для всех порталов
		$path = STRUCTURES_PATH . 'Default/' . $this->_table . '.php';
		if (file_exists($path)) return $path;

		//нигде файла не оказалось. Создаем его. Он будет создан в STRUCTURES_PATH . PROJECT_NAME . '/' . $this->_table . '.php';
		$path = $this->_createFile();
		//Проверяем(вдруг таблицы не существовало и файл не создался)
		if (file_exists($path)) return $path;
		return null;
	}

	protected function _createFile() {
		require_once(CLASSES_PATH . 'Models/Structure/From_files/CreateStructureFile.php');
		$creatorStructureFile = new ModelsStructureFrom_filesCreateStructureFile($this->_table);
		return $creatorStructureFile->createFile();
	}
}