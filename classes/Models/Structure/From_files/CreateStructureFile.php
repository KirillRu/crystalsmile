<?php
class ModelsStructureFrom_filesCreateStructureFile {
	private $_table, $_params;
	/**
	 * @var ModelsStructureFrom_filesCreateStructureFileDrivers
	 */
	private $_driver;

	function __construct($table, $params = array()) {
		$this->_table = $table;
		$this->_params = $params;
	}

	private function _getStructure() {
		$structure = array();
		$this->_driver = ModelsStructureFrom_filesCreateStructureFileDrivers::getDriver();
		if ($this->_driver->checkTable($this->_table)) {
			$structure['id'] = '';
			if (!empty($this->_params['id'])) $structure['id'] = $this->_params['id'];
			$possibleId = '';
			if (empty($structure['id'])) {
				if (substr($this->_table, -9) == '_statuses') $possibleId = 'id_' . (substr($this->_table, 0, -2));
				else $possibleId = 'id_' . (substr($this->_table, 0, -1));
			}

			$structure['table'] = $this->_table;
			$structure['tableName'] = $this->_table;
			$structure['fields'] = array();
			$fields = $this->_driver->getFields($this->_table);
			foreach ($fields as $i=>$field) {
				$iAF = $this->_table . '_' . $i;
				$structure['fields'][$iAF] = array();
				$structure['fields'][$iAF]['name'] = $field['Field'];
				$structure['fields'][$iAF]['nameEn'] = $field['Field'];
				$structure['fields'][$iAF]['idAllField'] = $iAF;
				if ($field['Key'] == 'PRI') $structure['id'] = $field['Field'];
				switch ($field['Field']) {
					case 'position': $structure['isPosition'] = 1; break;
					case 'active': $structure['isActive'] = 1; break;
					case 'leftKey': $structure['isNested'] = 1; break;
				}
				if (!empty($possibleId)&&empty($structure['id'])&&($field['Field'] == $possibleId)) $structure['id'] = $possibleId;
			}
		}
		unset($this->_driver);
		return $structure;
	}

	function createFile() {
		$structure = $this->_getStructure();
		if (!empty($structure)) {
			$s = print_r($structure, 1);
			$search = array();					$replace = array();
			$search[] = "/\[(\d+)\]/";			$replace[] = '${1}';
			$search[] = "/\[([a-z_0-9\-\.]+)\]/i";		$replace[] = '\'${1}\'';
			$search[] = "/=> (.*)/";			$replace[] = '=> \'${1}\',';
			$search[] = "/=> '(\d+)',/";		$replace[] = '=> ${1},';
			$s = preg_replace($search, $replace, $s);
			$s = trim($s) . ';';
			$s = str_replace(array("=> 'Array',", ")\n"), array("=> Array", "),\n"), $s);

			$path = STRUCTURES_PATH . PROJECT_NAME . '/' . $this->_table . '.php';
			file_put_contents($path, '<?php return ' . $s . "\n" . ';');
			chmod($path, 0664);
			return $path;
		}
		return false;
	}
}

class ModelsStructureFrom_filesCreateStructureFileDrivers extends ModelsDriverBDSimpleDriver {

	function checkTable($table) {
		if (empty($table)) return false;
		$sql = 'show tables like ' . Text::get()->mest($table);
		$result = $this->fieldSelect($sql);
		return $result;
	}

	function getFields($table) {
		$sql = 'SHOW COLUMNS FROM ' . $table;
		return $this->multiSelect($sql);
	}

}