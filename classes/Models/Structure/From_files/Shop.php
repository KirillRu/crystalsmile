<?php
/**
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com
 * Date: 19.02.14
 * Time: 13:39
 * To change this template use File | Settings | File Templates.
 */

require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
class ModelsStructureFrom_filesShop extends ModelsStructureFrom_files {

	protected function _getFields($structureFields) {
		$fields = array();
		$position = 0;
		foreach($structureFields as $iAF=>$field) if (!empty($field['showShop'])) {
			$fields[$iAF] = $this->_prepareField($field, array('showShop', 'idSpr', 'usedIn'));
			$fields[$iAF]['position'] = ++$position;
			$fields[$iAF]['isRootField'] = 1;
		}
		return $fields;
	}

}