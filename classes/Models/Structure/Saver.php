<?php
ini_set('max_execution_time', 600);
ini_set('memory_limit', '512M');

function ModelsStructureSaver__cmpChildIds($a, $b) {
	return $a['lK'] - $b['lK'];
}

class ModelsStructureSaver{
	protected $_idProjectTable, $_projectTable, $_projectTableFields, $_categoriesWithFields, $_idsCategoriesInBase, $_childTables, $_categoriesFields;
	/**@var ModelsStructureSaverDriver*/
	private $_driver;

	function __construct($sessId = '') {
		$this->_driver = ModelsStructureSaverDriver::getDriver();
		$this->_senderLongPolling = new RestructLPSender($sessId);
	}

	/*пересчёт структуры только одного project table*/
	function setIdProjectTable($idProjectTable){
		$this->_idProjectTable = $idProjectTable;
	}

	function saveStructureFiles() {
		$startTime = microtime(true);
		foreach($this->_getProjectTables() as $this->_projectTable) {
			$this->_senderLongPolling->sendProject($this->_projectTable);
			$this->_setConsts();
			/*Создаём структуру projectTable*/
			$this->_saveFile($this->_buildStructureArray(
				$this->_projectTable,
				$this->_projectTableFields,
				$this->_idsCategoriesInBase)
			);
			/*Создаём структуры дочерних от projectTable категорий*/
			$fieldsWithIsRoot = $this->_setIsRootField($this->_projectTableFields);
			$this->_saveCategorySetsWithFields($fieldsWithIsRoot);
			if (!empty($this->_categoriesWithFields)) {
				$this->_saveCategorySetsOther($fieldsWithIsRoot);
			}
		}
		$this->_senderLongPolling->lastSend(round(microtime(true)-$startTime, 4).' сек.');
	}

	private function _setIsRootField($fields){
		foreach($fields as &$field) $field['isRootField'] = true;
		return $fields;
	}

	private function _setConsts(){
		$this->_projectTableFields = $this->_getProjectFields();

		$this->_categoriesWithFields = $this->_getCategoriesWithFields();

		$this->_categoriesFields = array();//будет заполнен ниже
		$categoryFields = $this->_getCategoriesFields(); //поля обычным двумерным массивом(ниже будет сортировка)
		$this->_idsCategoriesInBase = $this->_getIdsCategoriesInBase();
		/*Создаём массив childTables. Ключ - категория. Значение - массив дочерних категорий*/
		/*-----*/
			$this->_childTables = array();
			foreach($this->_idsCategoriesInBase as $idCategory){
				foreach($this->_categoriesWithFields as $category){
					if (($this->_categoriesWithFields[$idCategory]['leftKey']>=$category['leftKey']) && ($this->_categoriesWithFields[$idCategory]['rightKey']>=$category['rightKey']))
						if (!isset($this->_childTables[$idCategory])) $this->_childTables[$idCategory] = array($category['id_category']=>array('lK'=>$category['leftKey'], 'rK'=>$category['rightKey']));
						else $this->_childTables[$idCategory][$category['id_category']] = array('lK'=>$category['leftKey'], 'rK'=>$category['rightKey']);
//						if (!isset($this->_childTables[$idCategory])) $this->_childTables[$idCategory] = array($category['id_category']);
//						else $this->_childTables[$idCategory][] = $category['id_category'];
				}
			}
		if (!empty($this->_childTables)) {
			foreach ($this->_childTables as &$child) uasort($child, 'ModelsStructureSaver__cmpChildIds');
		}
		/*-----*/
		/*-----*/

		$projectBlocks = $this->_getProjectTableBlocks();

		/*Удаляем из _projectTableFields все поля, которые по nameEn совпадают хотя бы с 1 из дочерних и при этом не совпадают по idAllField*/
		/*-----*/
			$ptHash = $cfHash = array();
			foreach($this->_projectTableFields as &$field) {
				$this->_buildSpr($field);
				$this->_setIdBlock($field, $projectBlocks);
				$ptHash[$field['nameEn']] = $field['idAllField'];
			}
			unset($field);
			foreach($categoryFields as $field) {
				if (isset($cfHash[$field['nameEn']])){
					/*Удаляем из _projectTableFields, потому что уже 2 разных idAllField(с одинаковым nameEn) в полях дочерних категорий*/
					if ($cfHash[$field['nameEn']] != $field['idAllField']) {
						unset($this->_projectTableFields[$field['idAllField']],$this->_projectTableFields[$cfHash[$field['nameEn']]]);
					}
				} else {
					$cfHash[$field['nameEn']] = $field['idAllField'];
				}
			}

			foreach($ptHash as $nameEn=>$idAllField){
				if (isset($cfHash[$nameEn]) && ($cfHash[$nameEn] != $idAllField)) unset($this->_projectTableFields[$idAllField]/*, $ptHash[$nameEn]*/);
			}
		/*-----*/
		/*-----*/


		foreach($categoryFields as $field){
			$idCategory = $field['id_category'];
			unset($field['id_category']); //idCategory не нужен во всех полях в структуре
			$this->_buildSpr($field, $idCategory);
			/*Ставим isRootField у всех дочерних полей, name_en которых совпатает с родительским*/
			if (isset($ptHash[$field['nameEn']])) $field['isRootField'] = true;
			$this->_setIdBlock($field, $projectBlocks);
			$this->_categoriesFields[$idCategory][$field['idAllField']] = $field;
		}

	}

	/*сохраняем все дочерние категории, у которых есть поля*/
	private function _saveCategorySetsWithFields($preparedProjectTableFields){
		foreach($this->_categoriesFields as $idCategory=>$fields){
			$this->_saveFile($this->_buildStructureArray(
				$this->_projectTable + array('categoryName'=>$this->_categoriesWithFields[$idCategory]['name'], 'idCategory' => $idCategory),
				$fields + $preparedProjectTableFields,
				$this->_getChildTables($this->_categoriesWithFields[$idCategory], $idCategory)
			),$idCategory);
			$this->_senderLongPolling->sendCategory($this->_projectTable, $idCategory);
		}
	}

	/*сохраняем все дочерние категории, у которых нет полей*/
	private function _saveCategorySetsOther($preparedProjectTableFields){
		$this->_senderLongPolling->sendCategory($this->_projectTable, 'идёт пересчёт категорий без полей...');
		foreach($this->_getCategoriesOther() as $category){
			$idParentCategory = null;
			$childTables = array();
			$fields = $preparedProjectTableFields;
			foreach($this->_categoriesWithFields as $categoryParent){
				if ($categoryParent['leftKey']<$category['leftKey'] && $categoryParent['rightKey']>$category['rightKey']){
					$idParentCategory = $categoryParent['id_category'];
					break;//_categoriesWithFields отсортирован по level, поэтому первой мы получим самую близкую категорию(она нам и нужна)
				}
			}
			//если есть поля у родительской категории, то добавляем их
			if ($idParentCategory!==null) {
				$fields += $this->_categoriesFields[$idParentCategory];
				$childTables = $this->_getChildTables($category, $idParentCategory);
			}
			$this->_saveFile($this->_buildStructureArray(
				$this->_projectTable + array('categoryName'=>$category['name'], 'idCategory' => $category['id_category']),
				$fields,
				$childTables
			),$category['id_category']);
		}
	}

	/*Необходимо найти для данных полей соответствующие им блоки, после чего записать в showForm[idBlock] значение.
	Блоки могут быть для всего projectTable, могут быть для определенной категории. Для поиска этой категории используем
	leftKey\rightKey в поле. Если там нет leftkey\rightKey - то берем блок для всего projectTable
	*/
	private function _setIdBlock(&$field, $projectBlocks){
		if (!empty($field['showForm'])){//idBlock ставим только полям с showForm
			if (!empty($field['leftKey'])){//это поле - поле категории.
				foreach ($projectBlocks as $block) {
					//блоки отсортированы по level, поэтому мы наткнемся в первую очередь на самые дочерние - они нам и нужны
				    if (isset($block['fields'][$field['nameEn']]) && $block['leftKey'] <= $field['leftKey'] && $block['rightKey'] >= $field['leftKey']){
					    //если этот блок создан для этой категории(или для одной из родительских)
					    $field['showForm'] = unserialize($field['showForm']);
					    $field['showForm']['idBlock'] = $block['id_project_table_block'];
					    $field['showForm'] = serialize($field['showForm']);
					    return;//поле исправлено. Выходим из функции
				    }
				}
			}
			//сюда попадают поля из project_table_fields либо поля категорий, для которых не было создано блоков
			//Если id_category === 0, то это поля для данного projectTable
			foreach ($projectBlocks as $block) {
				if ($block['id_category'] == 0 && isset($block['fields'][$field['nameEn']])){
					$field['showForm'] = unserialize($field['showForm']);
				    $field['showForm']['idBlock'] = $block['id_project_table_block'];
				    $field['showForm'] = serialize($field['showForm']);
				}
			}
		}
	}

	/*Добавляем к структуре все возможные поля. $mainData - это категория или projectTable*/
	private function _buildStructureArray($mainData, $fields, $childTables = array()){
		$structure = array(
			'id' => 'id_' . substr($mainData['name_en'], 0, -1),
			'tableName' => Text::get()->strToUpperFirst($mainData['tableName']),
			'table' => $mainData['name_en'],
			'categoryName' => (isset($mainData['categoryName'])?$mainData['categoryName']:$mainData['tableName'])
		);
		/*Добавляем isActive, isPosition, isNested*/
		/*-----*/
			$nestedFields = 0;
			$tableFotos = false;
			foreach($fields as $field){
				switch($field['nameEn']){
					case 'active': $structure['isActive'] = true; break;
					case 'position': $structure['isPosition'] = true; break;
					case 'level': case 'id_parent': case 'leftKey': case 'rightKey': $nestedFields++; break;
					case 'id_foto': $tableFotos = substr($mainData['name_en'], 0, -1) . '_fotos'; break;
					case 'id_logo': $tableFotos = substr($mainData['name_en'], 0, -1) . '_logos'; break;
				}
			}
			/*если все 4 поля, то isNested*/
			if ($nestedFields == 4) $structure['isNested'] = true;
			if ($tableFotos) $structure['tableFotos'] = $tableFotos;
			unset($nestedFields);
		/*-----*/
		/*-----*/
		if (!empty($childTables)) $structure['childTables'] = $childTables;
		$structure['fields'] = $fields;
		return $structure;
	}

	/*Создание справочника*/
	/*-----*/
		private function _buildSpr(&$field, $idCategory = false) {
			$idSpr = (int) $field['idSpr'];
			if (!empty($idSpr)){
				$spr = $this->_driver->rowSelect('select id_spr, name, use_table, settings from sprs where (id_spr = ' . $idSpr . ') limit 0, 1');
				if (empty($spr)) $idSpr = 0;
				elseif (empty($spr['use_table'])) $idSpr = $this->_sprsToSprs($idSpr);
				else {
					$settings = unserialize($spr['settings']);
					switch ($settings['sourceData']) {
						case 'structure': $idSpr = $this->_sprsToStructure($settings['table']); break;
						case 'array': $idSpr = $this->_sprsToString($settings['data']); break;
						case 'table': $idSpr = $this->_sprsToTable($settings['data']); break;
						case 'tableToArray': $idSpr = $this->_sprsToTableToArray($settings['data'], $idCategory); break;
					}
				}
			}
			$field['idSpr'] = $idSpr;
		}

		private function _sprsToSprs($idSpr) {
			$query = ''.
				'select id_spr, name '.
				'from sprs '.
				'where (active = 1) '.
					'and (id_parent = ' . $idSpr . ') '.
				'order by position '.
			'';
			$result = $this->_driver->getDataInTable($query, 'id_spr');
			return (!empty($result))?serialize(array('sourceData'=>'array', 'table'=>'sprs', 'data'=>$result)):0;
		}

		protected function _sprsToStructure($table) {
			$path = STRUCTURES_PATH . 'Default/serialize/' . $table . '.php';
			if (!$table || !file_exists($path)) return 0;
			return serialize(array('sourceData' => 'structure', 'table' => $table));
		}

		private function _sprsToString($result) {
//			$result = $this->_stringToArray($s);
			return (!empty($result))?serialize(array('sourceData'=>'array', 'table'=>'', 'data'=>$result)):0;
		}

		private function _stringToArray($s) {
			$result = array();
			$params = explode('&', $s);
			foreach ($params as $param) {
				$param = explode('=', $param, 2);
				$result[array_shift($param)] = array_shift($param);
			}
			return $result;
		}

		private function _sprsToTableToArray($data, $idCategory) {
			if (empty($data['query'])) return 0;
			if (empty($data['query']['select'])) return 0;
			if (empty($data['query']['from'])) return 0;
			$sql = ''.
				'select ' . implode(',', $data['query']['select']) . ' '.
				'from ' . implode(',', $data['query']['from']) . ' '.
				(empty($data['query']['join'])?'':str_replace('{id_category}', $idCategory, implode(' ', $data['query']['join'])) . ' ').
				(empty($data['query']['where'])?'':'where ' . str_replace('{id_category}', $idCategory, implode(' and ', $data['query']['where'])) . ' ').
				(empty($data['query']['group'])?'':'group by ' . implode(' and ', $data['query']['group']) . ' ').
				(empty($data['query']['having'])?'':'having ' . implode(' and ', $data['query']['having']) . ' ').
				(empty($data['query']['order'])?'':'order by ' . implode(' and ', $data['query']['order']) . ' ').
			'';
			$result = $this->_driver->getDataInTable($sql, 'id');
			if (empty($result)) return 0;
			return serialize(array('sourceData'=>'array', 'table'=>$data['table'], 'data'=>$result));
		}

		private function _sprsToTable($data) {
			$table = $data['table'];
			unset($data['table']);
			return serialize(array('sourceData'=>'table', 'table'=>$table, 'data'=>$data));
		}
	/*-----*/
	/*-----*/

	protected function _saveFile(array $data, $idCategory = null){
		$file = STRUCTURES_PATH . PROJECT_NAME . '/' . $this->_projectTable['name_en'];
		if ($idCategory !== null) {
			if (!file_exists(STRUCTURES_PATH . PROJECT_NAME)) mkdir(STRUCTURES_PATH . PROJECT_NAME, 0777, 1);
			$file .= '/' . $idCategory;
		}

		$file .= '.php';

		$dir = dirname($file);
		if (!file_exists($dir)||!is_dir($dir)) mkdir($dir, 0777, 1);
		file_put_contents($file, '<?php return ' . var_export($data, true) . ';');

		chmod($file, 0664);
	}

	/*Получает все блоки текущего projectTable*/
	private function _getProjectTableBlocks(){
		$sql = 'select t.id_project_table_block, t.fields, t.id_category, tCS.leftKey, tCS.rightKey ' .
			'from project_table_blocks t ' .
			'left join project_tables tPT using (id_project_table) ' .
			'left join category_sets tCS on (t.id_category = tCS.id_category and tPT.id_root_alias = tCS.id_root_alias) ' .
			'where t.id_project_table = ' . $this->_projectTable['id_project_table'] . ' ' .
			'order by tCS.level desc ' .//важно
			'';
		$blocks = $this->_driver->multiSelect($sql);
		foreach ($blocks as &$block) {
			$block['fields'] = unserialize($block['fields']);
		}
		return $blocks;
	}

	/*Возвращает все поля определённого projectTable (ключ массива - idAllField)*/
	private function _getProjectFields(){
		$sql = ''.
			'select t.id_all_field as idAllField, t.name, t.position, t.showList, t.showForm, t.showShop, t.showAdminboard, t.showAdmin, t.showDetail, t.usedIn, t.bdParams, '.
				'tAF.name_en as nameEn, tAF.type, '.
				'case tAF.data_type '.
					'when "tinyint" then "integer" '.
					'when "longtext" then "text" '.
					'when "first_big" then "string" '.
					'else tAF.data_type '.
				'end as dataType, 1 as isRootField, '.
				'tAF.name_en_small as nameEnSmall, tAF.type_search as typeSearch, tAF.id_spr as idSpr '.
			'from project_table_fields t '.
			'left join all_fields tAF on (t.id_all_field = tAF.id_all_field) '.
			'where t.id_project_table = '.$this->_projectTable['id_project_table'].' '.
			'order by t.position'.
		'';
		return $this->_driver->multiSelect($sql,'idAllField');
	}

	/*Получаем все категории определённого projectTable, у которых есть поля*/
	private function _getCategoriesWithFields(){
		$sql = ''.
			'select t.id_category, t.position, t.leftKey, t.rightKey, tAC.name '.
			'from category_sets t '.
				'inner join category_set_fields tSF on (tSF.id_category = t.id_category) and (tSF.id_project_table = '.$this->_projectTable['id_project_table'].') '.
				'left join all_categorys tAC on (tAC.id_category = t.id_category) '.
			'where (t.id_root_alias = '.$this->_projectTable['id_root_alias'].') '.
			'group by t.id_category_set '.
			'order by t.level desc './/важно при создании структур категорий без полей
		'';
		return $this->_driver->multiSelect($sql,'id_category');
	}

	/*Получить оставшиеся категории(которые без полей) у этого же _projectTable*/
	private function _getCategoriesOther(){
		$condition = array('(t.id_root_alias = '.$this->_projectTable['id_root_alias'].')');
		if (!empty($this->_categoriesWithFields)) $condition[] = '(t.id_category not in ('.implode(',',array_keys($this->_categoriesWithFields)).'))';
		$sql = ''.
			'select t.id_category, t.position, tAC.name, t.leftKey, t.rightKey '.
			'from category_sets t '.
			'left join all_categorys tAC on (tAC.id_category = t.id_category) '.
			'where '.implode(' and ',$condition).' '.
			'order by t.position '.
		'';
		return $this->_driver->multiSelect($sql,'id_category');
	}

	/*Получаем все идентификаторы категорий, таблицы которых существуют в БД(типа messages_1, messages_21,...)*/
	private function _getIdsCategoriesInBase(){
		static $allTables = null;
		if ($allTables === null) $allTables = $this->_driver->queryColReverse('SELECT t.TABLE_NAME FROM INFORMATION_SCHEMA.tables as t where t.TABLE_SCHEMA = "'.Cfg__get('db', 'database').'"');
		$result = array();
		foreach(array_keys($this->_categoriesWithFields) as $idCategory){
			if (isset($allTables[$this->_projectTable['name_en'].'_'.$idCategory])) $result[] = $idCategory;
		}
		return $result;
	}

	private function _getCategoriesFields(){
		if (!empty($this->_categoriesWithFields)){
			$sql = ''.
				'select t.id_category, t.id_all_field as idAllField, t.name, t.position, t.showList, t.showForm, t.showShop, t.showAdminboard, t.showAdmin, t.showDetail, t.usedIn, t.bdParams, '.
					'tAF.name_en as nameEn, tAF.type, '.
					'case tAF.data_type '.
						'when "tinyint" then "integer" '.
						'when "longtext" then "text" '.
						'when "first_big" then "string" '.
						'else tAF.data_type '.
					'end as dataType, '.
					'tAF.name_en_small as nameEnSmall, tAF.type_search as typeSearch, tAF.id_spr as idSpr, tCS.leftKey, tCS.rightKey '.
				'from category_set_fields t '.
				'left join category_sets tCS on (t.id_project_table = tCS.id_root_alias and t.id_category = tCS.id_category) '.
				'left join all_fields tAF on (t.id_all_field = tAF.id_all_field) '.
				'left join all_categorys tAC on (t.id_category = tAC.id_category) '.
				'where (t.id_category in('.implode(', ',array_keys($this->_categoriesWithFields)).')) '.
					'and (id_project_table = ' . (int) $this->_projectTable['id_project_table'] . ') '.
				'order by t.position '.
			'';
			return $this->_driver->multiSelect($sql);
		}
		return array();
	}

	private function _getProjectTables(){
		static $pt = null;
		if ($pt === null){
			$pt = $this->_driver->multiSelect(
				'select id_project_table, name as tableName, name_en, id_root_alias '.
				'from project_tables '.
				(!empty($this->_idProjectTable)?('where id_project_table='.$this->_idProjectTable):'')
			);
		}
		return $pt;
	}


	/** ищем дочерние категории для текущей категории, сначала ищем дочерние текущей категории, если не нашли, то берем ближайшую родительскую
	 * @param $category array текущая категория
	 * @param $idCCT integer ид в массиве $this->_childTables
	 *
	 * @return array
	 */
	private function _getChildTables($category, $idCCT) {
		$childTables = array();
		if (isset($this->_childTables[$idCCT])) {
			$allTables = $this->_childTables[$idCCT];
			foreach ($allTables as $id=>$child) {
				if (($category['leftKey'] >= $child['lK'])&&($category['rightKey'] <= $child['rK'])) $childTables[] = $id;
			}
			if (empty($childTables)) {
				foreach ($allTables as $id=>$child) {
					if (isset($child['lK'])&&($category['leftKey'] <= $child['lK'])&&($category['rightKey'] >= $child['rK'])) $childTables = array($id);
				}
			}
		}
		return $childTables;
	}
}

class ModelsStructureSaverDriver extends ModelsDriverBDSimpleDriver {

	function multiSelect($sql, $key = false) {
		if ($key) return parent::multiSelectKey($sql, $key);
		return parent::multiSelect($sql);
	}

	function queryColReverse($sql) {
		$result = $this->query($sql);
		if ($result) {
			$return = array();
			while ($row = $result->fetch_row()) $return[array_shift($row)] = true;
			$result->close();
			return $return;
		}
		$this->_throwException('Request error ' . self::$_mysqli->error, self::$_mysqli->errno);
		return array();
	}

	function getDataInTable($sql, $key = 'idAllField') {
		$result = $this->query($sql);
		if ($result) {
			$return = array();
			while ($row = $result->fetch_assoc()) {
				if (empty($key)) $return[] = $row;
				else {
					$keyValue = $row[$key];
					unset($row[$key]);
					if (count($row) == 1) $return[$keyValue] = array_shift($row);
					else $return[$keyValue] = $row;
				}
			}
			$result->close();
			return $return;
		}
		$this->_throwException('Request error ' . self::$_mysqli->error, self::$_mysqli->errno);
		return array();
	}
}

class RestructLPSender{
	private $_sender, $_sessionId, $_key;

	function __construct($sessId){
		$this->_sessionId = $sessId;
		require_once(CLASSES_PATH . 'LongPolling.php');
		$this->_sender = new LongPolling();
		$this->_key = PROJECT_NAME . $sessId;
	}

	function sendProject($project){
		$this->_sender->message($this->_key, [
			'name_en' => $project['name_en']
		]);
	}

	function sendCategory($project, $id){
		$this->_sender->message($this->_key, [
			'name_en' => $project['name_en'],
			'category' => $id
		]);
	}

	function lastSend($time){
		$this->_sender->message($this->_key, [
			'time' => $time
		]);
	}
}