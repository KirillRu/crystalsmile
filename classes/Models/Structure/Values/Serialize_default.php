<?php
/*Created by Edik (27.11.14 14:59)*/
/*Подготавливаем значения в массиве values(в структуре) у поля с типом serialize(multiFields)*/
function ModelsStructureValues__serialize_default($field, $allValues, $typeKey){
	$values = isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : [];
	$structure = $field['idSpr']->getStructure();
	if (!is_array($values)){ //значения из БД
		if ($field['dataType'] == 'serialize'){
			$values = ($values) ? unserialize($values) : array();
		} else {
			$values = $allValues;
		}
	}
	return $structure->getValues($values, $typeKey);
}