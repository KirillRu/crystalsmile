<?php
/*Created by Edik (03.12.14 10:02)*/
function ModelsStructureValues__date_interval($field, $allValues){
	$value = isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : '';
	if (is_array($value)) { //Из формы
		foreach($value as $k => &$val) {
			$val = str_replace(',', '', $val);
			if (!$val) unset($value[$k]);
		}
		return $value;
	}
	return explode(', ', $value);
}