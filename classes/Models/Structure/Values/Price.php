<?php
/*Created by Edik (03.12.14 9:54)*/
function ModelsStructureValues__price($field, $allValues){
	$value = (isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : '');
	$value = (float) str_replace(array(',', ' '), array('.', ''), $value);
	return array(
		$field['nameEn'] => $value,
	    'id_money' => (isset($allValues['id_money']) ? $allValues['id_money'] : 1)
	);
}