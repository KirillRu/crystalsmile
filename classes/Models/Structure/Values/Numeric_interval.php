<?php
/*Created by Edik (03.12.14 10:02)*/
function ModelsStructureValues__numeric_interval($field, $allValues){
	$valueStart = (float) str_replace(' ', '', (isset($allValues[$field['nameEn'] . '_start']) ? $allValues[$field['nameEn'] . '_start'] : 0));
	$valueEnd = (float) str_replace(' ', '', (isset($allValues[$field['nameEn'] . '_end']) ? $allValues[$field['nameEn'] . '_end'] : 0));
	if ($valueStart || $valueEnd) {
		return array(
			$field['nameEn'] . '_start' => $valueStart,
			$field['nameEn'] . '_end' => $valueEnd
		);
	}
	return array(
		$field['nameEn'] . '_start' => '',
		$field['nameEn'] . '_end' => ''
	);
}