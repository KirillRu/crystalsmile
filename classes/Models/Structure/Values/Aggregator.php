<?php
/*Created by Edik (04.06.2015 15:49)*/
function ModelsStructureValues__aggregator($field, $allValues){
	$value = isset($allValues[$field['nameEnSmall']]) ? $allValues[$field['nameEnSmall']] : (isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : null);
	if (is_array($value)) $value = array_filter($value, function($val){return (bool)$val;});
	return [
		'value' => $value,
		'id_category' => isset($allValues['aggregator']['id_category']) ? $allValues['aggregator']['id_category'] : Data::get()->g('id_category'),
		'table' => isset($allValues['aggregator']['table']) ? $allValues['aggregator']['table'] : Data::get()->g('moduleName')
	];
}