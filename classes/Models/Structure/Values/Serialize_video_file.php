<?php
/*Created by Edik (27.11.14 14:59)*/
//$field['dataType'] == 'multiFields'
function ModelsStructureValues__serialize_video_file($field, $allValues, $typeKey){
	$allValues = isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : $allValues;

	$result = $field['idSpr']->getStructure()->getValues($allValues, $typeKey);

	if (!empty($allValues['isEdit'])) $result['id_video'] = $allValues['id_video'];
	return $result;
}