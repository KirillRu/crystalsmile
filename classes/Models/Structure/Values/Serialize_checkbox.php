<?php
/*Created by Edik (27.11.14 14:59)*/
function ModelsStructureValues__serialize_checkbox($field, $allValues, $typeKey){
	$values = isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : null;
	$structure = $field['idSpr']->getStructure();
	if (is_array($values)){ //Значит эти значения - значения из формы
		/*сheckbox не выбран*/
		if (!empty($field[$typeKey]) && ($field[$typeKey] == 'checkbox') && empty($values[$field['nameEn']])) $result = array();
		else $result = $structure->getValues($values, $typeKey);
	} else { //значения из БД
		if ($field['dataType'] == 'serialize'){
			$values = ($values) ? unserialize($values) : array();
			if (!empty($field[$typeKey]) && ($field[$typeKey] == 'checkbox') && !$values) $result = array();
			else $result = $structure->getValues($values, $typeKey);
		} else {
			if (!empty($field[$typeKey]) && ($field[$typeKey] == 'checkbox') && !$values) $result = array();
			else $result = $structure->getValues($allValues, $typeKey);
		}
	}
	return $result;
}