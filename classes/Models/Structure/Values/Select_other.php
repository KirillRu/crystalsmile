<?php
/*Created by Edik (03.12.14 9:59)*/
function ModelsStructureValues__select_other($field, $allValues){
	$value = str_replace(' ', '', isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : 0);
	if ($value) {
		return array(
			$field['nameEn'] => $value,
			$field['nameEn'] . '_other' => isset($allValues[$field['nameEn'] . '_other']) ? $allValues[$field['nameEn'] . '_other'] : ''
		);
	}
	return array();
}