<?php
/*Created by Edik (27.11.14 14:59)*/
//$field['dataType'] == 'multiFields'
function ModelsStructureValues__serialize_multi_lines($field, $allValues, $typeKey){
	$values = isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : [];
	$result = [];
	if (is_string($values)) $values = unserialize($values);
	if ($values && !is_numeric(array_keys($values)[0])){ //значения из Формы
		/*заменяем массив вида array('id_type'=>array(0=>1; 1=>3)) на array(0=>array('id_type'=>1), 1=>array('id_type'=>3))*/
		foreach($values as $nameEn => $fieldValue){
			foreach($fieldValue as $id => $oneValue){
				$result[$id][$nameEn] = $oneValue;
			}
		}
		$values = $result; unset($result);
	}
	foreach($values as $i => &$row){
		$row = $field['idSpr']->getStructure()->getValues($row, $typeKey);
	}
	return $values;
}