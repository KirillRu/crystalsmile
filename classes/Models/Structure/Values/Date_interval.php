<?php
/*Created by Edik (03.12.14 10:02)*/
function ModelsStructureValues__date_interval($field, $allValues){
	$valueStart = (isset($allValues[$field['nameEn'] . '_start']) ? $allValues[$field['nameEn'] . '_start'] : 0);
	$valueEnd = (isset($allValues[$field['nameEn'] . '_end']) ? $allValues[$field['nameEn'] . '_end'] : 0);
	if ($valueStart || $valueEnd) {
		/*1.12.2012*/
		$reg = '/^(0?[1-9]|[12]\d|3[01])[- \/.](0?[1-9]|1[012])[- \/.]([12]\d{3})$/';
		if (!is_numeric($valueStart) && preg_match($reg, $valueStart, $m)) $valueStart = mktime(0, 0, 0, $m[2], $m[1], $m[3]);
		if (!is_numeric($valueEnd) && preg_match($reg, $valueEnd, $m)) $valueEnd = mktime(23, 59, 59, $m[2], $m[1], $m[3]);
		return array(
			$field['nameEn'] . '_start' => $valueStart,
			$field['nameEn'] . '_end' => $valueEnd
		);
	}
	return array(
		$field['nameEn'] . '_start' => 0,
		$field['nameEn'] . '_end' => 0
	);
}