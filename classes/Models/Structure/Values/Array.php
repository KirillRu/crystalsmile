<?php
/*Created by Edik (31.07.2015 11:27)*/
//возвращает значение $allValues[$field['nameEn']], при этом очищая его от пустых строк(незаполненных значений)
function ModelsStructureValues__array($field, $allValues){
	$value = isset($allValues[$field['nameEn']]) ? $allValues[$field['nameEn']] : null;

	//вырезаем из массива не выбранные значения
	return $value ? array_filter($value, function($val){return $val !== '';}) : null;
}