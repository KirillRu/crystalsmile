<?php
class ModelsStructureTableBuilder {
	/**
	 * @var ModelsStructure
	 */
	protected $_structure;
	protected $_items;

	function __construct(ModelsStructure $structure, $items = array()) {
		$this->_structure = $structure;
		$this->_items = $items;
	}

	/**
	 * @param $i - номер в массиве $items
	 * @param $iAF - либо $iAF, либо name_en
	 */
	function getValue($i, $iAF) {
		$fields = $this->_structure->getFields();
		if (isset($fields[$iAF])){
			$field = $fields[$iAF];
			switch ($field['type']) {
				case 'date_interval':
					$result = array();
					if (!empty($this->_items[$i][$field['nameEn'] . '_start'])) $result[] = 'с ' . date('d.m.Y H:i', $this->_items[$i][$field['nameEn'] . '_start']);
					if (!empty($this->_items[$i][$field['nameEn'] . '_end'])) $result[] = 'по ' . date('d.m.Y H:i', $this->_items[$i][$field['nameEn'] . '_end']);
					return implode(' ', $result);
					break;
				case 'date_calendar_interval':
					$result = array();
					if (!empty($this->_items[$i][$field['nameEn'] . '_start'])) $result[] = 'с ' . date('d.m.Y', $this->_items[$i][$field['nameEn'] . '_start']);
					if (!empty($this->_items[$i][$field['nameEn'] . '_end'])) $result[] = 'по ' . date('d.m.Y', $this->_items[$i][$field['nameEn'] . '_end']);
					return implode(' ', $result);
					break;
				case 'numeric_interval':
					$result = array();
					$value = $this->getItemValue($i, $field, array());
					if ($field['nameEn'] == 'ip'){
						if (!empty($value[$field['nameEn'] . '_start'])) $result[] = 'от ' . _getIpFormSum($this->_items[$i][$field['nameEn'] . '_start']);
						if (!empty($value[$field['nameEn'] . '_end'])) $result[] = 'до ' . _getIpFormSum($this->_items[$i][$field['nameEn'] . '_end']);
					} else {
						if (!empty($value[$field['nameEn'] . '_start'])) $result[] = 'от ' . $this->_items[$i][$field['nameEn'] . '_start'];
						if (!empty($value[$field['nameEn'] . '_end'])) $result[] = 'до ' . $this->_items[$i][$field['nameEn'] . '_end'];
					}
					return implode(' ', $result);
					break;
				case 'price_interval':
					$result = array();
					$value = $this->getItemValue($i, $field, array());
					if (!empty($value[$field['nameEn'] . '_start'])) $result[] = 'от ' . date('d.m.Y H:i', $this->_items[$i][$field['nameEn'] . '_start']);
					if (!empty($value[$field['nameEn'] . '_end'])) $result[] = 'до ' . date('d.m.Y H:i', $this->_items[$i][$field['nameEn'] . '_end']);
					return empty($result) ? '' : (implode(' ', $result) . 'руб');
					break;
				case 'price':
					if (!empty($this->_items[$i]['id_money'])) return Price::get()->textPrice($this->getItemValue($i, $field, array()), $this->_items[$i]['id_money']);
					return empty($value) ? '' : ($value . ' руб');
					break;
				case 'checkbox':
//					return FormRow::get()->viewValue($field, $this->getItemValue($i, $field))->fetch();
					break;
				case 'select':
				case 'select_search':
				case 'radio_row':
				case 'search_select_child':
					if (!empty($field['idSpr'])){
						$data = $field['idSpr']->getItems();
						$value = $this->getItemValue($i, $field, '');
						if (isset($data[$value])) return $data[$value];
					}
					return '';
					break;
				case 'select_child':
					return $this->_selectChildValue($i, $field);
					break;
				default:
					$value = $this->getItemValue($i, $field, '');
					if (empty($value)) return '';
					switch ($field['dataType']) {
						case 'date': return date('d.m.Y', $value); break;
						case 'datetime': return date('d.m.Y H:i', $value); break;
						case 'serialize': break;
						case 'bool':
							if (empty($value)) return '<img src="' . SITE_ROOT . '_ps/images/checkbox_unchecked.gif">';
							return '<img src="' . SITE_ROOT . '_ps/images/checkbox_checked.gif">';
							break;
					}
					return $value;
					break;
			}
		} elseif (isset($this->_items[$i][$iAF])) return $this->_items[$i][$iAF];
		return '';
	}

	protected function _selectChildValue($i, $field) {
		$result = array();
		$value = $this->getItemValue($i, $field, '');
		/** @var $structure ModelsStructure*/
		$structure = $field['idSpr']->getStructure();
		if ($structure->getTable() == 'category_sets'){
			$sql = '' .
				'select tC.name, t.id_parent, t.leftKey, t.rightKey ' .
				'from ' . $structure->getTable() . ' t ' .
					'join all_categorys tC on (tC.id_category = t.id_category) ' .
				'where (t.' . $structure->getId() . ' = ' . ((int)$value) . ') ' .
				'limit 0, 1 ' .
			'';
			$item = ModelsDriverBDCache::get()->rowSelect($sql, array($structure->getTable(), 'all_categorys'));
		} else {
			$sql = '' .
				'select name, id_parent, leftKey, rightKey ' .
				'from ' . $structure->getTable() . ' ' .
				'where (' . $structure->getId() . ' = ' . ((int)$value) . ') ' .
				'limit 0, 1' .
			'';
			$item = ModelsDriverBDCache::get()->rowSelect($sql, $structure->getTable());
		}
		if (empty($item)) return '';

		if (!empty($item['id_parent'])){
			if ($structure->getTable() == 'category_sets'){
				$sql = '' .
					'select t.' . $structure->getId() . ', tC.name ' .
					'from ' . $structure->getTable() . ' t ' .
					'join all_categorys tC on (tC.id_category = t.id_category) ' .
					'where (t.leftKey <= ' . $item['leftKey'] . ') ' . 'and (t.rightKey >= ' . $item['rightKey'] . ') ' .
					'order by t.leftKey ' .
				'';
				$items = ModelsDriverBDCache::get()->multiSelect($sql, array($structure->getTable(), 'all_categorys'));
			} else {
				$sql = '' .
					'select ' . $structure->getId() . ', name ' .
					'from ' . $structure->getTable() . ' ' .
					'where (leftKey <= ' . $item['leftKey'] . ') ' . 'and (rightKey >= ' . $item['rightKey'] . ') ' .
					'order by leftKey ' .
				'';
				$items = ModelsDriverBDCache::get()->multiSelect($sql, $structure->getTable());
			}
			while ($_item = array_shift($items)) $result[$_item[$structure->getId()]] = $_item['name'];
		} else $result[] = $item['name'];
//		_print_r($result);
		return implode(', ', $result);
	}

	function getItemValue($i, $field, $default = null) {
		if (isset($this->_items[$i][$field['nameEn']])) return $this->_items[$i][$field['nameEn']];
		if (isset($this->_items[$i][$field['idAllField']])) return $this->_items[$i][$field['idAllField']];
		return $default;
	}

	function getValueByName($i, $fieldName) {
		$iAF = $this->_structure->getIAF($fieldName);
		if (empty($iAF)) return false;
		return $this->getValue($i, $iAF);
	}

//	function viewFieldFilter($iAF) {
//		$fields = $this->_structure->getFields();
//		if (!isset($fields[$iAF])) return new View();
//		$field = $fields[$iAF];
//		if (empty($field['typeSearch']) || empty($field['usedIn']['search'])) return new View(); else return FormRow::viewValue($field, $this->getFilterValue($iAF), 'filter', 'typeSearch');
//	}

//	function getFilterValue($iAF, $default = null) {
//		//structure->getValues() уже должен был быть выполнен, и значения должны быть поготовлены
//		$filterValues = $this->_structure->getValues();
//		if (isset($filterValues[$iAF])) $default = $filterValues[$iAF];
//		return $default;
//	}


//	function getIAFs() { return array_keys($this->_structure->getFields()); }

//	function getKeys() { return array_keys($this->_items); }

//	function getId() { return $this->_structure->getId(); }

//	function isEmpty() { return empty($this->_items); }

//	function getFieldName($iAF) {
//		$fields = $this->_structure->getFields();
//		if (!isset($fields[$iAF])) return false;
//
//		$field = $fields[$iAF];
//		if (empty($field['showList'])) return false;
//
//		return $field['name'];
//	}

//	function isFilter() {
//		foreach ($this->_structure->getFields() as $field) {
//			if (!empty($field['usedIn']['search'])) return true;
//		}
//		return false;
//	}

//	function getStructure() { return $this->_structure; }

//	function setItems(array $items) {
//		$this->_items = $items;
//	}
}