<?php
class ModelsStructureQueryBuilder {
	protected $_structure;

	function __construct(ModelsStructure $structure) {
		$this->_structure = $structure;
	}

	/**
	 * @param string $show showList, showDetail, ...
	 * @param array $condition
	 * @return string sql запрос, построенный по структуре
	 */
	function get($show = 'showList', $condition = []) {
		list($select, $join) = $this->getSelectJoin($show);
		return $this->buildQuery([
			'select' => $select,
			'join' => $join,
			'table' => $this->_structure->getTable(),
		    'condition' => $condition
		]);
	}

	function buildQuery($queryParams){
		$sql = '' .
			'select ' . implode(', ', $queryParams['select']) . ' ' .
			'from ' . $queryParams['table'] . ' as t ' .
			(!empty($queryParams['join']) ? implode(' ', $queryParams['join']) . ' ' : '') .
			(!empty($queryParams['condition']) ? ('where ' . implode(' and ', $queryParams['condition'])) . ' ' : '') .
			(!empty($queryParams['order']) ? ('order by ' . implode(' ', $queryParams['order'])) . ' ' : '') .
			'';
		return $sql;
	}

	/**Возвращает массив, который строится по структуре
	 * @param string $show showList, showDetail, ...
	 * @return array массив вида array($select, $join)
	 */
	function getSelectJoin($show = 'showList') {
		$select = array($this->_structure->getId() => 't.' . $this->_structure->getId());
		$join = array();
		$addChildTables = false;
		foreach ($this->_structure->getFields() as $field) {
			if (empty($show) || !empty($field[$show])){
				$fieldSelect = $this->_prepareSelect($field, $show);
				if (!empty($fieldSelect)) $select[$field['nameEn']] = implode(', ', $fieldSelect);
				if (empty($field['isRootField'])) $addChildTables = true;
			}
		}
		if ($addChildTables) $join = array_merge($join, $this->_getChildJoins());
		return array($select, $join);
	}

	private function _getChildJoins() {
		$joins = array();
		foreach ($this->_structure->getChildTables() as $idChild) $joins['t' . $idChild] = '' . 'left join ' . $this->_structure->getTable() . '_' . $idChild . ' as t' . $idChild . ' ' . 'on (t' . $idChild . '.' . $this->_structure->getId() . ' = t.' . $this->_structure->getId() . ')' . '';
		return $joins;
	}

	protected function _prepareSelect($field, $show) {
		$select = array();
		$aliases = array();
		if (empty($field['isRootField'])){
			foreach ($this->_structure->getChildTables() as $idChild) $aliases[] = 't' . $idChild;
		}
        switch ($field['type']) {
			case 'price':
				if (!empty($field['isRootField'])){
					$select[] = 't.' . $field['nameEn'];
					$select[] = 't.id_money';
				} elseif (!empty($aliases)) {
					$names = array();
					$idMoneys = array();
					foreach ($aliases as $alias) {
						$names[] = $this->_prepareSelectName($alias, $field['nameEn']);
						$idMoneys[] = $this->_prepareSelectName($alias, 'id_money');
					}
					$select[] = 'case ' . implode(' ', $names) . ' else "" end as ' . $field['nameEn'];
					$select[] = 'case ' . implode(' ', $idMoneys) . ' else "" end as id_money';
				}
				break;
			case 'select_other':
				if (!empty($field['isRootField'])){
					$select[] = 't.' . $field['nameEn'];
					$select[] = 't.' . $field['nameEn'] . '_other';
				} elseif (!empty($aliases)) {
					$names = array();
					$namesO = array();
					foreach ($aliases as $alias) {
						$names[] = $this->_prepareSelectName($alias, $field['nameEn']);
						$namesO[] = $this->_prepareSelectName($alias, $field['nameEn'] . '_other');
					}
					$select[] = 'case ' . implode(' ', $names) . ' else "" end as ' . $field['nameEn'];
					$select[] = 'case ' . implode(' ', $namesO) . ' else "" end as ' . $field['nameEn'] . '_other';
				}
				break;
			case 'price_interval':
				if (!empty($field['isRootField'])){
					$select[] = 't.' . $field['nameEn'] . '_start';
					$select[] = 't.' . $field['nameEn'] . '_end';
					$select[] = 't.id_money';
				} elseif (!empty($aliases)) {
					$namesStart = array();
					$namesEnd = array();
					$idMoneys = array();
					foreach ($aliases as $alias) {
						$namesStart[] = $this->_prepareSelectName($alias, $field['nameEn'] . '_start');
						$namesEnd[] = $this->_prepareSelectName($alias, $field['nameEn'] . '_end');
						$idMoneys[] = $this->_prepareSelectName($alias, 'id_money');
					}
					$select[] = 'case ' . implode(' ', $namesStart) . ' else "" end as ' . $field['nameEn'] . '_start';
					$select[] = 'case ' . implode(' ', $namesEnd) . ' else "" end as ' . $field['nameEn'] . '_end';
					$select[] = 'case ' . implode(' ', $idMoneys) . ' else "" end as id_money';
				}
				break;
			case 'numeric_interval':
			case 'date_interval':
			case 'date_calendar_interval':
	        case 'date_calendar_interval_datepicker':
				if (!empty($field['isRootField'])){
					$select[] = 't.' . $field['nameEn'] . '_start';
					$select[] = 't.' . $field['nameEn'] . '_end';
				} elseif (!empty($aliases)) {
					$namesStart = array();
					$namesEnd = array();
					foreach ($aliases as $alias) {
						$namesStart[] = $this->_prepareSelectName($alias, $field['nameEn'] . '_start');
						$namesEnd[] = $this->_prepareSelectName($alias, $field['nameEn'] . '_end');
					}
					$select[] = 'case ' . implode(' ', $namesStart) . ' else "" end as ' . $field['nameEn'] . '_start';
					$select[] = 'case ' . implode(' ', $namesEnd) . ' else "" end as ' . $field['nameEn'] . '_end';
				}
				break;
			case 'checkbox_row':
				break;
			case 'textarea':
			case 'textarea_simple':
				if (!empty($field['isRootField'])){
					//в форме и на детальной странице берем из БД все
					if (in_array($show, ['showForm', 'showDetail']) || in_array($field['nameEn'], array('url_banner', 'firm_requisites', 'text_link'))) $select[] = 't.' . $field['nameEn'] . ' as ' . $field['nameEn'];
					else $select[] = 'left(t.' . $field['nameEn'] . ', 255) as ' . $field['nameEn'];
				} elseif (!empty($aliases)) {
					$names = array();
					if (in_array($field['nameEn'], array('url_banner', 'text_link'))) foreach ($aliases as $alias) $names[] = $this->_prepareSelectName($alias, $field['nameEn']);
					else foreach ($aliases as $alias) $names[] = $this->_prepareSelectName($alias, $field['nameEn'], true);
					$select[] = 'case ' . implode(' ', $names) . ' else "" end as ' . $field['nameEn'];
				}
				break;
			default:
				if (!empty($field['isRootField'])) {
					if ($field['dataType'] === 'multiFields'){
						foreach ($field['idSpr']->getStructure()->getFields() as $field) {
							$names = $this->_prepareSelect($field, $show);
							if ($names) $select[] = implode(', ', $names);
						}
					} else {
						$select[] = 't.' . $field['nameEn'];
					}
				} else {
					$names = array();
					foreach ($aliases as $alias) $names[] = $this->_prepareSelectName($alias, $field['nameEn']);
					if (!empty($names)) $select[] = 'case ' . implode(' ', $names) . ' else "" end as ' . $field['nameEn'];
				}
				break;
		}
		return $select;
	}

	protected function _prepareSelectName($alias, $name, $cut = false, $length = 255) {
		if ($cut) return 'when (' . $alias . '.' . $name . ' is not null) then left(' . $alias . '.' . $name . ', ' . $length . ')';
		return 'when (' . $alias . '.' . $name . ' is not null) then ' . $alias . '.' . $name;
	}


}