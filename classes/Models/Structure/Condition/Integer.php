<?php
/*Created by Edik (20.05.2015 11:52)*/
function ModelsStructureCondition__integer($field, $value, $alias){
	if (is_array($value)) {
		$_value = [];
		foreach ($value as $v) {
			$v = (int) $v;
			if ($v) $_value[] = $v;
		}
		if ($_value) return '('.$alias.'.'.$field['nameEn'].' in (' . implode(',', $_value) . '))';
	} else {
		$value = (int) $value;
		if ($value)	return '(' . $alias . '.' . $field['nameEn'] . ' = ' . $value . ')';
	}
	return null;
}