<?php
/*Created by Edik (20.05.2015 11:51)*/
function ModelsStructureCondition__interval($field, $value, $alias){
	if (!$value) return null;

	$start = (int) $value[$field['nameEn'].'_start'];
	$end = (int) $value[$field['nameEn'].'_end'];
	if ($start || $end) {
		switch (true) {
			case ($start && $end && ($start < $end)):
				$cond = [];
				$cond[] = '('.$alias.'.'.$field["nameEn"].' >= '.(int)($start).')';
				$cond[] = '('.$alias.'.'.$field["nameEn"].' <= '.(int)($end).')';
				return implode(' and ', $cond);
				break;
			case (!$end || ($start > $end)):
				return '('.$alias.'.'.$field["nameEn"].' >= '.(int)($start).')';
				break;
			case (!$start && $end):
				return '('.$alias.'.'.$field["nameEn"].' <= '.(int)($end).')';
				break;
			case ($start && ($start == $end)):
				return '('.$alias.'.'.$field["nameEn"].' = '.(int)($end).')';
				break;
		}
	}
	return null;
}