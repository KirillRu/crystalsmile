<?php
/*Created by Edik (20.05.2015 11:52)*/
/*здесь мы строим условия на все валюты. Например если фильтр указан > 2$, то в результирующий набор должны попасть
	строки, у которых цена 2+$ ИЛИ 100+Рублей ИЛИ xx+...в зависимости от курса*/
function ModelsStructureCondition__price_interval($field, $value, $alias){
	if (!$value) return null;

	$start = (int) $value[$field['nameEn'].'_start'];
	$end = (int) $value[$field['nameEn'].'_end'];
	$id_money = (int)$value['id_money'];

	if (($id_money > 0) && ($start || $end)) {

		$moneys = ModelsDriverBDCache::get()->multiSelect('select id_money, rate from moneys', ['moneys']);
		$rate = 1;
		foreach($moneys as $money) if ($money['id_money'] == $id_money) $rate = $money['rate'];

		switch (true){
			case ($start && $end && ($start < $end)):
				$cond = [];
				foreach($moneys as $money){
					$money['rate'] = ($rate / $money['rate']);
					$cond[] = '(('.$alias.'.'.$field["nameEn"].' >= '.($start * $money['rate']).') and '.
							  '('.$alias.'.'.$field["nameEn"].' <= '.($end * $money['rate']).') and '.
							  '('.$alias.'.id_money = '.$money['id_money'].'))';
				}
				return ($cond) ? ('('.implode(' or ', $cond).')') : null;
				break;
			case (!$end || ($start > $end)):
				$cond = [];
				foreach($moneys as $money){
					$money['rate'] = ($rate / $money['rate']);
					$cond[] = '(('.$alias.'.'.$field["nameEn"].' >= '.($start * $money['rate']).') and '.
							  '('.$alias.'.id_money = '.$money['id_money'].'))';
				}
				return ($cond) ? ('('.implode(' or ', $cond).')') : null;
				break;
			case (!$start && $end):
				$cond = [];
				foreach($moneys as $money){
					$money['rate'] = ($rate / $money['rate']);
					$cond[] = '(('.$alias.'.'.$field["nameEn"].' <= '.($end * $money['rate']).') and '.
							  '('.$alias.'.id_money = '.$money['id_money'].'))';
				}
				return ($cond) ? ('('.implode(' or ', $cond).')') : null;
				break;
			case ($start && ($start == $end)):
				$cond = array();
				foreach($moneys as $money){
					$money['rate'] = ($rate / $money['rate']);
					$cond[] = '(('.$alias.'.'.$field["nameEn"].' = '.($end * $money['rate']).') and '.
							  '('.$alias.'.id_money = '.$money['id_money'].'))';
				}
				return ($cond) ? ('('.implode(' or ', $cond).')') : null;
				break;
		}
	}
	return null;
}