<?php
/*Created by Edik (20.05.2015 11:50)*/
function ModelsStructureCondition__spr($field, $value, $alias){
	if (!$value || empty($field['idSpr'])) return null;
	$sStructure = $field['idSpr']->getStructure();

	switch ($field['dataType']) {
		case 'serialize':
			$sTable = $sStructure->getTable();
			if (!$sTable) return null;

			if (is_array($value)){
				//убираем пустые элементы
				$value = array_filter($value, function($val){return (bool)$val;});
				//очищаем от инъекций
				$value = array_map(function($val){return Text::get()->mest($val);}, $value);
			}

			if (!$value) return null;

			foreach ($sStructure->getFields() as $sField) {
				if (!empty($sField['idSpr'])) {
					return '(' . $alias . '.' . $sStructure->getId() . ' in (' .
						'select ' . $sStructure->getId() . ' '.
						'from ' . $sTable . ' where ('.
						(is_array($value) ? ($sField['nameEn'] .' in ('.implode(', ', $value).'))') : ($sField['nameEn'] . ' = ' . Text::get()->mest($value)) . ')').
					'))';
				}
			}
			break;
		default:
			if ($sStructure->isNested()) {
				$sTable = $sStructure->getTable();
				$sId = $sStructure->getId();

				$sql = 'select leftKey, rightKey from ' . $sTable . ' where ' . $sId . ' = ' . (int)$value . ' limit 0, 1';

				$item = ModelsDriverBDCache::get()->rowSelect($sql, [$sTable]);
				if ($item) {
					$sql = ''.
						'select ' . $sId .
						'from ' . $sTable . ' '.
						'where (leftKey >= ' . $item['leftKey'] . ') '.
							'and (rightKey <= ' . $item['rightKey'] . ') '.
					'';
					$ids = ModelsDriverBDCache::get()->queryCol($sql, [$sTable]);
					if ($ids) return '('.$alias.'.'.$field['nameEn'].' in (' . implode(',', $ids) . '))';
				}
			}
			require_once(CLASSES_PATH . 'Models/Structure/DataType/DataType.php');
			$dataTypes = new ModelsStructureDataType();

			if ($dataTypes->checkNumericType($field['dataType'])) return '('.$alias.'.'.$field['nameEn'].' = '.(int)$value.')';
			return '('.$alias.'.'.$field['nameEn'].' = '.Text::get()->mest($value).')';
			break;
	}
	return null;
}