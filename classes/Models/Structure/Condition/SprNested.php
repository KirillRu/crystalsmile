<?php
/*Created by Edik (20.05.2015 11:52)*/
function ModelsStructureCondition__sprNested($field, $value, $alias){
	if (empty($field['idSpr']) || !$value) return null;

	$sStructure = $field['idSpr']->getStructure();
	if ($sStructure->isNested()) {
		$sTable = $sStructure->getTable();
		$sId = $sStructure->getId();
		if ($sTable === 'category_sets'){
			//если таблица category_sets то сюда в значении value передается id_category
			$item = $field['idSpr']->getItems(['where' => ['id_category' => 't.id_category = ' . (int)$value], 'limit' => 1]);

			if ($item){
				$sId = 'id_category';
				$item = ModelsDriverBDCache::get()->rowSelect('select leftKey, rightKey from ' . $sTable . ' where id_category_set = ' . (int)$item[0]['id'] . ' limit 0, 1', $sTable);
			}
		} else {
			$sId = $sStructure->getId();
			$sql = 'select leftKey, rightKey from ' . $sTable . ' where ' . $sId . ' = ' . (int)$value . ' limit 0, 1';
			$item = ModelsDriverBDCache::get()->rowSelect($sql, [$sTable]);
		}


		if ($item) {
			$sql = ''.
				'select ' . $sId . ' '.
				'from ' . $sTable . ' '.
				'where (leftKey >= ' . $item['leftKey'] . ') '.
					'and (rightKey <= ' . $item['rightKey'] . ') '.
			'';
			$ids = ModelsDriverBDCache::get()->queryCol($sql, [$sTable]);
			if ($ids) return '('.$alias.'.'.$field['nameEn'].' in (' . implode(',', $ids) . '))';
		}
	} else {
		require_once(CLASSES_PATH . 'Models/Structure/DataType/DataType.php');
		$dataTypes = new ModelsStructureDataType();

		if ($dataTypes->checkNumericType($field['dataType'])) $value = (int) $value;
		else $value = Text::get()->mest($value);

		return '('.$alias.'.'.$field['nameEn'].' = ' . $value . ')';
	}
	return null;
}