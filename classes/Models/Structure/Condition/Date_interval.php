<?php
/*Created by Edik (20.05.2015 11:51)*/
function ModelsStructureCondition__date_interval($field, $value, $alias){
	if (!$value) return null;

	$start = (int) $value[$field['nameEn'] . '_start'];
	$end = (int) $value[$field['nameEn'] . '_end'];

	if ($start || $end) {
		switch (true) {
			case ($start && $end && ($start < $end)):
				$cond = [];
				$condition = '(( ';
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_start >= '.(int)($start).')';
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_start <= '.(int)($end).')';
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_end >= '.(int)($start).')';
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_end <= '.(int)($end).')';
				$condition .= implode(' and ', $cond) . ') or (';
				$cond = [];
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_start <= '.(int)($start).')';
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_end >= '.(int)($start).')';
				$condition .= implode(' and ', $cond). ') or (';
				$cond = [];
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_start <= '.(int)($end).')';
				$cond[] = '('.$alias.'.'.$field["nameEn"].'_end >= '.(int)($end).')';
				$condition .= implode(' and ', $cond);
				$condition .= ')) ';
				return $condition;
				break;
			case (!$end || ($start > $end)):
				return '('.$alias.'.'.$field["nameEn"].'_start >= '.(int)($start).')';
				break;
			case (!$start && $end):
				return '('.$alias.'.'.$field["nameEn"].'_end <= '.(int)($end).')';
				break;
			case ($start && ($start == $end)):
				return '('.$alias.'.'.$field["nameEn"].'_start = '.(int)($end).')';
				break;
		}
	}
	return null;
}