<?php
class ModelsStructureDataTypeFieldSite{
	private $_types = array();

	function __construct(){
		$this->setTypes(include(dirname(__FILE__).'/Types/FieldSite.php'));
	}

	function setTypes($types) {
		$this->_types = $types;
	}

	function getSprNameEn_name() {
		$spr = '';
		foreach ($this->_types as $type) {
			$spr .= $type['nameEn'].'='.$type['name'].'&';
		}
		return substr($spr, 0, -1);
	}

	function getNameEn_name() {
		$array = array();
		foreach ($this->_types as $type) {
			$array[$type['nameEn']] = $type['name'];
		}
		return $array;
	}

	function checkStringType($nameEn) {return ($this->getType($nameEn) == 'string');}

	function checkIntervalType($nameEn) {return ($this->getType($nameEn) == 'interval');}

	function getType($nameEn) {
		foreach ($this->_types as $type) {
			if ($type['nameEn'] == $nameEn)
				return $type['type'];
		}
		return null;
	}
}
