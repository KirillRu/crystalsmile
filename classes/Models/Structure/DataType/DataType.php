<?php
class ModelsStructureDataType {
	private $_types = array();

	function __construct(){
		$this->_types = include(dirname(__FILE__).'/Types/DataType.php');
	}

	function getSprDataType_name() {
		$spr = array();
		foreach ($this->_types as $type) $spr[] = $type['name'].'='.$type['name'];
		return implode('&', $spr);
	}

	function getNameEn_name() {
		$array = array();
		foreach ($this->_types as $type) $array[$type['name']] = $type['name'];
		return $array;
	}

	function checkStringType($dataType) { return (!$this->checkNumericType($dataType)&&!$this->checkFloatType($dataType)); }

	function checkNumericType($dataType) { return ($this->_getType($dataType) == 'integer');}

	function checkFloatType($dataType) { return ($this->_getType($dataType) == 'float');}

	private function _getType($dataType) {
		foreach ($this->_types as $type) {
			if ($type['name'] == $dataType) return $type['type'];
		}
		return null;
	}

}
