<?php
return array(
	array('nameEn'=>'text',					'name'=>'Текст',								'type'=>'string'),
	array('nameEn'=>'textarea',				'name'=>'Текстарея',							'type'=>'string'),
	array('nameEn'=>'select',				'name'=>'Селект',								'type'=>'select'),
	array('nameEn'=>'select_indent',		'name'=>'Селект c отступом',					'type'=>'filter'),
	array('nameEn'=>'select_other',			'name'=>'Селект с добавочным выбором',			'type'=>'select'),
	array('nameEn'=>'checkbox',				'name'=>'Чекбокс',								'type'=>'bool'),
	array('nameEn'=>'date_interval',		'name'=>'Интервал дат',							'type'=>'interval'),
	array('nameEn'=>'date_calendar_interval','name'=>'Интервал дат с календарём',			'type'=>'interval'),
	array('nameEn'=>'price_interval',		'name'=>'Ценовой интервал',						'type'=>'interval'),
	array('nameEn'=>'numeric_interval',		'name'=>'Числовой интервал',					'type'=>'interval'),
	array('nameEn'=>'price',				'name'=>'Цена',									'type'=>'object'),
	array('nameEn'=>'textarea_simple',		'name'=>'Текстария простая',					'type'=>'string'),
	array('nameEn'=>'select_child',			'name'=>'Select Child',							'type'=>'select'),
	array('nameEn'=>'window_search_select_child',		'name'=>'Window Search Select Child','type'=>'select'),
	array('nameEn'=>'select_category',		'name'=>'Выбор категории',						'type'=>'select'),
	array('nameEn'=>'window_search_select_category',		'name'=>'Window Search Select Category','type'=>'select'),
	array('nameEn'=>'checkbox_row',			'name'=>'Ряд чекбоксов',						'type'=>'select_inOtherTable'),
	array('nameEn'=>'radio_row',			'name'=>'Ряд радиобатонов',						'type'=>'select'),
	array('nameEn'=>'wordpanel',			'name'=>'HTML-редактор',						'type'=>'string'),
	array('nameEn'=>'date_calendar',		'name'=>'Дата календарь',						'type'=>'calendar'),
	array('nameEn'=>'hidden',				'name'=>'скрытое поле блок (для serialize)', 	'type'=>'')
);