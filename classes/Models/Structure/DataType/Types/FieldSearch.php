<?php
return array(
	array('nameEn'=>'text',				'name'=>'Текст',				'type'=>'string'),
	array('nameEn'=>'textarea',			'name'=>'Текстарея',			'type'=>'string'),
	array('nameEn'=>'select',			'name'=>'Селект',				'type'=>'filter'),
	array('nameEn'=>'select_indent',	'name'=>'Селект c отступом',	'type'=>'filter'),
	array('nameEn'=>'checkbox',			'name'=>'Чекбокс',				'type'=>'filter'),
	array('nameEn'=>'date_interval',	'name'=>'Интервал дат',			'type'=>'filter'),
	array('nameEn'=>'price_interval',	'name'=>'Ценовой интервал',		'type'=>'filter'),
	array('nameEn'=>'numeric_interval',	'name'=>'Числовой интервал',	'type'=>'filter'),
	array('nameEn'=>'price',			'name'=>'Цена',					'type'=>'object'),
	array('nameEn'=>'fio',				'name'=>'ФИО',					'type'=>'object'),
	array('nameEn'=>'textarea_simple',	'name'=>'Текстария простая',	'type'=>'string'),
	array('nameEn'=>'select_array',		'name'=>'Select массив',		'type'=>'filter'),
	array('nameEn'=>'multi_select',		'name'=>'Массив из селектов',	'type'=>'filter'),
	array('nameEn'=>'city',				'name'=>'Город',				'type'=>'filter'),
	array('nameEn'=>'select_onclick',	'name'=>'Select on Click',		'type'=>'filter'),
	array('nameEn'=>'select_child',		'name'=>'Select Child',			'type'=>'filter'),
	array('nameEn'=>'blok_center',		'name'=>'Центральный блок',		'type'=>'widget'),
	array('nameEn'=>'blok_left',		'name'=>'Левый блок',			'type'=>'widget'),
	array('nameEn'=>'blok_right',		'name'=>'Правый блок',			'type'=>'widget')
);