<?php
/**
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com
 * Date: 19.02.14
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */

class ModelsStructureFilterBuilder {
	/**
	 * @var ModelsStructure
	 */
	protected $_structure;
	protected $_data;

	function __construct(ModelsStructure $structure, $data) {
		$this->_structure = $structure;
		$this->_data = $data;
	}

	function getCondition() {
		if (empty($this->_data) || !is_array($this->_data)) return array();
		$filterValues = $this->_structure->getValues($this->_data, 'typeSearch');
		$condition = array();
		foreach ($this->_structure->getFields() as $iAF=>$field) {
			if (!empty($field['usedIn']['search'])&&isset($filterValues[$iAF])) {
				switch ($field['typeSearch']) {
					case 'date_calendar':
					case 'date_calendar_interval':
					case 'date_calendar_interval_datepicker':
						$start = 0;
						$end = 0;
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_start'])) $start = $filterValues[$iAF][$field['nameEn'] . '_start'];
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_end'])) $end = $filterValues[$iAF][$field['nameEn'] . '_end'];
						if (!empty($start)||!empty($end)) {
							if (empty($start)) $condition[$iAF] = '(t.' . $field['nameEn'] . ' <= ' . $end . ')';
							elseif (empty($end)) $condition[$iAF] = '(t.' . $field['nameEn'] . ' >= ' . $start . ')';
							else $condition[$iAF] = '(t.' . $field['nameEn'] . ' between ' . $start . ' and ' . $end . ')';
						}
						break;
					case 'string': case 'text': case 'textarea':
						if (!empty($field['relation'])){
							$condition[$iAF] = '('.$field['relation']['alias'].'.' . $field['relation']['nameEn'] . ' like ' . Text::get()->mest('%' . $filterValues[$iAF] . '%') . ')';
						} elseif (!empty($filterValues[$iAF]))
							$condition[$iAF] = '(t.' . $field['nameEn'] . ' like ' . Text::get()->mest('%' . $filterValues[$iAF] . '%') . ')';
						break;
					case 'multiSelect':
						if (!empty($filterValues[$iAF])){
							$condition[$iAF] = array();
							if ($field['dataType']=='integer'){
								$condition[$iAF] = '(t.'.$field['nameEn'].' in ('. implode(', ', $condition[$iAF]) . '))';
							} else {
								foreach($filterValues[$iAF] as $val) {
									$condition[$iAF][] .= '(t.' . $field['nameEn'] . ' like ' . Text::get()->mest('%' . $val . '%') . ')';
								}
								$condition[$iAF] = '(' . implode(' or ', $condition[$iAF]) . ')';
							}
						}
						break;
					case 'select':
					case 'select_search':
						if (($field['type'] == 'checkbox' && !in_array($filterValues[$iAF],array(0,1))) || ($field['type'] != 'checkbox' && empty($filterValues[$iAF]))) break;
						if(in_array($field['type'],array('string','text','textarea'))) $condition[$iAF] = '(t.' . $field['nameEn'] . ' like ' . Text::get()->mest('%' . $filterValues[$iAF][$field['nameEn']] . '%') . ')';
						else $condition[$iAF] = '(t.' . $field['nameEn'] . ' = ' . (int)$filterValues[$iAF] . ')';
						break;
					default:
						$condition[$iAF] = '(t.' . $field['nameEn'] . ' = ' . (int)$filterValues[$iAF] . ')';
						break;
				}
			}
		}
		return $condition;
	}

	function getJoin() {
		$join = array();
		foreach($this->_structure->getFields() as $field){
			if (!empty($field['relation'])) {
				$join[] = $field['relation']['joinType'].' '.$field['relation']['table'].' as '.$field['relation']['alias'].' '.
					'on '.$field['relation']['alias'].'.'.$field['relation']['id'].' = t.'.$field['relation']['id'];
			}
		}
		return $join;
	}

	function getNames() {
		if (empty($this->_data) || !is_array($this->_data)) return array();
		$filterValues = $this->_structure->getValues($this->_data, 'typeSearch');
		$result = array();
		foreach ($this->_structure->getFields() as $iAF=>$field) {
//			if (!empty($field['usedIn']['search'])&&isset($filterValues[$iAF])) {
			if (isset($filterValues[$iAF])&&!empty($field['typeSearch'])) {
				switch ($field['typeSearch']) {
					case 'select':
						if (!empty($field['idSpr'])) {
							$items = $field['idSpr']->getItems();
							if (isset($items[$filterValues[$iAF]])) $result[$field['name']] = $items[$filterValues[$iAF]];
						}
						if (empty($result[$field['name']])) $result[$field['name']] = '';
						break;
					case 'date_calendar':
					case 'date_calendar_interval':
					case 'date_interval':
						$date = array();
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_start']) && !empty($filterValues[$iAF][$field['nameEn'] . '_start']))
							$date[] =  'с ' . date('d.m.Y', $filterValues[$iAF][$field['nameEn'] . '_start']);
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_end']) && !empty($filterValues[$iAF][$field['nameEn'] . '_end']))
							$date[] = 'по ' . date('d.m.Y', $filterValues[$iAF][$field['nameEn'] . '_end']);
						if (!empty($date))
							$result[$field['name']] = implode(' ', $date);
						break;
					case 'multiSelect': $result[$field['name']] = implode(', ',$filterValues[$iAF]); break;
					case 'price_interval':
						$start = 0;
						$end = 0;
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_start'])) $start = $filterValues[$iAF][$field['nameEn'] . '_start'];
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_end'])) $end = $filterValues[$iAF][$field['nameEn'] . '_end'];
						if (!empty($start)||!empty($end)) {
							$result[$field['name']] = '';
							if (!empty($start)) $result[$field['name']] .= 'c ' . Price::get()->textPrice($start, $filterValues[$iAF]['id_money']) . ' ';
							if (!empty($end)) $result[$field['name']] .= 'до ' . Price::get()->textPrice($end, $filterValues[$iAF]['id_money']);
							$result[$field['name']] = trim($result[$field['name']]);
//							if (isset($items[$filterValues[$iAF]['id_money']])) $result[$field['name']] .= ' ' . Price__getNameMoney($filterValues[$iAF]['id_money']);
						}
						break;
					case 'numeric_interval':
						$start = 0;
						$end = 0;
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_start'])) $start = $filterValues[$iAF][$field['nameEn'] . '_start'];
						if (is_numeric($filterValues[$iAF][$field['nameEn'] . '_end'])) $end = $filterValues[$iAF][$field['nameEn'] . '_end'];
						if (!empty($start)||!empty($end)) {
							$result[$field['name']] = '';
							if (!empty($start)) $result[$field['name']] .= 'c ' . $start . ' ';
							if (!empty($end)) $result[$field['name']] .= 'до ' . $end;
							$result[$field['name']] = trim($result[$field['name']]);
						}
						break;
					case 'checkbox':
						if (!empty($filterValues[$iAF])) $result[$field['name']] = 'да';
						break;
					default: $result[$field['name']] = $filterValues[$iAF]; break;
				}
			}
		}

		return $result;
	}


}