<?php
/*Created by Edik (29.05.2015 10:33)*/
class ModelsStructureFilterListBuilder{

	function __construct(ModelsStructure $structure){
		$this->_structure = $structure;
	}

	protected function _getTable(){
		return $this->_structure->getTable();
	}

	protected function _getId(){
		return $this->_structure->getId();
	}

	protected function _getChildTable(){
		return $this->_structure->getChildTables() ? $this->_structure->getChildTables()[0] : '';
	}

	function getConditionJoin($filterFields, $values){
		//условия для этих полей - отдельно
		$uniqueFields = ['id_user_type' => null, 'foto_exists' => null, 'good_exists' => null, 'service_exists' => null, 'finish_work_exists' => null];
		list($condition, $join) = $this->_getConditionJoinUnique(array_intersect_key($filterFields, $uniqueFields), $values);

		$filterFields = array_diff_key($filterFields, $uniqueFields);

		$childTable = $this->_getChildTable();

		foreach ($filterFields as $idAF => $field) {
			if (!empty($field['showList']['simpleSearch'])) {
				$alias = 't' . (empty($field['isRootField']) ? $childTable : '');
				switch ($field['typeSearch']) {
					case 'price_interval':
						$cond = $this->conditionUnique($field, $values[$idAF], $alias, 'price_interval');
						if (!empty($cond)) $condition[$field['nameEn']] = $cond;
						break;
					case 'date_interval':
					case 'date_calendar_interval':
					case 'date_calendar_interval_datepicker':
						if (($field['dataType'] == 'date')){
							$cond = $this->conditionUnique($field, $values[$idAF], $alias, 'date_interval');
						} else {
							$cond = $this->conditionUnique($field, $values[$idAF], $alias, 'interval');
						}
						if (!empty($cond)) $condition[$field['nameEn']] = $cond;
						break;
					case 'numeric_interval':
						$cond = $this->conditionUnique($field, $values[$idAF], $alias, 'interval');
						if (!empty($cond)) $condition[$field['nameEn']] = $cond;
						break;
					case 'aggregator_left':
					case 'aggregator_center':
					case 'aggregator_right':
						switch ($field['dataType']) {
							case 'serialize':
								$cond = $this->conditionUnique($field, $values[$idAF]['value'], $alias, 'spr');
								if (!empty($cond)) $condition[$field['nameEn']] = $cond;
								break;
							case 'integer':
							case 'tinyint':
								$cond = $this->conditionUnique($field, $values[$idAF]['value'], $alias, 'integer');
								if (!empty($cond)) $condition[$field['nameEn']] = $cond;
								break;
						}
						break;
					case 'select':
						if (!empty($values[$idAF]) || isset($values[$idAF]) && (string)$values[$idAF] === '0')
							$condition[$field['nameEn']] = '('.$alias.'.'.$field['nameEn'].' = '.Text::get()->mest($values[$idAF]).')';
						break;
					case 'select_child':
						$cond = $this->conditionUnique($field, $values[$idAF], $alias, 'sprNested');
						if (!empty($cond)) $condition[$field['nameEn']] = $cond;
						break;
					case 'checkbox':
						if (!empty($values[$idAF]))
							$condition[$field['nameEn']] = '('.$alias.'.'.$field['nameEn'].' = '.Text::get()->mest($values[$idAF]).')';
						break;
					default:
						switch ($field['dataType']) {
							case 'serialize': break;
							case 'integer':
								$cond = $this->conditionUnique($field, $values[$idAF], $alias, 'integer');
								if (!empty($cond)) $condition[$field['nameEn']] = $cond;
								break;
							default:
								if (!empty($values[$idAF]))
									$condition[$field['nameEn']] = '('.$alias.'.'.$field['nameEn'].' = '.Text::get()->mest($values[$idAF]).')';
								break;
						}
						break;
				}

				if (empty($field['isRootField']) && isset($condition[$field['nameEn']])) {
					//если поле не idRoot, то делаем join к таблице
					if (empty($join[$alias]))
						$join[$alias] = 'inner join ' . $this->_getTable() . '_' . $childTable . ' ' . $alias . ' on (' . $alias . '.' . $this->_getId() . ' = t.' . $this->_getId() . ')';
					$join[$alias] .= ' and ' . $condition[$field['nameEn']];
					unset($condition[$field['nameEn']]);
				}
			}
		}
		return [$condition, $join];
	}

	/**Функция возвращает condition\join для полей, созданных вручную в структуре, таких как foto_exist и пр.
	 * @param $fields
	 * @param $values
	 * @return array
	 */
	protected function _getConditionJoinUnique($fields, $values){
		$condition = $join = [];
		foreach ($fields as $idAF => $field) {
			if (empty($values[$idAF])) continue;
			switch ($idAF) {
				case 'foto_exists':
					if (in_array($this->_getTable(), ['firms', 'brigades', 'expositions'])) $condition['id_logo'] = '(t.id_logo > 0)';
					else $condition['id_foto'] = '(t.id_foto > 0)';
					break;
				case 'id_user_type':
					require_once(FUNCTIONS_PATH . 'UserTypes.php');
					//Строим условие с условием дочерних id_user_type
					$ids = [];
					foreach ($values[$idAF] as $idUserType) $ids = array_merge($ids, array_keys(UserTypes__getChildren($idUserType)));

					$join['tU'] = 'inner join users tU on (t.id_user = tU.id_user)';
					$condition['id_user_type'] = '(tU.id_user_type in (' . implode(', ', $ids) . '))';
					unset($ids);
					break;
				case 'good_exists':
				case 'service_exists':
				case 'finish_work_exists':
					//доделать
					break;
			}
		}
		return [$condition, $join];
	}

	/*подключает файл в папке condition и выполняет функцию того файла*/
	function conditionUnique($field, $values, $alias, $conditionName){
		require_once(CLASSES_PATH . 'Models/Structure/Condition/' . Text::get()->strToUpperFirst($conditionName) . '.php');
		return call_user_func('ModelsStructureCondition__' . $conditionName, $field, $values, $alias);
	}


	/**Возвращает массив где ключи - это nameEn, а значение - это строка для фильтра(по-русски)
	 * @param ModelsStructure $structure
	 * @param $formValues
	 * @return array
	 */
	function getFilterStrings(ModelsStructure $structure, array $formValues){
		$filterFields = $structure->getFields();
		$values = $structure->getValues($formValues, 'typeSearch');

		$result = [];

		foreach ($filterFields as $iAF=>$field) {
			if (!empty($field['showList']['simpleSearch']) && isset($values[$iAF])) {
				switch ($field['typeSearch']) {
					case 'price_interval':
						if (empty($values[$iAF])) break;

						$start = 0;
						$end = 0;
						if (is_numeric($values[$iAF][$field['nameEn'] . '_start'])) $start = $values[$iAF][$field['nameEn'] . '_start'];
						if (is_numeric($values[$iAF][$field['nameEn'] . '_end'])) $end = $values[$iAF][$field['nameEn'] . '_end'];
						if ($start || $end) {
							$result[$field['nameEn']] = '';
							if ($start) $result[$field['nameEn']] .= 'c ' . Price::get()->textPrice($start, !$end ? $values[$iAF]['id_money'] : false) . ' ';
							if ($end) $result[$field['nameEn']] .= 'до ' . Price::get()->textPrice($end, $values[$iAF]['id_money']);
							$result[$field['nameEn']] = $field['name'] . ': ' . trim($result[$field['nameEn']]);
						}
						unset($start, $end);
						break;
					case 'numeric_interval':
						if (empty($values[$iAF])) break;

						$start = 0;
						$end = 0;
						if (is_numeric($values[$iAF][$field['nameEn'] . '_start'])) $start = $values[$iAF][$field['nameEn'] . '_start'];
						if (is_numeric($values[$iAF][$field['nameEn'] . '_end'])) $end = $values[$iAF][$field['nameEn'] . '_end'];
						if ($start || $end) {
							$result[$field['nameEn']] = '';
							if ($start) $result[$field['nameEn']] .= 'c ' . $start . ' ';
							if ($end) $result[$field['nameEn']] .= 'до ' . $end;
							$result[$field['nameEn']] = $field['name'] . ': ' . trim($result[$field['nameEn']]);
						}
						unset($start, $end);
						break;
					case 'date_interval':
					case 'date_calendar':
					case 'date_calendar_interval':
					case 'date_calendar_interval_datepicker':
						if (empty($values[$iAF])) break;

						$date = [];
						if (is_numeric($values[$iAF][$field['nameEn'] . '_start']) && !empty($values[$iAF][$field['nameEn'] . '_start']))
							$date[] =  'с ' . date('d.m.Y', $values[$iAF][$field['nameEn'] . '_start']);
						if (is_numeric($values[$iAF][$field['nameEn'] . '_end']) && !empty($values[$iAF][$field['nameEn'] . '_end']) && $values[$iAF][$field['nameEn'] . '_start'] < $values[$iAF][$field['nameEn'] . '_end'])
							$date[] = 'по ' . date('d.m.Y', $values[$iAF][$field['nameEn'] . '_end']);

						if ($date) $result[$field['nameEn']] = $field['name'] . ': ' . implode(' ', $date);
						unset($date);
						break;
					case 'checkbox':
						if (empty($values[$iAF])) break;

						$result[$field['nameEn']] = $field['name'];
						break;
					case 'checkbox_row':
						if (empty($values[$iAF])) break;

						$items = $field['idSpr']->getItems();
						$selectedValues = array_intersect_key($items, array_flip($values[$iAF]));
						if ($selectedValues) {
							$result[$field['nameEn']] = $field['name'] . ': ' . implode(', ', $selectedValues);
						}
						unset($selectedValues, $items);
						break;
					case 'aggregator_left':
					case 'aggregator_center':
					case 'aggregator_right':
						if (empty($values[$iAF])) break;

						$filterNames = SitesAggregator::get()->getFilterNames([$iAF => $field], $values);
						if ($filterNames) $result[$field['nameEn']] = $field['name'] . ': ' . $filterNames[$field['nameEn']];
						unset($filterNames);
						break;
					case 'select':
						if (!empty($field['idSpr'])) {
							if ($field['dataType'] == 'integer' && $values[$iAF] === '') break;

							$items = $field['idSpr']->getItems();
							if (isset($items[$values[$iAF]])) $result[$field['nameEn']] = $field['name'] . ': ' . $items[$values[$iAF]];
							unset($items);
						}
						break;
					case 'select_child':
						if (empty($values[$iAF])) break;

						if (!empty($field['idSpr'])) {
							switch ($field['idSpr']->getStructure()->getTable()) {
								case 'citys':
									$items = SitesCitys::get()->getCity($values[$iAF]);
									if (!empty($items['name'])) $result[$field['nameEn']] = $field['name'] . ': ' . $items['name'];
									break;
								case 'category_sets':
									$items = $field['idSpr']->getItems(['where' => ['id_category' => 't.id_category = ' . (int)$values[$iAF]]]);
									$items = array_shift($items);
									if (!empty($items['name'])) $result[$field['nameEn']] = $field['name'] . ': ' . $items['name'];
									break;
								default:
									$items = $field['idSpr']->getItems();
									if (!empty($items[$values[$iAF]])) $result[$field['nameEn']] = $field['name'] . ': ' . $items[$values[$iAF]];
									break;
							}
							unset($items);
						}
						break;
					default:
						if (empty($values[$iAF])) break;

						$result[$field['nameEn']] = $field['name'] . ': ' . htmlspecialchars($values[$iAF]);
						break;
				}
			}
		}

		$tag = SitesAggregator::get()->getActiveTagName();
		if (!$tag && !empty($formValues['tag'])) {
			$tag = SitesAggregator::get()->getTagName(is_array($formValues['tag']) ? current($formValues['tag']) : $formValues['tag']);
		}

		if ($tag) $result['tag'] = 'Тэг: ' . $tag;

		if (!empty($formValues['foto_exists']))
			$result['foto_exists'] = 'Только с фото';

		if (!empty($formValues['search'])) $result['search'] = 'Ключевые слова: ' . htmlspecialchars($formValues['search']);

		return $result;
	}
}