<?php
class ModelsStructureValues {
	protected $_structure, $_values, $_type;

	function __construct(ModelsStructure $structure, array $values, $type) {
		$this->_structure = $structure;
		$this->_values = $values;
		$this->_type = $type;
	}

	function get() {
		$resultValues = [];
		foreach ($this->_structure->getFields() as $iAF=>$field) {
			if (!empty($field['dataType']) && ($field['dataType'] == 'serialize' || $field['dataType'] == 'multiFields')) {
				$resultValues[$iAF] = $this->_getSerializeValues($field);
			} elseif (!empty($field[$this->_type])) {
				switch ($field[$this->_type]) {
					case 'price':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'price');
						break;
					case 'select_other':
					case 'radio_row_other':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'select_other');
						break;
					case 'price_interval':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'price_interval');
						break;
					case 'checkbox':
						$resultValues[$iAF] = $this->_getValue($field['nameEn']) ? 1 : 0;
						break;
					case 'select_category':
					case 'select':
					case 'select_child':
					case 'captcha':
						$resultValues[$iAF] = $this->_getValue($field['nameEn']);
						break;
					case 'checkbox_row':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'array');
						break;
					case 'numeric_interval':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'numeric_interval');
						break;
					case 'date_interval':
					case 'date_calendar_interval':
					case 'date_calendar_interval_datepicker':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'date_interval');
						break;
					case 'aggregator_left':
					case 'aggregator_right':
					case 'aggregator_center':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'aggregator');
						break;
					case 'multi_string':
						$resultValues[$iAF] = $this->_getSimpleValues($field, 'multi_string');
						break;
					case 'select_search_parent':
						$resultValues[$iAF] = array(
							'id' => $this->_getValue($this->_structure->getId()),
							'id_parent' => $this->_getValue($field['nameEn']),
						);
						break;
					default:
						$resultValues[$iAF] = $this->_getValue($field['nameEn']);
						if (!empty($resultValues[$iAF])) {
							switch ($field['dataType']) {
								case 'double':
									$resultValues[$iAF] = (float) str_replace([',', ' '], ['.', ''], $resultValues[$iAF]);
									break;
								case 'integer':
								case 'tinyint':
									$resultValues[$iAF] = (int) str_replace(' ', '', $resultValues[$iAF]);
									break;
								case 'date':
									if (!is_numeric($resultValues[$iAF])) {
										/*1.12.2012*/
										if (preg_match('/^(0?[1-9]|[12]\d|3[01])[\- \/.](0?[1-9]|1[012])[\- \/.]([12]\d{3})$/', $resultValues[$iAF], $m)) $resultValues[$iAF] = mktime(0, 0, 0, $m[2], $m[1], $m[3]);
										else $resultValues[$iAF] = 'Не правильный формат даты';
									}
								break;
								case 'datetime':
									if (!is_numeric($resultValues[$iAF])) {
										/*1.12.2012 20:54*/
										if (preg_match('/^(0?[1-9]|[12]\d|3[01])[\- \/.](0?[1-9]|1[012])[\- \/.]([12]\d{3}) +(0?[1-9]|[1]\d|[2][0-4]):(0?\d|[1-5]\d)$/', $resultValues[$iAF], $m)) $resultValues[$iAF] = mktime($m[4], $m[5], 0, $m[2], $m[1], $m[3]);
										else $resultValues[$iAF] = 'Не правильный формат даты';
									}
								break;
								case 'time':
									if (!is_numeric($resultValues[$iAF])) {
										/*19:43*/
										if (preg_match("/^(0?[1-9]|1\d|2[0-4]):(0?\d|[1-5]\d)$/", $resultValues[$iAF], $m)) $resultValues[$iAF] = mktime($m[1], $m[2], 0, date("m"), date("d"), date("Y"));
										else $resultValues[$iAF] = 'Не правильный формат времени';
									}
								break;
							}
						}
					break;
				}
			}
		}
		return $resultValues;
	}

	private function _getSimpleValues($field, $valueName){
		if (file_exists(CLASSES_PATH . 'Models/Structure/Values/' . Text::get()->strToUpperFirst($valueName) . '.php')){
			require_once(CLASSES_PATH . 'Models/Structure/Values/' . Text::get()->strToUpperFirst($valueName) . '.php');
			return call_user_func('ModelsStructureValues__' . $valueName, $field, $this->_values);
		}
		return null;
	}

	private function _getSerializeValues($field){
		$type = 'default';
		if (!empty($field[$this->_type]) && file_exists(CLASSES_PATH . 'Models/Structure/Values/Serialize_' . $field[$this->_type] . '.php')){
			$type = $field[$this->_type];
		}
		require_once(CLASSES_PATH . 'Models/Structure/Values/Serialize_' . $type . '.php');
		return call_user_func('ModelsStructureValues__serialize_' . $type, $field, $this->_values, $this->_type);
	}

	protected function _getValue($name, $default = null) {
		if (isset($this->_values[$name])) return $this->_values[$name];
		return $default;
	}

}