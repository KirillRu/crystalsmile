<?php
/**
 *  в конструкторе дочерних необходимо заполнить массив _structure (id, table, tableName, fields, childTables)
*/
abstract class ModelsStructure {
	protected $_structure = array();
	/**
	 * @var ModelsStructureValues
	 */
	protected $_values = null;

	function getTable() {
		if (isset($this->_structure['table'])) return $this->_structure['table'];
		return '';
	}

	function getChildTables() {
		if (isset($this->_structure['childTables'])) return $this->_structure['childTables'];
		return array();
	}

	function getId() {
		if (isset($this->_structure['id'])) return $this->_structure['id'];
		return '';
	}

	function getTableName() {
		if (isset($this->_structure['tableName'])) return $this->_structure['tableName'];
		return '';
	}

	function getCategoryName() {
		if (isset($this->_structure['categoryName'])) return $this->_structure['categoryName'];
		return '';
	}

	function getTableFotos() {
		if (isset($this->_structure['tableFotos'])) return $this->_structure['tableFotos'];
		return '';
	}

	function getFields() {
		if (isset($this->_structure['fields'])) return $this->_structure['fields'];
		return array();
	}

	function getField($nameEn) {
		foreach ($this->getFields() as $field) {
			if ($field['nameEn'] == $nameEn) return $field;
		}
		return null;
	}

	function getIAF($nameEn) {
		foreach ($this->getFields() as $iAF=>$field) {
			if ($field['nameEn'] == $nameEn) return $iAF;
		}
		return null;
	}

	function setField($field, $iAF = null) {
		if (isset($this->_structure['fields'])) $this->_structure['fields'][($iAF === null) ? $field['idAllField'] : $iAF] = $field;
	}

	function setFields($fields) {
		$this->_structure['fields'] = $fields;
	}

	function isActive() { return !empty($this->_structure['isActive']); }
	function isPosition() { return !empty($this->_structure['isPosition']); }
	function isNested() { return !empty($this->_structure['isNested']); }

	/**
	 * @param array $values - массив значений из формы или БД
	 * @return array подготовленный массив для данной структуры. Ключи массива - idAllField
	 */
	function getValues(array $values = array(), $type = 'type') {
		if ($this->_values === null) {
			require_once(CLASSES_PATH . 'Models/Structure/Values.php');
			$this->_values = (new ModelsStructureValues($this, $values, $type))->get();
		}
		return $this->_values;
	}

	protected function _parseStructureInFileArray($k, $v) {
		switch ($k) {
			case 'id':
			case 'table':
			case 'tableName':
			case 'childTables':
			case 'tableFotos':
			case 'isActive':
			case 'isPosition':
			case 'isNested':
			case 'categoryName':
				$this->_structure[$k] = $v;
				break;
		}
	}

	protected function _serializeField($field, $k) {
		if (empty($field[$k])) return array();
		return (!is_array($field[$k]) ? unserialize($field[$k]) : $field[$k]);
	}

	/**
	 * @return array массив параметров поля, убирает параметры-объекты, кроме полей указанных в массиве $showsField
	 */
	protected function _prepareField($field, $showsField = array()) {
		$result = array();
		foreach ($field as $k=>$v) {
			switch ($k) {
				case 'showList':
				case 'showForm':
				case 'showShop':
				case 'showAdminboard':
				case 'showManagement':
				case 'showAdmin':
				case 'showDetail':
				case 'usedIn':
				case 'bdParams':
					if (in_array($k, $showsField)) {
						$result[$k] = $v;
						if (!is_array($result[$k])) $result[$k] = (!empty($result[$k])) ? unserialize($result[$k]) : array();
					}
					break;
				case 'idSpr':
					if (in_array($k, $showsField)) {
						if (!is_array($field['idSpr'])) $field['idSpr'] = (!empty($field['idSpr'])) ? unserialize($field['idSpr']) : array();
						require_once(CLASSES_PATH . 'Models/DataSource/DataSource.php');
						$result[$k] = ModelsDataSource__getObject($field['idSpr']);
						if (method_exists($result[$k], 'setRunFilename'))
							$result[$k]->setRunFilename($this->_getRunFilename());
						unset($dataSource);
					}
					break;
				default: $result[$k] = $v;
			}
		}
		return $result;
	}

	final protected function _getRunFilename() {
		$runClassName = static::class;
		foreach (['Form'] as $fileName) { //заполнять массив по мере добавления файлов в From_serialize
		    if (substr($runClassName, -(strlen($fileName))) == $fileName) return $fileName;
		}
		return false;
	}


}