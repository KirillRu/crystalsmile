<?php

/** Перессчитывает ключи категорий
 * Class ModelsManagerNestedSet
 */
class ModelsManagerNestedSet {
	private $_table, $_structure = null,$_keyGenerator;
	function __construct($table = ''){
		$this->_table = $table;
	}

	function restoreKeys(){
		$this->_keyGenerator = 1;
		$structure = $this->_getStructure();

		$copyTable = $this->_tableCopy();
		$items = $this->_prepareItems($this->_getItems($copyTable));
		$this->_getDriver()->openQuery($copyTable, $structure->getId());
		foreach ($items as $item) $this->_restoreKeys($item, 1);
		$this->_tableRename($copyTable);

		$this->_getDriver()->closeQuery();
	}

	protected function _getItems($copyTable) {
		$structure = $this->_getStructure();
		return $this->_getDriver()->getItems('select ' . $structure->getId() . ', id_parent from ' . $copyTable . ' order by position');
	}

	protected function _tableRename($copyTable, $dropCopyTable = true) {
		$structure = $this->_getStructure();
/*		$sql = ''.
			'drop table if exists ' . $structure->getTable() . '; '.
			'rename ' . $copyTable . ' to ' . $structure->getTable() . '; '.
			'update ' . $structure->getTable() . ' t, ' . $copyTable . ' tCopy set '.
				't.level = tCopy.level, '.
				't.leftKey = tCopy.leftKey, '.
				't.rightKey = tCopy.rightKey, '.
			'where (t.' . $structure->getId() . ' = tCopy.' . $structure->getId() . ') '.
		'';*/
		$sql = ''.
			'SET @DISABLE_TRIGGERS := 1; '.
			'truncate table ' . $structure->getTable() . '; '.
			'insert into ' . $structure->getTable() . ' select * from ' . $copyTable . '; '.
		'';
		if ($dropCopyTable) $sql .= 'drop table if exists ' . $copyTable . '; ';
		$sql .= '@DISABLE_TRIGGERS := 0; ';
		$this->_getDriver()->multiSimpleQuery($sql);
	}

	protected function _tableCopy() {
		$structure = $this->_getStructure();
		$copyTable = '_' . $structure->getTable() . '_';
		$sql = ''.
			'drop table if exists ' . $copyTable . '; '.
			'create table ' . $copyTable . ' like ' . $structure->getTable() . '; '.
			'insert into ' . $copyTable . ' select * from ' . $structure->getTable() . '; '.
		'';
		$this->_getDriver()->multiSimpleQuery($sql);
		return $copyTable;
	}

	protected function _prepareItems($items){
		$hash = array();
		$idField = $this->_getStructure()->getId();
		for ($i = 0; $i < sizeof($items); $i++) {
			if (!isset($items[$i]["children"])) $items[$i]["children"] = array();
			$hash[$items[$i][$idField]] =& $items[$i];
		}
		$root = array();
		for ($i = 0; $i < sizeof($items); $i++) {
			if ($items[$i]['id_parent'] && isset($hash[$items[$i]['id_parent']])) {
				$items[$i]['parent'] =& $hash[$items[$i]['id_parent']];
				$hash[$items[$i]['id_parent']]["children"][] =& $items[$i];
			}
			if ($items[$i]['id_parent'] == 0) $root[] =& $items[$i];
		}
		return $root;
	}

	protected function _restoreKeys($item, $level){
		$item['leftKey'] = $this->_keyGenerator++;
		foreach ($item['children'] as $it) $this->_restoreKeys($it, $level + 1);
		$item['rightKey'] = $this->_keyGenerator++;
		$item['level'] = $level;
		$this->_getDriver()->callQuery($item['leftKey'], $item['rightKey'], $item['level'], $item[$this->_getStructure()->getId()]);
	}

	/**
	 * @return ModelsStructure
	 */
	protected function _getStructure(){
		if ($this->_structure === null){
			require_once(CLASSES_PATH.'Models/Structure/From_files/From_files.php');
			$this->_structure = new ModelsStructureFrom_files($this->_table);
		}
		return $this->_structure;
	}


	/**
	 * @return null|ModelsStructureManagerNestedSetDriver
	 */
	protected function _getDriver(){
		static $driver = null;
		if ($driver === null) $driver = ModelsStructureManagerNestedSetDriver::getDriver();
		return $driver;
	}
}

require_once(CLASSES_PATH . 'Sql/Mysqli.php');
class ModelsStructureManagerNestedSetDriver extends SqlMysqli{
	private $_query = null;

	function openQuery($table, $nameId){
		$this->_query = $this->_driver->prepare(
			'update ' . $table . ' '.
			'set leftKey=?, rightKey=?, level=? '.
			'where ' . $nameId . '=?');
	}

	function closeQuery(){
		$this->_query->close();
	}

	function callQuery($leftKey, $rightKey, $level, $id) {
		$this->_query->bind_param('iiii',$leftKey, $rightKey, $level, $id);
		$this->_query->execute();
	}

	function getItems($sql) {
		$return = array();
		$result = $this->_result_list($sql);
		if ($result) {
			while ($row = $result->fetch_assoc()) $return[] = $row;
			$result->close();
		}
		return $return;
	}

	function multiSimpleQuery($sql) {
		if ($this->_result_multi($sql)) {
			do {
				$res = $this->_driver->store_result();
				if ($res) $res->close();
			} while ($this->_driver->more_results() && $this->_driver->next_result());
		}
	}

}