<?php
/*
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com (17.09.14 11:17)
*/

class ModelsCacheMemcache {
	private static $_self = null;
	/**
	 * @var Memcache
	 */
	private $_cache = null;

	final private function __construct() {
		if (Cfg__get('memcache', 'enable')) {
			$this->_cache = new Memcache();
			$result = @$this->_cache->connect(Cfg__get('memcache', 'host'), Cfg__get('memcache', 'port'), Cfg__get('memcache', 'timeout', 1));
			if ($result === false) {
				//Подключение к memcache не удалось. Пробую переподключиться.
				$this->_cache->connect(Cfg__get('memcache', 'host'), Cfg__get('memcache', 'port'), Cfg__get('memcache', 'timeout', 1));
			}
		}
	}

	function __destruct() {
		if ($this->_cache !== null) $this->_cache->close();
		unset($this);
	}

	/** достать значение из памяти
	 * @param $key
	 *
	 * @return array|bool|string
	 */
	function read($key) {
		if ($this->_cache === null) return false;
		$key = $this->_getKey($key);
		return $this->_cache->get($key);
	}

	/** записать значение в память
	 * @param $key string ключ для значения
	 * @param $value array|string само значение
	 * @param $partition string|array раздел в который запишется значение
	 * @param int $time время хранения
	 *
	 * @return bool
	 */
	function write($key, $value, $partitions, $time = 0) {
		if ($this->_cache === null) return false;

		$key = $this->_getKey($key);

		$this->_savePartitions($key, is_array($partitions) ? $partitions : [$partitions]);

		return $this->_cache->set($key, $value, MEMCACHE_COMPRESSED, $time);
	}

	private function _savePartitions($newKey, $partitions){
		$partitionsString = '';
		foreach ($partitions as $partition) {
			$idUserPos = strrpos($partition, '-');
			if ($idUserPos !== false){
				/*Сохраняем ключ запроса в партиццию с idUser*/
				$this->_appendString($partition, $newKey);
				$partition = substr($partition, 0, $idUserPos);
			}

			/*Сохраняем ключ запроса в общую партицию(без idUser)*/
			$this->_appendString($partition, $newKey);

			$partitionsString .= ';' . $partition;
		}

		/*Сохраняем партиции*/
		$this->_appendString('allPartitions', substr($partitionsString, 1));
	}

	private function _appendString($key, $append){
		$key = $this->_getKey($key);
		if (!$this->_cache->append($key, ';' . $append, 0, 0)){
			$this->_cache->set($key, $append, 0, 0);
		}
	}

	/** удаляет одно значение из памяти по ключу
	 * @param $key
	 *
	 * @return bool
	 */
	function delete($key) {
		if ($this->_cache === null) return false;

		$key = $this->_getKey($key);
		if ($this->_cache->delete($key)) return true;
		return $this->_cache->set($key, MEMCACHE_COMPRESSED, 0, 1);
	}

	/** форматирует раздел. Удаляет из памяти все значения, ключи которых хранятся в разделах
	 * @param $partitions
	 */
	function formatting($partitions) {
		if ($this->_cache === null) return;

		$alreadyCleared = [];
		foreach ((array)$partitions as $partition) {
			$partitionKey = $this->_getKey($partition);
			$sqlKeys = $this->_cache->get($partitionKey); // получаем все ключи раздела

			if ($sqlKeys){
				//все ключи разделены знаком ';'
				while($pos = strpos($sqlKeys, ';')){
					//для каждого ключа...
					$key = substr($sqlKeys, 0, $pos);

					//если еще не чистили этот ключ - чистим
					if (!isset($alreadyCleared[$key])) {
						if (!$this->_cache->delete($key)) $this->_cache->set($key, MEMCACHE_COMPRESSED, 0, 1); // удаляем по ключу
						$alreadyCleared[$key] = true;
					}
					$sqlKeys = substr($sqlKeys, $pos + 1);
				}

				//чистим последний ключ. Верхний цикл последний ключ не чистит
				if (!isset($alreadyCleared[$sqlKeys])) {
					if (!$this->_cache->delete($sqlKeys)) $this->_cache->set($sqlKeys, MEMCACHE_COMPRESSED, 0, 1); // удаляем по ключу
					$alreadyCleared[$sqlKeys] = true;
				}

				// удаляем раздел
				if (!$this->_cache->delete($partitionKey)) $this->_cache->set($partitionKey, MEMCACHE_COMPRESSED, 0, 1);
			}
		}
	}

	/** чистит мемкэш
	 * @return bool
	 */
	function flush() {
		if ($this->_cache === null) return false;
		$this->_cache->flush();
		return true;
	}

	private function _getKey($key) { return md5(Cfg__get('db', 'database') . $key); }

	static function get() {
		if (self::$_self === null) self::$_self = new ModelsCacheMemcache();
		return self::$_self;
	}

}