<?php
/*
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com (17.09.14 15:39)
*/

class ModelsCacheFileView {
	protected static $_self = null;

	final private function __construct() {}

	function read($file, $replace = false, $lifetime = 86400) {
		if (!Cfg__get('filecache', 'enable')) return false;
		if (!file_exists($file)) return false;

		$fileTime = ModelsCacheMemcache::get()->read($file);
		if ($fileTime === false) return false;

		if (($lifetime === false)||((time() - (int) $lifetime) < (int) $fileTime)) {
			if ($replace === false) {
				require_once(CLASSES_PATH . 'View/File.php');
				return new ViewFile($file);
			}
			if (is_array($replace)) {
				require_once(CLASSES_PATH . 'View/FileReplace.php');
				return new ViewFileReplace($file, $replace);
			}
			if ($replace instanceof AbstractAction) {
				$replace->s('file', $file);
				return $replace->getAction()->run();
			}
		}
		return false;
	}

	function write($file, View &$view, $replace = [], $lifetime = 86400){
		$s = $view->fetch();

		$dir = dirname($file);
		if (!file_exists($dir)) Functions__mkdirRecursive($dir);
		if (file_put_contents($file, $s)) chmod($file, 0664);

		if (is_array($replace)) {
			require_once(CLASSES_PATH . 'View/FileReplace.php');
			$view = new ViewFileReplace($file, $replace);
		} elseif($replace instanceof AbstractAction) {
	        $replace->s('file', $file);
	        $view = $replace->getAction()->run();
	    }
		if ($lifetime !== false) ModelsCacheMemcache::get()->write($file, time(), 'ModelsCacheFileView', $lifetime);
	}

	static function get() {
		if (self::$_self === null) self::$_self = new ModelsCacheFileView();
		return self::$_self;
	}

}