<?php
/*
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com (09.09.14 9:43)
*/

class ModelsCacheFilePhp {
	protected static $_self = null;

	final private function __construct() {}

	function read($file) {
		if (!Cfg__get('filecache', 'enable')) return false;

		if (file_exists($file)) return include($file);
		return false;
	}

	function write($file, array $data){
		if (Cfg__get('filecache', 'enable')) {
			$dir = dirname($file);
			if (!file_exists($dir)||!is_dir($dir)) mkdir($dir, 0777, 1);
			file_put_contents($file, '<?php return ' . var_export($data, true) . ';');
		}
	}

	static function get() {
		if (self::$_self === null) self::$_self = new ModelsCacheFilePhp();
		return self::$_self;
	}

}