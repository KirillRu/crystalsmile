<?php
/*
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com (03.07.14 12:14)
*/
define('PROJECT_NAME', '');

interface ModelsDriverBDSimpleDriverInterface {
	function rowSelect($sql);
	function multiSelect($sql);
	function multiSelectKey($sql, $key);
	function fieldSelect($sql);
	function queryCol($sql);
	function simpleQuery($sql);
	function multiQuery($sqls);
	function real_escape_string($s);
}

interface ModelsDriverBDDriver_iuInterface {
	function insert($sql, $returnId = false);
	function update($sql);
	function multiQuery($sql);
}