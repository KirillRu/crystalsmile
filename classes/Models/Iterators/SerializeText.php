<?php
/*Created by Edik (02.09.2015 10:04)*/
/*Класс работает с переменной serializeText. Наследником данного класса следует описать функцию
	_setSerialiseText. Эта функция устанавливает переменную _serializeText. Эта переменная вида
	serialize . $this->_delimiter . serialize . $this->_delimiter ...
Итератор проходит по все serialize и возвращает unserialize этих строк.
*/
abstract class ModelsIteratorsSerializeText implements Iterator, Countable{
	const delimiter = "\n<!>\n";
	protected $_serializeText = null, $_nextSerializeString = '', $_position = -1;

	abstract protected function _setSerialiseText();

	function getSerialiseText(){
		if ($this->_serializeText === null) $this->_setSerialiseText();
		return $this->_serializeText;
	}

	function rewind() {
		$this->_setSerialiseText();
		$this->_position = -1;
		$this->next();
	}

	function current(){
		return unserialize($this->_nextSerializeString);
	}

	function key() {
		return $this->_position;
	}

	function next() {
		$next = $this->_nextItem();
		if ($next !== false) $this->_position++;
		return $next;
	}

	protected function _nextItem(){
		$this->_nextSerializeString = false;

		$pos = strpos($this->_serializeText, self::delimiter);
		if ($pos){
			$this->_nextSerializeString = substr($this->_serializeText, 0, $pos + 1);
			$this->_serializeText = substr($this->_serializeText, $pos + strlen(self::delimiter));
		}
		return $this->_nextSerializeString;
	}

	function count(){
		return $this->_position + substr_count($this->getSerialiseText(), self::delimiter) + 1;
	}

	function valid() {
		return $this->_nextSerializeString !== false;
	}
}