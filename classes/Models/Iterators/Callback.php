<?php
/*Created by Edik (14.08.2015 11:00)*/
require_once(CLASSES_PATH . 'Action/Iterator.php');
class ModelsIteratorsCallback extends ActionIterator {
	private $_callBack;

    function current(){
	    return $this->_callBack->__invoke($this->_items[$this->_position]);
    }

	function setCallback(Callable $callback) {
		$this->_callBack = $callback;
	}

	function next() {
		unset($this->_items[$this->_position]);
		parent::next();
	}
}