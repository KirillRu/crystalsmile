<?php
/*Created by Edik (14.08.2015 11:02)*/
class ModelsIteratorsMySqli implements Iterator, Countable {
	private $_position, $_maxPosition, $_mysqliResult, $_callBack = null;

	function __construct(mysqli_result $result) {
		$this->_position = 1;
		$this->_maxPosition = $result->num_rows;
		$this->_mysqliResult = $result;
		if (!$this->_maxPosition) $this->_mysqliResult->free();
	}

	function rewind() {
		$this->position = 1;
	}

	function current() {
		$result = $this->_mysqliResult->fetch_assoc();
		if ($this->_position == $this->_maxPosition) $this->_mysqliResult->free();
		if ($this->_callBack) return $this->_callBack->__invoke($result);
		return $result;
	}

	function setCallback($function) {
		$this->_callBack = $function;
	}

	function key() {
		return $this->_position;
	}

	function next() {
		$this->_position++;
	}

	function valid() {
		return ($this->_position <= $this->_maxPosition);
	}

	function count(){
		return $this->_maxPosition;
	}
}