<?php
/*
 * Created by ИП Ищейкин В.А.
 * User: Рухлядев Кирилл Витальевич kirill.ruh@gmail.com (02.07.14 17:50)
*/

class ModelsCacheReplace extends AbstractAction {

	function run() {
		$file = $this->g('file');
		if ((!$file)||!file_exists($file)) return new View();

		require_once(CLASSES_PATH . 'View/FileReplace.php');
		return new ViewFileReplace($file, $this->_getReplace());
	}

	protected function _getReplace() {
		return $this->g('replace', array());
	}

}
 