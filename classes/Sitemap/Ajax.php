<?php
class SitemapAjax extends AbstractAction {
	function getAction() { return $this->_getHandler($this->g('path')); }
	function run() { return new View(); }

	/**
	 * @param $path
	 * @return View
	 * @throws ExceptionAction
	 */
	function _getHandler($path) {
		$data = array_merge($_GET, $_POST);
		$sitesPath = Path::get();
		switch (true) {
			case ($path == 'kcaptcha.ajax'):
				$classFile = CLASSES_PATH . 'Kcaptcha/Content.php';
				$className = 'KcaptchaContent';
				break;
			case preg_match($sitesPath->preparePattern('form/@s.ajax'), $path, $m):
				$module = Text::get()->strToUpperFirst($m[1]);
				$classFile = CLASSES_PATH . 'Form/Action/' . $module . '.php';
				$className = 'FormAction' . $module;
				break;
			case preg_match($sitesPath->preparePattern('runCron/@s.ajax'), $path, $m):
				$argv = [1 => PROJECT_NAME];
				require_once(ROOT_DIR . '_cron/' . $m[1] . '/' . $m[1] . '.php');
				exit;
				break;
			case preg_match($sitesPath->preparePattern('runCron/@s/@s.ajax'), $path, $m):
				$argv = [1 => PROJECT_NAME];
				require_once(ROOT_DIR . '_cron/' . $m[1] . '/' . $m[2] . '.php');
				exit;
				break;
		}
		if ($data) Data::get()->s($data);
		if (!empty($className) && !empty($classFile)){
			require_once($classFile);
			return new $className;
		}
		return $this->_notFound();
	}

	/**
	 * @throws Exception
	 * @return null
	 */
	protected function _notFound(){
		require_once(CLASSES_PATH . 'Exceptions.php');
		throw Exceptions::get()->getExcceptionAction('Sites.Action.Exceptions.NotFoundPage', 404);
	}
}