<?php
class Sitemap extends AbstractAction {
	function getAction() { return $this->_getHandler($this->g('path')); }
	function run() { return new View(); }

	/**
	 * @param $path
	 * @return View
	 * @throws ExceptionAction
	 */
	protected function _getHandler($path) {
		$data = array_merge($_POST, $_GET);
		$sitesPath = Path::get();
		switch (true) {
			case ($path == 'redirect/'):
				Header::get()->redirectSimple($data['url'])->siteExit();
				return new View();
				break;
			case ($path == 'kcaptcha/'):
				$classFile = CLASSES_PATH . 'Kcaptcha/Content.php';
				$className = 'KcaptchaContent';
				break;
		}
		if (!empty($classFile) && !empty($className) && file_exists($classFile)){
			Data::get()->s($data);
			require_once($classFile);
			return (new $className)->getAction();
		}
		return $this->_notFound();
	}

	/**
	 * @throws Exception
	 * @return null
	 */
	protected function _notFound(){
		require_once(CLASSES_PATH . 'Exceptions.php');
		throw Exceptions::get()->getExcceptionAction('Action.Exceptions.NotFoundPage', 404);
	}
}