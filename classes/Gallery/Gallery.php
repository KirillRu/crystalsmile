<?php
/*Created by Edik (15.08.14 11:41)*/
require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
require_once(FUNCTIONS_PATH . 'Gallery.php');
class Gallery extends AbstractAction{

    function run(){
	    require_once(CLASSES_PATH . 'Gallery/View/Gallery.php');
		$view = new GalleryView();
	    $view->assign('items', $this->_getItems());
	    $view->assign('fotoParams', Gallery__getFotoParams(Functions__getFromStructure('tableFotos', $this->g('table'))));
	    $view->assign('tableFotos', Functions__getFromStructure('tableFotos', $this->g('table')));
	    $view->assign('id', $this->g('id'));
	    $view->setTpl(dirname(__FILE__) . '/View/gallery.tpl');
	    return $view;
    }

	protected function _getItems() {
		$tableFotos = Functions__getFromStructure('tableFotos', $this->g('table'));
		$id = Functions__getFromStructure('id', $this->g('table'));
		$idTableFotos = Functions__getFromStructure('id', $tableFotos);
		return ModelsDriverBDSimpleDriver::getDriver()->multiSelect(
			'select ' . $idTableFotos . ' as id '.
			'from ' .$tableFotos . ' '.
			'where ' . $id . ' = ' . $this->g('id')
		);
	}
}