<?php
/*Created by Edik (30.09.2015 11:31)*/
class GalleryView extends ViewObjects{

	protected function _getPathFileFoto($idFoto, $label, $tableFoto, $name = '', $checkExists = false){
		if (!is_numeric($idFoto)) return $idFoto;
		return Fotos__getPathFileFoto($idFoto, $label, $tableFoto, $name, $checkExists);
	}
}