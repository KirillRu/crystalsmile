<?php /**@var $this ViewObjects*/
$items = $this->get('items');
$count = count($items);
$fotoParams = $this->get('fotoParams');
if ($count > 1):
	$tableFotos = $this->get('tableFotos');
	?>
	<div class="js_galleryContainer gallery-container" data-max-height="<?=$fotoParams['maxHeight']?>" data-max-width="<?=$fotoParams['maxWidth']?>"  data-min-size-name="<?=$fotoParams['minSizeName']?>" data-max-size-name="<?=$fotoParams['maxSizeName']?>">
		<?php foreach ($items as $item): ?>
			<div class="img-small js_galleryShow">
				<a href="<?= $this->_getPathFileFoto($item['id'], $fotoParams['maxSizeName'], $tableFotos);?>">
					<img class="js_galleryFoto" src="<?= $this->_getPathFileFoto($item['id'], $fotoParams['minSizeName'], $tableFotos); ?>" alt="<?= '' ?>" title="<?= '' ?>">
				</a>
			</div>
		<?php endforeach; ?>
	</div>
	<link rel="stylesheet" type="text/css" href="<?=SITE_ROOT?>css/modules/gallery.css"/>
	<script type="text/javascript">
		scriptLoader('modules/gallery.js').load();
	</script>
<?php else: ?>
	<div class="detail-img-small" id="img_foto_large" style="">
		<img src="<?= SITE_ROOT ?>img/no-foto-large.gif" width="50" height="40" alt="" title="">
	</div>
<?php endif; ?>