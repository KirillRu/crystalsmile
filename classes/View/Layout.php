<?php
require_once(CLASSES_PATH . 'View/View.php');
class ViewLayout extends View {
	
	private $_widgets = array();

	function __construct($widgets = array()) {
		$this->_widgets = $widgets;
	}
	
	function addWidget($widget) {
		$this->_widgets[] = $widget;
	}
	
	function display() {
		foreach($this->_widgets as $w) $w->display();
	}
}