<?php
class ViewErrors extends View{
	private $_errors, $_class;

	function __construct($errors, $class = 'error-message') {
		$this->_errors = $errors;
		$this->_class = $class;
	}

	function display() {
		if ($this->_errors) include dirname(__FILE__) . '/errors_content.tpl';
	}

	protected function _displayErrorsArray(array $errors){
		include dirname(__FILE__) . '/errors.tpl';
	}
}

