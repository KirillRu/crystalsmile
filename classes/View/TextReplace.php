<?php
require_once(CLASSES_PATH . 'View/View.php');
class ViewTextReplace extends View {
	private $_text = null, $_replace = array();

	function __construct($text, array $replace){
		$this->_text = $text;
		$replace['{region_url}'] = Path::get()->getRegionUrl();
		$this->_replace = $replace;
	}

	function display() {
		$s = $this->_text;
		$s = str_replace(array_keys($this->_replace), array_values($this->_replace), $s);
		$s = preg_replace('/{[^\}]+}/u', '', $s);
		echo $s;
	}
}