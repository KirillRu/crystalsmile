<?php
require_once(CLASSES_PATH . "View/Pager/LinkGenerator.php");
class TemplateLinkGenerator extends LinkGenerator {
	
	var $template;
	var $firstPageTemplate;
	
	function TemplateLinkGenerator($template, $firstPageTemplate = NULL) {
		$this->template = $template;
		if ($firstPageTemplate)
			$this->firstPageTemplate = $firstPageTemplate;
		else
			$this->firstPageTemplate = $this->template;
	}
	
	function generate($page) {
		if ($page == 1)
			return self::EscapeLink(sprintf($this->firstPageTemplate, $page));
		return self::EscapeLink(sprintf($this->template, $page));
	}
}
?>