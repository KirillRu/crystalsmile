<?php if ($this->get('pages', 0) > 1): ?>
	<div class="pager">
		<?php if ($this->get('page', 0) > 1): ?>
			<a <?= $this->_getAttributesString($this->get('page', 0) - 1) ?>>&laquo; предыдущая</a>
		<?php endif; ?>
		<?php if ($this->get('start', 0) > 1) : ?>
			<a <?= $this->_getAttributesString(1)?>>1</a>
			<a <?= $this->_getAttributesString($this->get('start', 0) - 1) ?>>...</a>
		<?php endif; ?>
		<?php for ($i = $this->get('start', 0); $i <= $this->get('finish', 0); $i++):
			if ($i == $this->get('page', 0)) : ?><span><?= $i ?></span>
			<?php else : ?>
				<a <?= $this->_getAttributesString($i) ?>><?= $i ?></a>
			<?php endif;
		endfor; ?>
		<?php if ($this->get('finish', 0) < $this->get('pages', 0)) : ?>
			<a <?= $this->_getAttributesString($this->get('finish', 0) + 1); ?>>...</a>
			<a <?= $this->_getAttributesString($this->get('pages', 0)); ?>><?= $this->get('pages', 0); ?></a>
		<?php endif; ?>
		<?php if ($this->get('page', 0) < $this->get('pages', 0)): ?>
			<a <?= $this->_getAttributesString($this->get('page', 0) + 1) ?>>следующая &raquo;</a>
		<?php endif; ?>
	</div>
<?php endif; ?>