<?php
require_once(CLASSES_PATH . "View/Pager/LinkGenerator.php");
class SimpleLinkGenerator extends LinkGenerator {
	
	var $base;
	
	function SimpleLinkGenerator($base) {
		$this->base = $base;
	}
	
	function generate($page) {
		return $this->base . "page-" . $page . "/";
	}
}
?>