<?php
require_once(CLASSES_PATH . "View/Pager/SimpleLinkGenerator.php");
class SearchGenerator extends SimpleLinkGenerator {
	function SearchGenerator($base, $searchLine) {
		parent::SimpleLinkGenerator($base);
		$this->searchLine = $searchLine;
	}
	
	function generate($page) {
		return parent::generate($page) . "?" . $this->searchLine;
	}
}
?>