<?php
/*Created by Edik (19.09.14 14:32)*/
class ViewPager extends ViewObjects{
	protected $_vars = array(
		'page' => 1,
	    'amount' => 0,
	    'countToPage' => 50,
//	    'url' => ''
//      'ajaxUrl' => '',
//		'block' => '',
//		'idForm'=>'',
//      'scroll' => ''
//		'onclick' => ''
	);
	protected $_url, $_onclick;

	function __construct() {
		$this->assign('pagesOnRow', Cfg__get('pager', 'pages_on_row', 15));
		$this->setTpl(CLASSES_PATH . 'View/Pager/pager.tpl');
	}

	protected function _getAttributesString($page){
		$attributesString = '';
		if ($this->_url) $attributesString .= str_replace('{page}', ($page > 1) ? ('page=' . $page) : '', $this->_url);
		if ($this->_onclick) $attributesString .= ($attributesString ? ' ' : '') . str_replace('{page}', $page, $this->_onclick);
		return $attributesString;
	}

	protected function _prepare() {
		if (!empty($this->_vars['url'])){
			$this->_vars['url'] = $this->_prepareUrl($this->_vars['url']) . '{page}';
			$this->_url = 'href="' . $this->_vars['url'] . '"';
		}
		if (empty($this->_vars['onclick']) && !empty($this->_vars['ajaxUrl'])){
			$this->_vars['ajaxUrl'] = $this->_prepareUrl($this->_vars['ajaxUrl']) . 'page={page}';

			$url = htmlspecialchars($this->_vars['ajaxUrl']);
			$idForm = !empty($this->_vars['idForm']) ? $this->_vars['idForm'] : '';
			$scroll = (int)!empty($this->_vars['scroll']);
			$block = $this->_vars['block'];

			$this->_vars['onclick'] = "ajax.pagerUpdate({
				url: '$url',
				idForm: '$idForm',
				scroll: $scroll,
				block: '$block'
			}); return false;";
		}
		if (!empty($this->_vars['onclick'])) $this->_onclick .= ' onclick="' . str_replace(array("\n","\r","\t"), '', $this->_vars['onclick']).'"';

		$this->_vars['pages'] = ceil($this->get('amount', 1)/max(1, $this->get('countToPage', $this->get('amount', 1))));

		$pOR2 = ceil($this->get('pagesOnRow', 0)/2);
		if ($this->get('page', 0) < $pOR2) $this->_vars['start'] = 1;
		else $this->_vars['start'] = $this->get('page', 0) - $pOR2;

		if ($this->get('page', 0) > $this->_vars['pages']-$pOR2) $this->_vars['finish'] = $this->_vars['pages'];
		else $this->_vars['finish'] = $this->get('page', 0)+$pOR2;

		if ($this->_vars['start'] == 1) $this->_vars['finish'] = $this->get('pagesOnRow', 0);
		if ($this->_vars['finish'] == $this->_vars['pages']) $this->_vars['start'] = $this->_vars['finish']-$this->get('pagesOnRow', 0);
		if ($this->_vars['start'] < 1) $this->_vars['start'] = 1;
		if ($this->_vars['finish'] > $this->_vars['pages']) $this->_vars['finish'] = $this->_vars['pages'];
	}

	protected function _prepareUrl($url){
		if (strpos($url, '?') === false) $url .= '?';
		elseif(substr($url, -1) != '&') $url .= '&';
		return $url;
	}

	function display() {
		$this->_prepare();
		parent::display();
	}
}