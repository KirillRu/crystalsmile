<?php
class ViewPagerAjaxLinkGenerator extends LinkGenerator {

	var $elementId;
	var $formId;
	var $top = false;

	function ViewPagerAjaxLinkGenerator($template, $elementId = null, $formId = null) {
		$this->template = $template;
		$this->elementId = $elementId;
		$this->formId = $formId;
	}

	function generate($page) {
		$link = self::EscapeLink(sprintf($this->template, $page));
		if($this->top === false) $scroll = '';
		//elseif (is_string($this->top)) $scroll = "ajax_scrollJump('" . $this->top . "'); ";
		//else $scroll = "ajax_scrollJump('_top'); ";
		else $scroll="ajax_scrollJump('_top'); ";
		return 'onclick="formAjaxRequest(\'' . $link . '\', '.(is_null($this->formId)?'null':'ajax_getObject(\''.$this->formId.'\')').', \'' . $this->elementId . '\'); ' . $scroll . 'return false;"';
	}
}