<?php
class LinkGenerator {

	function generate($page) {
		return "";
	}

	static function EscapeLink($link){
		$linkArray = parse_url($link);
		if (empty($linkArray['query'])) return $link;
		parse_str($linkArray['query'], $query);
		if (isset($query['q'])) unset($query['q']);
		$linkArray['query'] = http_build_query($query);
		return self::HttpBuildUrl($linkArray);
	}

	static function HttpBuildUrl($parsed_url){
		$scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
		$host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
		$port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
		$user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
		$pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
		$pass     = ($user || $pass) ? "$pass@" : '';
		$path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
		$query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
		$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
		$resultUrl = $scheme.$user.$pass.$host.$port.$path.$query.$fragment;
		if (substr($resultUrl, -1) == '?') $resultUrl = substr($resultUrl, 0, -1);
		return $resultUrl;
	}
	
}
?>