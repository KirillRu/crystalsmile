<?php /**@var $this ViewObjects*/
$citys = array_reverse($this->get('citys', array()));
$zpt = 0;
foreach ($citys as $i=>$city) :
	if (!in_array($city['name'], array('Другие страны', 'Россия'))) {
		if (empty($zpt)): ?><?= $city['name'] ?><?php
		else: ?>, <?= $city['name'] ?><?php
		endif;
		$zpt++;
	}
endforeach;