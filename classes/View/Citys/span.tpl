<?php /**@var $this ViewObjects*/
$citys = array_reverse($this->get('citys', array()));
$zpt = 0;
foreach ($citys as $i=>$city) :
	if (!in_array($city['name'], array('Другие страны', 'Россия'))) {
		if (empty($zpt)): ?><?= $city['name'] ?><?php
		elseif ($zpt == 1): ?><span><?= $city['name'] ?><?php
		else: ?>, <?= $city['name'] ?><?php
		endif;
		if ($i == (count($citys) - 1)): ?></span><?php endif;
		$zpt++;
	}
endforeach;