<?php /**@var $this ViewObjects*/
$citys = array_reverse($this->get('citys', array()));
foreach ($citys as $i=>$city) :
	if (empty($i)): ?><?= $city['name'] ?><?php
	elseif ($i == 1): ?>(<?= $city['name'] ?><?php
	elseif ($i == (count($citys) - 1)): ?>, <?= $city['name'] ?>)<?php
	else: ?>, <?= $city['name'] ?><?php
	endif;
endforeach;