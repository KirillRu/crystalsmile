<?php /**@var $this ViewObjects*/
$zpt = false;
foreach (array_reverse($this->get('citys', array())) as $i=>$city) :
	if (!in_array($city['name'], array('Другие страны', 'Россия'))) {
		if (empty($zpt)): $zpt = true; ?><?= $city['name'] ?><?php
		else: ?>, <?= $city['name'] ?><?php
		endif;
	}
endforeach;