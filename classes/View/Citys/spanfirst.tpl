<?php /**@var $this ViewObjects*/
$citys = array_reverse($this->get('citys', array()));
$zpt = 0;
foreach ($citys as $i=>$city) :
	if (!in_array($city['name'], array('Другие страны', 'Россия'))) {
		if (empty($zpt)): ?><span><?= $city['name'] ?></span><?php
		else: ?>, <?= $city['name'] ?><?php
		endif;
		$zpt++;
	}
endforeach;