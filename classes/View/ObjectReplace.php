<?php
require_once(CLASSES_PATH . 'View/View.php');
class ViewObjectReplace extends View {

	private $_obj = null, $_replace = [];

	function __construct($obj, $replace){
		$this->_obj = $obj;
		$this->_replace = array_merge($replace, array('{region_url}'=>Path::get()->getUrl()));
	}

	function assign(){}

	function fetch() {
		ob_start();
		$this->_obj->display();
		$res = ob_get_contents();
		ob_end_clean();
		return $res;
	}

	function display() {
		$s = $this->fetch();
		$s = str_replace(array_keys($this->_replace), array_values($this->_replace), $s);
		$s = preg_replace('/{[^\}]+}/u', '', $s);
		echo $s;
	}
}