<?php
require_once(CLASSES_PATH . 'View/View.php');
class ViewTemplate extends View {
	private $_path, $_vars = [];
	
	function ViewTemplate($path) {
		$this->_path = $path;
	}
	
	function assign($key, $value) {
		$this->_vars[$key] = $value;
	}

	function get($key, $default = null) {
		if (isset($this->_vars[$key])) return $this->_vars[$key];
		return $default;
	}

	function display() {include $this->_path;}

	function setTpl($tpl) {if ($tpl && file_exists($tpl)) $this->_path = $tpl;}
}