<?php
require_once(CLASSES_PATH . 'View/View.php');
class ViewText extends ViewObjects {
	function __construct($text) {$this->assign('text', $text);}
	
	function fetch() {return $this->get('text');}

	function display() {echo $this->get('text');}
}