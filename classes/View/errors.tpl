<?php foreach($errors as $error): ?>
	<?php if (is_array($error)): ?>
		<?php $this->_displayErrorsArray($error) ?>
	<?php else: ?>
		<?= $error ?><br />
	<?php endif; ?>
<?php endforeach;?>
