<?php
require_once(CLASSES_PATH . 'View/File.php');
class ViewFileReplace extends ViewFile {
	private $_replace = [];

	function __construct($path, $replace){
		parent::__construct($path);
		$this->_replace = array_merge($replace, array('{region_url}'=>Path::get()->getRegionUrl()));
	}

	function display() {
		$s = file_get_contents($this->_path);
		$s = str_replace(array_keys($this->_replace), $this->_replace, $s);
		$s = preg_replace('/{[a-zA-Z_-]+?}/u', '', $s);
		echo $s;
	}
}