<?php
require_once(CLASSES_PATH . 'View/View.php');
class ViewJson extends View {
	private $_jsonStr;
	function __construct($array) {$this->_jsonStr = json_encode($array);}
	
	function fetch() {return $this->_jsonStr;}

	function display() {echo $this->_jsonStr;}
}