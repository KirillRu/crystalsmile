<?php
class View {
	function display() {}

	function fetch() {
		ob_start();
			$this->display();
			$res = ob_get_contents();
		ob_end_clean();
		return $res;
	}

	function __get($name) {
		return new View();
	}

	function __call($name, $arguments){
		return new View();
	}
}

 