<?php
require_once(CLASSES_PATH . 'View/View.php');
class ViewFile extends View {

	protected $_path = null;

	function __construct($path){$this->_path = $path;}

	function assign(){}

	function fetch(){return file_get_contents($this->_path);}

	function display() {readfile($this->_path);}
}