<?php
/*Created by Edik (13.01.15 12:05)*/
class WordCaseCreator{
	protected $_morphy, $_cases = array(
		'i'=>'ИМ',//Именительный Кто? Что?
		'r'=>'РД',//Родительный нет Кого? Чего?
		'd'=>'ДТ',//Дательный дать Кому? Чему?
		'v'=>'ВН',//Винительный вижу Кого? Что?
		't'=>'ТВ',//Творительный горжусь Кем? Чем?
		'p'=>'ПР',//Предложный думаю О ком? О чём?
	);

	function __construct() {
		require_once(CLASSES_PATH . 'Search/PhpMorphy.php');
		$this->_morphy = new SearchPhpMorphy();
	}

	/**@param string $word русское слово(или словосочетание)
	 * @return array array('many'=>array('i'=>'..','r'=>'..',...), 'one'=>array('i'=>'..','r'=>'..',...))
	 */
	function createCases($word){
		$result = array();
		foreach($this->_cases as $caseNameEn => $case){
			$result['many'][$caseNameEn] = preg_replace_callback('/\w+/ui', function($w) use ($case){return $this->_getCase($w[0], $case, "МН");}, $word);
			$result['one'][$caseNameEn] = preg_replace_callback('/\w+/ui', function($w) use ($case){return $this->_getCase($w[0], $case, "ЕД");}, $word);
		}
		return $result;
	}

	/**Возвращает слово в данном падеже и в данном количестве
	 * @param string $word
	 * @param string $case падеж 'ИМ'|'РД'|'ДТ'|'ВН'|'ТВ'|'ПР'
	 * @param string $countCase 'ЕД'|'МН'
	 * @return string
	 */
	private function _getCase($word, $case, $countCase){
		$upperWord = Text::get()->strToUpper($word);
		$grammems = $this->_morphy->getGramInfoMergeForms($upperWord, phpMorphy::IGNORE_PREDICT);
		if ($grammems)
			foreach ($grammems as $grammer) {
				//слово может подходить к нескольким грамматикам. Берем первую попавшуюся.
				if (!empty($grammer['grammems'])){
					$grammer = array_diff($grammer['grammems'], array('МН', 'ЕД'), $this->_cases);
					$word = $this->_morphy->castFormByGramInfo($upperWord, null, array_merge(array($case, $countCase), $grammer), true);
					$word = current($word);
					break;
				}
			}
		return Text::get()->strToLower($word);
	}
}