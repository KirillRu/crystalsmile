<?php
/*Created by Edik (13.01.15 12:02)*/
class WordCase {

	/**возвращает слово в заданном падеже единственного числа
	 * @param string $word слово(русское)
	 * @param string $case падеж 'i'|'r'|'d'|'v'|'t'|'p'
	 * @return mixed
	 */
	function one($word, $case = 'r'){
		$cases = $this->getCases($word);
		return $cases ? $cases['one'][$case] : $word;
	}

	/**возвращает слово в заданном падеже множественного числа
	 * @param string $word слово(русское)
	 * @param string $case падеж 'i'|'r'|'d'|'v'|'t'|'p'
	 * @return mixed
	 */
	function many($word, $case = 'r'){
		$cases = $this->getCases($word);
		return $cases ? $cases['many'][$case] : $word;
	}

	function getCases($word){
		$word = Text::get()->strToLower($word);
		$file = TEMPORARY_PATH . 'WordCases/' . $word . '.php';
		if (!file_exists($file)) {
			require_once(dirname(__FILE__) . '/Creator.php');
			$handler = new WordCaseCreator();
			$cases = $handler->createCases($word);
			ModelsCacheFilePhp::get()->write($file, $cases);
			return $cases;
		}
		return include($file);
	}
}