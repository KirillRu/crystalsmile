<?php
class FormActionWindowSearch extends AbstractAction {
	protected $_alias = 'ws', $_searchPhrase = '', $_searchFieldName = 'name';

	function run() {
		$this->_searchPhrase = $this->_getSearchPhrase();
		if (!$this->_check()) return new View();
		$this->_searchFieldName = Data::get()->gav($this->_alias, 'searchField', 'name');
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Form/View/window_search.tpl');
		$view->assign('items', $this->_getItems());
		return $view;
	}

	protected function _check() {
		return (strlen($this->_searchPhrase) > 2);
	}

	protected function _getItems() {
		require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
		$table = Data::get()->gav($this->_alias, 'table', '');
		switch (true) {
			case ($table == 'category_sets'):
				$items = $this->_getCategorySet();
				break;
			case ((new ModelsStructureFrom_files($table))->isNested()):
				$items = $this->_getNested();
				break;
			default:
				$items = $this->_getDefault();
				break;
		}
		return $items;
	}

	protected function _getDefault() {
		require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
		$structure = new ModelsStructureFrom_files(Data::get()->gav($this->_alias, 'table', ''));
		if (!empty($structure)){
			$sql = '' .
				'select t.' . $structure->getId() . ' as id, t.' . $this->_searchFieldName . ' as name ' .
				'from ' . $structure->getTable() . ' t ' .
				'where ' . implode(' and ',$this->_getCondition()) . ' ' .
				'order by t.' . $this->_searchFieldName;
			return ModelsDriverBDSimpleDriver::getDriver()->multiSelect($sql);
		}
		return array();
	}

	protected function _getNested() {
		require_once(CLASSES_PATH . 'Form/Find/SearchNested.php');
		$handler = new FormFindSearchNested(Data::get()->gav($this->_alias, 'table', ''));
		return $handler->getNestedNames($this->_searchFieldName, $this->_getCondition());
	}

	protected function _getCategorySet() {
		require_once(CLASSES_PATH . 'Form/Find/CategorySet.php');
		$condition = $this->_getCondition('tAC.');
		$condition[] = 't.id_root_alias = ' . Data::get()->gav($this->_alias, 'id_root_alias', '');
		return (new FormFindCategorySet())->getNestedNames($condition);
	}

	private function _getCondition($alias = 't.') {
		$puntoSearchPhrase = Text::get()->puntoSwitch($this->_searchPhrase, 'en');
		if ($this->_searchPhrase == $puntoSearchPhrase){
			return array('(' . $alias . $this->_searchFieldName . ' LIKE ' . Text::get()->mest('%' . $this->_searchPhrase . '%') . ')');
		} else {
			return array('(' . $alias . $this->_searchFieldName . ' LIKE ' . Text::get()->mest('%' . $this->_searchPhrase . '%') . ' or ' . $alias . $this->_searchFieldName . ' LIKE ' . Text::get()->mest('%' . $puntoSearchPhrase . '%') . ')');
		}
	}

	private function _getSearchPhrase() {
		$regExp = '[^a-zA-Zа-яА-Я0-9\Q;:\'",<.>[{]}- \E]';
		$value = str_replace('  ', ' ', trim(Data::get()->gav($this->_alias, 'searchPhrase', '')));
		$value = preg_replace('/' . $regExp . '/u', '', $value);
		return $value;
	}
}