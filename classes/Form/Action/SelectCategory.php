<?php
/*Created by Edik (28.07.14 10:15)*/
require_once(CLASSES_PATH . 'Form/Action/SelectChild.php');
//Для правильной работы этого класса необходим параметр $data['jsParams']['url']!
class FormActionSelectCategory extends FormActionSelectChild {

	/**
	 * @return $this|FormActionSelectCategoryCS
	 */
	function getAction(){
		if ($this->g('table') == 'category_sets'){
			require_once(dirname(__FILE__) . '/SelectCategoryCS.php');
			return (new FormActionSelectCategoryCS($this->_getData()))->getAction();
		}
		return $this;
	}

	function run(){
		$view = parent::run();
		$view->setTpl(CLASSES_PATH . 'Form/View/select_category.tpl');
		return $view;
	}

	function getJsParams(){
		//'name' => имя поля. Добавится к POST параметрам в скрипте. (Прим. 'goods[id_category]')
		//'url' => '' - required! Url отправки на сервер
		//'data' => string - это улетит на сервер при выборе значения из выпадающего списка (прим. table=goods&id_good=21)
		$jsParams = $this->g('jsParams', array());
		$jsParams['name'] = $this->g('name');
		$jsParams['idName'] = Form__getId($jsParams['name']);
		if (!empty($jsParams['data'])) $jsParams['data'] = Functions__getQueryStr($jsParams['data']);
		return $jsParams;
	}
}