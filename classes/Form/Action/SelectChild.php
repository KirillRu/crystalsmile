<?php

/*Created by Edik (25.07.14 11:18)*/
require_once(FUNCTIONS_PATH . 'Form.php');
class FormActionSelectChild extends AbstractAction {
	protected $_item = null, $_structure = null;

	/**
	 * @return $this|FormActionSelectChildCS
	 */
	function getAction(){
		if ($this->g('table') == 'category_sets'){
			require_once(dirname(__FILE__) . '/SelectChildCS.php');
			return (new FormActionSelectChildCS($this->_getData()))->getAction();
		}
		return $this;
	}

	function run() {
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Form/View/select_child.tpl');
		$view->assign('jsParams', $this->_getJsParams());
		$view->assign('name', $this->g('name'));
		$view->assign('value', $this->g('value'));
		$view->assign('linksData', $this->_getLinksData());
		$view->assign('selectData', $this->_getSelectData());
		return $view;
	}

	protected function _getJsParams(){
		//'reloadForm' => false,
		//'url' => '/select-child.ajax'
		$jsParams =  $this->g('jsParams', array());
		$jsParams['table'] = $this->g('table', '');
		$jsParams['name'] = $this->g('name');
		$jsParams['idName'] = Form__getId($jsParams['name']);
		if (!empty($jsParams['data'])) $jsParams['data'] = Functions__getQueryStr($jsParams['data']);
		return $jsParams;
	}

	protected function _getLinksData() {
		$item = $this->_getItem();
		if (!$item) return array();
		$structure = $this->_gs();
		$sql = '' .
			'select t.' . $structure->getId() . ' as id, t.name, t.id_parent ' .
			'from ' . $structure->getTable() . ' t ' .
			'where (t.leftKey <= ' . $item['leftKey'] . ') ' . 'and (t.rightKey >= ' . $item['rightKey'] . ') ' .
			'order by t.leftKey ';
		return ModelsDriverBDSimpleDriver::getDriver()->multiSelect($sql);
	}

	protected function _getSelectData() {
		$item = $this->_getItem();
		if ($item && (($item['leftKey'] + 1) == $item['rightKey'])) return array();
		$structure = $this->_gs();
		$sql = '' .
			'select ' . $structure->getId() . ' as id, name ' .
			'from ' . $structure->getTable() . ' ' .
			'where (id_parent = ' . (int)$this->g('value') . ') ' . (($structure->isActive()) ? 'and (active = 1) ' : '') .
			'order by leftKey';
		return ModelsDriverBDSimpleDriver::getDriver()->multiSelect($sql);
	}

	/**@return ModelsStructureFrom_files*/
	protected function _gs() {
		if ($this->_structure === null){
			require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
			$this->_structure = new ModelsStructureFrom_files($this->g('table'));
		}
		return $this->_structure;
	}

	protected function _getItem() {
		if ($this->_item === null){
			$value = (int)$this->g('value');
			if (!$value) return $this->_item = array();
			$structure = $this->_gs();
			$sql = '' .
				'select ' . $structure->getId() . ' as id, leftKey, rightKey, id_parent ' .
				'from ' . $structure->getTable() . ' ' .
				'where (' . $structure->getId() . ' = ' . $value . ') ' .
				'limit 0, 1 ';
			$this->_item = ModelsDriverBDCache::get()->rowSelect($sql, $structure->getTable());
		}
		return $this->_item;
	}
}