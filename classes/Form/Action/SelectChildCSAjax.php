<?php
/*Created by Edik (25.07.14 11:18)*/
require_once(dirname(__FILE__) . '/SelectChildCS.php');
class FormActionSelectChildCSAjax extends FormActionSelectChildCS {

	protected function _getItem() {
		if ($this->_item === null){
			$value = (int)$this->g('value');
			if (!$value) return $this->_item = array();
			$sql = '' .
				'select id_category_set, id_category, leftKey, rightKey, id_parent, id_root_alias ' .
				'from category_sets ' .
				'where (id_category_set = ' . $value . ') ' .
				'limit 0, 1 ';
			$this->_item = ModelsDriverBDSimpleDriver::getDriver()->rowSelect($sql);
		}
		return $this->_item;
	}

	function getidRootAlias(){
		return Data::get()->gav('selectChild', 'id_root_alias');
	}

	protected function _getIdRootAliasCond(){
		return '(t.id_root_alias = ' . $this->g('id_root_alias') .')';
	}

	function g($name, $default = null){
		return Data::get()->gav('selectChild', $name, $default);
	}
}