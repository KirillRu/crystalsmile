<?php
/*Created by Edik (11.12.14 9:46)*/
require_once(FUNCTIONS_PATH . 'Form.php');
class FormActionSelectChildCS extends AbstractAction{
	protected $_item = null;

	function run(){
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Form/View/select_child_cs.tpl');
		$view->assign('jsParams', $this->_getJsParams());
		$view->assign('name', $this->g('name'));
		$view->assign('linksData', $this->_getLinksData());
		$view->assign('selectData', $this->_getSelectData());

		$item = $this->_getItem();
		if ($item) $view->assign('value', $item['id_category']);
		return $view;
	}

	protected function _getJsParams(){
		$jsParams =  $this->g('jsParams', array());
		$jsParams['table'] = $this->g('table', '');
		$jsParams['name'] = $this->g('name');
		$jsParams['idName'] = Form__getId($jsParams['name']);
		if (empty($jsParams['url'])) $jsParams['url'] = 'form/selectChildCSAjax.ajax';

		$jsParams['data']['selectChild']['id_root_alias'] = $this->getidRootAlias();

		if (!empty($jsParams['data'])) $jsParams['data'] = Functions__getQueryStr($jsParams['data']);
		return $jsParams;
	}


	protected function _getSelectData() {
		$item = $this->_getItem();
		if ($item) {
			if (($item['leftKey'] + 1) == $item['rightKey']) return array();
			$where = 'and (t.id_parent = ' . $item['id_category_set'] . ')';
		} else {
			$idRTCond = $this->_getIdRootAliasCond();
			$where = 'and (t.id_parent = 0)' . ($idRTCond ? (' and ' . $idRTCond) : '');
		}

		$sql = '' .
			'select t.id_category_set as id, tC.name ' .
			'from category_sets t ' .
			'join all_categorys tC on (tC.id_category = t.id_category) ' .
			'where (t.active = 1) ' . $where . ' '.
			'order by t.leftKey';
		return ModelsDriverBDCache::get()->multiSelect($sql, ['category_sets', 'all_categorys']);
	}

	protected function _getLinksData(){
		$item = $this->_getItem();
		if (!$item) return array();
		$sql = '' .
			'select tC.name, t.id_parent ' .
			'from category_sets t ' .
			'join all_categorys tC on (tC.id_category = t.id_category) ' .
			'where (t.leftKey <= ' . $item['leftKey'] . ') and (t.rightKey >= ' . $item['rightKey'] . ') and (t.id_root_alias = ' . $item['id_root_alias'] . ')' .
			'order by t.leftKey ';
		return ModelsDriverBDCache::get()->multiSelect($sql, ['category_sets', 'all_categorys']);
	}

	protected function _getItem() {
		if ($this->_item === null){
			$value = (int)$this->g('value');
			if (!$value) return $this->_item = array();
			$where = $this->g('queryParams');
			$where = !empty($where['where']) ? ('and (' . implode(') and (', $where['where']). ') ') : '';
			$sql = '' .
				'select t.id_category_set, t.id_category, t.leftKey, t.rightKey, t.id_parent, t.id_root_alias ' .
				'from category_sets t ' .
				'where (t.id_category = ' . $value . ') ' . $where .
				'limit 0, 1 ';
			$this->_item = ModelsDriverBDCache::get()->rowSelect($sql, 'category_sets');
		}
		return $this->_item;
	}

	function getidRootAlias(){
		$item = $this->_getItem();
		if ($item) return $item['id_root_alias'];

		$condition = $this->_getIdRootAliasCond();
		$m = [];
		if (preg_match('/id_root_alias *= *(\d+)/u', $condition, $m)) return $m[1];
		return 0;
	}

	protected function _getIdRootAliasCond(){
		$queryParams = $this->g('queryParams');
		return !empty($queryParams['where']) ? (implode(' and ', $queryParams['where']). ' ') : '';
	}
}