<?php
/*Created by Edik (28.07.14 10:15)*/
require_once(CLASSES_PATH . 'Form/Action/SelectChildCS.php');
class FormActionSelectCategoryCS extends FormActionSelectChildCS {

	function run(){
		$view = parent::run();
		$view->setTpl(CLASSES_PATH . 'Form/View/select_category.tpl');
		return $view;
	}

	protected function _getSelectData() {
		$item = $this->_getItem();
		if ($item) {
			if (($item['leftKey'] + 1) == $item['rightKey']) return array();
			$where = 'and (t.id_parent = ' . $item['id_category_set'] . ')';
		} else {
			$idRTCond = $this->_getIdRootAliasCond();
			$where = 'and (t.id_parent = 0)' . ($idRTCond ? (' and ' . $idRTCond) : '');
		}

		$sql = '' .
			'select t.id_category as id, tC.name ' .
			'from category_sets t ' .
			'join all_categorys tC on (tC.id_category = t.id_category) ' .
			'where (t.active = 1) ' . $where . ' '.
			'order by t.leftKey';
		return ModelsDriverBDSimpleDriver::getDriver()->multiSelect($sql);
	}

	protected function _getLinksData(){
		$item = $this->_getItem();
		if (!$item) return array();
		$sql = '' .
			'select tC.name, tC.id_parent ' .
			'from category_sets t ' .
			'join all_categorys tC on (tC.id_category = t.id_category) ' .
			'where (t.leftKey <= ' . $item['leftKey'] . ') and (t.rightKey >= ' . $item['rightKey'] . ') and (t.id_root_alias = ' . $item['id_root_alias'] . ')' .
			'order by t.leftKey ';
		return ModelsDriverBDSimpleDriver::getDriver()->multiSelect($sql);
	}

	function getJsParams(){
		//'name' => имя поля. Добавится к POST параметрам в скрипте. (Прим. 'goods[id_category]')
		//'url' => '' - required! Url отправки на сервер
		//'data' => string - это улетит на сервер при выборе значения из выпадающего списка (прим. table=goods&id_good=21)
		$jsParams = $this->g('jsParams', array());
		$jsParams['name'] = $this->g('name');
		$jsParams['idName'] = Form__getId($jsParams['name']);
		if (!empty($jsParams['data'])) $jsParams['data'] = Functions__getQueryStr($jsParams['data']);
		return $jsParams;
	}

	function getidRootAlias(){
		$item = $this->_getItem();
		if ($item) return $item['id_root_alias'];

		$condition = $this->_getIdRootAliasCond();
		$m = [];
		if (preg_match('/id_root_alias *= *(\d+)/u', $condition, $m)) return $m[1];
		return 0;
	}
}