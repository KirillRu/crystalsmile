<?php
/*Created by Edik (25.07.14 11:18)*/
require_once(CLASSES_PATH . 'Form/Action/SelectChild.php');
class FormActionSelectChildAjax extends FormActionSelectChild {

	function getAction(){
		if ($this->g('table') == 'category_sets'){
			require_once(dirname(__FILE__) . '/SelectChildCSAjax.php');
			return (new FormActionSelectChildCSAjax($this->_getData()))->getAction();
		}
		return $this;
	}

	function g($name, $default = null){
		return Data::get()->gav('selectChild', $name, $default);
	}
}