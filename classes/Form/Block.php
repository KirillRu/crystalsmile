<?php
/*Created by Edik (23.07.14 10:53)*/

require_once(CLASSES_PATH . 'Form/Fields.php');

class FormBlock extends FormFields {
	protected $_defaultBlockName = 'Подробная информация', $_block;

	function run() {
		if (empty($this->_block)) return new View();
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Form/View/block.tpl');
		$view->assign('blockName', isset($this->_block['blockName']) ? $this->_block['blockName'] : $this->_defaultBlockName);
		$view->fields = parent::run();
		return $view;
	}

	function getRows() {
		$rows = parent::getRows();
		if (isset($this->_block['blockName']) && (count($rows) == 1)){
			$rows[0]->setTpl(CLASSES_PATH . 'Form/View/row_value.tpl');
		}
		return $rows;
	}

	protected function _getFields() {
		return $this->_block['fields'];
	}
}