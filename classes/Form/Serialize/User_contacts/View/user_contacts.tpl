<?php /**@var $this ViewObjects*/$field = $this->get('field'); $alias = $this->get('alias');?>
<div class="form-edit__row">
	<div class="form-edit__name">
		<?php if (!empty($field['showForm']['requireField'])): ?>
			<div class="star">*</div>
		<?php endif; ?>
		<div class="title"><?= $field['name'] ?></div>
	</div>
	<div class="form-edit__value">
		<div class="contacts-headers">
			<?php foreach ($this->get('fields') as $field): ?>
				<div class="ci-<?= $field['nameEn'] ?>">
					<?= $field['name'] ?>
					<?php if (!empty($field['showForm']['requireField'])): ?>
						<span class="star">*</span>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
		<div id="js_msContainer_<?=Form__getId($alias)?>" class="contacts-items">
			<?php foreach ($this->get('values') as $i => $values): ?>
				<div class="contacts-item js_msRow">
					<?php foreach ($this->get('fields') as $idAF => $field): ?>
						<div class="ci-<?= $field['nameEn'] ?>"><div>
							<?php
								$view = FormRow::get()->viewValue($field, (isset($values[$idAF]) ? $values[$idAF] : null), $alias);
								$view->assign('name', $view->get('name') . '[]');
								$view->display();
							?>
						</div></div>
					<?php endforeach; ?>
					<div class="ci-buttons js_msButtonsContainer">
						<?php if ($i == 0): ?>
							<img class="js_msButtonAdd ci-add" src="/img/multistring-add.png">
						<?php else: ?>
							<img class="js_msButtonDelete ci-delete" src="/img/multistring-delete.png">
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<script type="text/javascript">
			scriptLoader('modules/multiString.js').callFunction(function(){
				multiString().init(<?=json_encode($this->get('jsParams'))?>);
			});
		</script>
	</div>
</div>