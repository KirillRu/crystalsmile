<?php
/*Created by Edik (25.11.14 12:31)*/
require_once(CLASSES_PATH . 'Form/Fields.php');
class FormSerializeCheckbox extends AbstractAction{

	function run(){
		$view = new ViewObjects();
		$view->rows = $this->_getRows();
		$view->checkboxRow = $this->_getCheckboxRow();
		$view->setTpl(dirname(__FILE__) . '/View/checkbox.tpl');
		return $view;
	}

	private function _getCheckboxRow(){
		$field = $this->g('field');
		unset($field['dataType']);
		$field['showForm']['attributes']['onclick'] = '$(\'.' . 'js_showSerializeCheckbox_' . $this->_getId() . '\').toggle()';
		return FormRow::get()->viewRow($field, $this->g('value'), Form__getName($field['nameEn'], $this->g('alias')), $this->g('typeKey'));
	}

	private function _getRows(){
		require_once(CLASSES_PATH . 'Form/Serialize/Default/Default.php');
		$view = new FormSerializeDefault($this->_getData());
		$view = $view->getAction()->run();

		$trAttributes = array('class' => 'js_showSerializeCheckbox_' . $this->_getId(), 'style' => 'background-color:#ebf1f7;');
		if (!$this->g('value')) $trAttributes['style'] .= ' display: none;';
		foreach($view->get('rows') as $row) $row->assign('rowAttributes', $trAttributes);
		return $view;
	}

	private function _getId(){
		$field = $this->g('field');
		return Form__getId(Form__getName($field['nameEn'], $this->g('alias')));
	}
}