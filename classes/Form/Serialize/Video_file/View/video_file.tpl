<?php /**@var $this ViewObjects*/$switchFormValue = $this->switchForm->get('value');?>
<div class="js_videoBlock">
	<?php $this->switchForm->display()?>
	<ul>
		<?php foreach ($this->get('items') as $v => $name): ?>
			<li class="<?=$v == $switchFormValue ? 'act' : ''?>" onclick="formFunctions.toggleVideoBlock(this, <?=$v?>)">
				<?=$name?>
			</li>
		<?php endforeach; ?>
	</ul>
	<div <?=$switchFormValue == 2 ? '' : 'style="display: none;"'?> class="js_toggleBlock js_toggleBlock_2">
		<?php $this->youtubeForm->display()?>
	</div>
	<div <?=$switchFormValue != 2 ? '' : 'style="display: none;"'?> class="js_toggleBlock js_toggleBlock_1">
		<?php $this->fileForm->display()?>
	</div>

</div>

<style type="text/css">
	.js_videoBlock li{
		cursor: pointer;
	}
	.js_videoBlock li.act{
		color: red;
	}
</style>