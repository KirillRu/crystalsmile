<?php
/*Created by Edik (25.11.14 12:00)*/
require_once(CLASSES_PATH . 'Form/Fields.php');
class FormSerializeVideo_file extends AbstractAction{

	function run(){
		$view = new ViewObjects();
		$view->setTpl(dirname(__FILE__) . '/View/video_file.tpl');
		$view->youtubeForm = $this->_getViewYoutubeForm();
		$view->fileForm = $this->_getViewFileForm();
		$view->switchForm = $this->_getViewSwitchForm();

		$view->assign('items', $this->_getField('switch_form')['idSpr']->getItems());
		return $view;
	}

	private function _getViewYoutubeForm(){
		$view = new ViewObjects();
		$view->setTpl(dirname(__FILE__) . '/View/youtube_form.tpl');

		$field = $this->_getField('youtube_id');
		$view->input = FormRow::get()->viewValue($field, $this->_getValue($field), $this->_getAlias(), $this->g('typeKey'));
		$view->assign('name', $field['name']);
		return $view;
	}

	private function _getViewFileForm(){
		if ($this->gav('values', 'id_video')){
			require_once(CLASSES_PATH . 'Sites/Panels/Files/Action/Files.php');
			return (new SitesPanelsFilesAction(['moduleName' => 'videos', 'table' => 'video_files', 'tableId' => $this->g('values')['id_video']]))->getAction()->run();
		} else {
			require_once(CLASSES_PATH . 'Sites/Panels/SessionFiles/Action/SessionFiles.php');
			return (new SitesPanelsSessionFilesAction(['moduleName' => 'videos', 'table' => 'session_video_files']))->getAction()->run();
		}
	}

	private function _getViewSwitchForm(){
		$field = $this->_getField('switch_form');
		$field['showForm']['attributes']['class'] = 'js_switchForm';

		return FormRow::get()->viewValue($field, $this->_getValue($field)?:2, $this->_getAlias(), $this->g('typeKey'));
	}

	private function _getAlias(){
		return Form__getName($this->g('field')['nameEn'], $this->g('alias'));
	}

	private function _getField($nameEn){
		return $this->g('field')['idSpr']->getStructure()->getField($nameEn);
	}

	private function _getValue($field){
		return $this->g('values')[$field['idAllField']];
	}
}