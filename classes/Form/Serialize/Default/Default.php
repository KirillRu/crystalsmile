<?php
/*Created by Edik (25.11.14 12:00)*/
require_once(CLASSES_PATH . 'Form/Fields.php');
class FormSerializeDefault extends AbstractAction{

	function run(){
		$field = $this->g('field');
		$view = new FormFields();
		$view->fields = $field['idSpr']->getStructure()->getFields();
		$view->values = $this->g('values');
		$view->type = $this->g('typeKey');
		$view->alias = Form__getName($field['nameEn'], $this->g('alias'));
		$view = $view->run();
		$view->setTpl(dirname(__FILE__) . '/View/default.tpl');
		return $view;
	}
}