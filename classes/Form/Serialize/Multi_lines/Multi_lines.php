<?php
/*Created by Edik (26.11.14 10:54)*/
class FormSerializeMulti_lines extends AbstractAction{

	function run(){
		$field = $this->g('field');

		$view = new ViewObjects();
		$view->setTpl(dirname(__FILE__) . '/View/multi_lines.tpl');
		$view->assign('fields', $field['idSpr']->getStructure()->getFields());
		$view->assign('field', $field);
		$view->assign('values', $this->g('values') ? :[[]]);
		$view->assign('alias', Form__getName($field['nameEn'], $this->g('alias')));
		$view->assign('jsParams', $this->_getJsParams());
		return $view;
	}

	protected function _getJsParams(){
		$field = $this->g('field');
		$jsParams = (isset($field['showForm']['jsParams']) ? $field['showForm']['jsParams'] : array());
		$jsParams['id'] = 'js_msContainer_' . Form__getId(Form__getName($field['nameEn'], $this->g('alias')));
		return $jsParams;
	}
}