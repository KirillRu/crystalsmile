<?php /**@var $this ViewObjects*/
$field = $this->get('field');
$alias = $this->get('alias');
$fields = $this->get('fields');
$jsParams = $this->get('jsParams');
$jsParams['postfixId'] = '_multiString';
$jsParams['wordpanel'] = [];//для html-редактора
$jsParams['colorpicker'] = [];//для цветовой палитры
?>
<div class="form-edit__row">
	<div class="form-edit__name">
		<?php if (!empty($field['showForm']['requireField'])): ?>
			<div class="star">*</div>
		<?php endif; ?>
		<div class="title"><?= $field['name'] ?></div>
	</div>
	<div class="form-edit__value">
		<div class="contacts-headers">
			<?php foreach ($fields as $field): ?>
				<div class="ci-<?= $field['nameEn'] ?>">
					<?= $field['name'] ?>
					<?php if (!empty($field['showForm']['requireField'])): ?>
						<span class="star">*</span>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
		<div id="js_msContainer_<?=Form__getId($alias)?>" class="contacts-items">
			<?php foreach ($this->get('values') as $i => $values): ?>
				<div class="contacts-item js_msRow">
					<?php foreach ($fields as $idAF => $field): ?>
						<div class="ci-<?= $field['nameEn'] ?>"><div>
							<?php if ($field['nameEn'] == 'position'): ?>
								<span class="js_msButtonUp ci-up" title="на одну позицию вверх"></span>
								<span class="js_msButtonDown ci-down" title="на одну позицию вниз"></span>
								<span class="js_msButtonFirst ci-first" title="вначало"></span>
								<span class="js_msButtonLast ci-last" title="вконец"></span>
							<?php else:
								$view = FormRow::get()->viewValue($field, (isset($values[$idAF]) ? $values[$idAF] : null), $alias);
                                $name = $view->get('name') . '[]';
								$id = Form__getId($name);
                                $view->assign('name', $name);
                                if (($i == 0)):
                                    if ($field['type'] == 'wordpanel') $jsParams['wordpanel'][$id] = $view->get('jsParams', []);
                                    elseif ($field['type'] == 'colorpicker') $jsParams['colorpicker'][$id] = $view->get('jsParams', []);
                                    $view->display();
								else:
									?><?= str_replace(['"' . $id . '"', "'" . $id . "'"], ['"' . $id . $jsParams['postfixId'] . ($i+1) . '"', "'" . $id . $jsParams['postfixId'] . ($i+1) . "'"], $view->fetch()) ?><?php
								endif;
							endif;
							?>
						</div></div>
					<?php endforeach; ?>
					<div class="ci-buttons js_msButtonsContainer">
						<?php if ($i == 0): ?>
							<img class="js_msButtonAdd ci-add" src="<?= SITE_ROOT ?>img/multistring-add.png">
						<?php else: ?>
							<img class="js_msButtonDelete ci-delete" src="<?= SITE_ROOT ?>img/multistring-delete.png">
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<script type="text/javascript">
			scriptLoader('modules/multiString.js').callFunction(function(){
				multiString().init(<?=json_encode($jsParams)?>);
			});
		</script>
	</div>
</div>