<?php
/*Created by Edik (04.06.2015 10:20)*/
require_once(CLASSES_PATH . 'Form/Form.php');
class FormCache extends Form{

	function run() {
		$key = md5(print_r($this->_fields, true) . print_r($this->_values, true));
		$folder = Functions__getCacheFilename('/form/formCache', $key);
		$view = ModelsCacheFileView::get()->read($folder);
		if ($view === false) {
			$view = parent::run();
			ModelsCacheFileView::get()->write($folder, $view);
		}
		return $view;
	}
}