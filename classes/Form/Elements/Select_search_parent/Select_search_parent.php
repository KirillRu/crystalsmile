<?php
/*Created by Edik (24.07.14 10:26)*/
require_once(CLASSES_PATH . 'Form/Elements/Select_search/Select_search.php');
function FormElementsSelect_search_parent($field, $value, $alias) {
	if (!is_array($value)) return FormElementsSelect_search($field, $value, $alias);
	if (!isset($value['id_parent'])) $value['id_parent'] = 0;
	if (empty($value['id'])) return FormElementsSelect_search($field, $value['id_parent'], $alias);

	$view = new FormElementsSelect_search_parent(array('field' => $field, 'value' => $value, 'alias' => $alias));
	return $view->getAction('')->run();
}

class FormElementsSelect_search_parent extends FormElementsSelect_search {

	protected function _getSelectDataLevels(){
		$levels = $selectData = array();
		$structure = $this->_field['idSpr']->getStructure();
		if ($structure->getField('name')&&$structure->getField('level')) {
			$idField = $structure->getId();
			$sql = ''.
				'select leftKey, rightKey '.
				'from ' . $structure->getTable() . ' '.
				'where (' . $idField . ' = ' . (int) $this->_value['id'] . ') '.
				'order by leftKey '.
			'';
			$rows = ModelsDriverBDSimpleDriver::getDriver()->rowSelect($sql);
			$condition = array();
			if (!empty($rows)) $condition[] = '(' . $idField . ' not in (select ' . $idField . ' from ' . $structure->getTable() . ' where (leftKey >= ' . $rows['leftKey'] . ') and (rightKey <= ' . $rows['rightKey'] . ')))';
			$items = $this->_field['idSpr']->getItems(array('condition'=>$condition, 'select' => array($idField, 'name', 'level')));
			list($selectData, $levels) = $this->_splitItems($items, $idField);
		}
		if (!empty($this->_field['showForm']['requireField'])) $selectData[0] = 'Выбрать..';
		return array($selectData, $levels);
	}
}
