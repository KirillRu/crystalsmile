<?php
/*Created by Edik (21.07.14 10:30)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsDate_interval($field, $value, $alias) {
	$attrs = ['type' => 'text', 'class' => 'textfield'];
	switch ($field['dataType']) {
		case 'datetime':
			$attrs['style'] = 'width:120px;';
			$dateFormat = 'd.m.Y H:i';
			break;
		case 'time':
			$attrs['style'] = 'width:50px;';
			$dateFormat = 'H:i';
			break;
		default:
			$attrs['style'] = 'width:100px;';
			$dateFormat = 'd.m.Y';
			break;
	}
	$view = new ViewObjects();
	$view->start = _FormElementsDate_intervalStart($field, $value, $alias, $attrs, $dateFormat);
	$view->end = _FormElementsDate_intervalEnd($field, $value, $alias, $attrs, $dateFormat);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Date_interval/View/date_interval.tpl');
	return $view;
}

function _FormElementsDate_intervalStart($field, $value, $alias, $attrs, $dateFormat) {
	if (!empty($field['showForm']['attributes']['start'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['start']);
	$field = [
		'nameEn' => $field['nameEn'] . '_start',
		'showForm' => ['attributes' => $attrs]
	];
	if (!empty($value[$field['nameEn']])) {
		if (is_numeric($value[$field['nameEn']])) $value[$field['nameEn']] = date($dateFormat, $value[$field['nameEn']]);
	} else {
		$value[$field['nameEn']] = '';
	}
	return FormElementsInput($field, $value[$field['nameEn']], $alias);
}

function _FormElementsDate_intervalEnd($field, $value, $alias, $attrs, $dateFormat) {
	if (!empty($field['showForm']['attributes']['end'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['end']);
	$field = [
		'nameEn' => $field['nameEn'] . '_end',
		'showForm' => ['attributes' => $attrs]
	];
	if (!empty($value[$field['nameEn']])) {
		if (is_numeric($value[$field['nameEn']])) $value[$field['nameEn']] = date($dateFormat, $value[$field['nameEn']]);
	} else {
		$value[$field['nameEn']] = '';
	}
	return FormElementsInput($field, $value[$field['nameEn']], $alias);
}