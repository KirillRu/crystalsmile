<?php
/*Created by Edik (21.07.14 10:15)*/
require_once(FUNCTIONS_PATH . 'Form.php');
function FormElementsDate_calendar($field, $value, $alias) {
	$name = Form__getName($field['nameEn'], $alias);
	$attrs = array('class' => 'calendarfield', 'type' => 'text', 'onclick' => 'get_date_calendar("' . $name . '");');
	if (!empty($field['showForm']['attributes'])) $attrs = array_merge($attrs, $field['showForm']['attributes']);
	switch (true) {
		case (empty($value)): $value = date('d.m.Y'); break;
		case ($value == 'no_date'): $value = ''; break;
		default: $value = date('d.m.Y', $value); break;
	}
	$view = new ViewObjects();
	$view->assign('name', $name);
	$view->assign('value', (string)$value);
	$view->assign('attributes', $attrs);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Date_calendar/View/date_calendar.tpl');
	return $view;
}