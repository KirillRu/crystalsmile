<?php
/*Created by Edik (18.07.14 15:35)*/
require_once(FUNCTIONS_PATH . 'Date.php');
require_once(FUNCTIONS_PATH . 'Form.php');
require_once(CLASSES_PATH . 'Form/Elements/Date_calendar_datepicker/Date_calendar_datepicker.php');
function FormElementsDate_calendar_interval_datepicker($field, $value, $alias) {
	if (!is_array($value)) $value = [];

	if (!empty($value[$field['nameEn'] . '_start']) && is_numeric($value[$field['nameEn'] . '_start'])) $value[$field['nameEn'] . '_start'] = date('d.m.Y', $value[$field['nameEn'] . '_start']);
	else $value[$field['nameEn'] . '_start'] = '';

	if (!empty($value[$field['nameEn'] . '_end']) && is_numeric($value[$field['nameEn'] . '_end'])) $value[$field['nameEn'] . '_end'] = date('d.m.Y', $value[$field['nameEn'] . '_end']);
	else $value[$field['nameEn'] . '_end'] = '';

	$field['showForm']['attributes']['class'] = 'calendarfield js_calendarfield';
	$inputField = [
		'showForm' => ['attributes' => $field['showForm']['attributes']],
		'nameEn' => $field['nameEn'] . '_start'
	];

	$view = new ViewObjects();
	$view->start = FormElementsDate_calendar_datepicker($inputField, $value[$field['nameEn'] . '_start'], $alias);
	$inputField['nameEn'] = $field['nameEn'] . '_end';
	$view->end = FormElementsDate_calendar_datepicker($inputField, $value[$field['nameEn'] . '_end'], $alias);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Date_calendar_interval_datepicker/View/date_calendar_interval_datepicker.tpl');
	$view->assign('alias', $alias);
	return $view;
}