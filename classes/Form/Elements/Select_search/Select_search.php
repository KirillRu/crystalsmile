<?php
/*Created by Edik (22.07.14 10:31)*/
require_once(CLASSES_PATH . 'Form/Elements/Select/Select.php');
function FormElementsSelect_search($field, $value, $alias) {
	$view = new FormElementsSelect_search(array('field' => $field, 'value' => $value, 'alias' => $alias));
	return $view->getAction('')->run();
}

class FormElementsSelect_search extends FormElementsSelect {

	function run() {
		$view = new ViewObjects();
		$view->assign('name', Form__getName($this->_field['nameEn'], $this->_alias));
		$view->assign('value', (string)$this->_value);
		list($selectData, $levels) = $this->_getSelectDataLevels();
		$view->assign('selectData', $selectData);
		$view->assign('levels', $levels);
		$view->setTpl(CLASSES_PATH . 'Form/Elements/Select_search/View/select_search.tpl');
		return $view;
	}

	protected function _getSelectDataLevels() {
		$levels = $selectData = array();
		if (!empty($this->_field['idSpr'])){
			$structure = $this->_field['idSpr']->getStructure();
			if ($structure->getField('level') && $structure->getField('name')){
				$items = $this->_field['idSpr']->getItems(array('select' => array($structure->getId(), 'name', 'level')));
				list($selectData, $levels) = $this->_splitItems($items, $structure->getId());
			}
			if (empty($selectData)) return array($this->_getSelectData(), array());
			if (!empty($this->_field['showForm']['requireField'])) $selectData[0] = 'Выбрать..';
		}
		return array($selectData, $levels);
	}

	protected function _splitItems($items, $idField){
		$levels = $selectData = array();
		$firstElement = current($items);
		if (!is_array($firstElement)) return array($items, array()); //когда в селекте 2 поля и массив не многомерный
		foreach ($items as $item) {
			$id = $item[$idField];
			$selectData[$id] = $item['name'];
			if ($item['level'] > 1) $levels[$id] = $item['level'];
		}
		return array($selectData, $levels);
	}
}