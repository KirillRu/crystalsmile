<?php $value = $this->get('value'); $id = Form__getId($this->get('name')); $selectData = $this->get('selectData'); $levels = $this->get('levels') ?>
<div class="js_SS select-search-js" onclick="selectSearch.run('<?=$id?>')" id="js_SSText_<?= $id ?>"><?= isset($selectData[$value]) ? $selectData[$value] : 'Выбрать...' ?></div>
<div id="js_SSButtonClear_<?= $id ?>" onclick="selectSearch.clear('<?=$id?>')" class="js_SSButtonClear select-search-clear-js <?= empty($value) ? 'none' : '' ?>" title="Очистить"></div>
<input type="hidden" id="<?= $id ?>" name="<?= $this->get('name') ?>" value="<?= $value ?>">
<?php
$val = (isset($value) ? $value : @current(array_keys($selectData)));?>
<div id="js_SSWindow_<?= $id ?>" class="popup-SS">
	<div class="popup-SS-search">
		<input id="js_SSTextInput_<?= $id ?>" type="text" placeholder="Введите текст для поиска"></div>
	<div class="popup-SS-result" id="js_SSSlideWindow_<?= $id ?>">
		<div id="js_SSNoResults_<?= $id ?>" class="none">Ничего не найдено</div>
		<ul id="js_SSList_<?= $id ?>">
			<?php foreach ($selectData as $value => $name): ?>
				<li class="js_SSValue <?= ($value == $val ? 'SS-selected' : '') ?> <?= !empty($levels[$value]) ? 'l' . $levels[$value] : '' ?>" value="<?= $value ?>"><?= $name ?></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<script type="text/javascript">scriptLoader('modules/selectSearch.js').load();</script>
<link rel="stylesheet" type="text/css" href="<?=SITE_ROOT?>css/modules/select_search.css"/>