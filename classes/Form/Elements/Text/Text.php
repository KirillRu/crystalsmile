<?php
/*Created by Edik (18.07.14 15:11)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsText($field, $value, $alias) {
	if (isset($field['dataType']) && is_numeric($value)){
		switch ($field['dataType']) {
			case 'date':
				$value = (empty($value)) ? '' : date('d.m.Y', $value);
				break;
			case 'datetime':
				$value = (empty($value)) ? '' : date('d.m.Y H:i', $value);
				break;
			case 'time':
				$value = (empty($value)) ? '' : date('H:i', $value);
				break;
		}
	}
	$field['showForm']['attributes']['type'] = 'text';
	if (isset($field['showForm']['attributes']['class'])) $field['showForm']['attributes']['class'] .= ' textfield';
	else $field['showForm']['attributes']['class'] = 'textfield';
	$field['showForm']['attributes']['class'] .= ' field-' . $field['nameEn'];
	return FormElementsInput($field, $value, $alias);
}
