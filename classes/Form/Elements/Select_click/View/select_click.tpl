<?php /**@var $this ViewObjects*/ ?>
<?php $value = $this->get('value'); $id = Form__getId($this->get('name'));?>
<script type="text/javascript">scriptLoader('new/form.js').load();</script>
<span id="<?=$id?>_container" class="select-click">
	<?php foreach ($this->get('selectData') as $k => $v) : ?>
		<a <?= ($k == $value) ? ' class="act"' : '' ?> onclick="formFunctions.selectClick(this, '<?=$id?>', '<?= htmlspecialchars($k) ?>')"><?= $v ?></a>
	<?php endforeach ?>

	<input type="hidden" name="<?= $this->get('name') ?>" id="<?=$id?>" value="<?=$value?>"/>
</span>

<style type="text/css">
	.select-click .act {
		color: red;
	}
</style>