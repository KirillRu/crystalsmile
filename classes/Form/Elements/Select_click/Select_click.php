<?php
/*Created by Edik (24.04.2015 13:48)*/
function FormElementsSelect_click($field, $value, $alias){
	$view = new FormElementsSelect_click(array('field'=>$field, 'value' => (string)$value, 'alias' => $alias));
	return $view->getAction()->run();
}

require_once(CLASSES_PATH . 'Form/Elements/Select/Select.php');
class FormElementsSelect_click extends FormElementsSelect{

	function run(){
		$view = parent::run();
		if (!$view->get('value')) $view->assign('value', array_keys($view->get('selectData'))[0]);
		
		$view->setTpl(CLASSES_PATH . 'Form/Elements/Select_click/View/select_click.tpl');
		return $view;
	}
}