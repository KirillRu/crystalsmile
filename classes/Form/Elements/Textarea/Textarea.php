<?php
/*Created by Edik (18.07.14 14:40)*/
function FormElementsTextarea($field, $value, $alias) {
	$attrs = array('class' => 'textfield');
	if (!empty($field['showForm']['attributes'])) $attrs = array_merge($attrs, $field['showForm']['attributes']);

	$view = new ViewObjects();
	$view->assign('name', Form__getName($field['nameEn'], $alias));
	$view->assign('value', (string)$value);
	$view->assign('attributes', $attrs);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Textarea/View/textarea.tpl');
	return $view;
}