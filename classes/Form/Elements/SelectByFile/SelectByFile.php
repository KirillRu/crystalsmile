<?php
/*Created by Кирилл (14.02.2016 0:59)*/
require_once(CLASSES_PATH . 'Form/Elements/Action.php');
function FormElementsSelectByFile($field, $value, $alias){
	$view = new FormElementsSelectByFile(array('field'=>$field, 'value' => (string)$value, 'alias' => $alias));
	return $view->getAction()->run();
}

class FormElementsSelectByFile extends FormElementsAction {

	function run(){
		$attrs = array();
		if (!empty($this->_field['showForm']['attributes'])) $attrs = array_merge($attrs, $this->_field['showForm']['attributes']);
		if (isset($attrs['class'])) $attrs['class'] = 'textfield ' . $attrs['class'];
		else $attrs['class'] = 'textfield';

		$view = new ViewObjects();
		$view->assign('name', Form__getName($this->_field['nameEn'], $this->_alias));
		$view->assign('value', $this->_value);
		$view->assign('attributes', $attrs);
		$view->assign('selectData', $this->_getSelectData());
		$view->assign('fieldId', $this->_field['nameEn']);
		$view->setTpl(CLASSES_PATH . 'Form/Elements/SelectByFile/View/select_by_file.tpl');
		return $view;
	}

	protected function _getSelectData() {
		$selectData = [];
		$file = CLASSES_PATH . 'Zubi/FileData/' . $this->_field['idSpr']->getStructure()->getTable() . '.db';
		if (file_exists($file)) {
			require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/' . Text::get()->strToUpperFirst($this->_field['idSpr']->getStructure()->getTable()) . '/SerializeText.php');
			$iteratorName = 'ManagementModulesListIterators' . Text::get()->strToUpperFirst($this->_field['idSpr']->getStructure()->getTable()) . 'SerializeText';
			foreach ((new $iteratorName()) as $item) $selectData[$item[$this->_field['idSpr']->getStructure()->getId()]] = $item;
		}
//		if (!empty($this->_field['idSpr'])) $selectData = $this->_field['idSpr']->getItems();
//		if (empty($this->_field['showForm']['requireField'])){
//			switch(true){
//				case (!empty($this->_field['dataType']))&&($this->_field['dataType'] == 'string'):
//					$selectData = array(''=>'Выбрать..') + $selectData;
//					break;
//				case empty($selectData[0]):
//					$selectData = array(0=>'Выбрать..') + $selectData;
//					break;
//			}
//		}
		return $selectData;
	}
}