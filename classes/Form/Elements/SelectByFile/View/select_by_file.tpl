<?php
$value = $this->get('value');
$id = Form__getId($this->get('name'));
$jsParams = $this->get('jsParams');
$jsParams['id'] = $id;
?>
<select name="<?= $this->get('name') ?>" id="<?= $id ?>" <?=Form__getAttributesString($this->get('attributes', array()))?>>
	<?php foreach ($this->get('selectData') as $k => $item) : ?>
		<option <?php foreach($item as $key=>$val):
			?>data-<?= Text::get()->strToLower($key) ?>="<?= $val ?>" <?php
		endforeach; ?>value="<?= htmlspecialchars($k) ?>"<?= ((string)$k === (string)$value) ? ' selected="selected"' : '' ?>>
			<?php if (!empty($item['photoPath'])): ?><img width="40" class="mr10" src="<?= $item['photoPath'] ?>" /><?php endif; ?><?= $item['name'] ?>
		</option>
	<?php endforeach ?>
</select>
<script type="text/javascript ">
	scriptLoader('modules/selectToDiv.js').callFunction(function(){
		selectToDiv().init(<?=json_encode($jsParams)?>);
	});
</script>
