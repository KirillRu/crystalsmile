<?php
/*Created by Edik (18.07.14 15:17)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsPassword($field, $value, $alias) {
	$field['showForm']['attributes']['type'] = 'password';
	if (!isset($field['showForm']['attributes']['class'])) $field['showForm']['attributes']['class'] = 'passwordfield';
	return FormElementsInput($field, $value, $alias);
}