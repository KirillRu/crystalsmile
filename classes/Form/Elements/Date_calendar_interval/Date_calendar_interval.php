<?php
/*Created by Edik (18.07.14 15:35)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
require_once(FUNCTIONS_PATH . 'Date.php');
function FormElementsDate_calendar_interval($field, $value, $alias) {
	if (!is_array($value)) $value = array();
	$jsParams = array(
		'id_start' => Form__getId(Form__getName($field['nameEn'] . '_start', $alias)),
		'id_end' => Form__getId(Form__getName($field['nameEn'] . '_end', $alias))
	);
	if (empty($value[$field['nameEn'] . '_start']) || (empty($value[$field['nameEn'] . '_end']))){
		$value[$field['nameEn'] . '_start'] = $value[$field['nameEn'] . '_end'] = '';
		$str = 'Не выбрано';
	} else {
		if (is_numeric($value[$field['nameEn'] . '_start'])) $value[$field['nameEn'] . '_start'] = date('d.m.Y', $value[$field['nameEn'] . '_start']);
		if (is_numeric($value[$field['nameEn'] . '_end'])) $value[$field['nameEn'] . '_end'] = date('d.m.Y', $value[$field['nameEn'] . '_end']);
		$str = Date__getStringDateInterval(strtotime($value[$field['nameEn'] . '_start']), strtotime($value[$field['nameEn'] . '_end']));
	}
	if (!empty($field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $field['showForm']['jsParams']);
	$field['showForm']['attributes']['type'] = 'hidden';
	$inputField = array('showForm' => array('attributes' => $field['showForm']['attributes']), 'nameEn' => $field['nameEn'] . '_start');

	$view = new ViewObjects();
	$view->assign('jsParams', $jsParams);
	$view->assign('str', $str);
	$view->start = FormElementsInput($inputField, $value[$field['nameEn'] . '_start'], $alias);
	$inputField['nameEn'] = $field['nameEn'] . '_end';
	$view->end = FormElementsInput($inputField, $value[$field['nameEn'] . '_end'], $alias);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Date_calendar_interval/View/date_calendar_interval.tpl');
	return $view;
}