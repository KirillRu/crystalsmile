<div class="field-value">
	<a onclick="initCalendar(<?=htmlspecialchars(json_encode($this->get('jsParams')))?>, this)"><?=$this->get('str')?></a>
	<?php $this->start->display(); ?>
	<?php $this->end->display(); ?>
</div>
<link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/modules/date_calendar_interval.css">
<script type="text/javascript">
	(function(){
		var slider = scriptLoader('modules/slider.js').load(),
			calendar = scriptLoader('modules/calendar.js').load();
		initCalendar = function(params, target){
			slider.callFunction(function(){calendar.callFunction(function(){mainCalendar.display(params, target)})});
		}
	}());
</script>