<?php
/*Created by Edik (18.07.14 10:57)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsHidden($field, $value, $alias) {
	$field['showForm']['attributes']['type'] = 'hidden';
	return FormElementsInput($field, $value, $alias);
}