<?php
/*Created by Viktor (27.08.2015 15:17)*/
require_once(FUNCTIONS_PATH . 'Form.php');
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsDate_calendar_datepicker($field, $value, $alias) {
	$attrs = array('class' => 'calendarfield', 'type' => 'text');
	if (!empty($field['showForm']['attributes'])) $attrs = array_merge($attrs, $field['showForm']['attributes']);
	$field['showForm']['attributes'] = $attrs;
	unset($attrs);

	switch (true) {
		case ($value == 'cur_date'): $value = date('d.m.Y'); break;
		case ($value == 'no_date'): $value = ''; break;
		case ($value == 'use_date'): $value = date('d.m.Y', $value); break;
	}

	$view = new ViewObjects();
	$view->input = FormElementsInput($field, $value, $alias);
	$view->assign('id', Form__getId(Form__getName($field['nameEn'], $alias)));
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Date_calendar_datepicker/View/date_calendar_datepicker.tpl');
	return $view;
}