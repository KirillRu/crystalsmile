<?php
/*Created by Edik (18.07.14 15:24)*/
function FormElementsInput($field, $value, $alias){
	$name = Form__getName($field['nameEn'], $alias);

	$view = new ViewObjects();
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Input/View/input.tpl');
	$view->assign('name', $name);
	$view->assign('value', (string)$value);
	$view->assign('attributes', !empty($field['showForm']['attributes'])?$field['showForm']['attributes']:array());
	return $view;
}