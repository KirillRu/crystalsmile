<?php
/*Created by Edik (18.07.14 14:35)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsCheckbox($field, $value, $alias) {
	$attrs = array();
	if (!empty($field['showForm']['attributes'])) $attrs = array_merge($attrs, $field['showForm']['attributes']);

	if (isset($attrs['class'])) $attrs['class'] .= ' checkboxfield';
	else $attrs['class'] = 'checkboxfield';

	$attrs['type'] = 'checkbox';
	if ($value == 1) $attrs['checked'] = 'checked';

	$field['showForm']['attributes'] = $attrs;
	return FormElementsInput($field, 1, $alias);
}