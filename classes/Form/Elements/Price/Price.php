<?php
/*Created by Edik (21.07.14 11:38)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
require_once(CLASSES_PATH . 'Form/Elements/Select/Select.php');
function FormElementsPrice($field, $value, $alias) {
	$view = new ViewObjects();
	$view->price = _FormElementsPrice($field, $value, $alias);
	$view->id_money = _FormElementsPriceId_money($field, $value, $alias);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Price/View/price.tpl');
	return $view;
}

function _FormElementsPrice($field, $value, $alias) {
	$attrs = array('type' => 'text', 'class' => 'textfield field-' . $field['nameEn']);
	if (!empty($field['showForm']['attributes']['price'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['price']);
	$field = array(
		'nameEn' => 'price',
		'showForm' => array('attributes' => $attrs)
	);
	return FormElementsInput($field, (!empty($value[$field['nameEn']]) ? $value[$field['nameEn']] : ''), $alias);
}

function _FormElementsPriceId_money($field, $value, $alias) {
	$attrs = array('class' => 'field-id_money');
	if (!empty($field['showForm']['attributes']['id_money'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['id_money']);
	$field['nameEn'] = 'id_money';
	$field['showForm']['attributes'] = $attrs;
	$field['showForm']['requireField'] = true;
	return FormElementsSelect($field, (!empty($value['id_money']) ? $value['id_money'] : 1), $alias);
}