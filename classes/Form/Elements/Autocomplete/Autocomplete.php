<?php
/*Created by Edik (18.07.14 15:19)*/
require_once(CLASSES_PATH . 'Form/Elements/Action.php');
function FormElementsAutocomplete($field, $value, $alias) {
	$view = new FormElementsAutocomplete(['field' => $field, 'value' => $value, 'alias' => $alias]);
	return $view->getAction()->run();
}

class FormElementsAutocomplete extends FormElementsAction {

	function run(){
		$name = Form__getName($this->_field['nameEn'], $this->_alias);

		$attrs = ['type' => 'text'];
		if (!empty($this->_field['showForm']['attributes'])) $attrs = array_merge($attrs, $this->_field['showForm']['attributes']);
		if (isset($attrs['class'])) $attrs['class'] .= ' textfield';
		else $attrs['class'] = 'textfield';

		$id = Form__getId($name);

		$jsParams = ['idText' => $id . '_text', 'idInput' => $id, 'data' => ['table' => $this->_field['idSpr']->getStructure()->getTable()]];
		if (!empty($field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $field['showForm']['jsParams']);

		$tpl = CLASSES_PATH . 'Form/Elements/Autocomplete/View/autocomplete.tpl';
		if (!empty($this->_field['showForm']['tpl'])) $tpl = $this->_field['showForm']['tpl'];

		$view = new ViewObjects();
		$view->setTpl($tpl);
		if (!empty($this->_field['showForm']['action'])) $view->assign('action', $this->_field['showForm']['action']);
		$view->assign('name', $name);
		$view->assign('item', $this->_getItem());
		$view->assign('value', $this->_value);
		$view->assign('attributes', $attrs);
		$view->assign('jsParams', $jsParams);
		return $view;
	}

	private function _getItem(){
		if (!$this->_value) return [];

		$structure = $this->_field['idSpr']->getStructure();
		switch ($structure->getTable()) {
			case 'exposition_venues':
				$select = $structure->getId() . ' as id, concat(name, ", ", address) as name';
				break;
			default: $select = $structure->getId() . ' as id, name';
		}
		$sql = '' .
			'select ' . $select . ' ' .
			'from ' . $structure->getTable() . ' ' .
			'where (' . $structure->getId() . ' = ' . Text::get()->mest($this->_value) . ') ' .
			'limit 0, 1 ';
		return ModelsDriverBDSimpleDriver::getDriver()->rowSelect($sql);
	}
}