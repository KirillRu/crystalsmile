<?php
/*Created by Edik (23.07.14 16:57)*/
function FormElementsOpen_id($field, $value, $alias){
	$view = new ViewObjects();
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Open_id/View/open_id.tpl');
	if (!empty($field['action'])) $view->assign('action', $field['action']);
	if (!isset($field['socials'])) {
		$view->assign('socials', array_keys(Cfg__get('open_id')));
	} else $view->assign('socials', $field['socials']);
	$view->assign('jsParams', $field['showForm']['jsParams']);
	return $view;
}