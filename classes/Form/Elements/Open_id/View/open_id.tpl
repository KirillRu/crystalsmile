<?php
foreach ($this->get('socials') as $social):?>
	<div class="button-<?= $social ?>" id="button_<?= $social ?>"></div>
<?php endforeach; ?>
<?php if ($this->get('action') == 'auth' || $this->get('action') == 'connect'): ?>
	<script type="text/javascript">
		scriptLoader('modules/openId.js').callFunction(
			function() {setAuthButton(<?= json_encode($this->get('jsParams')); ?>);}
		);
	</script>
<?php elseif ($this->get('action') == 'reg'): ?>
	<script type="text/javascript">
		scriptLoader('modules/openId.js').callFunction(
			function() {setInsertFieldsButton(<?= json_encode($this->get('jsParams')); ?>);}
		);
	</script>
<?php endif ?>
