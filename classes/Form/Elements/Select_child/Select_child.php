<?php
/*Created by Edik (29.07.14 11:46)*/
function FormElementsSelect_child($field, $value, $alias){
	/*проверка нужна для того, что бы можно было нарисовать это поле без структуры. Просто FormRow::get()->viewValue()*/
	$table = is_array($field['idSpr']) ? $field['idSpr']['table'] : $field['idSpr']->getStructure()->getTable();
	$data = array(
		'value' => $value,
		'name' => Form__getName($field['nameEn'], $alias),
		'table' => $table
	);
	if (!empty($field['showForm']['jsParams'])) $data['jsParams'] = $field['showForm']['jsParams'];

	if ($table == 'category_sets') $data['queryParams'] = is_array($field['idSpr']) ? $field['idSpr']['queryParams'] : $field['idSpr']->getQueryParams();

	require_once(CLASSES_PATH . 'Form/Action/SelectChild.php');
	$view = (new FormActionSelectChild($data))->getAction('')->run();
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Select_child/View/select_child.tpl');
	return $view;
}