<?php
/*Created by Edik (29.07.14 11:46)*/
require_once(CLASSES_PATH . 'Form/Action/SelectCategory.php');
function FormElementsSelect_category($field, $value, $alias){
	$table = $field['idSpr']->getStructure()->getTable();
	$data = array(
		'value' => $value,
		'name' => Form__getName($field['nameEn'], $alias),
		'table' => $table
	);
	if (!empty($field['showForm']['jsParams'])) $data['jsParams'] = $field['showForm']['jsParams'];

	if ($table == 'category_sets') $data['queryParams'] = $field['idSpr']->getQueryParams();

	require_once(CLASSES_PATH . 'Form/Action/SelectCategory.php');
	$view = (new FormActionSelectCategory($data))->getAction('')->run();

	$view->setTpl(CLASSES_PATH . 'Form/Elements/Select_category/View/select_category.tpl');
	return $view;
}