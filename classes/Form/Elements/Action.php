<?php
/*Created by Edik (29.07.2015 14:40)*/
abstract class FormElementsAction extends AbstractAction{
	protected $_field, $_value, $_alias;

	function getAction(){
		$this->_field = $this->g('field');
		$this->_value = $this->g('value');
		$this->_alias = $this->g('alias');
		return parent::getAction();
	}
}