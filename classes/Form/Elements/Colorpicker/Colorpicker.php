<?php
/*Created by Кирилл (09.02.2016 18:52)*/
require_once(CLASSES_PATH . 'Form/Elements/Text/Text.php');

function FormElementsColorpicker($field, $value, $alias){
    $field['type'] = 'text';
    $view = FormElementsText($field, $value, $alias);
   	$view->setTpl(CLASSES_PATH . 'Form/Elements/Colorpicker/View/colorpicker.tpl');
	$attr = $view->get('attributes', []);
	if (isset($attr['class'])) $attr['class'] .= ' js-colorpicker';
	else $attr['class'] = 'js-colorpicker';

	$view->assign('attributes', $attr);
   	return $view;
}