<?php
/*Created by Edik (21.07.14 11:52)*/
require_once(CLASSES_PATH . 'Form/Elements/Action.php');
function FormElementsSelect($field, $value, $alias){
	$view = new FormElementsSelect(array('field'=>$field, 'value' => (string)$value, 'alias' => $alias));
	return $view->getAction()->run();
}

class FormElementsSelect extends FormElementsAction{

	function run(){
		$attrs = array();
		if (!empty($this->_field['showForm']['attributes'])) $attrs = array_merge($attrs, $this->_field['showForm']['attributes']);
		if (isset($attrs['class'])) $attrs['class'] = 'textfield ' . $attrs['class'];
		else $attrs['class'] = 'textfield';

		$view = new ViewObjects();
		$view->assign('name', Form__getName($this->_field['nameEn'], $this->_alias));
		$view->assign('value', $this->_value);
		$view->assign('attributes', $attrs);
		$view->assign('selectData', $this->_getSelectData());
		$view->setTpl(CLASSES_PATH . 'Form/Elements/Select/View/select.tpl');
		return $view;
	}

	protected function _getSelectData() {
		$selectData = array();
		if (!empty($this->_field['idSpr'])) $selectData = $this->_field['idSpr']->getItems();
		if (empty($this->_field['showForm']['requireField'])){
			switch(true){
				case (!empty($this->_field['dataType']))&&($this->_field['dataType'] == 'string'):
					$selectData = array(''=>'Выбрать..') + $selectData;
					break;
				case empty($selectData[0]):
					$selectData = array(0=>'Выбрать..') + $selectData;
					break;
			}
		}
		return $selectData;
	}
}