<?php $value = $this->get('value'); ?>
<select name="<?= $this->get('name') ?>" id="<?= Form__getId($this->get('name')) ?>" <?=Form__getAttributesString($this->get('attributes', array()))?>>
	<?php foreach ($this->get('selectData') as $k => $v) : ?>
		<option value="<?= htmlspecialchars($k) ?>"<?= ((string)$k === (string)$value) ? ' selected="selected"' : '' ?>><?= $v ?></option>
	<?php endforeach ?>
</select>