<?php
	$values = $this->get('value');
	$attrs = $this->get('attributes');
	$id = Form__getId($this->get('name'));
	$jsParams = $this->get('jsParams');
	$jsParams['id'] = $id;
?>
<div id="js_fms_<?=$id?>_container" class="field-multistring">
	<div class="fms-block">
		<input name="<?= $this->get('name')?>[]" id="<?= $id?>" value="<?= htmlspecialchars(array_shift($values)) ?>" <?=Form__getAttributesString($attrs['field'])?> />
	</div>
	<?php foreach($values as $value):?>
		<div class="fms-block">
			<input name="<?= $this->get('name')?>[]" id="<?= $id?>" value="<?= htmlspecialchars($value) ?>" <?=Form__getAttributesString($attrs['additionalFields'])?> /><img class="js_fms_buttonDelete fms-delete" src="http://static.izhart.ru/img/admin/multistring-delete.png">
		</div>
	<?php endforeach;?>
	<img id="js_fms_<?=$id?>_buttonAdd" class="fms-add" <?=($jsParams['currentCount']==$jsParams['maxCount'])?'style="display:none"':''?>src="http://static.izhart.ru/img/admin/multistring-add.png">
</div>
<script type="text/javascript ">
	scriptLoader('modules/multiString.js').callFunction(function(){
		multiStringInit(<?=json_encode($jsParams)?>);
	});
</script>
<link rel="stylesheet" type="text/css" href="<?=SITE_ROOT?>css/modules/multi_string.css"/>