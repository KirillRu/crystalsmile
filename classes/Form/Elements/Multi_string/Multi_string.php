<?php
/*Created by Edik (25.07.14 9:30)*/
function FormElementsMulti_string($field, $value, $alias){
	$value = (array)$value;
	$jsParams = array('maxCount' => 5);
	$attrs = array(
		'field'=>array('type' => 'text', 'class' => 'textfield'),
		'additionalFields'=>array('type' => 'text', 'class' => 'textfield')
	);
	if (!empty($field['showForm']['attributes']['field'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['field']);
	if (!empty($field['showForm']['attributes']['additionalFields'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['additionalFields']);
	if (!empty($field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $field['showForm']['jsParams']);
	$value = array_slice($value, 0, $jsParams['maxCount']);
	$jsParams['currentCount'] = count($value);
	
	$view = new ViewObjects();
	$view->assign('name', Form__getName($field['nameEn'], $alias));
	$view->assign('value', $value);
	$view->assign('jsParams', $jsParams);
	$view->assign('attributes', $attrs);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Multi_string/View/multi_string.tpl');
	return $view;
}