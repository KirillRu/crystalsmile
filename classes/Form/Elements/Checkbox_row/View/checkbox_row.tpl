<?php /**@var $this ViewObjects */
$jsParams = $this->get('jsParams');
$name = $this->get('name');
$value = $this->get('value');
$id = Form__getId($name);
$attrs = $this->get('attributes');
$selectData = $this->get('selectData');
$countOnCol = ceil(count($selectData) / $jsParams['countColumns']);
?>

<?php for ($col = 0; $col < $jsParams['countColumns']; $col++): $start = $col * $countOnCol; ?>
	<div class="div-form-checkbox-col">
		<?php for ($i = $start; $i < $start + $countOnCol; $i++): if (!empty($selectData[$i])): ?>
			<div class="form-checkbox-row">
				<input name="<?= $name ?>[]" id="<?= $id . '_' . $i ?>" value="<?= $selectData[$i]['value'] ?>" <?= in_array($selectData[$i]['value'], $value) ? 'checked' : '' ?> <?= Form__getAttributesString($attrs) ?>>
				<label for="<?= $id . '_' . $i ?>"><?= $selectData[$i]['name'] ?></label>
			</div>
		<?php endif; endfor; ?>
	</div>
<?php endfor; ?>