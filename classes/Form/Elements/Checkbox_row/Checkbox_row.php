<?php
/*Created by Edik (30.07.14 12:31)*/
require_once(CLASSES_PATH . 'Form/Elements/Action.php');
function FormElementsCheckbox_row($field, $value, $alias) {
	$view = new FormElementsCheckbox_row(array('field' => $field, 'value' => (array)$value, 'alias' => $alias));
	return $view->getAction()->run();
}

class FormElementsCheckbox_row extends FormElementsAction {

	function run(){
		$attrs = ['class' => 'checkboxfield', 'type' => 'checkbox'];
		$jsParams = ['countColumns' => 1];
		if (!empty($this->_field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $this->_field['showForm']['jsParams']);
		if (!empty($this->_field['showForm']['attributes'])) $attrs = array_merge($attrs, $this->_field['showForm']['attributes']);

		$view = new ViewObjects();
		$view->assign('name', Form__getName($this->_field['nameEn'], $this->_alias));
		$view->assign('value', $this->_value);
		$view->assign('attributes', $attrs);
		$view->assign('jsParams', $jsParams);
		$view->assign('selectData', $this->_getSelectData());
		$view->setTpl(CLASSES_PATH . 'Form/Elements/Checkbox_row/View/checkbox_row.tpl');
		return $view;
	}

	protected function _getSelectData() {
		$selectData = [];
		if (!empty($this->_field['idSpr'])) {
			$i=0;
			foreach($this->_field['idSpr']->getItems() as $value => $name){
				$selectData[$i]['value'] = $value;
				$selectData[$i++]['name'] = $name;
			}
		}
		return $selectData;
	}
}