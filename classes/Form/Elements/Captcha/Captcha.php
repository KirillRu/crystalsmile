<?php
/*Created by Edik (18.07.14 15:19)*/
function FormElementsCaptcha($field, $value, $alias) {
	$name = Form__getName($field['nameEn'], $alias);
	$attrs = array('type' => 'captcha', 'class' => 'captchafield');
	if (!empty($field['showForm']['attributes'])) $attrs = array_merge($attrs, $field['showForm']['attributes']);

	$view = new ViewObjects();
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Captcha/View/captcha.tpl');
	$view->assign('name', $name);
	$view->assign('value', (string)$value);
	$view->assign('attributes', $attrs);
	return $view;
}