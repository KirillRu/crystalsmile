<?php $id = Form__getId($this->get('name')); $table = $this->get('jsParams')['selectChild']['table']; $jsParams = json_encode($this->get('jsParams')); ?>
<?php $this->selectChild->display(); ?>
<div id='ws_<?=$id?>_toggle' onclick="windowSearch.init('<?=$id?>', <?=htmlspecialchars($jsParams);?>)" class="value-search-icon js_wsToggle" xmlns="http://www.w3.org/1999/html"></div>
<script type="text/javascript">$(g('<?=$id?>')).closest('.select-child').append(g('ws_<?=$id?>_toggle'))</script>
<div class="relative">
	<div id="ws_<?=$id?>_mainWindow" class="popup pp-search ws-<?=$table?>">
		<div onclick="windowSearch.close()" class="close"><img src="<?= SITE_ROOT ?>/img/popup/icon-close.png"></div>
		<h1>Поиск</h1>
		<input type="text" id="ws_<?=$id?>" class="textfield pp-search-input"/>
		<div class="pp-search-content">
			<div id="ws_<?=$id?>_container" class="pp-search-container"></div>
		</div>
	</div>
</div>

<script type="text/javascript">scriptLoader('form.js').load();</script>
<script type="text/javascript">scriptLoader('modules/windowSearch.js').load();</script>
<link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/modules/window_search.css">
