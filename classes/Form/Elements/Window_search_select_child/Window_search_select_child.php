<?php
/*Created by Edik (25.07.14 10:50)*/
function FormElementsWindow_search_select_child($field, $value, $alias){
	$name = Form__getName($field['nameEn'], $alias);
	$jsParams = array(
	    'selectChild' => array(
	        'table' => is_array($field['idSpr']) ? $field['idSpr']['table'] : $field['idSpr']->getStructure()->getTable(),
	        'name' => $name,
	        'idName' => Form__getId($name)
	    )
	);
	if (!empty($field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $field['showForm']['jsParams']);
	$data = array(
		'value' => $value,
	    'name' => $name,
	    'table' => $jsParams['selectChild']['table'],
		'jsParams' => $jsParams['selectChild']
	);

	if ($data['table'] == 'category_sets') $data['queryParams'] = is_array($field['idSpr']) ? $field['idSpr']['queryParams'] : $field['idSpr']->getQueryParams();

	require_once(CLASSES_PATH . 'Form/Action/SelectChild.php');

	$view = new ViewObjects();
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Window_search_select_child/View/window_search_select_child.tpl');

	$handler = (new FormActionSelectChild($data))->getAction();
	$view->selectChild = $handler->run();
	if ($data['table'] == 'category_sets') $jsParams['data'] = 'ws[id_root_alias]=' . $handler->getidRootAlias();

	$jsParams['selectChild'] = array_merge($jsParams['selectChild'], $view->selectChild->get('jsParams'));
	$view->assign('name', $name);
	$view->assign('jsParams', $jsParams);
	return $view;
}