<?php /**@var $this ViewObjects */ $id = Form__getId($this->get('name')); ?>
<div id="map_content_<?= $id ?>" <?= Form__getAttributesString($this->get('attributes')); ?>></div>
<?php $this->input->display(); ?>
<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU&mode=debug&load=Map,Placemark,geocode,geoObject.addon.balloon,control.ZoomControl,control.SearchControl" type="text/javascript"></script>
<script type="text/javascript">
	scriptLoader('modules/yandexMaps.js').callFunction(function() {
		yandexMaps.run('<?=$id?>', <?=json_encode($this->get('jsParams'));?>);
	});
</script>