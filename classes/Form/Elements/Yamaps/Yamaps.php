<?php
/*Created by Edik (30.07.14 15:09)*/
require_once(CLASSES_PATH . 'Form/Elements/Hidden/Hidden.php');
/*jsParams => array(
	'idTextAddress' => ..., //Id инпута, куда будет записываться адресс в виде строки
	'dragPoint' => true|false, //Если true - то можно двигать точку
	'firmInfo' => array(
		'img' => ...,
		'name' => ...
	)

)*/
function FormElementsYamaps($field, $value, $alias){
	$attrs = array('style'=>'width: 600px; height: 400px');
	/** модуль может быть coordinates или place. Если place - то в инпут будет вставляться строка выбранной позиции(прим.: "Ижевск, Удмуртская 12")*/
	$jsParams = array('module' => 'coordinates');
	if (!empty($field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $field['showForm']['jsParams']);
	if (!empty($field['showForm']['attributes']['div'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['div']);
	$fieldInput = array('nameEn'=>$field['nameEn'], 'showForm' => array('attributes' => array('type' => 'hidden')));
	if (!empty($field['showForm']['attributes']['input'])) $fieldInput['showForm']['attributes'] = $field['showForm']['attributes']['input'];

	$view = new ViewObjects();
	$view->input = FormElementsInput($fieldInput, $value, $alias);
	$view->assign('name', Form__getName($field['nameEn'], $alias));
	$view->assign('attributes', $attrs);
	$view->assign('jsParams', $jsParams);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Yamaps/View/yamaps.tpl');
	return $view;
}