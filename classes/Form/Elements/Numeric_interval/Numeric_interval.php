<?php
/*Created by Edik (21.07.14 10:51)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsNumeric_interval($field, $value, $alias){
	$view = new ViewObjects();
	$view->start = _FormElementsNumeric_intervalStart($field, $value, $alias);
	$view->end = _FormElementsNumeric_intervalEnd($field, $value, $alias);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Numeric_interval/View/numeric_interval.tpl');
	return $view;
}

function _FormElementsNumeric_intervalStart($field, $value, $alias){
	$attrs = array('type' => 'text', 'class' => 'textfield');
	if (!empty($field['showForm']['attributes']['start'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['start']);
	$field = array(
		'nameEn' => $field['nameEn'] . '_start',
		'showForm' => array('attributes' => $attrs)
	);
	return FormElementsInput($field, (!empty($value[$field['nameEn']]) ? $value[$field['nameEn']] : ''), $alias);
}

function _FormElementsNumeric_intervalEnd($field, $value, $alias){
	$attrs = array('type' => 'text', 'class' => 'textfield');
	if (!empty($field['showForm']['attributes']['end'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['end']);
	$field = array(
		'nameEn' => $field['nameEn'] . '_end',
		'showForm' => array('attributes' => $attrs)
	);
	return FormElementsInput($field, (!empty($value[$field['nameEn']]) ? $value[$field['nameEn']] : ''), $alias);
}