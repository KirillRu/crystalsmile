<?php
/*Created by Edik (21.07.14 11:03)*/
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
require_once(CLASSES_PATH . 'Form/Elements/Select/Select.php');
function FormElementsPrice_interval($field, $value, $alias) {
	$view = new ViewObjects();
	$view->start = _FormElementsPrice_intervalStart($field, $value, $alias);
	$view->end = _FormElementsPrice_intervalEnd($field, $value, $alias);
	$view->id_money = _FormElementsPrice_intervalId_money($field, $value, $alias);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Price_interval/View/price_interval.tpl');
	return $view;
}

function _FormElementsPrice_intervalStart($field, $value, $alias) {
	$attrs = array('type' => 'text', 'class' => 'textfield');
	if (!empty($field['showForm']['attributes']['start'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['start']);
	$field = array(
		'nameEn' => $field['nameEn'] . '_start',
		'showForm' => array('attributes' => $attrs)
	);
	return FormElementsInput($field, (!empty($value[$field['nameEn']]) ? $value[$field['nameEn']] : ''), $alias);
}

function _FormElementsPrice_intervalEnd($field, $value, $alias) {
	$attrs = array('type' => 'text', 'class' => 'textfield');
	if (!empty($field['showForm']['attributes']['end'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['end']);
	$field = array(
		'nameEn' => $field['nameEn'] . '_end',
		'showForm' => array('attributes' => $attrs)
	);
	return FormElementsInput($field, (!empty($value[$field['nameEn']]) ? $value[$field['nameEn']] : ''), $alias);
}

function _FormElementsPrice_intervalId_money($field, $value, $alias) {
	$attrs = array();
	if (!empty($field['showForm']['attributes']['id_money'])) $attrs = array_merge($attrs, $field['showForm']['attributes']['id_money']);
	$field['nameEn'] = 'id_money';
	$field['showForm']['attributes'] = $attrs;
	return FormElementsSelect($field, (!empty($value['id_money']) ? $value['id_money'] : 1), $alias);
}