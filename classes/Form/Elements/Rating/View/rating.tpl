<?php $id = Form__getId($this->get('name')); ?>
<div class="raiting" onmouseleave="previewRating(this, 'restore');">
	<input name="<?= $this->get('name') ?>" id="<?= $id ?>" value="<?= htmlspecialchars($this->get('value')) ?>" <?= Form__getAttributesString($this->get('attributes', array())) ?> />
	<?php foreach ($this->get('selectData') as $k => $v) :
		?><div id="js_<?= $id ?>_<?= $k ?>" class="js_rating_<?= $k ?> <?= ($k <= $this->get('value')) ? ' act' : '' ?>" onmouseenter="previewRating(this, 'on');" onmouseleave="previewRating(this, 'off');" onclick="markRating(this, '<?= $k ?>')"></div><?php
	endforeach;
?></div>
<script type="text/javascript">
	function markRating(t, rating) {
		previewRating(t, 'on');
		var $t = $(t);
		$t.siblings('input').get(0).value = rating;
	}

	function previewRating(t, action) {
		var $t = $(t);
		switch (action) {
			case 'on':
				$t.siblings('div').removeClass('act');
				$t.addClass('act');
				$t.prevAll('div').addClass('act');
				break;
			case 'off':
				$t.removeClass('act');
				$t.siblings('div').removeClass('act');
				break;
			case 'restore':
				var rating = $t.children('input[type=hidden]').val();
				if (rating) {
					var $rT = $t.children('div.js_rating_' + rating);
					previewRating($rT.get(0), 'on');
				}
				break;
		}
	}
</script>