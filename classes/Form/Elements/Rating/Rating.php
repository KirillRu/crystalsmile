<?php
/*Created by Edik (21.07.14 13:06)*/
function FormElementsRating($field, $value, $alias) {
	$attrs = array('type' => 'hidden');
	if (!empty($field['showForm']['attributes'])) $attrs = array_merge($attrs, $field['showForm']['attributes']);
	if (!empty($field['idSpr'])) $selectData = $field['idSpr']->getItems(); else $selectData = array();

	$view = new ViewObjects();
	$view->assign('name', Form__getName($field['nameEn'], $alias));
	$view->assign('value', (int)$value);
	$view->assign('attributes', $attrs);
	$view->assign('selectData', $selectData);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Rating/View/rating.tpl');
	return $view;
}