<?php $id = Form__getId($this->get('name'));?>
<div class="multiselect-block">
	<a id="1<?= $id ?>" class="multiselect-link">Не выбрано</a>
	<div id="<?= $id ?>1" class="multiselect-div">
		<div class="multiselect-checkall">
			<input class='multiSelect_checkAll' type='checkbox' value='Выделить все'/>
		</div>
		<?php foreach ($this->get('selectData') as $val => $name): ?>
			<div class="multiselect-check">
				<input name="<?= $this->get('name') ?>[]" id="<?= $id ?>_<?= $val ?>" type="checkbox" value="<?= $val ?>" <?= in_array($val, $this->get('value')) ? 'checked' : '' ?>/><label for="<?= $id ?>_<?= $val ?>"><?= $name ?></label>
			</div>
		<?php endforeach ?>
		<div class="multiselect-buttons">
			<input type='button' class='button' onclick="multiSelect.hide('1<?= $id ?>')" value="Сохранить">
			<input type='button' class='button' onclick="multiSelect.cancel('1<?= $id ?>')" value="Отмена">
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?=SITE_ROOT?>css/modules/multi_select.css"/>
<script type="text/javascript">
	scriptLoader('modules/multiSelect.js').callFunction(function(){multiSelect.run('1<?=$id?>');});
</script>