<?php
/*Created by Edik (24.07.14 11:01)*/
require_once(CLASSES_PATH . 'Form/Elements/Select/Select.php');
function FormElementsMulti_select($field, $value, $alias) {
	$view = new FormElementsMulti_select(array('field' => $field, 'value' => (array)$value, 'alias' => $alias));
	return $view->getAction('')->run();
}

class FormElementsMulti_select extends FormElementsSelect {
	function run(){
		$view = parent::run();
		$view->setTpl(CLASSES_PATH . 'Form/Elements/Multi_select/View/multi_select.tpl');
		return $view;
	}
}