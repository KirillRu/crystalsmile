<?php
/*Created by Edik (24.04.2015 13:48)*/
require_once(CLASSES_PATH . 'Form/Elements/Action.php');
function FormElementsAggregator_left($field, $value, $alias){
	$view = new FormElementsAggregator_left(array('field'=>$field, 'value' => $value, 'alias' => $alias));
	return $view->getAction()->run();
}

class FormElementsAggregator_left extends FormElementsAction{

	function run(){
		$view = new ViewObjects();
		$view->assign('name', Form__getName($this->_field['nameEn'], $this->_alias));

		$selectData = $this->_getSelectData();
		$value = $this->_value['value'] !== null ? $this->_value['value'] : array_keys($selectData)[0];
		$view->assign('value', is_array($value) ? array_shift($value) : $value);
		$view->assign('selectData', $selectData);
		$view->setTpl(CLASSES_PATH . 'Form/Elements/Aggregator_left/View/aggregator_left.tpl');
		return $view;
	}

	protected function _getSelectData() {
		$selectData = [];

		$selectData[''] = [
			'name' => 'Все',
		    'href' => SitesPath::get()->getCategoryPage($this->_value['table'], $this->_value['id_category'])
		];

		if (!empty($this->_field['idSpr'])) {
			foreach ($this->_field['idSpr']->getItems() as $k => $name) {
				$selectData[$k] = [
					'name' => $name,
					'href' => SitesPath::get()->getCategoryPage($this->_value['table'], $this->_value['id_category'], '', SitesAggregator::get()->getPathPart($this->_field['nameEnSmall'], $k, $name))
				];
			}
		}
		return $selectData;
	}
}