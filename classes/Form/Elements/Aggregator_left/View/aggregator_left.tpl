<?php /**@var $this ViewObjects*/ ?>
<?php $value = $this->get('value'); $id = Form__getId($this->get('name'));?>
<script type="text/javascript">scriptLoader('form.js').load();</script>
<span id="<?=$id?>_container" class="select-click">
	<?php foreach ($this->get('selectData') as $k => $item) : ?>
		<a href="<?=$item['href']?>" <?= ($k == $value) ? ' class="act"' : '' ?> onclick="formFunctions.selectClick(this, '<?=$id?>', '<?= htmlspecialchars($k) ?>'); return false;"><?= $item['name'] ?></a>
	<?php endforeach ?>

	<input type="hidden" name="<?= $this->get('name') ?>" id="<?=$id?>" value="<?=$value?>"/>
</span>

<style type="text/css">
	.select-click .act {
		color: red;
	}
</style>