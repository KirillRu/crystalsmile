<?php /**@var $this ViewObjects*/ $name = $this->get('name'); $id = Form__getId($name);?>
<textarea id="<?= $id ?>" name="<?= $name ?>" <?= Form__getAttributesString($this->get('attributes', array())) ?>><?= $this->get('value') ?></textarea>
<script type="text/javascript">
	(function(){
		function runCKEDITOR(){
			if (CKEDITOR.instances['<?= $id ?>']) CKEDITOR.instances['<?= $id ?>'].destroy(true);
			if (g('<?= $id ?>')) CKEDITOR.replace('<?= $id ?>', <?=json_encode($this->get('jsParams'))?>);
		};
		if (typeof(CKEDITOR) == 'undefined') scriptLoader('modules/ckeditor/ckeditor.js').callFunction(runCKEDITOR);
		else runCKEDITOR();
	}());
</script>