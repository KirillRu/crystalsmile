<?php
/*Created by Edik (30.07.14 14:22)*/
function FormElementsWordpanel($field, $value, $alias){
	$attrs = (!empty($field['showForm']['attributes']) ? $field['showForm']['attributes'] : array());
	$jsParams = array('defaultLanguage' => 'ru');
	if (!empty($field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $field['showForm']['jsParams']);

	$view = new ViewObjects();
	$view->assign('name', Form__getName($field['nameEn'], $alias));
	$view->assign('value', $value);
	$view->assign('attributes', $attrs);
	$view->assign('jsParams', $jsParams);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Wordpanel/View/wordpanel.tpl');
	return $view;
}