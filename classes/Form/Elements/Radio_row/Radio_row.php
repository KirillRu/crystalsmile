<?php
/*Created by Edik (30.07.14 11:28)*/
require_once(CLASSES_PATH . 'Form/Elements/Select/Select.php');
function FormElementsRadio_row($field, $value, $alias) {
	$view = new FormElementsRadio_row(array('field' => $field, 'value' => $value, 'alias' => $alias));
	return $view->getAction()->run();
}

class FormElementsRadio_row extends FormElementsSelect {

	function run(){
		$attrs = ['class' => 'radiofield', 'type' => 'radio'];
		$jsParams = ['countColumns' => 1];
		if (!empty($this->_field['showForm']['jsParams'])) $jsParams = array_merge($jsParams, $this->_field['showForm']['jsParams']);
		if (!empty($this->_field['showForm']['attributes'])) $attrs = array_merge($attrs, $this->_field['showForm']['attributes']);

		$selectData = $this->_getSelectData();
		if ($this->_value === null) $this->_value = $selectData[0]['value'];

		$view = new ViewObjects();
		$view->assign('alias', $this->_alias);
		$view->assign('name', Form__getName($this->_field['nameEn'], $this->_alias));
		$view->assign('value', $this->_value);
		$view->assign('attributes', $attrs);
		$view->assign('jsParams', $jsParams);
		$view->assign('selectData', $selectData);
		$view->setTpl(CLASSES_PATH . 'Form/Elements/Radio_row/View/radio_row.tpl');
		return $view;
	}

	protected function _getSelectData() {
		$selectData = []; $i=0;
		foreach(parent::_getSelectData() as $value => $name){
			$selectData[$i]['value'] = $value;
			$selectData[$i++]['name'] = $name;
		}
		return $selectData;
	}
}