<?php /**@var $this ViewObjects */
$jsParams = $this->get('jsParams');
$name = $this->get('name');
$value = $this->get('value');
$id = Form__getId($name);
$attrs = $this->get('attributes');
$selectData = $this->get('selectData');
$countOnCol = ceil(count($selectData) / $jsParams['countColumns']);
?>

<?php for ($col = 0; $col < $jsParams['countColumns']; $col++): $start = $col * $countOnCol; ?>
	<div class="div-form-radio-col">
		<?php for ($i = $start; $i < $start + $countOnCol; $i++): if (!empty($selectData[$i])): ?>
			<div class="form-radio-row">
				<div class="form-radio-row-input">
					<input name="<?= $name ?>" id="<?= $id . '_' . $i ?>" value="<?= $selectData[$i]['value'] ?>" <?= ($selectData[$i]['value'] == $value) ? 'checked' : '' ?> <?= Form__getAttributesString($attrs) ?>>
				</div>
				<div class="form-radio-row-text">
					<label for="<?= $id . '_' . $i ?>"><?= $selectData[$i]['name'] ?></label>
				</div>
			</div>
		<?php endif; endfor; ?>
	</div>
<?php
endfor;
if (!empty($jsParams['scriptJs'])) {
	switch ($jsParams['scriptJs']) {
		case 'dancingFields.js': ?><script type="text/javascript ">
			scriptLoader("modules/dancingFields.js").callFunction(function(){dancingFields().init(<?= json_encode(['name'=>$name, 'alias'=>$this->get('alias')]) ?>);});
		</script><?php break;
	}
	//;
}
?>