<?php
/*Created by Edik (18.07.14 14:47)*/
function FormElementsDescription_generator($field, $value, $alias) {
	$name = Form__getName($field['nameEn'], $alias);
	$jsParams = array('idTextarea' => $name, 'alias' => $alias);
	$attrs = array('class' => 'textfield');
	if (!empty($field['showForm']['attributes'])) $attrs = array_merge($attrs, $field['showForm']['attributes']);
	if (!empty($field['showForm']['jsParams']))	$jsParams = array_merge($jsParams, $field['showForm']['jsParams']);

	$view = new ViewObjects();
	$view->assign('name', $name);
	$view->assign('value', $value);
	$view->assign('attributes', $attrs);
	$view->assign('jsParams', $jsParams);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Description_generator/View/description_generator.tpl');
	return $view;
}