<?php
/*Created by Edik (22.07.14 9:24)*/
require_once(CLASSES_PATH . 'Form/Elements/Select/Select.php');
require_once(CLASSES_PATH . 'Form/Elements/Input/Input.php');
function FormElementsSelect_other($field, $value, $alias) {
	$view = new ViewObjects();
	if (!is_array($value)) $value = array();
	$view->select = _FormElementsSelect_otherSelect($field, array_shift($value), $alias);
	$view->input = _FormElementsSelect_otherInput($field, array_shift($value), $alias);
	$view->setTpl(CLASSES_PATH . 'Form/Elements/Select_other/View/select_other.tpl');
	return $view;
}

function _FormElementsSelect_otherSelect($field, $value, $alias){
	if (!empty($field['showForm']['attributes']['select'])) $field['showForm']['attributes'] = $field['showForm']['attributes']['select'];
	if (empty($field['showForm']['attributes']['class'])) $field['showForm']['attributes']['class'] = 'textfield js_selectOther';
	else $field['showForm']['attributes']['class'] .= ' textfield js_selectOther';
	return FormElementsSelect($field, $value, $alias);
}

function _FormElementsSelect_otherInput($field, $value, $alias){
	if ($value == -1){
		$field['showForm']['attributes']['type'] = 'text';
		$field['showForm']['attributes']['class'] = 'textfield';
	} else {
		$field['showForm']['attributes']['type'] = 'hidden';
	}
	if (!empty($field['showForm']['attributes']['input'])) $field['showForm']['attributes'] = $field['showForm']['attributes']['input'];
	$field['nameEn'] .= '_other';
	return FormElementsInput($field, $value, $alias);
}