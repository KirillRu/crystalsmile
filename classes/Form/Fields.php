<?php
/*Created by Edik (23.07.14 11:53)*/

class FormFields {
	protected $_fields = array(), $_values = array(), $_alias = '', $_type = 'type';

	function  __set($name, $value) {
		$name = '_' . $name;
		if (isset($this->$name)) $this->$name = $value;
	}

	function run() {
		if (!$this->_fields) return new View();
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Form/View/fields.tpl');
		$view->assign('rows', $this->getRows());
		return $view;
	}

	function getRows() {
		$rows = [];
		foreach ($this->_getFields() as $iAF => $field) {
			$rows[] = $this->_getViewRow($field, isset($this->_values[$iAF]) ? $this->_values[$iAF] : null);
		}
		return $rows;
	}

	protected function _getViewRow($field, $value){
		return FormRow::get()->viewRow($field, $value, $this->_alias, $this->_type);
	}

	protected function _getFields(){
		return $this->_fields;
	}

}