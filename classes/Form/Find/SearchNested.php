<?php

class FormFindSearchNested {
	private $_structure, $_structureTable;

	function __construct($table) {
		$this->_structureTable = $table;
	}

	/**@param string $searchField в каком поле искать */
	function getNestedNames($searchField = 'name', $condition = array()) {
		$structure = $this->_gs();
		if (empty($structure)) return array();
		return $this->_getItems($searchField, $condition);
	}

	protected function _getItems($searchField, array $condition) {
		$sql = '' .
			'select t.' . $this->_getId() . ', t.id_parent, t.' . $searchField . ' as name, t.leftKey ' .
			'from ' . $this->_getTable() . ' t ' .
			'where ' . implode(' and ', array_merge($condition, $this->_getCondition())) . ' ' .
			'union '.
			'select t.' . $this->_getId() . ', t.id_parent, t.' . $searchField . ' as name, t.leftKey '.
			'from ' . $this->_getTable() . ' t '.
				'join (select id_parent as ' . $this->_getId() . ' from ' . $this->_getTable() . ' where ' . implode(' and ', $this->_getCondition('')) . ' group by id_parent) tT using ('.$this->_getId().') '.
			'where ' . implode(' and ', $condition) . ' ' .
			'order by leftKey ' .
			'';
		$items = ModelsDriverBDSimpleDriver::getDriver()->multiSelect($sql);

		$parents = array();
		if ($this->_getTable() == 'citys') {
			foreach ($items as $i=>$c) {
				if ($c['id_parent'] == 963) {
					$parents[$c['id_city']] = $c;
					unset($items[$i]);
				}
			}
		}
		$pIds = array();
		foreach ($items as $c) if (!in_array($c['id_parent'], $pIds)) $pIds[] = $c['id_parent'];

		if (!empty($pIds)) {
			$sql = ''.
				'select ' . $this->_getId() . ', name '.
				'from ' . $this->_getTable() . '  '.
				'where (' . $this->_getId() . ' in (' . implode(',', $pIds) . ')) '.
				'order by leftKey '.
			'';
			$parents = array_merge($parents, ModelsDriverBDSimpleDriver::getDriver()->multiSelectKey($sql, $this->_getId()));
		}
		$result = array();
		foreach ($parents as $id=>$p) {
			$result[$id]['id'] = $p[$this->_getId()];
			$result[$id]['name'] = $p['name'];
			$result[$id]['childs'] = array();
			foreach ($items as $c) {
				if ($c['id_parent'] == $p[$this->_getId()]) $result[$id]['childs'][] = array('id'=>$c[$this->_getId()], 'name'=>$c['name']);
			}
		}
		return $result;
	}

	protected function _getId() { return $this->_gs()->getId(); }

	protected function _getTable() { return $this->_gs()->getTable(); }

	/**@return ModelsStructureFrom_files */
	private function _gs() {
		if ($this->_structure === null){
			require_once(CLASSES_PATH . 'Models/Structure/From_files/From_files.php');
			$this->_structure = new ModelsStructureFrom_files($this->_structureTable);
		}
		return $this->_structure;
	}

	protected function _getCondition($alias = 't.') { return array('(' . $alias . 'rightKey = (' . $alias . 'leftKey + 1))', '(' . $alias . 'active = 1)'); }
}