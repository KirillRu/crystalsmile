<?php
class FormFindCategorySet{

	function getNestedNames($condition = array()) {
		return $this->_getItems($condition);
	}

	protected function _getItems(array $condition) {
		$sql = '' .
			'select t.id_category, t.id_category_set, t.id_parent, tAC.name, t.leftKey ' .
			'from category_sets t ' .
				'join all_categorys tAC using (id_category) '.
			'where ' . implode(' and ', array_merge($condition, $this->_getCondition())) . ' ' .
			'union '.
			'select t.id_category, t.id_category_set, t.id_parent, tAC.name, t.leftKey '.
			'from category_sets t '.
				'join all_categorys tAC using (id_category) '.
				'join (select id_parent as id_category_set from category_sets where ' . implode(' and ', $this->_getCondition('')) . ' group by id_parent) tT using (id_category_set) '.
			'where ' . implode(' and ', $condition) . ' ' .
			'order by leftKey ' .
			'';
		$items = ModelsDriverBDSimpleDriver::getDriver()->multiSelect($sql);

		$parents = array();
		$pIds = array();
		foreach ($items as $i=>$c) if (!in_array($c['id_parent'], $pIds)) {
			if (empty($c['id_parent'])) {
				$parents[$c['id_category']] = ['id_category_set'=>$c['id_category_set'], 'id_category'=>$c['id_category'], 'name'=>$c['name']];
				unset($items[$i]);
			}
			else $pIds[] = $c['id_parent'];
		}

		if (!empty($pIds)) {
			$sql = ''.
				'select t.id_category_set, t.id_category, tAC.name '.
				'from category_sets t '.
					'join all_categorys tAC using (id_category) '.
				'where (t.id_category_set in (' . implode(',', $pIds) . ')) '.
				'order by t.leftKey '.
			'';
			$parents = array_merge($parents, ModelsDriverBDSimpleDriver::getDriver()->multiSelectKey($sql, 'id_category_set'));
		}
		$result = array();
		foreach ($parents as $id=>$p) {
			$result[$id]['id'] = $p['id_category'];
			$result[$id]['name'] = $p['name'];
			$result[$id]['childs'] = array();
			foreach ($items as $c) {
				if ($c['id_parent'] == $p['id_category_set']) $result[$id]['childs'][] = array('id'=>$c['id_category'], 'name'=>$c['name']);
			}
		}
		return $result;
	}

	protected function _getCondition($alias = 't.') { return array('(' . $alias . 'rightKey = (' . $alias . 'leftKey + 1))', '(' . $alias . 'active = 1)'); }
}