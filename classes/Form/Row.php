<?php
/*Created by Edik (18.07.14 10:45)*/

require_once(FUNCTIONS_PATH . 'Form.php');

class FormRow {
	/**@return FormRow */
	static function get() {
		//переопределять в get() нельзя. Сломается левая часть
		static $self = null;
		if ($self !== null) return $self;
		$self = new self;
		return $self;
	}

	/**
	 * @param $field
	 * @param null $value
	 * @param string $alias
	 * @param string $typeKey прим. typeSearch
	 * @return ViewObjects | View
	 */
	function viewValue($field, $value = null, $alias = null, $typeKey = 'type') {
		$type = $this->_getTypeName($field, $typeKey);
		if (!$type) return new View();
		if ($value === null && isset($field['showForm']['defaultValue'])) $value = $field['showForm']['defaultValue'];

		/*В папке CLASSES_PATH . 'Form/Elements/...' содержатся функции(классы), которые нарисуют нам элемент*/
		/*Проверка на наличие метода нужна для того, что бы была возможность переопределить рисовальщик элемента*/
		if (method_exists($this, $type)){
			$view = $this->$type($field, $value, $alias);
		} else {
			$type = Text::get()->strToUpperFirst($type);
			if (!file_exists(CLASSES_PATH . 'Form/Elements/' . $type . '/' . $type . '.php')) {
				trigger_error('Неправитьный ' . $typeKey . ' (' . $type . ') у поля ' . $field['name']);
				return new View();
			}
			require_once(CLASSES_PATH . 'Form/Elements/' . $type . '/' . $type . '.php');
			$view = call_user_func('FormElements' . $type, $field, $value, $alias);
		}
		if (($view instanceof ViewObjects) && !empty($field['example'])){
			$view->example = new ViewObjects();
			if (isset($field['example']['tpl'])) $view->example->setTpl($field['example']['tpl']);
			$view->example->assign('text', (!empty($field['example']['text']) ? ('Например: ' . $field['example']['text']) : (!empty($field['example']['href']) ? $field['example']['href'] : '')));
		}
		return $view;
	}

	/**Создаём tr(или 2 tr).
	 * @return ViewObjects когда serialize, то во ViewObjects передаются другие данные
	 */
	function viewRow($field, $value = null, $alias = null, $typeKey = 'type') {
		if (empty($field['showForm'])) return new View();
		$type = $this->_getTypeName($field, $typeKey);
		if (!empty($field['dataType']) && ($field['dataType'] == 'serialize' || $field['dataType'] == 'multiFields')) {
			/*Это поле состоит из нескольких полей*/
			$type = Text::get()->strToUpperFirst($type);
			if (!file_exists(CLASSES_PATH . 'Form/Serialize/' . $type . '/' . $type . '.php')) $type = 'Default';
			/*Этот класс нарисует нам поле(или поля)*/
			require_once(CLASSES_PATH . 'Form/Serialize/' . $type . '/' . $type . '.php');
			$className = 'FormSerialize' . $type;
			$handler = new $className(array(
				'field' => $field,
			    'values' => $value,
			    'alias' => $alias,
			    'typeKey' => $typeKey
			));
			return $handler->getAction()->run();
		}

		$fieldValueView = $this->viewValue($field, $value, $alias, $typeKey);
		if (!($fieldValueView instanceof ViewObjects)) return new View();

		/**Cоздаём объект строки tr*/
		$view = new ViewObjects();
		$view->fieldValueView = $fieldValueView;
		$view->fieldNameView = $this->_getNameView($field);
		$view->setTpl(dirname(__FILE__) . '/View/row.tpl');
		return $view;
	}

	/**Возвращает объект: имя(которое рисуется перед самим элементом в форме)
	 * @param $field
	 * @return null|ViewObjects
	 */
	protected function _getNameView($field) {
		if (empty($field['name'])) return null;
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Form/View/name.tpl');
		$view->assign('name', $field['name']);
		$view->assign('required', !empty($field['showForm']['requireField']));
		if (!empty($field['explanationFull'])){
			$view->explanation = new ViewObjects();
			$view->explanation->setTpl(CLASSES_PATH . 'Form/View/explanation.tpl');
			$view->explanation->assign('field', $field);
		}
		return $view;
	}

	private function _getTypeName($field, $typeKey) {
		if (!isset($field[$typeKey])) return '';
		$type = $field[$typeKey];
		switch ($type) {
			case 'textarea_simple':
				return 'textarea';
				break;
			case '':
				return 'text';
				break;
			default:
				return $type;
				break;
		}
	}
}