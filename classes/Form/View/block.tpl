<div class="form-block">
	<?php if ($this->get('blockName')): ?>
		<h2 class="form-header"><?= $this->get('blockName') ?>:</h2>
		<div class="form-block-content">
			<?php $this->fields->display() ?>
		</div>
	<?php else: ?>
		<div class="form-block-content-no-header">
			<?php $this->fields->display() ?>
		</div>
	<?php endif; ?>
</div>