<ul><?
foreach($this->get('items', []) as $idLeaf=>$parent) :
	?><li><a class="find_category ws_items" data-leaf="<?= $parent['id'] ?>"><?= $parent['name'] ?></a><?php if (!empty($parent['childs'])):
		?><ul><?php
		foreach ($parent['childs'] as $item):
			?><li><a class="find_category ws_items" data-leaf="<?= $item['id'] ?>"><?= $item['name'] ?></a></li><?php
		endforeach;
		?></ul><?php
	endif;
	?></li><?php
endforeach;
?></ul><?php
/*
foreach($this->get('items') as $item) :?>
	<div class="ws_items"><a class="find_category" data-leaf="<?= $item['id'] ?>"><?= $item['name'] ?></a></div>
<?php endforeach;
 */