<?php $id = Form__getId($this->get('name')); $jsParams = htmlspecialchars(json_encode($this->get('jsParams'))); ?>
<div class="select-child">
	<?php if ($this->get('linksData')): ?>
		<?php foreach ($this->get('linksData') as $i=>$v):?>
			<?php if ($i):?>
				<div>&nbsp;/&nbsp;</div>
			<?php endif;?>
			<div>
				<a class="pointer" onclick="formFunctions.selectChildUpdate(<?= $v['id_parent'] ?>, <?= $jsParams ?>)"><?= $v['name'] ?></a>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php if ($this->get('selectData')): ?>
		<?php if ($this->get('linksData')): ?><div>&nbsp;/&nbsp;</div><?php endif; ?>
		<div class="select">
			<select class="textfield" onchange="formFunctions.selectChildUpdate(this.value, <?= $jsParams ?>)" title="Выбрать">
				<option>Выбрать..</option>
				<?php foreach ($this->get('selectData') as $v) : ?>
					<option value="<?= $v['id'] ?>"><?= $v['name'] ?></option>
				<?php endforeach ?>
			</select>
		</div>
	<?php endif; ?>
	<input id="<?= $id ?>" name="<?= $this->get('name') ?>" type="hidden" value="<?= $this->get('value', 0) ?>"/>
</div>