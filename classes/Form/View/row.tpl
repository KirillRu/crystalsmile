<?php $attrs = $this->get('rowAttributes'); ?>
<?php $attrs['class'] = !empty($attrs['class']) ? ($attrs['class'] . ' form-edit__row') : 'form-edit__row';?>
<div <?= Form__getAttributesString($attrs); ?>>
	<div class="form-edit__name">
		<?php $this->fieldNameView->display()?>
	</div>
	<div class="form-edit__value">
		<?php $this->fieldValueView->display() ?>
	</div>
	<div class="clr"></div>
</div>