<div class="js_formBlocks">
	<?php foreach ($this->_hiddenRows as $name => $value): ?>
		<input type="hidden" name="<?= $name; ?>" value="<?= $value; ?>"/>
	<?php endforeach; ?>

	<?php foreach ($this->get('blocks') as $block): ?>
	    <?php $block->display(); ?>
	<?php endforeach; ?>
</div>