<?php $id = Form__getId($this->get('name')); $jsParams = json_encode($this->get('jsParams')); ?>
<div class="select-child">
	<?php if ($this->get('linksData')): ?>
		<?php foreach ($this->get('linksData') as $i=>$v):?>
			<?php if ($i):?>
				<div>&nbsp;/&nbsp;</div>
			<?php endif;?>
			<div>
				<a class="pointer" id="js_<?=$id?>_<?=$v['id_parent']?>" onclick=""><?= $v['name'] ?></a>
				<script type="text/javascript" id="js_<?=$id?>_<?=$v['id_parent']?>_script">g('js_<?=$id?>_<?=$v['id_parent']?>').onclick = function(){formFunctions.selectChildUpdate(<?= $v['id_parent'] ?>, <?= $jsParams ?>);};</script>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php if ($this->get('selectData')): ?>
		<?php if ($this->get('linksData')): ?><div>&nbsp;/&nbsp;</div><?php endif; ?>
		<div class="select">
			<select class="textfield" id="<?= $id ?>" name="<?= $this->get('name') ?>" onchange="" title="Выбрать">
				<option value="<?= $this->get('value', 0) ?>">Выбрать..</option>
				<?php foreach ($this->get('selectData') as $v) : ?>
					<option value="<?= $v['id'] ?>"><?= $v['name'] ?></option>
				<?php endforeach ?>
			</select>
			<script type="text/javascript">g('<?=$id?>').onchange = function(){formFunctions.selectChildUpdate(this.value, <?= $jsParams ?>);};</script>
		</div>
	<?php else: ?>
		<input id="<?= $id ?>" name="<?= $this->get('name') ?>" type="hidden" value="<?= $this->get('value', 0) ?>"/>
	<?php endif; ?>
</div>