<?php

/*Created by Edik (23.07.14 10:42)*/
require_once(CLASSES_PATH . 'Form/Block.php');
class Form extends FormBlock{
	protected $_allBlocks = null;

	function __construct($data = []) {
		foreach ($data as $k => $value) {
			$k = '_' . $k;
			if (isset($this->$k)) $this->$k = $value;
		}
	}

	/**@return FormView*/
	function run() {
		require_once(CLASSES_PATH . 'Form/Block.php');
		$blockViews = [];
		foreach ($this->_getBlocks() as $block) {
			if (!empty($block)){
				$this->_block = $block;
				$blockViews[] = parent::run();
			}
		}
		require_once(dirname(__FILE__) . '/View/Form.php');
		$view = new FormView();
		$view->setTpl(CLASSES_PATH . 'Form/View/form.tpl');
		$view->assign('blocks', $blockViews);
		return $view;
	}

	protected function _getBlocks() {
		$allBlocks = null;
		$blocks = [];
		foreach ($this->_fields as $idAllField => $field) {
			if (!empty($field['showForm'])){
				$block = (isset($field['showForm']['idBlock'])) ? $this->_searchBlock($field['showForm']['idBlock']) : [];
				$blockPosition = (isset($block['position']) ? $block['position'] : 100);
				if (!isset($blocks[$blockPosition])){
					$blocks[$blockPosition] = array(
						'blockName' => (!empty($block['name']) ? $block['name'] : $this->_defaultBlockName),
						'fields' => [],
					    'order' => (!empty($block['fields']) ? $block['fields'] : [])//ключи - это NameEn. В каком порядке поля - в таком они и должны быть в форме
					);
				}
				$blocks[$blockPosition]['fields'][$idAllField] = $field;

				$blocks[$blockPosition]['order'][$field['nameEn']] = $idAllField;//заменяем значения на idAllField. Может быть что в order добавятся новые поля(они окажутся в конце)
			}
		}
		ksort($blocks);

		//сортируем поля в соответствии с "order"
		$resultBlocks = [];
		foreach ($blocks as $position => $block) {
			$sortedFields = [];

			//пробегается по order и заполняем поля в нужном порядке
			foreach ($block['order'] as $idAllField) {
				//может оказаться что в order будут те поля, которых нет в массиве fields
				if (isset($block['fields'][$idAllField])) $sortedFields[$idAllField] = $block['fields'][$idAllField];
			}

			$resultBlocks[$position] = [
				'blockName' => $block['blockName'],
			    'fields' => $sortedFields
			];
		}
		return $resultBlocks;
	}

	protected function _searchBlock($idBlock) {
		if ($this->_allBlocks === null){
			$this->_allBlocks = [];
			$idsBlocks = $nameBlocks = [];
			$blockPosition = 0;
			foreach ($this->_fields as $field) {
				if (isset($field['showForm']['idBlock'])){
					if (is_numeric($field['showForm']['idBlock'])) $idsBlocks[$field['showForm']['idBlock']] = true;
					else $nameBlocks[] = $field['showForm']['idBlock'];
				}
			}
			if ($idsBlocks){
				$sql = '' .
					'select id_project_table_block as id_block, name, fields ' .
					'from project_table_blocks ' .
					'where id_project_table_block in (' . (implode(', ', array_keys($idsBlocks))) . ') ' .
					'order by position ' .
				'';

				foreach (ModelsDriverBDCache::get()->multiSelect($sql, ['project_table_blocks']) as $block) {
					$block['fields'] = unserialize($block['fields']);
					$this->_allBlocks[$block['id_block']] = $block;
					$this->_allBlocks[$block['id_block']]['position'] = ++$blockPosition;
				}
			}
			if ($nameBlocks){
				foreach ($nameBlocks as $name) {
					$blockPosition++;
					$this->_allBlocks['t_' . $blockPosition] = [
						'id_block' => 't_' . $blockPosition,
						'name' => $name,
						'position' => $blockPosition
					];
				}
			}
		}
		if (is_numeric($idBlock)){
			if (isset($this->_allBlocks[$idBlock])) return $this->_allBlocks[$idBlock];
		} else {
			foreach ($this->_allBlocks as $block) {
				if ($block['name'] == $idBlock) return $block;
			}
		}
		return [];
	}
}