<?php
/*Created by Edik (21.05.2015 10:10)*/
class TextExtended{

	function multylineHtmlEditor_withoutA($message) {
		$message = Text::get()->multylineHtmlEditor($message);
		$message = preg_replace("/<a[^>]*>|<\/a>/Ui", "", $message);
		return $message;
	}

	function translit_detail($text, $sep = '_') {
		if (defined('SITE_ENCODING')) $text= mb_strtolower($text, SITE_ENCODING);
		else $text= mb_strtolower($text);
		$text = preg_replace("/[\\\,\.\/&\-_]/", " ", $text);
		$text = preg_replace("/[^ \w\dабвгдеёжзийклмнопрстуфхцчшщъыьэюя]/", "", $text);
		$text = trim($text);

		$text = Text::get()->removeDoubleSpaces($text);

		if (strlen($text) > 50) {
			$a = explode(' ', $text);
			if (count($a) > 3) {
				$a1 = array();
				for ($i=0; $i < count($a); $i++) {
					if (strlen($a[$i]) > 2) $a1[] = $a[$i];
					if (count($a1) == 3) break;
				}
				$a = $a1;
			}
			$text = implode(' ', $a);
		}

		$text= strtr($text, array(
			"а"=>"a",
			"б"=>"b",
			"в"=>"v",
			"г"=>"g",
			"д"=>"d",
			"е"=>"e",
			"ё"=>"e",
			"з"=>"z",
			"и"=>"i",
			"й"=>"y",
			"к"=>"k",
			"л"=>"l",
			"м"=>"m",
			"н"=>"n",
			"о"=>"o",
			"п"=>"p",
			"р"=>"r",
			"с"=>"s",
			"т"=>"t",
			"у"=>"u",
			"ф"=>"f",
			"х"=>"h",
			"ъ"=>"b",
			"ы"=>"i",
			"ь"=>"j",
			"э"=>"e",
			"ж"=>"zh",
			"ц"=>"c",
			"ч"=>"ch",
			"ш"=>"sh",
			"щ"=>"sch",
			"э"=>"e",
			"ю"=>"yu",
			"я"=>"ya",
			" "=>$sep
		));
		return $text;
	}

	/**
	 * variats[0] = изделий; variats[1] = изделие; variats[2] = изделия
	 * @static
	 * @param  array $variants
	 * @param  int $amount
	 * @return string
	 */
	function getItemsAmountText($variants, $amount) {
		$amount = $amount % 100;
		if ($amount >= 10 && $amount <= 20)
			return $variants[0];
		$amount %= 10;
		if ($amount > 4 || $amount == 0)
			return $variants[0];
		if ($amount > 1)
			return $variants[2];
		return $variants[1];
	}

	function endOfWord($n, $e1 = "", $e234 = "", $e567890 = ""){
		switch (true){
			case ($n%10 == 1): $r = $e1; break;
			case ($n%10 >= 2 && $n%10 <= 4): $r = $e234; break;
			default: $r = $e567890; break;
		}
		if ($n%100 >= 10 && $n%100 <= 20) $r = $e567890;
		return $r;
	}

	function prepareSearchPhrase($text, $clear = false){
		if ($clear){
			$text = Text::get()->strToLower($text);
			$text = preg_replace("/[^a-zа-я0-9]/iu", '', $text);
			return $text;
		}
		return trim($text);
	}

	function puntoSwitch($text, $source = 'en'){
		$replace = [
	        'й' => 'q',
	        'ц' => 'w',
	        'у' => 'e',
	        'к' => 'r',
	        'е' => 't',
	        'н' => 'y',
	        'г' => 'u',
	        'ш' => 'i',
	        'щ' => 'o',
	        'з' => 'p',
	        'х' => '[',
	        'ъ' => ']',
	        'ф' => 'a',
	        'ы' => 's',
	        'в' => 'd',
	        'а' => 'f',
	        'п' => 'g',
	        'р' => 'h',
	        'о' => 'j',
	        'л' => 'k',
	        'д' => 'l',
	        'ж' => ';',
	        'э' => '\'',
	        'я' => 'z',
	        'ч' => 'x',
	        'с' => 'c',
	        'м' => 'v',
	        'и' => 'b',
	        'т' => 'n',
	        'ь' => 'm',
	        'б' => ',',
	        'ю' => '.',
	        'Й' => 'Q',
	        'Ц' => 'W',
	        'У' => 'E',
	        'К' => 'R',
	        'Е' => 'T',
	        'Н' => 'Y',
	        'Г' => 'U',
	        'Ш' => 'I',
	        'Щ' => 'O',
	        'З' => 'P',
	        'Х' => '{',
	        'Ъ' => '}',
	        'Ф' => 'A',
	        'Ы' => 'S',
	        'В' => 'D',
	        'А' => 'F',
	        'П' => 'G',
	        'Р' => 'H',
	        'О' => 'J',
	        'Л' => 'K',
	        'Д' => 'L',
	        'Ж' => ':',
	        'Э' => '"',
	        'Я' => 'Z',
	        'Ч' => 'X',
	        'С' => 'C',
	        'М' => 'V',
	        'И' => 'B',
	        'Т' => 'N',
	        'Ь' => 'M',
	        'Б' => '<',
	        'Ю' => '>'
		];
		if ($source == 'ru') $text = strtr($text, $replace);
		elseif ($source == 'en') $text = strtr($text, array_flip($replace));
		return trim($text);
	}

//	function getMenuTemplateReplace($typePage, $moduleName, $dop = ''){
//		if (empty($typePage) && empty($moduleName)) return null;
//		if (!empty($moduleName)) $mn = explode(',', $moduleName);
//		else $mn = array('all');
//		if (!empty($typePage)) $tp = explode(',', $typePage);
//		else $tp = array('all');
//		if ($dop != '') $dop = '-' . $dop;
//		$replaceable = array();
//		foreach ($mn as $m){
//			foreach ($tp as $t){
//				$replaceable[] = Text::get()->strToLower('{'.$t.'-'.$m.$dop.'}');
//			}
//		}
//		return implode('', $replaceable);
//	}
//
//	function getMenuCacheReplace($typePage, $moduleName, $replaceTo, $dop = ''){
//		if ($dop != '') $dop = '-' . $dop;
//		return array(
//			Text::get()->strToLower('{'.$typePage.'-'.$moduleName.$dop.'}') => $replaceTo,
//			Text::get()->strToLower('{all-'.$moduleName.$dop.'}') => $replaceTo,
//			Text::get()->strToLower('{'.$typePage.'-all'.$dop.'}') => $replaceTo,
//		);
//	}

	/**@return TextExtended*/
	static function get() {
		static $self = null;
		if ($self === null) $self = new TextExtended();
		return $self;
	}
}