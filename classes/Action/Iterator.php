<?php
/*Created by Edik (06.08.2015 17:10)*/
abstract class ActionIterator implements Iterator, Countable {
	protected $_items, $_count, $_position;

	function __construct($items) {
		$this->_items = $items;
		$this->_count = count($this->_items);
		$this->_position = 0;
	}

	function rewind() {
		$this->_position = 0;
	}

	abstract function current();

	function key() {
		return $this->_position;
	}

	function next() {
		$this->_position++;
	}

	function valid() {
		return $this->_count > $this->_position;
	}

	function count(){
		return $this->_count;
	}
}