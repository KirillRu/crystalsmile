<?php
interface Action {
	/**
	 * @abstract
	 * @return \View
	 */
	function run();
	function getAction();
}

abstract class AbstractAction implements Action {

	protected $_data = array();

	function __construct($data = array()) {
		$this->_data = $data;
	}

	function getAction() {
		return $this;
	}

	function g($field, $default = null) {
		return isset($this->_data[$field]) ? $this->_data[$field] : $default;
	}

	function gav($arrName, $key, $default = null) {
		return isset($this->_data[$arrName][$key]) ? $this->_data[$arrName][$key] : $default;
	}

	function s($field, $value) {
		$this->_data[$field] = $value;
	}

	protected function _getData() {
		return $this->_data;
	}
}