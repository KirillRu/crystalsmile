<?php
/*Created by Edik (26.11.14 15:16)*/
class FieldsCheck{

	function checkFields($fields, $values, $typeKey = 'type') {
		$errors = array();
		foreach ($fields as $idAllField => $field) {
			$fieldErrors = $this->checkField($field, isset($values[$idAllField]) ? $values[$idAllField] : null, $typeKey);
			if ($fieldErrors) $errors[$idAllField] = (count($fieldErrors) == 1) ? array_shift($fieldErrors) : $fieldErrors;
		}
		return $errors;
	}

	function checkField($field, $value, $typeKey = 'type'){
		if (!empty($field['showForm'])){
			$fieldErrors = array();
			if ($field['dataType'] == 'serialize' || $field['dataType'] == 'multiFields'){
				$fieldErrors = $this->_checkSerializeField($field, $value, $typeKey);
			} else {
				if (!empty($field['showForm']['requireField'])){
					$fieldErrors = $this->checkRequiredField($field, $value, $typeKey);
				}
				if (!$fieldErrors && !in_array($field[$typeKey], array('multi_string', 'select_other'))){
					$fieldErrors = $this->checkByDataType($field, $value);
				}
			}
			return $fieldErrors;
		}
		return array();
	}

	private function _checkSerializeField($field, $value, $typeKey){
		/*Для этах dataType специальные обработчики ошибок в папке CLASSES_PATH . 'FieldsCheck'. Если его нет, то запускается default*/
		$checkName = 'serialize_' . (!empty($field[$typeKey]) ? $field[$typeKey] : 'default');
		if (!file_exists(CLASSES_PATH . 'FieldsCheck/' . Text::get()->strToUpperFirst($checkName) . '.php')){
			$checkName = 'serialize_default';
		}
		$path = CLASSES_PATH . 'FieldsCheck/' . Text::get()->strToUpperFirst($checkName) . '.php';
		require_once($path);
		/*Класс возвращает массив ошибок. Глубина массива может быть любой.*/
		return call_user_func('FieldsCheck__' . $checkName, $field, $value, $typeKey);
	}

	protected function _specialCheck($field, $value, $checkName){
		$path = CLASSES_PATH . 'FieldsCheck/' . Text::get()->strToUpperFirst($checkName) . '.php';
		require_once($path);
		return call_user_func('FieldsCheck__' . $checkName, $field, $value);
	}

	function checkRequiredField($field, $value, $typeKey) {
		switch ($field[$typeKey]) {
			case 'select_category':
			case 'select_child':
				$fieldErrors = $this->_specialCheck($field, $value, 'select_child');
			break;
			case 'date_interval':
			case 'date_calendar_interval':
			case 'price_interval':
			case 'numeric_interval':
				$fieldErrors = $this->_specialCheck($field, $value, 'numeric_interval');
			break;
			case 'radio_row_other':
			case 'select_other':
				$fieldErrors = $this->_specialCheck($field, $value, 'select_other');
				break;
			case 'captcha': $fieldErrors = $this->checkCaptcha($value); break;
			case 'multi_string': $fieldErrors = $this->_specialCheck($field, $value, 'multi_string'); break;
			default: $fieldErrors = $this->checkEmpty($value, $field['name']); break;
		}
		return $fieldErrors;
	}

	function checkByDataType($field, $value) {
		$error = array();
		switch ($field['dataType']) {
			case 'position':
			case 'tinyint':
				$error = $this->checkNumeric($value, $field['name']);
				break;
			case 'integer':
			case 'double':
				$error = $this->checkInteger($value, $field['name']);
				break;
			case 'date':
				$dateStart = isset($value[$field['nameEn'] . '_start']) ? $value[$field['nameEn'] . '_start'] : null;
				$dateEnd = isset($value[$field['nameEn'] . '_end']) ? $value[$field['nameEn'] . '_end'] : null;
				$error = $this->checkDate($dateStart, $dateEnd, $field['name']);
				break;
			case 'time':
				$error = $this->checkNumeric($value, $field['name']);
				break;
			case 'datetime':
				$error = $this->checkNumeric($value, $field['name']);
				break;
			case 'email':
				$error = $this->checkEmail($value, $field['name']);
				break;
		}
		return $error;
	}

	function checkEmail($s, $name) {
		if ($s && !preg_match('/^[\w\d-_]+(\.[\w\d-_]+)*@[\w\d-]+(\.[\w\d-]+)+$/iu', $s)) return array('Неправильно заполнен ' . $name);
		return array();
	}

	function checkEmpty($s, $name) {
		if (is_string($s)) $s = trim($s);
		if (!$s) return array('Не указали, либо указали неправильно ' . $name);
		return array();
	}

	function checkDate($dateStart, $dateEnd, $name) {
		if ($dateEnd === null){
			return $this->checkNumeric($dateStart, $name);
		} else {
			return array_merge($this->checkNumeric($dateStart, 'Начальную ' . $name), $this->checkNumeric($dateEnd, 'Конечную ' . $name));
		}
	}

	function checkNumeric($i, $name) {
		if ($i && !is_numeric($i)) return array('Не правильно указали ' . $name);
		return array();
	}

	function checkInteger($s, $name) {
		if (is_array($s)){
			$error = $this->checkNumeric(array_shift($s), $name);
			if ($error) return $error;
			if ($s)	return $this->checkNumeric(array_shift($s), $name);
		} else {
			return $this->checkNumeric($s, $name);
		}
		return array();
	}

	function checkCaptcha($captcha) {
		$errors = [];
		if (empty($captcha) || !isset($_SESSION['captcha_keystring']) || ($_SESSION['captcha_keystring'] != strtolower($captcha))) $errors = ['Проверочный код введен неверно'];
		unset($_SESSION['captcha_keystring']);
		return $errors;
	}

	/**@return FieldsCheck */
	static function get() {
		static $self = null;
		if ($self !== null) return $self;
		$self = new self;
		return $self;
	}
}