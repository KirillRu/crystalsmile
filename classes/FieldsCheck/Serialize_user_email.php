<?php
/*Created by Edik (27.11.14 10:58)*/
function FieldsCheck__serialize_user_email($field, $values, $typeKey){
	$structure = $field['idSpr']->getStructure();
	$fieldValue = $structure->getField('value');

	$email = $values[$fieldValue['idAllField']];
	if (!$email) {
		$errors = ['Не указали, либо указали неправильно ' . $fieldValue['name']];
	} else {
		require_once(CLASSES_PATH . 'FieldsCheck/Serialize_user_contacts.php');
		$errors = FieldsCheck__serialize_user_contacts_checkEmailRow($fieldValue, $email, $typeKey);
	}

	if (!$errors && ModelsDriverBDSimpleDriver::getDriver()->fieldSelect('select 1 from users where login = ' . Text::get()->mest($email))) {
		$errors = ['Указанный электронный адрес уже зарегистрирован'];
	}

	return $errors;
}