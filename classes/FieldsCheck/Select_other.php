<?php
/*Created by Edik (26.11.14 16:13)*/
function FieldsCheck__select_other($field, $value) {
	$selectValue = isset($value[$field['nameEn']]) ? $value[$field['nameEn']] : null;
	$otherValue = isset($value[$field['nameEn'] . '_other']) ? $value[$field['nameEn'] . '_other'] : null;

	if ($selectValue == -1) return FieldsCheck::get()->checkEmpty($otherValue, $field['name']);
	return FieldsCheck::get()->checkEmpty($selectValue, $field['name']);
}