<?php
/*Created by Edik (27.11.14 10:58)*/
function FieldsCheck__serialize_user_phones($field, $values, $typeKey){
	$errors = array();
	$fieldValue = $field['idSpr']->getStructure()->getField('value');
	$publishFieldIAF = $field['idSpr']->getStructure()->getField('publish')['idAllField'];
	$havePublishPhone = false;
	foreach($values as $i => $value){
		$error = FieldsCheck::get()->checkField($fieldValue, $value[$fieldValue['idAllField']], $typeKey);
		foreach($error as &$text) $text = 'Телефоны №' . ($i + 1) . ': ' . $text;
		if ($error) $errors[] = (count($error) == 1) ? array_shift($error) : $error;

		//хотя бы 1 телефон должен быть публичным
		$havePublishPhone = $havePublishPhone || (bool)$value[$publishFieldIAF];
	}
	if (!$havePublishPhone) $errors[] = 'Хотя бы один телефон должен быть публичным';
	return $errors;
}