<?php
/*Created by Edik (26.11.14 16:12)*/
function FieldsCheck__select_child($field, $value) {
	if (!$value) return array('Не указали ' . $field['name']);
	if (!empty($field['idSpr'])){
		$structure = $field['idSpr']->getStructure();
		if ($structure->getTable()){

			if ($structure->getTable() != 'category_sets'){
				$sql = '' .
					'select 1 ' .
					'from ' . $structure->getTable() . ' ' .
					'where (' . $structure->getId() . ' = ' . (int)$value . ') and (leftKey+1 = rightKey) ' .
					'';
			} else {
				$sql = '' .
					'select 1 ' .
					'from category_sets ' .
					'where (id_category = ' . (int)$value . ') and (leftKey+1 = rightKey) ' .
					'limit 1';
			}
			if (!ModelsDriverBDSimpleDriver::getDriver()->fieldSelect($sql)) return array('Не указали, либо указали неправильно ' . $field['name']);
		}
	}
	return array();
}