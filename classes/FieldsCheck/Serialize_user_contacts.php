<?php
/*Created by Edik (27.11.14 10:58)*/
function FieldsCheck__serialize_user_contacts($field, $values, $typeKey){
	$errors = array();
	$structure = $field['idSpr']->getStructure();
	$haveEmail = $havePublishEmail = false;
	$typeField = $structure->getField('id_type');
	$publishFieldIAF = $structure->getField('publish')['idAllField'];
	$fieldValue = $structure->getField('value');
	$selectItems = $typeField['idSpr']->getItems();

	foreach($values as $i => $value){
		$fieldValue['name'] = $selectItems[$value[$typeField['idAllField']]];
		if ($value[$typeField['idAllField']] == 1) {
			$error = FieldsCheck__serialize_user_contacts_checkEmailRow($fieldValue, $value[$fieldValue['idAllField']], $typeKey);
			$haveEmail = true;

			//хотя бы 1 email должен быть публичным
			$havePublishEmail = $havePublishEmail || (bool)$value[$publishFieldIAF];
		} else {
			$error = FieldsCheck::get()->checkField($fieldValue, $value[$fieldValue['idAllField']], $typeKey);
		}
		if ($error) {
			foreach($error as &$text) $text = 'Контакты №' . ($i + 1) . ': ' . $text;
			$errors[] = (count($error) == 1) ? array_shift($error) : $error;
		}
	}
	if (!$haveEmail) $errors[] = 'Необходимо указать хотя бы один e-mail';
	if (!$havePublishEmail) $errors[] = 'Хотя бы один e-mail должен быть публичным';
	return $errors;
}

function FieldsCheck__serialize_user_contacts_checkEmailRow($fieldValue, $value, $typeKey){
	require_once(FUNCTIONS_PATH . 'Registration.php');
	$fieldValue['dataType'] = 'email';
	$errors = FieldsCheck::get()->checkField($fieldValue, $value, $typeKey);
	if (!$errors) {
		$newEmail = true;
		if (SitesAuth::get()->id_user())
			/*Не нужно проверять email у самого себя, поэтому не делаем проверку если значение совпадает со своим email"om*/
			foreach (SitesAuth::get()->getUser('user_contacts') as $contact) {
				if (($contact['id_type'] == 1) && ($contact['value'] == $value)) {
					$newEmail = false;
					break;
				}
			}
		if ($newEmail) $errors = Registration__checkEmail($value);
	}
	return $errors;
}