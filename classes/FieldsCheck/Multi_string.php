<?php
/*Created by Edik (26.11.14 16:14)*/
function FieldsCheck__multi_string($field, $value) {
	$error = FieldsCheck::get()->checkEmpty($value, $field['nameEn']);
	if ($error) return $error;
	foreach ($value as $val) {
		$error = FieldsCheck::get()->checkByDataType($field, $val);
		if ($error) return $error;
	}
	return '';
}
