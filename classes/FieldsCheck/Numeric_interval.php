<?php
/*Created by Edik (26.11.14 16:12)*/
function FieldsCheck__numeric_interval($field, $value) {
	$dateStart = isset($value[$field['nameEn'] . '_start']) ? $value[$field['nameEn'] . '_start'] : null;
	$dateEnd = isset($value[$field['nameEn'] . '_end']) ? $value[$field['nameEn'] . '_end'] : null;
	return array_merge(FieldsCheck::get()->checkEmpty($dateStart, 'Начальную ' . $field['name']), FieldsCheck::get()->checkEmpty($dateEnd, 'Конечную ' . $field['name']));
}