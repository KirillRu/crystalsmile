<?php
/*Created by Кирилл (13.02.2016 23:48)*/
require_once(PROJECT_PATH . 'Modules/Forms/Action/Add.php');
class ZubiModulesFormsOrdersActionAdd extends ZubiModulesFormsActionAdd {

    function run() {
        $view = parent::run();
        $view->setTpl(PROJECT_PATH . 'Panels/Forms/View/orders.tpl');
//	    $view->goods = $this->_viewGoods();
        return $view;
    }

//	protected function _viewGoods() {
//		$data = [
//			'alias'=>'good',
//			'moduleName'=>'order_goods',
//		];
//		$file = CLASSES_PATH . 'Zubi/Panels/Forms/Action/Order_goods.php';
//		require_once($file);
//		return (new ZubiPanelsFormsActionOrder_goods($data))->getAction()->run();
//	}

}
