<?php
/*Created by Кирилл (14.02.2016 15:16)*/
require_once(PROJECT_PATH . 'Modules/Forms/Action/AddConfirm.php');
class ZubiModulesFormsOrdersActionAddConfirm extends ZubiModulesFormsActionAddConfirm {

	protected function _add() {
		$handler = $this->_getInsertManager();
		$handler->addValuesToSql([
			'date_add'=>time(),
			'new_order'=>1,
		]);

		return $handler->insertValuesByStructure();
	}

	protected function _success(){
		$view = new ViewErrors(['Заказ сформирован успешно.<br>С Вами свяжутся в ближайшее время'], 'edit-message');
		return $view;
	}


}
