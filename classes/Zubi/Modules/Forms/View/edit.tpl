<?php /**@var $this ViewObjects*/?>
<?php Head::get()->echoScriptOptions('formData')?>
<?php $this->errors->display(); ?>
<form onsubmit="management.form.requestInPopup('runModule/Forms/editConfirm.ajax', element.getParameters(this)); return false;">

	<?php foreach ($this->_widgets as $name => $widget): ?>
	    <?php if (!in_array($name, ['form', 'errors'])) $widget->display(); ?>
	<?php endforeach; ?>

	<?php $this->form->display(); ?>

	<button type="submit" class="button">Изменить</button>
</form>