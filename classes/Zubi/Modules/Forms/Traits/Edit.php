<?php
/*Created by ������ (21.01.2016 10:47)*/
//��������� �������� �������, ����������� �� ���� ������� �������������� (� �����)
require_once(CLASSES_PATH . 'Management/Modules/Forms/Traits/Form.php');
trait ManagementModulesFormsTraitsEdit {
	use ZubiModulesFormsTraitsForm;

	protected function _getValues(){
		return $this->_traitGetValues();
	}

	protected function _traitGetValues(){
		$formValues = $this->_getFormValues();
		if ($formValues) return $formValues;

		return $this->_getValuesFromBase();
	}

	protected function _getValuesFromBase(){
		require_once(CLASSES_PATH . 'Models/Structure/QueryBuilder.php');
		$sql = (new ModelsStructureQueryBuilder($this->_getStructure()))->get('showForm', ['t.' . $this->_getStructure()->getId() . ' = ' . $this->_getFormData('id')]);
		return ModelsDriverBDSimpleDriver::getDriver()->rowSelect($sql);
	}
}