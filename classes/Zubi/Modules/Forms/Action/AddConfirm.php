<?php
/*Created by Кирилл (21.01.2016 17:17)*/
require_once(CLASSES_PATH . 'View/Errors.php');
require_once(CLASSES_PATH . 'Zubi/Modules/Forms/Traits/Form.php');
class ZubiModulesFormsActionAddConfirm extends AbstractAction {
	use ZubiModulesFormsTraitsForm;

	function getAction() {
		if (static::class === self::class){
			$table = Text::get()->strToUpperFirst($this->_getModuleName());
            if (empty($table)) {
                exit;
            }
			$file = CLASSES_PATH . 'Zubi/Modules/Forms/' . $table . '/Action/AddConfirm.php';
			if (file_exists($file)){
				require_once($file);
				$className = 'ZubiModulesForms' . $table . 'ActionAddConfirm';
				return (new $className)->getAction();
			}
		}
		return parent::getAction();
	}

	function run(){
		$errors = $this->_check();
		if (!$errors && !$this->_add()) $errors = ['Ошибка при добавлении'];

		if ($errors){
			$view = $this->_getView();
			$view->errors = new ViewErrors($errors);
		} else {
			$view = $this->_success();
		}
		return $view;
	}

	protected function _success(){
		$view = $this->_getView();
		$view->errors = new ViewErrors(['Добавление успешно'], 'edit-message');
		return $view;
	}

	protected function _getView(){
		require_once(dirname(__FILE__) . '/Add.php');
		return (new ZubiModulesFormsActionAdd())->getAction()->run();
	}

	protected function _check(){
		return FieldsCheck::get()->checkFields($this->_getFields(), $this->_getStructure()->getValues($this->_getFormValues()));
	}

	protected function _add(){
		return $this->_getInsertManager()->insertValuesByStructure();
	}

	protected function _getInsertManager(){
		$structure = $this->_getStructure();
		require_once(CLASSES_PATH . 'Models/DriverBD/Managers/Structure.php');
		$manager = new ModelsDriverBDManagersStructure($structure, $structure->getValues($this->_getFormValues()));
		if ($structure->isPosition()) {
			$sql = 'select max(position) from ' . $structure->getTable();
			$manager->addValuesToSql([
				'position' => (int) ModelsDriverBDSimpleDriver::getDriver()->fieldSelect($sql) + 1,
			]);
		}
		return $manager;
	}
}