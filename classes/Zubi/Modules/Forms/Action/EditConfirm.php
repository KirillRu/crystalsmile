<?php
/*Created by Кирилл (21.01.2016 11:06)*/
require_once(CLASSES_PATH . 'View/Errors.php');
require_once(CLASSES_PATH . 'Management/Modules/Forms/Traits/Edit.php');
class ManagementModulesFormsActionEditConfirm extends AbstractAction {
	use ManagementModulesFormsTraitsEdit;

	function getAction(){
		if (static::class === self::class){
			$table = Text::get()->strToUpperFirst($this->_getModuleName());
			$file = CLASSES_PATH . 'Management/Modules/Forms/' . $table . '/Action/EditConfirm.php';
			if (file_exists($file)){
				require_once($file);
				$className = 'ManagementModulesForms' . $table . 'ActionEditConfirm';
				return (new $className)->getAction();
			}
		}
		return parent::getAction();
	}

	function run(){
		$errors = $this->_check();
		if (!$errors) $errors = $this->_edit();

		if ($errors){
			$view = $this->_getView();
			$view->errors = new ViewErrors($errors);
		} else {
			$view = $this->_success();
		}
		return $view;
	}

	protected function _success(){
		$view = $this->_getView();
		$view->errors = new ViewErrors(['Редактирование успешно'], 'edit-message');
		return $view;
	}

	protected function _getView(){
		require_once(dirname(__FILE__) . '/Edit.php');
		return (new ManagementModulesFormsActionEdit())->getAction()->run();
	}

	protected function _check(){
		return FieldsCheck::get()->checkFields($this->_getFields(), $this->_getStructure()->getValues($this->_getFormValues()));
	}

	protected function _edit(){
		require_once(CLASSES_PATH . 'Models/DriverBD/Managers/Structure.php');
		$handler = new ModelsDriverBDManagersStructure($this->_getStructure(), $this->_getStructure()->getValues($this->_getFormValues()));
		$success = $handler->updateValuesByStructure($this->_getFormData('id'));
		if ($success){
			//чистим кэш этого пользователя для этой таблицы если в formData был записан id_user
			if ($this->_getFormData('id_user')) {
				ModelsCacheMemcache::get()->formatting($this->_getStructure()->getTable() . '-' . $this->_getFormData('id_user'));
			}
			return false;
		}
		return ['Ошибка при редактировании'];
	}
}