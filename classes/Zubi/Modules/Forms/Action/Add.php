<?php
/*Created by ������ (21.01.2016 17:12)*/
require_once(CLASSES_PATH . 'Zubi/Modules/Forms/Traits/Form.php');
class ZubiModulesFormsActionAdd extends AbstractAction {
	use ZubiModulesFormsTraitsForm;

	function getAction(){
		if (static::class === self::class){
			$table = Text::get()->strToUpperFirst($this->_getModuleName());
			$file = CLASSES_PATH . 'Zubi/Modules/Forms/' . $table . '/Action/Add.php';
			if (file_exists($file)){
				require_once($file);
				$className = 'ZubiModulesForms' . $table . 'ActionAdd';
				return (new $className)->getAction();
			}
		}
		return parent::getAction();
	}

	function run(){
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Zubi/Modules/Forms/View/add.tpl');
		$view->form = $this->_getForm();

		Head::get()->appendOptions($this->_getFormData(), 'formData');
		return $view;
	}

	protected function _getForm(){
		$data = $this->_getPanelData();

		$moduleName = Text::get()->strToUpperFirst($this->_getModuleName());
		$file = CLASSES_PATH . 'Zubi/Panels/Forms/Action/' . $moduleName . '.php';
		if (file_exists($file)){
			require_once($file);
			$className = 'ZubiPanelsFormsAction' . $moduleName;
			return (new $className($data))->getAction()->run();
		}
		require_once(CLASSES_PATH . 'Zubi/Panels/Forms/Action/Forms.php');
		return (new ZubiPanelsFormsAction($data))->getAction()->run();
	}

	protected function _getPanelData(){
		$moduleName = $this->_getModuleName();
		$data = ['moduleName' => $moduleName, 'alias' => $moduleName, $moduleName => $this->_getFormValues(), 'defaultBlockName'=>$this->g('defaultBlockName')];
		return $data;
	}
}