<?php /**@var $this ViewObjects*/ $meta = $this->get('meta');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title><?= $meta['title'] ?></title>
    <meta name="description" content="<?= $meta['description'] ?>">
    <meta name="keywords" content="<?= $meta['keywords'] ?>">
    <meta content="all" name="robots">
    <link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/index_0.css">
   	<link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/tool.css">
   	<link rel=stylesheet type="text/css" href="<?= SITE_ROOT ?>css/service.css">
    <link rel="stylesheet" type="text/css" href="<?= SITE_ROOT ?>css/site.css">
    <script type="text/javascript" src="<?= SITE_ROOT ?>js/jquery.min.js"></script>
   	<script type="text/javascript" src="<?= SITE_ROOT ?>js/jquery-ui.min.js"></script>
   	<script type="text/javascript" src="<?= SITE_ROOT ?>js/jquery.cookie.min.js"></script>
   	<script type="text/javascript" src="<?= SITE_ROOT ?>js/modules/scriptLoader.js"></script>
   	<script type="text/javascript" src="<?= SITE_ROOT ?>js/modules/popup.js"></script>
   	<script type="text/javascript" src="<?= SITE_ROOT ?>js/functions.js"></script>
   	<script type="text/javascript" src="<?= SITE_ROOT ?>js/service.js"></script>
	<script type="text/javascript" src="<?= SITE_ROOT ?>js/sites.js"></script>
</head>
<body class="html"><div id="page"><?php
    $this->top->display();
    ?><h1><?= $meta['h1'] ?></h1><?php
    $this->content->display();
    $this->bottom->display();
?>
    <div class="footer-license"><div class="footer-copyright">
    © <a href="<?= Path::get()->getUrl() ?>"><?= Path::get()->getUrl() ?></a>, 2016<br>Электронная почта: <a href=""></a>
    </div></div>
</div></body>
</html>


