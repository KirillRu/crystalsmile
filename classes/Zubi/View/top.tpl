<?php /**@var $this ViewObjects*/
$settings = $this->get('settings');
?>
<div id="js_hat">
	<div id="js_hatLogo" style="top:<?= $settings['js_hatLogo']['top'] ?>px; left:<?= $settings['js_hatLogo']['left'] ?>px;"><img src="<?= Path::get()->getUrl() ?>img/top_logo.jpg"></div>
    <div id="js_hatContacts" style="top:<?= $settings['js_hatContacts']['top'] ?>px; left:<?= $settings['js_hatContacts']['left'] ?>px;"><?php $this->contacts->display(); ?></div>
    <div id="js_hatText" style="top:<?= $settings['js_hatText']['top'] ?>px; left:<?= $settings['js_hatText']['left'] ?>px;"><?php $this->text->display(); ?></div>
</div>
