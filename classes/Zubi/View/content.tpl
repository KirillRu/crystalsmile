<?php /**@var $this ViewObjects*/ ?>
<?php foreach ($this->get('contentBlocks', []) as $item): if (file_exists(CLASSES_PATH . $item['text'])): ?>
	<div class="p15" <?php if (!empty($item['color'])): ?>style="background-color: #<?= $item['color'] ?>;"<?php endif; ?>>
		<?php if (!empty($item['h2'])): ?><h2><?= $item['h2'] ?></h2><?php endif; ?>
		<?php include(CLASSES_PATH . $item['text']); ?>
		<div class="clr"></div>
	</div>
<?php endif; endforeach; ?>
