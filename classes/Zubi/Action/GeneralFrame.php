<?php
class ZubiActionGeneralFrame extends AbstractAction {

	function run() {
		$view = new ViewObjects();
		$view->setTpl(CLASSES_PATH . 'Zubi/View/general_frame.tpl');

		$view->top = $this->_viewTop();
        $view->bottom = $this->_viewBottom();
        $view->content = $this->_viewContent();
        $view->assign('meta', $this->_getMeta());
 		return $view;
	}

	protected function _viewTop() {
		$top = new ViewObjects();
		$top->setTpl(CLASSES_PATH . 'Zubi/View/top.tpl');
        $defaultSettings = [
            'js_hatLogo'=>['left'=>0,'top'=>0],
            'js_hatContacts'=>['left'=>0,'top'=>0],
            'js_hatText'=>['left'=>0,'top'=>0],
        ];
        if (Data::get()->g('preview')) $makeup = Functions__mergeArrays($defaultSettings, include(PROJECT_PATH . 'Settings/hat_preview.php'));
        else $makeup = Functions__mergeArrays($defaultSettings, include(PROJECT_PATH . 'Settings/hat.php'));

        require_once(CLASSES_PATH . 'View/Text.php');
        if (isset($makeup['js_hatContacts']['text'])) {
            $top->contacts = new ViewText($makeup['js_hatContacts']['text']);
            unset($makeup['js_hatContacts']['text']);
        }
        if (isset($makeup['js_hatText']['text'])) {
            $top->text = new ViewText($makeup['js_hatText']['text']);
            unset($makeup['js_hatText']['text']);
        }
        $top->assign('settings', $makeup);

		return $top;
	}

    protected function _viewBottom() {
        Data::get()->s(['moduleName' => 'send_letters']);
        require_once(PROJECT_PATH . 'Modules/Forms/Action/Add.php');
        $handler = new ZubiModulesFormsActionAdd();
        $view = $handler->getAction()->run();
        return $view;
    }

    protected function _viewContent() {
        $view = new ViewObjects();
	    $view->setTpl(PROJECT_PATH . 'View/content.tpl');
	    require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Site_content/SerializeText.php');
	    $view->assign('contentBlocks', new ManagementModulesListIteratorsSite_contentSerializeText());
        return $view;
    }

    protected function _getMeta() {
        $meta = ['title'=>'', 'description'=>'', 'keywords'=>'', 'h1'=>''];
        $fileContent = CLASSES_PATH . 'Zubi/FileData/meta.db';
        if (file_exists($fileContent)) $meta = array_merge($meta, unserialize(file_get_contents($fileContent)));
        return $meta;
    }

}
 