<?php
class ZubiActionDefaultPage extends AbstractAction {

    function run() {
	    require_once(CLASSES_PATH . 'Zubi/Action/GeneralFrame.php');
 		$view = (new ZubiActionGeneralFrame())->getAction()->run();
	    $view->content = $this->_viewContent();
	    return $view;
    }

	protected function _viewContent() {
		$moduleName = Text::get()->strToUpperFirst(Data::get()->g('moduleName'));
		$actionFile = CLASSES_PATH . 'Zubi/Modules/Action/' . $moduleName . '.php';
		if (file_exists($actionFile)) {
			require_once($actionFile);
			$action = 'ZubiModulesAction' . $moduleName;
			return (new $action)->getAction()->run();
		}
		require_once(CLASSES_PATH . 'View/Text.php');
		return new ViewText('Страница не найдена');
	}

}