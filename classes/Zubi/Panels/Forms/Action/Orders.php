<?php
/*Created by Кирилл (13.02.2016 23:50)*/
require_once(PROJECT_PATH . 'Panels/Forms/Action/Forms.php');
class ZubiPanelsFormsActionOrders extends ZubiPanelsFormsAction {
	protected function _getDataFromForm() {
        $data = parent::_getDataFromForm();
        $data['defaultBlockName'] = '';
        return $data;
    }

	protected function _getFields() {
		$fields = parent::_getFields();
		if (isset($fields['goods_0'])) {
			$file = CLASSES_PATH . 'Zubi/FileData/goods.db';
			if (file_exists($file)) {
				require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Goods/SerializeText.php');
				$fields['goods_0']['showForm']['jsParams']['maxCount'] = (new ManagementModulesListIteratorsGoodsSerializeText())->count();
//				$fields['goods_0']['showForm']['jsParams']['maxCount'] = 1;
			}
		}
		return $fields;
	}

	protected function _getValues(){
		$values = parent::_getValues();
		if (empty($values['goods_0'])) {
			$file = CLASSES_PATH . 'Zubi/FileData/goods.db';
			if (file_exists($file)) {
				require_once(CLASSES_PATH . 'Management/Modules/List/Iterators/Goods/SerializeText.php');
				$field = $this->_getStructure()->getField('goods')['idSpr']->getStructure()->getField('id_good');
				foreach (new ManagementModulesListIteratorsGoodsSerializeText() as $item) {
					$values['goods_0'][] = [$field['idAllField']=>$item['id_good']];
				}
			}
		}
		return $values;
	}


}