<?php
/*Created by ������ (21.01.2016 10:53)*/
class ZubiPanelsFormsAction extends AbstractAction{

    protected $_structure = null;

       function run(){
            require_once(CLASSES_PATH . 'Form/Form.php');
            $handler = new Form($this->_getDataFromForm());
            return $handler->run();
       }

   	/**���������� ���������� ����� ����������� ��������� ���� ���������!*/
   	protected function _getFields(){
   		return $this->_getStructure()->getFields();
   	}

   	protected function _getValues(){
   		return $this->_getStructure()->getValues($this->g($this->g('alias', 'form'), Data::get()->g($this->g('alias', 'form'), [])));
   	}

   	protected function _getStructure(){
   		if ($this->_structure === null){
   			require_once(CLASSES_PATH . 'Models/Structure/From_files/Form.php');
   			$this->_structure = new ModelsStructureFrom_filesForm($this->g('moduleName'));
   		}
   		return $this->_structure;
   	}

    protected function _getDataFromForm() {
        return [
            'fields' => $this->_getFields(),
            'values' => $this->_getValues(),
            'alias' => $this->g('alias', 'form'),
        ];
    }
}