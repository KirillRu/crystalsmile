<?php /**@var $this ViewObjects*/?>
<div id="js_order_block" class="form-block">
    <div class="form-header">Форма заказа</div>
<?php Head::get()->echoScriptOptions('formData')?>
<?php $this->errors->display(); ?>
<form onsubmit="ajax.replace('/runModule/forms/addConfirm.ajax', element.getParameters(this) + '&moduleName=orders', 'js_order_block'); return false;">

	<?php foreach ($this->_widgets as $name => $widget): ?>
	    <?php if (!in_array($name, ['form', 'errors'])) $widget->display(); ?>
	<?php endforeach; ?>

	<?php $this->form->display(); ?>

	<input type="submit" class="button" value="Заказать">
</form>
</div>