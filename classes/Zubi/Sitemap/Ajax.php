<?php
/*Created by Кирилл (07.02.2016 21:43)*/
require_once(CLASSES_PATH . 'Sitemap/Ajax.php');
class ZubiSitemapAjax extends AbstractAction{
	function getAction() { return $this->_getHandler($this->g('path')); }
	function run() { return new View(); }

	/**
	 * @param $path
	 * @return View
	 */
	function _getHandler($path) {
		$sitesPath = Path::get();
		$data = array_merge($_POST, $_GET); unset($data['q']);

		switch (true) {
			case preg_match($sitesPath::get()->preparePattern('runModule/@s.ajax$'), $path, $m):
				$moduleName = Text::get()->strToUpperFirst($m[1]);
				$classFile = PROJECT_PATH . 'Modules/' . $moduleName . '/Action/' . $moduleName . '.php';
				$className = PROJECT_NAME . 'Modules' . $moduleName . 'Action';
				break;
			case preg_match($sitesPath::get()->preparePattern('runModule/@s/@s.ajax$'), $path, $m):
				$moduleName = Text::get()->strToUpperFirst($m[1]);
				$actionName = Text::get()->strToUpperFirst($m[2]);
				$classFile = PROJECT_PATH . 'Modules/' . $moduleName . '/Action/' . $actionName . '.php';
				$className = PROJECT_NAME . 'Modules' . $moduleName . 'Action' . $actionName;
				break;
		}
		if (!empty($classFile) && !empty($className) && file_exists($classFile)){
			if ($data) Data::get()->s($data);
			require_once($classFile);
			return new $className;
		}
		return null;
	}

}
