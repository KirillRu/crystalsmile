<?php
require_once(CLASSES_PATH . 'Sitemap/Sitemap.php');
class ZubiSitemap extends Sitemap {

	function getAction() {
		$path = $this->g('path');
        if (substr($path, -5) == '.ajax') $handler = $this->_ajaxAction($path);
        else $handler = $this->_getHandler($path);
        if ($handler) return $handler->getAction($path);
		return $this;
	}

	protected function _getHandler($path) {
		$data = array_merge($_POST, $_GET);
		$sitesPath = Path::get();
		switch(true) {
			case preg_match($sitesPath->preparePattern('@s/$'), $path, $m):
				$data['moduleName'] = $m[1];
				$classFile = PROJECT_PATH . 'Action/DefaultPage.php';
				$className = PROJECT_NAME . 'ActionDefaultPage';
				break;
			default:
				$classFile = PROJECT_PATH . 'Action/GeneralFrame.php';
				$className = PROJECT_NAME . 'ActionGeneralFrame';
				break;
		}

		Data::get()->s($data);
		if (!empty($classFile) && !empty($className) && file_exists($classFile)){
			require_once($classFile);
			return new $className;
		} else {
			return parent::_getHandler($path);
		}
	}

    protected function _ajaxAction($path) {
   		require_once(PROJECT_PATH . '/Sitemap/Ajax.php');
   		$className = PROJECT_NAME . 'SitemapAjax';
   		return (new $className(array('path' => $path)))->getAction();
   	}


}